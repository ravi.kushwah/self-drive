/*--------------------- Copyright (c) 2020 -----------------------

[Master Javascript]

-------------------------------------------------------------------*/

(function($) {

    "use strict";



    /*-----------------------------------------------------

    	Function  Start

    -----------------------------------------------------*/

    var admin = {

        initialised: false,

        version: 1.0,

        mobile: false,

        collapseScreenSize: 991,

        sideBarSize: 1199,

        init: function() {

            if (!this.initialised) {

                this.initialised = true;

            } else {

                return;

            }

            /*-----------------------------------------------------

            	Function Calling

            -----------------------------------------------------*/

            this.userToggle();

            this.sideBarToggle();

            this.sideMenu();

            this.sideBarHover();

            this.searchToggle();

            this.rightSlide();

            this.tooltipHover();

            this.nubberSpin();

            this.loader();

            this.checkSiteName();

            this.copyToClipBoard();

            this.deleteWebsite();

            this.leadSettings();

            this.categorySettings();

            this.customerSettings();

            this.agencySettings();

            this.customProductsSettings();

            this.shareButton();

        },



        /*-----------------------------------------------------

            Fix Header User Button

        -----------------------------------------------------*/

        // loader			

        loader: function() {

            jQuery(window).on('load', function() {

                $(".loader").fadeOut();

                $(".spinner").delay(500).fadeOut("slow");

            });

        },

        // loader	

        userToggle: function() {

            var count = 0;

            $('.user-info').on("click", function() {

                if ($(window).width() <= admin.collapseScreenSize) {

                    if (count == '0') {

                        $('.user-info-box').addClass('show');

                        count++;

                    } else {

                        $('.user-info-box').removeClass('show');

                        count--;

                    }

                }

            });



            $(".user-info-box, .user-info").on('click', function(e) {

                if ($(window).width() <= admin.collapseScreenSize) {

                    event.stopPropagation();

                }

            });



            $('body').on("click", function() {

                if ($(window).width() <= admin.collapseScreenSize) {

                    if (count == '1') {

                        $('.user-info-box').removeClass('show');

                        count--;

                    }

                }

            });

        },

        sideBarToggle: function() {

            $(".toggle-btn").on('click', function(e) {

                e.stopPropagation();

                $("body").toggleClass('mini-sidebar');

                $(this).toggleClass('checked');



            });

            $('.sidebar-wrapper').on('click', function(event) {

                event.stopPropagation();

            });

        },

        sideMenu: function() {

            $('.side-menu-wrap ul li').has('.sub-menu').addClass('has-sub-menu');

            $.sidebarMenu = function(menu) {

                var animationSpeed = 300,

                    subMenuSelector = '.sub-menu';

                $(menu).on('click', 'li a', function(e) {

                    var $this = $(this);

                    var checkElement = $this.next();

                    if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {

                        checkElement.slideUp(animationSpeed, function() {

                            checkElement.removeClass('menu-show');

                        });

                        checkElement.parent("li").removeClass("active");

                    } else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {

                        var parent = $this.parents('ul').first();

                        var ul = parent.find('ul:visible').slideUp(animationSpeed);

                        ul.removeClass('menu-show');

                        var parent_li = $this.parent("li");

                        checkElement.slideDown(animationSpeed, function() {

                            checkElement.addClass('menu-show');

                            parent.find('li.active').removeClass('active');

                            parent_li.addClass('active');

                        });

                    }

                    if (checkElement.is(subMenuSelector)) {

                        e.preventDefault();

                    }

                });

            }

            $.sidebarMenu($('.main-menu'));

            $(function() {

                for (var a = window.location, counting = $(".main-menu a").filter(function() {

                        return this.href == a;

                    }).addClass("active").parent().addClass("active");;) {

                    if (!counting.is("li")) break;

                    counting = counting.parent().addClass("in").parent().addClass("active");

                }

            });

        },

        sideBarHover: function() {

            if ($(window).width() >= admin.sideBarSize) {

                $(".main-menu").hover(function() {

                    $('body').addClass('sidebar-hover');

                }, function() {

                    $('body').removeClass('sidebar-hover');

                });

            }

        },

        searchToggle: function() {

            $('.search-toggle').on("click", function() {

                $('.serch-wrapper').addClass('show-search');

            });

            $('.search-close, .main-content').on("click", function() {

                $('.serch-wrapper').removeClass('show-search');

            });

        },

        rightSlide: function() {

            $(".setting-info").on('click', function(e) {

                e.stopPropagation();

                $("body").toggleClass('open-setting');

            });

            $('body, .close-btn').on('click', function() {

                $('body').removeClass('open-setting');

            });

            $('.slide-setting-box').on('click', function(event) {

                event.stopPropagation();

            });



        },

        tooltipHover: function() {

            if ($('.toltiped').length > 0) {

                $(".toltiped").tooltip();

            }

            if ($('.toltiped-right').length > 0) {

                $(".toltiped-right").tooltip({

                    'placement': 'right',

                });

            }

        },

        nubberSpin: function() {

            if ($('.number-spin').length > 0) {

                $('.number-increase').on('click', function() {

                    if ($(this).prev().val() < 50000) {

                        $(this).prev().val(+$(this).prev().val() + 1);

                    }

                });

                $('.number-decrease').on('click', function() {

                    if ($(this).next().val() > 1) {

                        if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);

                    }

                });

            }

        },

        checkSiteName: function() {

            $(document).on('click', '.check_site_url', function() {

                let siteURL = $.trim($('input[name="w_siteurl"]').val());

                let textObj = $('.plr_url_text');

                if (siteURL.length > 3) {

                    var regexp = new RegExp(/^[a-z0-9 ]*$/);

                    if (regexp.test(siteURL)) {

                        textObj.html('<span class="mb-0 badge">Checking</span>');

                        $.ajax({

                            url: baseurl + 'home/checksiteurl',

                            type: "POST",

                            data: {

                                'w_siteurl': siteURL,

                                'w_id': $('input[name="w_id"]').val()

                            },

                            success: function(e) {

                                if (e == 0) {

                                    textObj.html('<span class="mb-0 badge badge-danger">Not Available</span>');

                                    showNotifications('error', 'Please, enter different site url. Someone is already using this url.');

                                } else

                                    textObj.html('<span class="mb-0 badge badge-success">Available </span>');

                            }

                        });

                    } else

                        showNotifications('error', 'Site URL can have letters (a-z) and numbers (0-9) only.');

                } else

                    showNotifications('error', 'Site URL should be atleast 3 characters.');

            });

        },

        copyToClipBoard: function() {

            $(document).on('click', '.copy_button', function() {

                navigator.clipboard.writeText($(this).data('tobecopied'));

                showNotifications('success', 'Content copied successfully to clipboard.');

            });

        },

        deleteWebsite: function() {

            $(document).on('click', '.deleteWebsite', function() {

                $('#deleteWebsiteBtn').data('webid', $(this).data('webid'))

                $('#deleteWebsite').modal('show');

            });



            $(document).on('click', '#deleteWebsiteBtn', function() {

                let webid = $(this).data('webid');

                $.ajax({

                    url: baseurl + 'home/deleteWebsite',

                    type: "POST",

                    data: {

                        'webid': webid

                    },

                    success: function(e) {

                        if (e == 1)

                            showNotifications('success', 'Website deleted successfully.');

                        else

                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');



                        setTimeout(function() {

                            location.reload();

                        }, 3000);

                    }

                });

            });

        },

        shareButton: function() {

            $(document).on('click', '.shareButton', function() {

                $('.socialul').attr('id', $(this).data('webid'))

                $('#socialicon').modal('show');

            });

        },

        leadSettings: function() {

            $(document).on('change', '.arCls', function() {

                getARList($(this));

            });

            var parts = $(location).attr('href').split("/"), // use like uri segment

                last_part = parts[parts.length - 2];

            var queryparams = last_part.split('?')[0];

            // console.log(queryparams)

            if (queryparams == 'lead_settings') {



                getARList($('#w_signuplist'));

                getARList($('#w_buyplanlist'));

                getARList($('#w_newsletterlist'));

            }



        },

        categorySettings: function() {

            $(document).on('click', '.deleteCategory', function() {

                $('#deleteCategoryBtn').data('cateid', $(this).data('cateid'));

                $('#deleteCategory').modal('show');

            });



            $(document).on('click', '#deleteCategoryBtn', function() {

                let cateid = $(this).data('cateid');

                let webid = $('#webid').val();

                $.ajax({

                    url: baseurl + 'home/web_categories/' + webid,

                    type: "POST",

                    data: {

                        'delete_cateid': cateid

                    },

                    success: function(e) {

                        if (e == 1)

                            showNotifications('success', 'Category deleted successfully.');

                        else

                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');



                        setTimeout(function() {

                            location.reload();

                        }, 3000);

                    }

                });

            });



            $(document).on('change', '.cateCls', function() {

                let sts = $(this).is(":checked") ? 1 : 0;

                let webid = $('#webid').val();

                $.ajax({

                    url: baseurl + 'home/web_categories/' + webid,

                    type: "POST",

                    data: {

                        'webid': webid,

                        'sts': sts,

                        'cateid': $(this).attr("id")

                    },

                    success: function(e) {

                        if (e == 1)

                            showNotifications('success', 'Category status updated successfully.');

                        else

                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');

                    }

                });

            });

            $(document).on('change', '.customcateCls', function() {

                let sts = $(this).is(":checked") ? 1 : 0;

                let webid = $('#webid').val();

                $.ajax({

                    url: baseurl + 'home/web_categories/' + webid,

                    type: "POST",

                    data: {

                        'webid': webid,

                        'sts': sts,

                        'customcateid': $(this).attr("id")

                    },

                    success: function(e) {

                        if (e == 1)

                            showNotifications('success', 'Category status updated successfully.');

                        else

                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');

                    }

                });

            });







            $(document).on('click', '.openModal', function() {

                $('#add_cloud').modal('show');

            });



            $(document).on('click', '.dropBox', function() {

                $('#dropBox').modal('show');

            });

            $(document).on('click', '.GoogleDrive', function() {

                $('#GoogleDrive').modal('show');

            });

            $(document).on('click', '.CreateFolder', function() {

                $('#CreateFolder').modal('show');

            });

            $(document).on('click', '.moveFile', function() {

                $('#moveFile').modal('show');

            });

            $(document).on('click', '.Renamefiles', function() {

                $('#Renamefiles').modal('show');

            });

            $(document).on('click', '.ShareURL', function() {

                $('#ShareURL').modal('show');

            });

            $(document).on('click', '.OneDrive', function() {

                $('#OneDrive').modal('show');

            });



            $(document).on('click', '.UploadFile', function() {

                var folderId = $('.UploadFile').attr('folder_id');

                $('.Upload').attr('folder_id', folderId);

                $('#UploadFile').modal('show');

            });

            $(document).on('click', '.amazonS3UploadFile', function() {

                var folderId = $('.amazons3fileupload').attr('folder_id');

                $('.s3UploadFile').attr('folder_id', folderId);

                $('#amazons3fileupload').modal('show');

            });

            $(document).on('click', '.anonfilesCon', function() {

                $('#anonfiles').modal('show');

            });

            $(document).on('click', '.googlePhoto', function() {

                $('#googlePhoto').modal('show');

            });

            

            $(document).on('click', '.previewFile', function() {

                $('#fileView').modal('show');

            });

            $(document).on('click', '.Box', function() {

                $('#Box').modal('show');

            });

            $(document).on('click', '.Ddownload', function() {

                $('#Ddownload').modal('show');

            });

            $(document).on('click', '.CreateFolderonedrive', function() {

                $('#OneDriveCreateFolder').modal('show');

            });

            $(document).on('click', '.amazonS3', function() {

                $('#amazonS3').modal('show');

            });

            $(document).on('click', '.agencyuserClsEdit', function() {

                $('#agencyuserClsEdit').modal('show');

            });



            $(document).on('click', '#submitCateData', function() {

                let cate_id = $.trim($('#cate_id').val());

                let cate_name = $.trim($('#cate_name').val());

                let cate_slug = $.trim($('#cate_slug').val());

                let webid = $('#webid').val();

                if (cate_name != '' && cate_slug != '') {

                    $.ajax({

                        url: baseurl + 'home/web_categories/' + webid,

                        type: "POST",

                        data: {

                            'cate_id': cate_id,

                            'cate_name': cate_name,

                            'cate_slug': cate_slug

                        },

                        success: function(e) {

                            if (e == 1)

                                showNotifications('success', 'Category updated successfully.');

                            else if (e == 2)

                                showNotifications('success', 'Category added successfully.');

                            else

                                showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');



                            setTimeout(function() {

                                location.reload();

                            }, 3000);

                        }

                    });

                } else

                    showNotifications('error', 'Please, fill in the details to continue.');

            });

        },

        customerSettings: function() {

            $(document).on('change', '.custCls', function() {

                let sts = $(this).is(":checked") ? 1 : 0;

                let webid = $('#webid').val();

                $.ajax({

                    url: baseurl + 'home/web_customers/' + webid,

                    type: "POST",

                    data: {

                        'sts': sts,

                        'custid': $(this).attr("id")

                    },

                    success: function(e) {

                        if (e == 1)

                            showNotifications('success', 'Customer status updated successfully.');

                        else

                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');

                    }

                });

            });





            $(document).on('click', '#submitCustomerData', function() {

                let custId = $.trim($('#c_id').val());

                let c_name = $.trim($('#c_name').val());

                let c_email = $.trim($('#c_email').val());

                let c_password = $.trim($('#c_password').val());

                let err = 0;

                if (c_name != '' && c_email != '') {

                    if (custId == 0) {

                        if (c_password == '') {

                            showNotifications('error', 'Please, enter passsword.');

                            err++;

                        }

                    }

                    if (err == 0) {

                        $.ajax({

                            url: baseurl + 'home/web_customers/' + $('#webid').val(),

                            type: "POST",

                            data: {

                                'custid': custId,

                                'c_name': c_name,

                                'c_email': c_email,

                                'c_password': c_password

                            },

                            success: function(e) {

                                if (e == 1)

                                    showNotifications('success', 'Customer data updated successfully.');

                                else if (e == 2)

                                    showNotifications('success', 'Customer added successfully.');

                                else if (e == 4)

                                    showNotifications('error', 'One of your customer already have this email.');

                                else

                                    showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');



                                setTimeout(function() {

                                    location.reload();

                                }, 3000);

                            }

                        });

                    }

                } else {

                    showNotifications('error', 'Please, fill in the details to continue.');

                }

            });

        },

          agencySettings:function(){

            $(document).on('change','.agencyuserCls',function(){

                let sts = $(this).is(":checked") ? 1 : 0 ;

                $.ajax({

                    url: baseurl + 'home/agency_users/', 

                    type: "POST",             

                    data: {'sts':sts,'userid':$(this).attr("id")},

                    success: function(e) {

                        if(e == 1)

                            toastr.success('Agency user status updated successfully.');

                        else

                            toastr.error('Something went wrong. Please, refresh the page and try again.');

                    }

                });

            });



            $(document).on('click','.agencyuserClsEdit',function(){

                let userId = $(this).attr("id");

                if( userId == 0 ){

                    $('.agencyuserAddCls').val('');

                    $('#id').val(0);

                    $('#add_agencyuser_title').text('Add New Agency User');

                    $('#add_agencyuser').modal('show');

                }

                else{

                    $.ajax({

                        url: baseurl + 'home/agency_users/', 

                        type: "POST",             

                        data: {'userid':userId},

                        success: function(e) {

                            let resp = JSON.parse(e);

                            $('#userName').val(resp.name)         

                            $('#UserEmail').val(resp.email)   

                            $('#id').val(userId);

                            $('#add_agencyuser_title').text('Edit Agency User');

                            $('#add_agencyuser').modal('show');      

                        }

                    });

                }

            });



            $(document).on('click','#submitAgencyUserData',function(){

                let userId = $.trim($('#id').val());

                let name = $.trim($('#userName').val());

                let email = $.trim($('#UserEmail').val());

                let password = $.trim($('#password').val());

                let is_fe = $('#is_fe').prop('checked');

                let is_oto1 = $('#is_oto1').prop('checked');

                let is_oto2 = $('#is_oto2').prop('checked');

                let err = 0;

              

                if( name != '' && email != '' ){

                    if( userId == 0 ){

                        if( password == '' ){

                            toastr.error('Please, enter passsword.');

                            err++;

                        }

                    }

                    if( err == 0 )

                    {

                        $.ajax({

                            url: baseurl + 'home/agency_users/', 

                            type: "POST",             

                            data: {'userid':userId,'name':name,'email':email,'password':password,'is_fe':is_fe,'is_oto1':is_oto1,'is_oto2':is_oto2},

                            success: function(e) {

                                if(e == 1){

                                    toastr.success('Agency User data updated successfully.');

                                    setTimeout(function(){ location.reload(); }, 3000);

                                }else if(e == 2){

                                    toastr.success('Agency User added successfully.');

                                    setTimeout(function(){ location.reload(); }, 3000);

                                }else if(e == 4 ){

                                    toastr.error('One of the User already have this email.');

                                    setTimeout(function(){ location.reload(); }, 3000);

                                }else if(e == 5 ){

                                    toastr.error('You have reached the limit of adding users.');

                                    setTimeout(function(){ location.reload(); }, 3000);

                                }else{

                                    toastr.error('Something went wrong. Please, refresh the page and try again.');

                                } 

                            }

                        });

                    }

                }

                else

                    toastr.error('Please, fill in the details to continue.');

            });

        },

        customProductsSettings: function() {

            $(document).on('click', '.custProdEdit', function() {

                let p_id = $(this).attr("id");

                let webid = $('#webid').val();

                if (p_id == 0) {

                    $('.productsAddCls').val('');

                    $('#p_id').val(0);

                    $('#add_products_title').text('Add Custom GiveAways');

                    $('#add_custom_products').modal('show');

                } else {

                    $.ajax({

                        url: baseurl + 'home/custom_products/' + webid,

                        type: "POST",

                        data: {

                            'p_id': p_id

                        },

                        success: function(e) {

                            let resp = JSON.parse(e);

                            $('#g_offer_name').val(resp.g_offer_name)

                            $('#slug').val(resp.slug)

                            $('#g_payout').val(resp.g_payout)

                            $('#g_priview').val(resp.g_priview)

                            $('#g_categories').val(resp.g_categories)

                            $('#g_countries').val(resp.g_countries)

                            $('#g_offer_url').val(resp.g_offer_url);

                            $('#g_sign_up_url').val(resp.g_sign_up_url);

                            $('#p_id').attr("p_id", resp.g_id);

                            $('#webid').val(resp.g_webid);

                            $('#add_products_title').text('Edit Custom Give Aways');

                            $('#add_custom_products').modal('show');

                        }

                    });

                }

            });





            $(document).on('click', '#deleteCustomProductBtn', function() {

                let custprodid = $(this).data('custprodid');

                let webid = $('#webid').val();

                $.ajax({

                    url: baseurl + 'home/custom_products/' + webid,

                    type: "POST",

                    data: {

                        'delete_prodid': custprodid

                    },

                    success: function(e) {

                        if (e == 1)

                            showNotifications('success', 'Product deleted successfully.');

                        else

                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');



                        setTimeout(function() {

                            location.reload();

                        }, 3000);

                    }

                });

            });

        }

    };



    admin.init();



})(jQuery);





toastr.options = {

    "closeButton": false,

    "debug": false,

    "newestOnTop": false,

    "progressBar": false,

    "positionClass": "toast-top-right",

    "preventDuplicates": true,

    "onclick": null,

    "showDuration": "300",

    "hideDuration": "1000",

    "timeOut": "5000",

    "extendedTimeOut": "1000",

    "showEasing": "swing",

    "hideEasing": "linear",

    "showMethod": "fadeIn",

    "hideMethod": "fadeOut"

}



function setCookie(name, value, days) {

    var expires = "";

    if (days) {

        var date = new Date();

        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

        expires = "; expires=" + date.toUTCString();

    }

    document.cookie = name + "=" + (value || "") + expires + "; path=/";

}



function getCookie(name) {

    var nameEQ = name + "=";

    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {

        var c = ca[i];

        while (c.charAt(0) == ' ') c = c.substring(1, c.length);

        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);

    }

    return null;

}

$(document).ready(function() {

    $(document).on('click', 'body', function(e) {

        $('.tbf_IconHolter').hide();

        $('#removeConnection').hide();

    });
    var  dataTableObj = $('.server_datatable').DataTable({
        searching: true,
        processing: true,
        dom: 'lf<"table-responsive" t >ip',
        language: {
            paginate: {
                previous: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>',
            },
            emptyTable: table_msg,
            search: ltr_search + ':',

        },
        pageLength: 10,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        responsive: true,
        serverSide: { "regex": true },
        columnDefs: [{
            targets: "_all",
            orderable: false
        }],       
       
        ajax: {
            "url": base_url + $('.server_datatable').attr('data-url'),
            "type": "POST"
        }
    });   
  

});



$(document).on('mousedown', '.sc_share_wrapper,.getSubFolderByFolderId', function(e) {

    var optionCheck = $(this).attr('option');

    if (e.button == 2) {

        document.oncontextmenu = function() {

            return false;

        };

         $('.' + optionCheck).show();

        return false;

    } else {

        document.oncontextmenu = function() {

            return true;

        };

    }

    return true;

});

$(document).on('mousedown', '#amazonS3Option', function(e) {

    var optionCheck = $(this).attr('option');

    if (e.button == 2) {

        document.oncontextmenu = function() {

            return false;

        };

         $('.' + optionCheck).show();

        return false;

    } else {

        document.oncontextmenu = function() {

            return true;

        };

    }

    return true;

});



$(document).on('mousedown', '.removeConnection', function(e) {

    var connectionID = $(this).attr('connectionID');

    if (e.button == 2) {

        document.oncontextmenu = function() {

            return false;

        };

            $('#confirmPopupBtn').attr('connectionID',connectionID);

            $('#confirmPopup').modal('show');

         

        // $('#removeConnection').show();

        return false;

    } else {

        document.oncontextmenu = function() {

            return true;

        };

    }

    return true;

});

$(document).on('click', '#confirmPopupBtn', function() {

    var connectionID = $(this).attr('connectionID');

        url: baseurl + 'home/RemoveConnection',

     $.ajax({

        url: baseurl + 'home/RemoveConnection',

        type: "POST",

        data: {

            'id': connectionID,

        },

        success: function(resp) {

            var resp = $.parseJSON(resp);

            if (resp['status'] == '1') {

                toastr.success('successfully Remove Account');

                setTimeout(function() {

                    location.reload();

                }, 2000);

            }else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }

    });

});



$(document).on('click', '.saveProfile', function() {

    var formdata = new FormData($(this).closest('form')[0]);

    $.ajax({

        method: "POST",

        url: baseurl + 'home/adminProfile',

        data: formdata,

        processData: false,

        contentType: false,

        success: function(resp) {

            var resp = $.parseJSON(resp);

            if (resp['status'] == '1') {

                toastr.success('Profile updated successfully.');

                setTimeout(function() {

                    location.reload();

                }, 3000);

            } else if (resp['status'] == '2') {

                toastr.success('Profile updated successfully.');

                return false;

            } else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

            $('.edu_preloader').fadeOut();

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }

    });



});



function saveProfile() {

    let u_name = $.trim($('#name').val());

    if (u_name != '') {

        $.ajax({

            url: baseurl + 'home/profile',

            type: "POST",

            data: {

                'name': u_name,

                'pwd': $('#pwd').val(),

                'email': $('#email').val()

            },

            success: function(e) {

                if (e == 1)

                    toastr.success('Profile updated successfully.');

                // showNotifications('success','Profile updated successfully.');

                else

                    showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');



                setTimeout(function() {

                    location.reload();

                }, 3000);

            }

        });

    } else

        showNotifications('error', 'Please enter the name.')

}





// upload file js   

$(document).on('click', '.plr_uploade_label', function(e) {

    $(this).parent().find('.plr_fileupload').click();

    var image_uploads = $(this).parent().find('.priViewImg');



    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();



            reader.onload = function(e) {

                image_uploads.attr('src', e.target.result);

            }

            reader.readAsDataURL(input.files[0]);

        }

    }



    $(".plr_fileupload").change(function() {

        readURL(this);

    });

});



$(document).on('click', '.folderBack', function(e) {

    window.location.reload();

    return false;

    var id = $(this).attr('id');

    $.ajax({

        url: baseurl + 'home/getSubFolderByFolderId',

        type: "POST",

        data: {

            'id': id

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            if (resp != "") {

                $('.sc_share_main_section').empty();

                $('.folderBack').hide();

            }

            if (item['path_lower'] == "") {

                var lPath = "";

            } else {

                var lPath = item['path_lower'];

            }

            var i = 1;

            jQuery.each(resp['data']['data'], function(index, item) {

                var c = i++;

                var folder = `<div class="sc_share_wrapper" option="option${c}">

				<div class="checkbox">

				<input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

				<label for="rememberme${c}" class="option"></label>

				</div>

				<div class="sc_share_img getSubFolderByFolderId" droType="${item['path_lower']}" id="${resp['userId']}" data-folder-name="${item['name']}" data-folder-Id="${item['id']}">

				<img src="${baseurl}assets/backend/images/folder.png" alt="icon">

				</div>

				<div class="sc_share_detail getSubFolderByFolderId" id="' + resp['userId'] + '" droType="' + lPath + '" data-folder-name="${item['name']}" data-folder-Id="${item['id']}">

				<h5>${item['name']}</h5>

				</div>

				</div>`;

                $('.sc_share_main_section').append(folder);

            });





        }

    });



});



$(document).on('click', '.UserCreateFolder', function(e) {

    var folder_name = $('#folder_name').val();

    var user_id = $('#user_id').val();

    $.ajax({

        url: baseurl + 'home/userCreateFolder',

        type: "POST",

        data: {

            'user_id': user_id,

            'folderName': folder_name

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            //   console.log(e)

            //   return false;

            if (resp['data']['status'] == '1') {

                toastr.success('Folder Create successfully.');

                setTimeout(function() {

                    location.reload();

                }, 1000);

            } else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });



});



$(document).on('click', '.userSubCreateFolder,#userSubCreateFolder', function(e) {

    e.preventDefault();

    var folder_name = $('#folder_name').val();

    var parentId = $(this).attr('folder_id');

    var drotype = $(this).attr('drotype');

    var user_id = $('#user_id').val();

    $.ajax({

        url: baseurl + 'home/userSubCreateFolder',

        type: "POST",

        data: {

            'user_id': user_id,

            'folderName': folder_name,

            'parentId': parentId,

            'drotype': drotype

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            if (resp['path_display'] == "") {

                folderAccess = parentId;

            } else {

                folderAccess = drotype;

            }

            $.ajax({

                url: baseurl + 'home/getSubFolderByFolderId',

                type: "POST",

                data: {

                    'folderId': folderAccess,

                    'id': user_id

                },

                success: function(e) {

                    var resp = $.parseJSON(e);

                    if (resp != "") {

                        $('.sc_share_main_section').empty();

                        $('.folderBack').show();

                        $('.UserCreateFolder').removeClass('UserCreateFolder');

                        $('#folder_name').val('New Folder');

                        $('#CreateFolder').modal('hide');

                    }

                    var i = 1;

                    if(resp.connection == "Googlephoto"){

                        jQuery.each(resp['data'], function(index, item) {

                        var c = i++;

                        var fileExtension = item['title'].substring(item['title'].lastIndexOf('.') + 1);

                        var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json'];

        

                        if (Extension.lastIndexOf(fileExtension) == -1) {

                            var icon = baseurl + 'assets/backend/images/folder.png';

                            var getFdrClass = 'getSubFolderByFolderId';

                            var downloadFIle = "javascript:;";

                            var hide = "hidden";

                        } else {

                            var icon = baseurl + 'assets/backend/images/file.png';

                            var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';

                            var getFdrClass = '';

                            var hide = "";

                        }

                        folderId = item['id'];

                        var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['title']}"  data-folder-Id="${folderId}">

                            				<div class="checkbox" hidden>

                            				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                            				    <label for="rememberme${c}" class="option"></label>

                            				</div>

                            				<div class="sc_share_img sc_share_file">

                            				    <img src="${icon}" alt="icon">

                            				</div>

                            				<div class="sc_share_detail">

                            				    <h5>${item['title']}</h5>

                            				</div>

                            				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">

                                				<div class="checkBoxSelect">

                                                    <a href="javascript:;" ${hide} class="ShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${user_id}" >

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                            <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                        </svg>

                                                        Share

                                                    </a>

                                                    

                                                    <a  href="javascript:;" ${hide} class="moveFile " filename="${item['title']}" userId="${user_id}" fileId="${folderId}" >

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                            <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                        </svg>

                                                        Move

                                                    </a>

                                                    

                                                    <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp['userId']}" folderID="${folderId}" >

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                        </svg>

                                                        Delete

                                                    </a>

                                                    

                                                    <a  href="javascript:;" ${hide} class="previewFile"  folderIDShareURL="${folderId}" userID="${user_id}">

                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                            <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                        </svg>

                                                        Preview

                                                    </a>

                                                  

                                				</div>

                            				</div>

        				                </div>`;

                        $('.sc_share_main_section').append(folder);

                    });

                    }else{

                        var i = 1;

                        jQuery.each(resp['data']['data'], function(index, item) {

                            var c = i++;

                            var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);

                            // 	 fileExtension = item['name'].split('.').pop();

                            var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sq', 'json'];

                            if (Extension.lastIndexOf(fileExtension) == -1) {

                                var icon = baseurl + 'assets/backend/images/folder.png';

                            } else {

                                var icon = baseurl + 'assets/backend/images/file.png';

                            }

    

                            // var folder = '<div class="sc_share_wrapper getSubFolderByFolderId " option="option' + c + '" id="' + resp['userId'] + '" data-folder-name="' + item['name'] + '" data-folder-Id="' + item['id'] + '"><div class="checkbox" hidden><input type="checkbox" id="rememberme' + c + '" name=""class="optionCheckBox" value="" >        <label for="rememberme' + c + '" class="option"></label>    </div>    <div class="sc_share_img"><img src="' + icon + '" alt="icon"></div><div class="sc_share_detail"><h5>' + item['name'] + '</h5></div><div class="checkOptionMenu tbf_IconHolter option' + c + '" clickValue="option' + c + '" style="display:none;"><div class="checkBoxSelect"><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px"><path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/></svg>Copy To</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px"><path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/></svg>Download</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Share</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Cut</a><a  href="javascript:;" class="DeletFileAndFolder" user_id="' + resp['userId'] + '" folderID="' + item['id'] + '" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Delete</a><a  href="javascript:;" class="Renamefiles"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px"><path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/></svg>Preview</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17"><path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path></svg>Rename</a><a hidden  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/></svg>Copy</a></div></div></div>';

                            var folder = `<div class="sc_share_wrapper getSubFolderByFolderId " option="option${c}" id="${ resp['userId']}" data-folder-name="${ item['name']}" data-folder-Id="${ item['id']}">

                                            <div class="checkbox" hidden>

                                                <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                                                    <label for="rememberme${c}" class="option"></label> 

                                            </div>

                                            <div class="sc_share_img">

                                                <img src="' + icon + '" alt="icon">

                                            </div>

                                            <div class="sc_share_detail">

                                                <h5>${ item['name']}</h5>

                                            </div>

                                            <div class="checkOptionMenu tbf_IconHolter option${c}" clickValue="option${c}" style="display:none;">

                                                <div class="checkBoxSelect">

                                                    <a  href="javascript:;">

                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">

                                                            <path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>

                                                        </svg>

                                                        Copy To

                                                    </a>

                                                    <a  href="javascript:;">

                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">

                                                            <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>

                                                        </svg>Download

                                                    </a>

                                                    <a  href="javascript:;">

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                            <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                        </svg>Share

                                                    </a>

                                                    <a  href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>Cut

                                                    </a>

                                                    <a  href="javascript:;" class="DeletFileAndFolder" user_id="${ resp['userId']}" folderID="${ item['id']}" >

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                        </svg>Delete

                                                    </a>

                                                    <a  href="javascript:;" class="Renamefiles">

                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                            <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                        </svg>Preview

                                                    </a>

                                                    <a  href="javascript:;" >

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">

                                                            <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63">

                                                            </path>

                                                        </svg>Rename

                                                    </a>

                                                    <a hidden  href="javascript:;" >

                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                            <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>

                                                        </svg>Copy

                                                    </a>

                                                </div>

                                            </div>

                                        </div>`;

                            $('.sc_share_main_section').append(folder);

                        });



                    }

                }

            });

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });



});



$('.folderBack').hide();

$('.UploadFile').hide();

$('.CreateAlbum').hide();

$(document).on('click', '.getSubFolderByFolderId', function(e) {



    var name = $(this).attr('data-folder-name');

    var folderId = $(this).attr('data-folder-Id');

    var droType = $(this).attr('drotype');



    $('.UploadFile').attr('folder_id', folderId);

    $('.amazonS3UploadFile').attr('folder_id', folderId);

    $('.UserCreateFolder').addClass('userSubCreateFolder ');

    $('.UserCreateFolder').attr('id', 'userSubCreateFolder');

    $('.UserCreateFolder').attr('folder_id', folderId);

    $('.userSubCreateFolder').attr('folder_id', folderId);

    $('.UserCreateFolder').attr('droType', droType);

    $('.CloudUploadFile').attr('folderId', folderId);

    $('.CloudUploadFile').attr('droType', droType);

    $('.RenameFile').attr('droType', droType);



    var type = $(this).attr('type');

    var id = $(this).attr('id');



    $.ajax({

        url: baseurl + 'home/getSubFolderByFolderId',

        type: "POST",

        data: {

            'name': name,

            'folderId': folderId,

            'id': id,

            'type': type,

            'drType': droType

        },

        success: function(e) {

            var resp = $.parseJSON(e);

           

            if (resp != "") {

                $('.sc_share_main_section').empty();

                $('.folderBack').show();

                $('.amazonS3UploadFile').show();

                $('.UserCreateFolder').removeClass('UserCreateFolder');

            }

            var i = 1;

            if(resp.connection == "Googlephoto"){

               

                $('.UploadFile').hide();

                $('.CreateAlbum').show();

                $('#add_customer_title').html('Create Album');

                $('#folder_name').html('Create Album');

                $('.Folderame').html('Create Album');

                $('#folder_name').val('New Album');

                $('.userSubCreateFolder').val('Create Album');

                

              

                jQuery.each(resp['data'], function(index, item) {

                   

                var c = i++;

                if(resp['data']['type']=="mediaitem"){

                    var fileExtension = item['filename'].substring(item['filename'].lastIndexOf('.') + 1);

                    var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json'];

    

                    if (Extension.lastIndexOf(fileExtension) == -1) {

                        var icon = baseurl + 'assets/backend/images/folder.png';

                        var getFdrClass = 'getSubFolderByFolderId';

                        var downloadFIle = "javascript:;";

                        var hide = "hidden";

                    } else {

                        var icon = baseurl + 'assets/backend/images/file.png';

                        var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';

                        var getFdrClass = '';

                        var hide = "";

                    }

                    folderId = item['id'];

                    var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['filename']}"  data-folder-Id="${folderId}">

                        				<div class="checkbox" hidden>

                        				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                        				    <label for="rememberme${c}" class="option"></label>

                        				</div>

                        				<div class="sc_share_img sc_share_file">

                        				    <img src="${icon}" alt="icon">

                        				</div>

                        				<div class="sc_share_detail">

                        				    <h5>${item['filename']}</h5>

                        				</div>

                        				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">

                            				<div class="checkBoxSelect">

                                                <a href="javascript:;"  class="ShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>

                                                    Share

                                                </a>

                                                <a href="javascript:;"  class="UnShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>

                                                    UnShare

                                                </a>

                                                <a  href="javascript:;" class="moveFile " filename="${item['filename']}" userId="${id}" fileId="${folderId}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>

                                                    Move

                                                </a>

                                                

                                                <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp['userId']}" folderID="${folderId}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>

                                                    Delete

                                                </a>

                                                

                                                <a  href="javascript:;" class="previewFile"  folderIDShareURL="${folderId}" userID="${id}">

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                    </svg>

                                                    Preview

                                                </a>

                                              

                            				</div>

                        				</div>

    				                </div>`;

                    $('.sc_share_main_section').append(folder);

                }else{

               

                    var fileExtension = item['title'].substring(item['title'].lastIndexOf('.') + 1);

                    var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json'];

    

                    if (Extension.lastIndexOf(fileExtension) == -1) {

                        var icon = baseurl + 'assets/backend/images/folder.png';

                        var getFdrClass = 'getSubFolderByFolderId';

                        var downloadFIle = "javascript:;";

                        var hide = "hidden";

                    } else {

                        var icon = baseurl + 'assets/backend/images/file.png';

                        var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';

                        var getFdrClass = '';

                        var hide = "";

                    }

                    folderId = item['id'];

                    var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['title']}"  data-folder-Id="${folderId}">

                        				<div class="checkbox" hidden>

                        				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                        				    <label for="rememberme${c}" class="option"></label>

                        				</div>

                        				<div class="sc_share_img sc_share_file">

                        				    <img src="${icon}" alt="icon">

                        				</div>

                        				<div class="sc_share_detail">

                        				    <h5>${item['title']}</h5>

                        				</div>

                        				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">

                            				<div class="checkBoxSelect">

                                                <a href="javascript:;"  class="ShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>

                                                    Share

                                                </a>

                                                <a href="javascript:;"  class="UnShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>

                                                    UnShare

                                                </a>

                                                <a  href="javascript:;" class="moveFile " filename="${item['title']}" userId="${id}" fileId="${folderId}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>

                                                    Move

                                                </a>

                                                

                                                <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp['userId']}" folderID="${folderId}" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>

                                                    Delete

                                                </a>

                                                

                                                <a  href="javascript:;" class="previewFile"  folderIDShareURL="${folderId}" userID="${id}">

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                    </svg>

                                                    Preview

                                                </a>

                                              

                            				</div>

                        				</div>

    				                </div>`;

                    $('.sc_share_main_section').append(folder);

                }

            });

            }else{

                 $('.UploadFile').show();

                jQuery.each(resp['data']['data'], function(index, item) {

                var c = i++;

                var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);

                var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json'];



                if (Extension.lastIndexOf(fileExtension) == -1) {

                    var icon = baseurl + 'assets/backend/images/folder.png';

                    var getFdrClass = 'getSubFolderByFolderId';

                    var downloadFIle = "javascript:;";

                    var hide = "hidden";

                } else {

                    var icon = baseurl + 'assets/backend/images/file.png';

                    var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';

                    var getFdrClass = '';

                    var hide = "";

                }

                console.log(item)

                let connType = resp.connection;



                if (connType == 'GoogleDrive') {

                    var folderId = item['id'];

                }

                if (connType == 'DropBox') {

                    var boxPath = item['path_lower'];

                }

                if (connType == 'Ddownload') {

                    if (item['file_code'] == '' || item['file_code'] == "undefined") {

                        var folderId = item['fld_id'];

                    } else {

                        var folderId = item['file_code'];

                    }

                    var urlByFile = `https://ddownload.com/${folderId}/${item['name']}`;

                    var hidden = 'hidden';

                }

                if (connType == 'Box') {

                    var folderId = item['id'];

                    var urlByFile = `https://ddownload.com/${folderId}/${item['name']}`;

                    var hidden = '';

                }

                if (connType == 'OneDrive') {

                    var folderId = item['id'];

                    var previewFile = 'OnePreviewFile';

                    var urlByFile = baseurl + `home/DownloadFilesOneDrive/${folderId}/${id}/${item['name']}`;

                }

                console.log(folderId);

                // if (item['path_display'] != "") {

                // 	var folderIdDelete = item['path_display'];

                // } else {

                // 	var folderIdDelete = item['id'];

                // }

                // https://ddownload.com/ddlt46f3x7u3/favicon.png



                if (item['path_lower'] != "undefined") {

                    var viewFileId = item['path_lower'];

                } else {

                    var viewFileId = item['id'];

                }



                console.log(item['path_lower']);

                var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['name']}" droType="${item['path_lower']}" data-folder-Id="${folderId}">

                    				<div class="checkbox" hidden>

                    				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                    				    <label for="rememberme${c}" class="option"></label>

                    				</div>

                    				<div class="sc_share_img sc_share_file">

                    				    <img src="${icon}" alt="icon">

                    				</div>

                    				<div class="sc_share_detail">

                    				    <h5>${item['name']}</h5>

                    				</div>

                    				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">

                        				<div class="checkBoxSelect">

                        				    ${(connType == 'Ddownload' || connType == 'OneDrive'? `<a  ${hide} href="${urlByFile}" class="" target="_blank" folderIDShareURL="${folderId}" userID="${id}" >

                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">

                                                    <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>

                                                </svg>

                                                Download

                                            </a>`:`<a  href="javascript:;" ${hide} class="DownloadFile" drotype = "${item['path_display']}"  folderIDShareURL="${folderId}" userID="${id}" >

                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">

                                                    <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>

                                                </svg>

                                                Download

                                            </a>`)}

                                			

                                            <a href="javascript:;" ${hide} class="ShareURL" drotype="${item['path_display']}" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >

                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                    <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                </svg>

                                                Share

                                            </a>

                                            

                                            <a  href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}" drotype="${item['path_lower']}">

                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                    <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                </svg>

                                                Move

                                            </a>

                                            

                                            <a  href="javascript:;"  class="DeletFileAndFolder" ${hidden} user_id="${resp['userId']}" folderID="${folderId}" drotyp="${item['path_display']}">

                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                    <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                </svg>

                                                Delete

                                            </a>

                                            

                                            <a  href="javascript:;" ${hide} class="${(connType == 'OneDrive'? previewFile : 'previewFile')}" drotype = "${item['path_display']}"  folderIDShareURL="${folderId}" userID="${id}">

                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                    <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                </svg>

                                                Preview

                                            </a>

                                            

                                            <a href="javascript:;" ${(connType == 'OneDrive'? 'hidden' : '')}${hide} class="Renamefiles" type="${item['type']}" fileID="${folderId}" fileCode="${folderId}" folderNAME="${item['name']}" droType="${viewFileId}"   >

                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">

                                                    <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63">

                                                    </path>

                                                </svg>

                                                Rename

                                            </a>

                        				</div>

                    				</div>

				                </div>`;

                $('.sc_share_main_section').append(folder);

            });

            }

        }

    });

});



$(document).on('click', '.ShareURL', function() {

    var folderId = $(this).attr('folderidshareurl');

    var fileId = $(this).attr('fileIDShareURL');

    var droType = $(this).attr('drotype');

    var id = $(this).attr('userid');

    $.ajax({

        url: baseurl + 'home/shareURLGet',

        type: "POST",

        data: {

            'folderId': folderId,

            'fileId': fileId,

            'id': id,

            'drType': droType,

        },

        success: function(e) {

            var data = $.parseJSON(e);



            $('#copyTarget').val(decodeURI(data['shareUrl']));

        }

    });

});

$(document).on('click', '.UnShareURL', function() {

    var folderId = $(this).attr('folderidshareurl');

    var fileId = $(this).attr('fileIDShareURL');

    var droType = $(this).attr('drotype');

    var id = $(this).attr('userid');

    $.ajax({

        url: baseurl + 'home/unShare',

        type: "POST",

        data: {

            'folderId': folderId,

            'fileId': fileId,

            'id': id,

            'drType': droType,

        },

        success: function(e) {

            var data = $.parseJSON(e);

           toastr.success('successfully unshare File');

        }

    });

});



$(document).on('click', '.previewFile', function() {

    var folderId = $(this).attr('folderidshareurl');

    var droType = $(this).attr('drotype');

    var id = $(this).attr('userid');



    $.ajax({

        url: baseurl + 'home/view_file',

        type: "POST",

        data: {

            'folderId': folderId,

            'id': id,

            'drType': droType

        },

        success: function(e) {

            var data = $.parseJSON(e);

           

            if (data['data']['connection'] == 'googleDrive') {

                var viewData = '<iframe src="' + data['data']['file']['data']['url'] + '" height="100%" width="100%" title="Iframe Example" class="fileReding"></iframe>';

                $('#AllFilePreview').empty();

                $('#AllFilePreview').append(viewData);

            }else if (data['connection'] == 'OneDrive') {

                window.open(data['shareUrl'], '_blank').focus();

            }else if(data['data']['connection'] == 's3'){

                       

                    if (data['data']['filetype'][4] == 'jpeg' || data['data']['filetype'][4] == 'jpg' || data['data']['filetype'][4] == 'png' || data['data']['filetype'][4] == 'gif' ) {

                        var viewData = '<img src="' + data['data']['url'] + '">';

                            $('#AllFilePreview').empty();

                            $('#AllFilePreview').append(viewData);

                    } else {

                         var viewData = `<center><h5 class="modal-title" id="add_customer_title">File Not Priview Only Download</h5><br>

                                            <div class="add-group">

            									<a href="${data['data']['url']}" class="ad-btn">Clieck Here Download </a>

            								</div></center>

                                        `;

                            $('#AllFilePreview').empty();

                            $('#AllFilePreview').append(viewData);

                    }

               

            }else{

                 var viewData = '<img src="' + data['data']['file']['data']['url'] + '">';

                $('#AllFilePreview').empty();

                $('#AllFilePreview').append(viewData);

            }

        }

    });

});



$(document).on('click', '.DownloadFile', function() {

    var folderId = $(this).attr('folderidshareurl');

    var droType = $(this).attr('drotype');

    var id = $(this).attr('userid');

    $.ajax({

        url: baseurl + 'home/DownloadFiles',

        type: "POST",

        data: {

            'folderId': folderId,

            'id': id,

            'drType': droType,

            // 		'fileName': fileName

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            window.open(resp['shareUrl'], '_blank').focus();

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

        }

    });

});

$(document).on('click', '.Renamefiles', function() {

    var path = $(this).attr('drotype');

    var folderNAME = $(this).attr('folderNAME');

    var fileID = $(this).attr('fileID');

    var fileCode = $(this).attr('fileCode');

    var type = $(this).attr('type');

    var fileName = $('#fileName').val();

    $('.RenameFile').attr('path', path);

    $('.RenameFile').attr('fileID', fileID);

    $('.RenameFile').attr('folderNAME', folderNAME);

    $('.RenameFile').attr('fileCode', fileCode);

    $('.RenameFile').attr('typefile', type);



});

$(document).on('click', '.RenameFile', function() {

    var path = $(this).attr('path');

    var fileID = $(this).attr('fileID');

    var foldername = $(this).attr('foldername');

    var user_id = $('#user_id').val();

    var fileName = $('#fileName').val();

    var folderPath = $(this).attr('drotype');

    var fileCode = $(this).attr('fileCode');

    var typefile = $('.RenameFile').attr('typefile');



    $.ajax({

        url: baseurl + 'home/RenameFile',

        type: "POST",

        data: {

            'fileName': fileName,

            'path': path,

            'fileID': fileID,

            'id': user_id,

            'folderPath': folderPath,

            'foldername': foldername,

            'fileCode': fileCode,

            'typefile': typefile

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            if (resp['data'] != '' && resp['status'] == '1') {

                toastr.success('Successfully Change File Name ');

                setTimeout(function() {

                    window.location.reload();

                }, 3000);

                return false;

            } else {

                toastr.error(resp['Something went wrong. Please, refresh the page and try again']);

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

        }

    });

});



$('.s3UploadFile').on('click', function() {

    var formdata = new FormData($(this).closest('form')[0]);

    var user_id = $('#user_id').val();

    formdata.append('type', 'amzonS3');

    $.ajax({

        xhr: function() {

            var xhr = new window.XMLHttpRequest();

            $('.progress').removeClass('hide');

            xhr.upload.addEventListener("progress", function(evt) {

                if (evt.lengthComputable) {

                    var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);

                    $(".progress-bar").width(percentComplete + '%');

                    $(".progress-bar").html(percentComplete + '%');

                }

            }, false);

            return xhr;

        },

        method: "POST",

		url: baseurl + 'home/userUploadFiles',

        data: formdata,

        processData: false,

        contentType: false,

        success: function(resp) {

            var resp = $.parseJSON(resp);

            console.log(resp);

            if (resp['status'] == '1') {

                toastr.success('Successfully Upload File');

                setTimeout(function() {

                    location.reload();

                }, 1000);

                return false;

                $.ajax({

                    url: baseurl + 'home/getSubFolderByFolderId',

                    type: "POST",

                    data: {

                        'name': name,

                        'folderId': folderid,

                        'id': user_id

                    },

                    success: function(e) {

                        var resp = $.parseJSON(e);



                        if (resp != "") {

                            $('.sc_share_main_section').empty();

                            $('.progress-bar').empty();

                            $('.UploadFile_Name').val('');

                            $('#UploadFile').modal('hide');

                        }



                        var i = 1;

                        jQuery.each(resp['data']['data'], function(index, item) {

                            var c = i++;

                            var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);

                            var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sq', 'json'];

                            // var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx'];



                            if (Extension.lastIndexOf(fileExtension) == -1) {

                                var icon = baseurl + 'assets/backend/images/folder.png';

                                var getFdrClass = 'getSubFolderByFolderId';

                            } else {

                                var icon = baseurl + 'assets/backend/images/file.png';

                                var getFdrClass = '';

                            }

                            if (item['path_display'] != "") {

                                var folderIdDelete = item['path_display'];

                            } else {

                                var folderIdDelete = item['id'];

                            }

                            // var folder = '<div class="sc_share_wrapper  ' + getFdrClass + '" option="option' + c + '" id="' + resp['userId'] + '" data-folder-name="' + item['name'] + '" droType="' + item['path_lower'] + '" data-folder-Id="' + item['id'] + '"><div hidden class="checkbox"><input type="checkbox" id="rememberme' + c + '" name=""class="optionCheckBox" value="" >        <label for="rememberme' + c + '" class="option"></label>    </div>    <div class="sc_share_img"><img src="' + icon + '" alt="icon"></div><div class="sc_share_detail"><h5>' + item['name'] + '</h5></div><div class="checkOptionMenu tbf_IconHolter option' + c + '" clickValue="option' + c + '" style="display:none;"><div class="checkBoxSelect"><a hidden href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px"><path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/></svg>Copy To</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px"><path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/></svg>Download</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Share</a><a hidden href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Cut</a><a  href="javascript:;" class="moveFile " userId="' + resp['userId'] + '" fileId="' + item['id'] + '" drotype="' + item['path_lower'] + '"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Move</a><a  href="javascript:;" class="DeletFileAndFolder" user_id="' + resp['userId'] + '" folderID="' + folderIdDelete + '" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Delete</a><a class="Renamefiles" href="' + baseurl + 'home/view_file/' + item['id'] + '/' + user_id + '" ><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px"><path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/></svg>Preview</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17"><path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path></svg>Rename</a><a hidden href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/></svg>Copy</a></div></div></div>';

                            var folder = `<div class="sc_share_wrapper  ${getFdrClass }" option="option${c}'" id="${resp['userId'] }" data-folder-name="${item['name']  }" droType="${item['path_lower'] }" data-folder-Id="${item['id'] }">

                                            <div hidden class="checkbox">

                                                <input type="checkbox" id="rememberme${c}'" name=""class="optionCheckBox" value="" >

                                                    <label for="rememberme${c}'" class="option"></label> 

                                            </div>

                                            <div class="sc_share_img">

                                                <img src="${icon }" alt="icon">

                                            </div>

                                            <div class="sc_share_detail">

                                                <h5>${item['name']  }</h5>

                                            </div>

                                            <div class="checkOptionMenu tbf_IconHolter option${c}'" clickValue="option${c}'" style="display:none;">

                                                <div class="checkBoxSelect">

                                                <a hidden href="javascript:;">

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">

                                                        <path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>

                                                    </svg>Copy To</a>

                                                <a  href="javascript:;">

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">

                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>

                                                    </svg>Download</a>

                                                <a  href="javascript:;">

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>Share</a>

                                                <a hidden href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>Cut</a>

                                                <a  href="javascript:;" class="moveFile " userId="${resp['userId'] }" fileId="${item['id'] }" drotype="${item['path_lower'] }">

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>Move</a>

                                                <a  href="javascript:;" class="DeletFileAndFolder" user_id="${resp['userId'] }" folderID="' + folderIdDelete + '" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>Delete</a>

                                                <a class="" href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                    </svg>Preview</a>

                                                <a  href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">

                                                        <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>

                                                    </svg>Rename</a>

                                                <a hidden href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>

                                                    </svg>Copy

                                                </a>

                                            </div>

                                        </div>

                                    </div>`;

                            $('.sc_share_main_section').append(folder);

                        });





                    }

                });

            } else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

            $('.edu_preloader').fadeOut();

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

        }

    });

});

$('.CloudUploadFile').on('click', function() {

    var formdata = new FormData($(this).closest('form')[0]);

    var path = $(this).attr('drotype');

    var folderid = $(this).attr('folderid');

    var user_id = $('#user_id').val();

    formdata.append('path', path);

    formdata.append('folderid', folderid);

    $.ajax({

        xhr: function() {

            var xhr = new window.XMLHttpRequest();

            $('.progress').removeClass('hide');

            xhr.upload.addEventListener("progress", function(evt) {

                if (evt.lengthComputable) {

                    var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);

                    $(".progress-bar").width(percentComplete + '%');

                    $(".progress-bar").html(percentComplete + '%');

                }

            }, false);

            return xhr;

        },

        method: "POST",

		url: baseurl + 'home/userUploadFiles',

        data: formdata,

        processData: false,

        contentType: false,

        success: function(resp) {

            var resp = $.parseJSON(resp);

            console.log(resp);

            if (resp['status'] == '1') {

                toastr.success('Successfully Upload File');

                setTimeout(function() {

                    location.reload();

                }, 1000);

                return false;

                $.ajax({

                    url: baseurl + 'home/getSubFolderByFolderId',

                    type: "POST",

                    data: {

                        'name': name,

                        'folderId': folderid,

                        'id': user_id

                    },

                    success: function(e) {

                        var resp = $.parseJSON(e);



                        if (resp != "") {

                            $('.sc_share_main_section').empty();

                            $('.progress-bar').empty();

                            $('.UploadFile_Name').val('');

                            $('#UploadFile').modal('hide');

                        }



                        var i = 1;

                        jQuery.each(resp['data']['data'], function(index, item) {

                            var c = i++;

                            var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);

                            var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sq', 'json'];

                            // var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx'];



                            if (Extension.lastIndexOf(fileExtension) == -1) {

                                var icon = baseurl + 'assets/backend/images/folder.png';

                                var getFdrClass = 'getSubFolderByFolderId';

                            } else {

                                var icon = baseurl + 'assets/backend/images/file.png';

                                var getFdrClass = '';

                            }

                            if (item['path_display'] != "") {

                                var folderIdDelete = item['path_display'];

                            } else {

                                var folderIdDelete = item['id'];

                            }

                            // var folder = '<div class="sc_share_wrapper  ' + getFdrClass + '" option="option' + c + '" id="' + resp['userId'] + '" data-folder-name="' + item['name'] + '" droType="' + item['path_lower'] + '" data-folder-Id="' + item['id'] + '"><div hidden class="checkbox"><input type="checkbox" id="rememberme' + c + '" name=""class="optionCheckBox" value="" >        <label for="rememberme' + c + '" class="option"></label>    </div>    <div class="sc_share_img"><img src="' + icon + '" alt="icon"></div><div class="sc_share_detail"><h5>' + item['name'] + '</h5></div><div class="checkOptionMenu tbf_IconHolter option' + c + '" clickValue="option' + c + '" style="display:none;"><div class="checkBoxSelect"><a hidden href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px"><path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/></svg>Copy To</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px"><path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/></svg>Download</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Share</a><a hidden href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Cut</a><a  href="javascript:;" class="moveFile " userId="' + resp['userId'] + '" fileId="' + item['id'] + '" drotype="' + item['path_lower'] + '"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Move</a><a  href="javascript:;" class="DeletFileAndFolder" user_id="' + resp['userId'] + '" folderID="' + folderIdDelete + '" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Delete</a><a class="Renamefiles" href="' + baseurl + 'home/view_file/' + item['id'] + '/' + user_id + '" ><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px"><path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/></svg>Preview</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17"><path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path></svg>Rename</a><a hidden href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/></svg>Copy</a></div></div></div>';

                            var folder = `<div class="sc_share_wrapper  ${getFdrClass }" option="option${c}'" id="${resp['userId'] }" data-folder-name="${item['name']  }" droType="${item['path_lower'] }" data-folder-Id="${item['id'] }">

                                            <div hidden class="checkbox">

                                                <input type="checkbox" id="rememberme${c}'" name=""class="optionCheckBox" value="" >

                                                    <label for="rememberme${c}'" class="option"></label> 

                                            </div>

                                            <div class="sc_share_img">

                                                <img src="${icon }" alt="icon">

                                            </div>

                                            <div class="sc_share_detail">

                                                <h5>${item['name']  }</h5>

                                            </div>

                                            <div class="checkOptionMenu tbf_IconHolter option${c}'" clickValue="option${c}'" style="display:none;">

                                                <div class="checkBoxSelect">

                                                <a hidden href="javascript:;">

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">

                                                        <path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>

                                                    </svg>Copy To</a>

                                                <a  href="javascript:;">

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">

                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>

                                                    </svg>Download</a>

                                                <a  href="javascript:;">

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>Share</a>

                                                <a hidden href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>Cut</a>

                                                <a  href="javascript:;" class="moveFile " userId="${resp['userId'] }" fileId="${item['id'] }" drotype="${item['path_lower'] }">

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                                    </svg>Move</a>

                                                <a  href="javascript:;" class="DeletFileAndFolder" user_id="${resp['userId'] }" folderID="' + folderIdDelete + '" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                                    </svg>Delete</a>

                                                <a class="" href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                                    </svg>Preview</a>

                                                <a  href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">

                                                        <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>

                                                    </svg>Rename</a>

                                                <a hidden href="javascript:;" >

                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                        <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>

                                                    </svg>Copy

                                                </a>

                                            </div>

                                        </div>

                                    </div>`;

                            $('.sc_share_main_section').append(folder);

                        });





                    }

                });

            } else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

            $('.edu_preloader').fadeOut();

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

        }

    });

});

$('.moveFileForm').hide();

$('.MoveBackFolder').hide();

$(document).on('click', '.moveFile', function(e) {

    var fileId = $(this).attr('fileId');

    var fileName = $(this).attr('filename');

    var drotype = $(this).attr('drotype');

  

    var id = $(this).attr('userId');

    $.ajax({

        url: baseurl + 'home/GetAllConnection',

        type: "POST",

        data: {

            'id': id

        },

        success: function(e) {

            var data = $.parseJSON(e);

            if (data != "") {

                $('.MovingFile').empty();

                $('#fileId').attr('MoveFileId', fileId);

                $('#fileId').attr('drotype', drotype);

                $('.moveFileDriveAndDropBox').attr('MoveFileId', fileId);

                $('.MoveUploadFile').attr('MoveFileName', fileName);

                $('.MoveBackFolder').show();

                $('#UserID').attr('MoveUserId', id);

            }

            var i = 1;

            jQuery.each(data['data'], function(index, item) {

                if (item['connection_type'] == 'GoogleDrive') {

                    var resp = $.parseJSON(item['user_info']);

                    if (resp['image'] != '') {

                        var img = baseurl + 'assets/backend/images/icon1.png';

                    } else {

                        var img = baseurl + 'assets/backend/images/icon1.png';

                    }

                    var name = resp['name'];

                    var email = resp['email'];

                    var usedSpace = resp['usedSpace'] / 1073741824;

                    var totalSpace = resp['totalSpace'] / 1073741824;



                } else if (item['connection_type'] == 'DropBox') {

                    var resp = $.parseJSON(item['user_info']);

                    var img = baseurl + 'assets/backend/images/dropbox.png';

                    var name = resp['name']['display_name'];

                    console.log(name);

                    var email = resp['email'];

                    var usedSpace = resp['usedSpace'] / 1073741824;

                    var totalSpace = resp['totalSpace'] / 1073741824;



                } else if (item['connection_type'] == 'Ddownload') {

                    var resp = $.parseJSON(item['user_info']);

                    var img = baseurl + 'assets/backend/images/ddownload.png';

                    var nam = resp['email'].split("@");

                    console.log(nam);

                    var name = resp[0];

                    var email = resp['email'];

                    var usedSpace = resp['storage_used'] / 1073741824;

                    var totalSpace = 10;



                } else if (item['connection_type'] == 'OneDrive') {

                    var resp = $.parseJSON(item['user_info']);

                    var img = baseurl + 'assets/backend/images/onedrive.png';

                    var name = resp['name'];

                    var email = resp['email'];

                } else if (item['connection_type'] == 'Box') {

                    var resp = $.parseJSON(item['user_info']);

                    var img = baseurl + 'assets/backend/images/icon8.png';

                    var name = resp['name'];

                    var email = resp['login'];

                }else if(item['connection_type'] == 's3'){

                    var resp = $.parseJSON(item['user_info']);

                    var img = baseurl + 'assets/backend/images/icon18.png';

                    var name = resp['name'];

                    var email = resp['email']; 

                }

                var connection = `<a href="javascript:;" userid="${item['id']}" connectionID="${item['id']}" MoveFileId="${fileId}" class="moveFileDriveAndDropBox">

                                    <div class="ad-info-card">

                                        <div class="card-body dd-flex align-items-center">

                                            <div class="icon-box-wrapper">

                                                <img src="${img}" alt="image">

                                            </div>

                                            <div class="icon-info-text">

                                                <h5>${name}</h5>

                                                <h6>${email}</h6>

                                            </div>

                                        </div>

                                    </div>

                                </a>`;

                $('.MovingFile').append(connection);

            });

        }

    });

});

$(document).on('click', '.moveFileDriveAndDropBox', function(e) {

    var MoveFileId = $(this).attr('MoveFileId');

    var drotype = $('#fileId').attr('drotype');

    var connectionID = $(this).attr('connectionID');

    var id = $(this).attr('userId');

    var fileName = $(this).data('MoveFileName');



    $.ajax({

        url: baseurl + 'home/moveFileDataget',

        type: "POST",

        data: {

            'id': id

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            console.log(resp)

            if (resp != "") {

                $('.MovingFile').empty();

                $('#fileId').attr('MoveFileId', MoveFileId);

                $('#fileId').attr('drotype', drotype);

                $('#fileId').attr('connectionID', connectionID);

                $('.MoveBackFolder').show();

                $('.MoveUploadFile').attr('MovingFile', MoveFileId);

                $('.MoveUploadFile').attr('MoveFileName', fileName);

                $('.MoveUploadFile').attr('drotype', drotype);

                $('.MoveUploadFile').attr('connectionID', connectionID);

                $('#UserID').attr('MoveUserId', id);

                $('.CloudUploadFile').attr('drotype', drotype);

                

            }else{

                toastr.error('Data Not Found ');

                return false;

            }

            var i = 1;

            jQuery.each(resp['data']['getAllFolder']['data'], function(index, item) {

                if(resp['data']['connectionType']=="s3"){

                    $('.moveFileForm').show();

                   

                    var c = i++;

                    var ans = item['Key'].includes('/') ? "true" : "false";

                    // var result= item['Key'].split("").reverse().join("");

                    

                    var key = MoveFileId;

                    var parts = key.split("/");

                    var filesName = parts[parts.length-1];

                    

                    $('.MoveUploadFile').attr('movefileid', MoveFileId);

                    $('.MoveUploadFile').attr('MoveFileName', filesName);

                    $('.MoveUploadFile').attr('drotype', drotype);

                    $('.MoveUploadFile').attr('connectionID', connectionID);

                    

                    if (ans==false) {

                        var icon = baseurl + 'assets/backend/images/folder.png';

                        var getFdrClass = 'MovegetSubFolderByFolderId';

                    } else {

                        var icon = baseurl + 'assets/backend/images/file.png';

                        var getFdrClass = '';

                    }

                        var folderId = item['Key'];

                        var folderIdDelete = item['Key'];

                        var viewFileId = item['Key'];

                        

                    var folder = `<div class="sc_share_wrapper ${getFdrClass}" option="option${c}" id="${resp['userId']}"  data-folder-name="${item['Key']}" droType="${drotype}" data-folder-Id="${folderId}">

                                	<div class="checkbox" hidden>

                                		<input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                                		<label for="rememberme${c}" class="option"></label>    

                                	</div>    

                                	<div class="sc_share_img sc_share_file">

                                		<img src="${icon}" alt="icon">

                                	</div>

                                	<div class="sc_share_detail">

                                		<h5>${filesName}</h5>

                                	</div>

                                	

                                </div>`;

                    $('.MovingFile').append(folder);

                }else{

                    var c = i++;

                    var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);

                    var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sq', 'json'];

                    // var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx'];

                    if (Extension.lastIndexOf(fileExtension) == -1) {

                        var icon = baseurl + 'assets/backend/images/folder.png';

                        var getFdrClass = 'MovegetSubFolderByFolderId';

                    } else {

                        var icon = baseurl + 'assets/backend/images/file.png';

                        var getFdrClass = '';

                    }

                    if (resp.data.connectionType == 'Google Drive') {

                        var folderId = item['id'];

                        var folderIdDelete = item['id'];

                        var viewFileId = item['id'];

                    }

                    if (resp.data.connectionType == 'DropBox') {

                        var folderIdDelete = item['path_display'];

                        var viewFileId = item['path_lower'];

                    }

                    if (resp.data.connectionType == 'Ddownload') {

                        var folderId = item['fld_id'];

                    }

                    if (resp.data.connectionType == 'OneDrive') {

                        var folderId = item['id'];

                    }

                    if (resp.data.connectionType == 'Box') {

                        // console.log(item);

                        var folderId = item['id'];

                    }

    

                    var folder = `<div class="sc_share_wrapper ${getFdrClass}" option="option${c}" id="${resp['userId']}"  data-folder-name="${item['name']}" droType="${drotype}" data-folder-Id="${folderId}">

                                	<div class="checkbox" hidden>

                                		<input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                                		<label for="rememberme${c}" class="option"></label>    

                                	</div>    

                                	<div class="sc_share_img sc_share_file">

                                		<img src="${icon}" alt="icon">

                                	</div>

                                	<div class="sc_share_detail">

                                		<h5>${item['name']}</h5>

                                	</div>

                                	

                                </div>`;

                    $('.MovingFile').append(folder);

                }

               

            });

        }

    });

});

$(document).on('click', '.OnePreviewFile', function() {

    var folderId = $(this).attr('folderidshareurl');

    var fileId = $(this).attr('fileIDShareURL');

    var droType = $(this).attr('drotype');

    var id = $(this).attr('userid');

    $.ajax({

        url: baseurl + 'home/PreviewFileOneDrive',

        type: "POST",

        data: {

            'folderId': folderId,

            'fileId': fileId,

            'id': id,

            'drType': droType,

        },

        success: function(e) {

            var data = $.parseJSON(e);

            window.open(data['shareUrl'], '_blank').focus();

        }

    });

});

$('.moveFileForm').hide();

$(document).on('click', '.MovegetSubFolderByFolderId', function() {

    var name = $(this).attr('data-folder-name');

    var folderId = $(this).attr('data-folder-Id');

    var movefileid = $('#fileId').attr('movefileid');

    var connectionID = $('.MoveUploadFile').attr('connectionID');

    var droType = $(this).attr('drotype');

    var olddroType = $('.MoveUploadFile').attr('drotype');

    var type = $(this).attr('type');

    var id = $(this).attr('id');



    $.ajax({

        url: baseurl + 'home/getSubFolderByFolderId',

        type: "POST",

        data: {

            'name': name,

            'folderId': folderId,

            'id': id,

            'type': type,

            'drType': droType

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            if (resp != "") {

                $('.MovingFile').empty();

                $('.moveFileForm').show();

                $('.MoveProgressBar').show();

                $('.MoveUploadFile').attr('folderId', folderId);

                $('.MoveUploadFile').attr('movefileid', movefileid);

                $('.MoveUploadFile').attr('connectionID', connectionID);

                $('.MoveUploadFile').attr('NewDrotype', name);

                $('.MoveUploadFile').attr('userID', id);

                $('#moveFolderName').html('To '+name+' Folder');

            }

            var i = 1;

            jQuery.each(resp['data']['data'], function(index, item) {

                var c = i++;

                var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);

                var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json'];

                // var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js'];

                if (Extension.lastIndexOf(fileExtension) == -1) {

                    var icon = baseurl + 'assets/backend/images/folder.png';

                    var getFdrClass = 'MovegetSubFolderByFolderId';

                } else {

                    var icon = baseurl + 'assets/backend/images/file.png';

                    var getFdrClass = '';

                }

                if (item['path_display'] != "") {

                    var folderIdDelete = item['path_display'];

                } else {

                    var folderIdDelete = item['id'];

                }

                if (item['path_lower'] != "undefined") {

                    var viewFileId = item['path_lower'];

                } else {

                    var viewFileId = item['id'];

                }



                // var folder = '<div class="sc_share_wrapper  "  id="' + resp['userId'] + '" data-folder-name="' + item['name'] + '" droType="' + item['path_lower'] + '" data-folder-Id="' + item['id'] + '"><div class="checkbox" hidden><input type="checkbox" id="rememberme' + c + '" name=""class="optionCheckBox" value="" >        <label for="rememberme' + c + '" class="option"></label>    </div>    <div class="sc_share_img sc_share_file"><img src="' + icon + '" alt="icon"></div><div class="sc_share_detail"><h5>' + item['name'] + '</h5></div><div class="checkOptionMenu tbf_IconHolter option' + c + '" clickValue="option' + c + '" style="display:none;"><div class="checkBoxSelect"><a hidden href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px"><path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/></svg>Copy To</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px"><path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/></svg>Download</a><a href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Share</a><a  href="javascript:;" class="moveFile " userId="' + id + '" fileId="' + item['id'] + '"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Move</a><a  href="javascript:;" class="DeletFileAndFolder" user_id="' + resp['userId'] + '" folderID="' + folderIdDelete + '" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Delete</a><a  href="' + baseurl + 'home/view_file/' + item['id'] + '/' + id + '" ><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px"><path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/></svg>Preview</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17"><path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path></svg>Rename</a><a hidden href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/></svg>Copy</a></div></div></div>';

               var folder = `<div class="sc_share_wrapper  "  id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item['path_lower']}'" data-folder-Id="${ item['id']}">

                                <div class="checkbox" hidden>

                                    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >

                                        <label for="rememberme${c}" class="option"></label>

                                </div> 

                                <div class="sc_share_img sc_share_file">

                                    <img src="${icon}" alt="icon">

                                </div>

                                <div class="sc_share_detail">

                                    <h5>${ item['name']}</h5>

                                </div>

                                <div class="checkOptionMenu tbf_IconHolter option${c}" clickValue="option${c}" style="display:none;">

                                    <div class="checkBoxSelect">

                                        <a hidden href="javascript:;">

                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">

                                                <path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>

                                            </svg>Copy To

                                        </a>

                                        <a  href="javascript:;">

                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">

                                                <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>

                                            </svg>Download

                                        </a>

                                        <a href="javascript:;">

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>

                                            </svg>Share

                                        </a>

                                        <a  href="javascript:;" class="moveFile " userId="' + id + '" fileId="${ item['id']}">

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>

                                            </svg>Move

                                        </a>

                                        <a  href="javascript:;" class="DeletFileAndFolder" user_id="${resp['userId']}" folderID="${folderIdDelete }" >

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>

                                            </svg>Delete

                                        </a>

                                        <a  href="${baseurl}home/view_file/${ item['id']}/${id}" >

                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">

                                                <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>

                                            </svg>Preview

                                        </a>

                                        <a  href="javascript:;" >

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">

                                                <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>

                                            </svg>Rename

                                        </a>

                                        <a hidden href="javascript:;" >

                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">

                                                <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>

                                            </svg>Copy

                                        </a>

                                    </div>

                                </div>

                            </div>`;

                $('.MovingFile').append(folder);

            });





        }

    });

});

$(document).on('click', '.MoveUploadFile', function(e) {

    var MoveFolderID = $(this).attr('folderid');

    var MoveFileID = $(this).attr('movefileid');

    var movefilename = $(this).attr('movefilename');

    var connectionID = $(this).attr('connectionid');

    var oldPath = $(this).attr('drotype');

    var NewFolderPath = $(this).attr('newdrotype');

    var userID = $('#user_id').val();



    $.ajax({

        xhr: function() {

            var xhr = new window.XMLHttpRequest();

            $('.progress').removeClass('hide');

            xhr.upload.addEventListener("progress", function(evt) {

                if (evt.lengthComputable) {

                    var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);

                    $(".progress-bar").width(percentComplete + '%');

                    $(".progress-bar").html(percentComplete + '%');

                }

            }, false);

            return xhr;

        },

        url: baseurl + 'home/SinkMoveFileUpload',

        type: "POST",

        data: {

            'MoveFolderID': MoveFolderID,

            'MoveFileID': MoveFileID,

            'oldPath': oldPath,

            'NewFolderPath': NewFolderPath,

            'connectionID': connectionID,

            'oldUserId': userID,

            'movefilename': movefilename,

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            if (resp.status = "1") {

                toastr.success('Successfully Move File ');

                setTimeout(function() {

                    location.reload();

                }, 1000);

            } else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });





});



$(document).on('click', '.DeletFileAndFolder', function(e) {

    var drotyp = $(this).attr('drotyp');

    var folderID = $(this).attr('folderID');

    var user_id = $(this).attr('user_id');

    var fileid = $(this).attr('fileid');

   

    $.ajax({

        url: baseurl + 'home/userDeleteFolder',

        type: "POST",

        data: {

            'folderID': folderID,

            'drotyp': drotyp,

            'user_id': user_id,

            'fileid': fileid

        },

        success: function(e) {

            var resp = $.parseJSON(e);

          

            toastr.success('Successfully Delete Folder');

            setTimeout(function() {

                location.reload();

            }, 1000);

            console.log(resp)

            return false;

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });



});

$(document).on('click', '#copyButton', function() {



    var res = copyToClipboard(document.getElementById("copyTarget"));

    if (res) {

        toastr.success('Copy Link');

    }

});





function copyToClipboard(elem) {

    var targetId = "_hiddenCopyText_";

    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";

    var origSelectionStart, origSelectionEnd;

    if (isInput) {

        target = elem;

        origSelectionStart = elem.selectionStart;

        origSelectionEnd = elem.selectionEnd;

    } else {

        target = document.getElementById(targetId);

        if (!target) {

            var target = document.createElement("textarea");

            target.style.position = "absolute";

            target.style.left = "-9999px";

            target.style.top = "0";

            target.id = targetId;

            document.body.appendChild(target);

        }

        target.textContent = elem.textContent;

    }

    // select the content

    var currentFocus = document.activeElement;

    target.focus();

    target.setSelectionRange(0, target.value.length);



    // copy the selection

    var succeed;

    try {

        succeed = document.execCommand("copy");

    } catch (e) {

        succeed = false;

    }

    // restore original focus

    if (currentFocus && typeof currentFocus.focus === "function") {

        currentFocus.focus();

    }



    if (isInput) {

        // restore prior selection

        elem.setSelectionRange(origSelectionStart, origSelectionEnd);

    } else {

        // clear temporary content

        target.textContent = "";

    }

    return succeed;

}







//////////////////// DDownload  JS ///////////////////

$(document).on('click', '#ddownload_submit', function() {

    var key = $('#ddownload_key').val();

    $.ajax({

        url: baseurl + 'api/ddownload',

        type: "POST",

        data: {

            'key': key,

        },

        success: function(e) {

            var resp = $.parseJSON(e);

            console.log(resp);

            if (resp['status'] == '1') {

                toastr.success('Connection successfully.');

                setTimeout(function() {

                    location.reload();

                }, 1000);

            } else {

                toastr.error('Something went wrong. Please, refresh the page and try again.');

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });



});

///////////////////// Amazon S3 ///////////////////////////////

$(document).on('click', '#amazonS3Connectoin', function() {

    var formdata = new FormData($(this).closest('form')[0]);

    $.ajax({

        method: "POST",

        url: baseurl + 'home/AmazonS3Auth',

        data: formdata,

        processData: false,

        contentType: false,

        success: function(resp) {

            var resp = $.parseJSON(resp);

            console.log(resp);

            if (resp['status'] == '1') {

                toastr.success(resp.smg);

                setTimeout(function() {

                   window.location.href=baseurl+"home/dashboard";

                }, 1000);

            } else {

                toastr.error(resp.smg);

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });



});





/////////////anonfilesConnection //////////////////////

$(document).on('click', '#anonfilesConnection', function() {

    var formdata = new FormData($(this).closest('form')[0]);

    $.ajax({

        method: "POST",

        url: baseurl + 'home/anonfiles',

        data: formdata,

        processData: false,

        contentType: false,

        success: function(resp) {

            var resp = $.parseJSON(resp);

            console.log(resp);

            if (resp['status'] == '1') {

                toastr.success(resp.smg);

                setTimeout(function() {

                   window.location.href=baseurl+"home/dashboard";

                }, 1000);

            } else {

                toastr.error(resp.smg);

                return false;

            }

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });



});



/////////////////// Google Photos Connection //////////////////

$(document).on('click', '#GooglePhotosCon', function() {

    var google_phpto_display_name = $('#displayName').val();

    $.ajax({

        method: "POST",

        url: baseurl + 'home/GooglePhotoCon',

        data: {'displayName' : google_phpto_display_name},

        success: function(resp) {

            var resp = $.parseJSON(resp);

            if (resp['status'] == '1') {

                //  window.open(resp['AuthURL']).focus();

                 window.location.href=resp['AuthURL'];

            } else if(resp['status'] == '0'){

                toastr.error(resp.sms);

                return false;

            }

          

        },

        error: function(resp) {

            toastr.error('Something went wrong. Please, refresh the page and try again.');

            return false;

        }



    });

    

});

