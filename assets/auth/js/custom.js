/*--------------------- Copyright (c) 2023 -----------------------
[Javascript]
Project: selfdrive 
*/
(function ($) {
    "use strict";
    /*-----------------------------------------------------
        Function  Start
    -----------------------------------------------------*/
    var selfDrive = {
        initialised: false,
        version: 1.0,
        mobile: false,
        init: function () {
            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }
            /*-----------------------------------------------------
                Function Calling
            -----------------------------------------------------*/
            
        }
    }
    selfDrive.init();
})(jQuery);

$(document).ready(function(){
    $(document).on('click','.close',function(){
        $('.plr_notification').removeClass('plr_success_msg');
        $('.plr_notification').removeClass('plr_error_msg');
    })
    $.ajax({
        url: baseurl + 'authenticate/login',
    });
    
    $(document).on("click", '[data-action="submitMe"]', (e) => {
        e.preventDefault()
    });
})
// login js
    $(document).on('click','.loginSection',function(e){
       e.preventDefault();
        var email = $.trim($('#em').val());
        var pwd = $.trim($('#pwd').val());
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
        if(email == ''){
            showNotifications('error','Please enter your email.');
            if(email == ''){ $("#em").focus();}
        }else if(!regex.test(email)){
            showNotifications('error','Please enter a valid email.');
            if(email == ''){ $("#em").focus();}
        }else if(pwd == ''){
            showNotifications('error','Please enter password.');
            
            if(pwd == ''){ $("#pwd").focus();}
        }else{
            let formdata = {};
            formdata.em = email;
            formdata.pwd = pwd;
            formdata.rem = $('#rememberme:checked').length;
            
            $.ajax({
                url: baseurl + 'authenticate/login', 
                type: "POST",             
                data: formdata,      
                success: function(e) {
                    if(e == 0)
                        showNotifications('error','We can\'t find your email.');
                    else if(e == 1)
                        showNotifications('error','Your password doesn\'t match with our records.');
                    else if(e == 2)
                        showNotifications('error','Your account is InActive. Please, contact support.');
                    else if(e == 3)
                        showNotifications('error','Your account is Blocked. Please, contact support.');
                    else if(e == 4)
                        showNotifications('error','Your account is Blocked. Please, contact your agency owner.');
                    else if( e == 5 ) {
                        showNotifications('success','Welcome, you logged in successfully.');
                        setTimeout(function(){
                            window.location = baseurl+'home/checkLoginUser';
                        },3000);
                    }else if(e == 6){
                        //  showNotifications('error','Your account is Blocked. Please, contact your agency owner.');
                         $('.plr_login_form').hide();
                         $('.WithoutFeUser').show();
                    }
                    else
                        showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                }
            });
        }
    })
   

// Sign Up js
function signupSection(){
    var uname = $.trim($('#nm').val());
    var email = $.trim($('#em').val());
    var pwd = $.trim($('#pwd').val());
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(uname == ''){
        showNotifications('error','Please enter your name.');
        if(uname == ''){ $("#nm").focus();}
    }else if(email == ''){
        showNotifications('error','Please enter your email.');
        if(email == ''){ $("#em").focus();}
    }else if(!regex.test(email)){
        showNotifications('error','Please enter a valid email.');
        if(email == ''){ $("#em").focus();}
    }else if(pwd == ''){
        showNotifications('error','Please enter password.');
        if(pwd == ''){ $("#pwd").focus();}
    }else{
        let formdata = {};
        formdata.nm = uname;
        formdata.em = email;
        formdata.pwd = pwd;

        $.ajax({
            url: baseurl + 'authenticate/signup', 
            type: "POST",             
            data: formdata,      
            success: function(e) {
                if(e == 2){
                    showNotifications('success','Thank you for buying access to Payola. We have just updated your account password.');
                    setTimeout(function(){
                        window.location = baseurl+'home';
                    },3000);
                }
                else if( e == 5 ) {
                    showNotifications('success','Welcome, you signed up successfully.');
                    setTimeout(function(){
                        window.location = baseurl+'home';
                    },3000);
                }
                else
                    showNotifications('error','Something went wrong. Please, refresh the page and try again.');
            }
        });
    }
    
}


// Forgot Password
function forgotSection(){
    var email = $.trim($('#em').val());
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(email == ''){
        showNotifications('error','Please enter your email.');
        if(email == ''){ $("#em").focus();}
    }else if(!regex.test(email)){
        showNotifications('error','Please enter a valid email.');
        if(email == ''){ $("#em").focus();}
    }else{
        let formdata = {};
        formdata.em = email;
        $.ajax({
            url: baseurl + 'authenticate/forgotSection', 
            type: "POST",             
            data: formdata,      
            success: function(e) {
                if(e == 0)
                    showNotifications('error','We can\'t find your email.');
                else if(e == 2)
                    showNotifications('error','Your account is InActive. Please, contact support.');
                else if(e == 3)
                    showNotifications('error','Your account is Blocked. Please, contact support.');
                else if( e == 5 ) {
                    showNotifications('success','Check your email for the new password.');
                    setTimeout(function(){
                        window.location = baseurl;
                    },3000);
                }
                else
                    showNotifications('error','Something went wrong. Please, refresh the page and try again.');
            }
        });
    }

}

function showNotifications(type, message){
    $('.plr_notification').removeClass('plr_success_msg');
    $('.plr_notification').removeClass('plr_error_msg');
    let img = baseurl+'assets/backend/images/'+type+'.png';
    $('.plr_happy_img img').attr('src',img);
    if( type == 'success' )
        $('.plr_yeah h5').text('Congratulations!');
    else
        $('.plr_yeah h5').text('Oops!');
    $('.plr_yeah p').text(message);
    $('.plr_notification').addClass('plr_'+type+'_msg');
}


