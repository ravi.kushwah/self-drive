var datafile = new plupload.Uploader({
	runtimes : 'html5,flash,silverlight,html4',
	browse_button : 'uploadFile', // you can pass in id...
	container: document.getElementById('container'), // ... or DOM Element itself
	chunk_size: '1mb', 
	url : baseurl + 'upload/uploadtoserver',
	max_file_count: 1,
    max_retries :2,
	//ADD FILE FILTERS HERE
	filters : {
	    max_file_size : '2gb',
		mime_types: [
				{title : "files", extensions : "xml,jpg,mov,jpeg,pdf,zip,gif,mp4,mp3,3gp,sql,svg,png"},
			]
		
	}, 

	// Flash settings
	flash_swf_url : baseurl + 'public/js/plupload/Moxie.swf',

	// Silverlight settings
	silverlight_xap_url : baseurl + 'public/js/plupload/Moxie.xap',
	 

	init: {
		PostInit: function() {
			document.getElementById('filelist').innerHTML = '';	 
			document.getElementById('upload').onclick = function() {
			datafile.start();
				return false;
			};
		},

		FilesAdded: function(up, files) {
			plupload.each(files, function(file) {
				document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
			});
		},

		UploadProgress: function(up, file) {
			document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
			 document.querySelector(".progress").innerHTML = '<div class="progress-bar" style="width: '+file.percent+'%;">'+file.percent+'%</div>';
			 if(file.percent=="100"){
			 //   window.location.reload();
			 }
		},

		Error: function(up, err) {
			document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
		}
	}
});

datafile.init();