toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
////////////////////// Cloud Trnaser js  ////////////////////////
$(document).on('click', '.cloudTransferBackBTN', function(e) {
    e.preventDefault();
    $('.cloudTransferBackBTN').hide();
    $('.cloudConnation').removeClass('sc_modal_nofilebox');
    var con_id = $(this).attr('connectionid');
    var backPage = $(this).attr('backPage');
    var condition = $(this).attr('condition');
    var type = $(this).attr('transtype');
    var data_cloud_type = $(this).attr('data-cloud-type');
    if (backPage == "getConnection") {
        GetAllConnection(type,id,con_id,backPage,condition);
    } else if (backPage == "folderback") {
        var name = $(this).attr('data-folder-name');
        var type = $(this).attr('transtype');
        var folderId = $(this).attr('data-folder-Id');
        var droType = $(this).attr('drotype');
        var id = $(this).attr('connectionid');
        BackFolders(type,id,name,folderId,droType,condition,data_cloud_type);
    }
});
$('.cloudTransferBackBTN').hide();
$(document).on('click', '.cloudTransferGetData', function(e) {
    var type = $(this).attr('type');
    var data_cloud_type = $(this).attr('data-cloud-type');
    var connectionID = $(this).attr('connectionid');
    $('.cloudConnation').removeClass('sc_modal_nofilebox');
    GetAllFolderAndFiles(type,connectionID,data_cloud_type);
});
$(document).on('click', '.cloudFileTransfer', function() {
    var name = $(this).attr('data-folder-name');
    var type = $(this).attr('type');
     $('.cloudTransferBackBTN').show();
    var folderId = $(this).attr('data-folder-Id');
    var data_cloud_type = $(this).attr('data-cloud-type');
    var connectionID = $('.MoveUploadFile').attr('id');
    var droType = $(this).attr('drotype');
    var id = $(this).attr('id');
    var back_url = $(this).attr('back-url');
    console.log(data_cloud_type)
    BackFolders(type,id,name,folderId,droType,'',data_cloud_type,back_url);
});
$(document).on('click', '.cloudTransfer', function(e) {
    // alert('Note: You can use Dropbox and Google Drive for now, other drives will be added soon')
    var type = $(this).attr('type');
    var id = $(this).attr('userId');
    var data_cloud_type = $(this).attr('data-cloud-type');
    $('.cloudTransferBackBTN').hide();
    $('.cloudConnation').removeClass('sc_modal_nofilebox');
    GetAllConnection(type,id,'','','',data_cloud_type);
});
function GetAllConnection(type="",id="",con_id="",backPage="",condition="",data_cloud_type=""){
    $.ajax({
        url: baseurl + 'home/GetAllConnection',
        type: "POST",
        data: {
            'id': id
        },
        success: function(e) {
            var data = $.parseJSON(e);
            var i = 1;
            if (data != "") {
                $('.cloudConnation').empty();
                $('.cloudTransferSelectFile').hide();
            }
            jQuery.each(data['data'], function(index, item) {
                if (item['connection_type'] == 'GoogleDrive') {
                    var resp = $.parseJSON(item['user_info']);
                    if (resp['image'] != '') {
                        var img = baseurl + 'assets/backend/images/icon1.png';
                    } else {
                        var img = baseurl + 'assets/backend/images/icon1.png';
                    }
                    var name = resp['name'];
                    var email = resp['email'];
                    var usedSpace = resp['usedSpace'] / 1073741824;
                    var totalSpace = resp['totalSpace'] / 1073741824;
                    var connection = `<a href="javascript:;" userid="${item['id']}" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
                } else if (item['connection_type'] == 'DropBox') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/dropbox.png';
                    var name = resp['name']['display_name'];
                    var email = resp['email'];
                    var usedSpace = resp['usedSpace'] / 1073741824;
                    var totalSpace = resp['totalSpace'] / 1073741824;
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
                } else if (item['connection_type'] == 'Ddownload') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/ddownload.png';
                    var name = resp['email'].split("@");
                    var email = resp['email'];
                    var usedSpace = resp['storage_used'] / 1073741824;
                    var totalSpace = 10;
                } else if (item['connection_type'] == 'OneDrive') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/onedrive.png';
                    var name = resp['name'];
                    var email = resp['email'];
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
                } else if (item['connection_type'] == 'Box') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon8.png';
                    var name = resp['name'];
                    var email = resp['email'];
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
                } else if (item['connection_type'] == 's3') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon18.png';
                    var name = resp['name'];
                    var email = resp['email'];
                } else if (item['connection_type'] == 'Backblaze') {
                    
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon26.png';
                    var name = resp['name'];
                    var email = item['email'];
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);

                } else if (item['connection_type'] == 'GooglePhoto') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon6.png';
                    var name = resp['name'];
                    var email = resp['email'];
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    if ( type == 'from' ) {
                        $('.cloudConnation').append(connection);
                    }
                }else if(item['connection_type'] == 'Gofile'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon31.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
                }else if(item['connection_type'] == 'Anon'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon_1.png';
                    var name = resp['name'];
                    var email = item['email']; 
                }else if(item['connection_type'] == 'Ftp'){
                     var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon7.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
                }else if(item['connection_type'] == 'Pcloud'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon3.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    var connection = `<a href="javascript:;" userid="${item['id']}" connection="dropbox" data-cloud-type="${data_cloud_type}" connectionID="${item['id']}" type="${type}" MoveFileId="${fileId}" class="cloudTransferGetData">
                                        <div class="ad-info-card">
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="${img}" alt="image">
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5>${name}</h5>
                                                    <h6>${email}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>`;
                    $('.cloudConnation').append(connection);
               }
            });
        }
    });
}
function BackFolders(type="",id="",name="",folderId="",droType="",condition="",data_cloud_type="",back_url=""){
 $.ajax({
        url: baseurl + 'home/getSubFolderByFolderId',
        type: "POST",
        data: {
            'name': name,
            'folderId': folderId,
            'id': id,
            'type': type,
            'drType': droType,
            'condition': condition,
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            var checkFile = 1;
            if (resp['data']['data'] != "") {
                $('.cloudConnation').empty();
                $('.cloudTransferSelectFile').show();
                $('.moveFileForm').show();
                $('.MoveProgressBar').show();
                $('.cloudTransferBackBTN').attr('backPage', 'folderback');
                $('.cloudTransferBackBTN').attr('connectionid', id);
                $('.cloudTransferBackBTN').attr('condition', folderId);
                if(back_url != ''){
                    $('.cloudTransferBackBTN').attr('droType', back_url);
                }
                $('.cloudTransferBackBTN').attr('transType', type);
            }
            if(type=="from"){
                var smg = '<h4>No Files Are Available</h4>';
            }else{
                var smg = '<h4>No Folder Are Available</h4>';
            }
            if(resp['data']['data'] !=""){
                if(resp.connection == "s3"){
                    $('.UploadFile').hide();
                    if (type == "from") {
                        if(resp['data'][0]!=""){
                            jQuery.each(resp['data'][0], function(index, item) {
                                var folderId = item['Prefix'];
                                temp = item.Prefix.split("/");
                                if(index==0){
                                     var name = temp[0];
                                }else{
                                     var name = temp[0];
                                }
                                var icon = baseurl + 'assets/backend/images/folder.svg';
                                var getFdrClassf = 'cloudFileTransfer';
                                var downloadFIle = '';
                                var getFdrClass = '';
                                var folder = `<div class="sc_share_wrapper  ${getFdrClassf}"  type="${type}" option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                                    <div class="sc_dots_img">
                                                          <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                                    </div>
                                    				<div class="checkbox" hidden>
                                    				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                                    				    <label for="rememberme$${i+'f'}" class="option"></label>
                                    				</div>
                                    				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                    				    <img src="${icon}" alt="icon">
                                    				</div>
                                    				<div class="sc_share_detail">
                                    				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                                    				</div>
                                    				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                                        				<div class="checkBoxSelect">
                                                            <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Delete
                                                            </a>
                                        				</div>
                                    				</div>
                    			                </div>`;
                                $('.cloudConnation').append(folder);
                                 i++;
                            });
                            jQuery.each(resp['data'][1], function(index, item) {
                                var c = j++;
                                var temp ;
                                var viewId = item['ETag'];
                                var filename = item['Key'];
                                var fileExtension =filename.substring(filename.lastIndexOf('/') + 1);
                                var name = fileExtension;
                                var icon = baseurl + 'assets/backend/images/file.png';
                                var getFdrClass = '';
                                var downloadFIle = "javascript:;";
                                var hide = "";
                                var folderId = item['Key'];
                                if(name!='test'){
                                    var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  type="${type}" option="option${c}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                                <div class="sc_dots_img">
                                                      <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                                </div>
                                				<div class="checkbox" hidden>
                                				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                				    <label for="rememberme${c}" class="option"></label>
                                				</div>
                                				<div class="sc_share_img sc_share_file">
                                				    <img src="${icon}" alt="icon">
                                				</div>
                                				<div class="sc_share_detail">
                                				    <h5>${name}</h5>
                                				</div>
                                				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                                    				<div class="checkBoxSelect">
                                    				    <a  href="javascript:;" ${hide} class="DownloadFile"  folderIDShareURL="${folderId}" userID="${id}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                                <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                            </svg>
                                                            Download
                                                        </a>
                                                        <a href="javascript:;" ${hide} class="ShareURL"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                            </svg>
                                                            Share
                                                        </a>
                                                        <a  href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                            </svg>
                                                            Move
                                                        </a>
                                                        <a  href="javascript:;"  class="DeletFileAndFolder" ${hide} user_id="${resp.userId}" fileid="${folderId}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                            </svg>
                                                            Delete
                                                        </a>
                                                        <a href="javascript:;" ${hide} class="previewFile"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                                <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                            </svg>
                                                            Preview
                                                        </a>
                                    				</div>
                                				</div>
                    		                </div>`;
                                        $('.cloudConnation').append(folder);
                                }
                                 i++;
                                 k++;
                            });
                        }else{
                             $('.cloudConnation').empty();
                              $('.cloudConnation').addClass('sc_modal_nofilebox');
                              var folder = `<div class="no_dash_box ">
                                <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                   ${smg}
                                </div>`;
                                $('.cloudConnation').append(folder);
                        }
                    }else{
                        if(resp['data'][0]!=""){
                            jQuery.each(resp['data'][0], function(index, item) {
                            temp = item.Prefix.split("/");
                            if(index==0){
                                 var name = temp[1];
                            }else{
                                 var name = temp[1];
                            }
                            var icon = baseurl + 'assets/backend/images/folder.svg';
                            var getFdrClassf = 'cloudFileTransfer';
                            var downloadFIle = '';
                            var getFdrClass = '';
                            var folderId = item['Prefix'];
                            var folder = `<div class="sc_share_wrapper  ${getFdrClassf}"  type="${type}" option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                                <div class="sc_dots_img">
                                                      <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                                </div>
                                				<div class="checkbox" hidden>
                                				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                                				    <label for="rememberme$${i+'f'}" class="option"></label>
                                				</div>
                                				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                				    <img src="${icon}" alt="icon">
                                				</div>
                                				<div class="sc_share_detail">
                                				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                                				</div>
                                				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                                    				<div class="checkBoxSelect">
                                                        <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                            </svg>
                                                            Delete
                                                        </a>
                                    				</div>
                                				</div>
                			                </div>`;
                            $('.cloudConnation').append(folder);
                             i++;
                        });
                        }else{
                             $('.cloudConnation').empty();
                              $('.cloudConnation').addClass('sc_modal_nofilebox');
                             var folder = `<div class="no_dash_box ">
                                <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                   ${smg}
                                </div>`;
                             $('.cloudConnation').append(folder);
                        }
                    }
                }
                // else if(resp.connection == "Googlephoto"){
                //        $('.cloudConnation').empty();
                //         $('.cloudTransferSelectFile').show();
                //         $('.moveFileForm').show();
                //         $('.MoveProgressBar').show();
                //         $('.cloudTransferBackBTN').attr('backPage', 'folderback');
                //         $('.cloudTransferBackBTN').attr('connectionid', id);
                //         $('.cloudTransferBackBTN').attr('condition', folderId);
                //         $('.cloudTransferBackBTN').attr('droType', droType);
                //         $('.cloudTransferBackBTN').attr('transType', type);
                //         if(resp['data']!=""){
                //              var i = 1;
                //             jQuery.each(resp['data'], function(index, item) {
                //                 var c = i++;
                //                 if(resp.type !="Photos" && resp.type !="mediaItemSearch" || resp.type =='ShareAlbum'){
                //                         var fileExtension = item['title'].substring(item['title'].lastIndexOf('.') + 1);
                //                         var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json'];
                //                         if (Extension.lastIndexOf(fileExtension) == -1) {
                //                             var icon = baseurl + 'assets/backend/images/folder.svg';
                //                             var getFdrClass = 'cloudFileTransfer';
                //                             var downloadFIle = "javascript:;";
                //                             var hide = "hidden";
                //                         } else {
                //                             var icon = baseurl + 'assets/backend/images/file.png';
                //                             var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';
                //                             var getFdrClass = '';
                //                             var hide = "";
                //                         }
                //                         folderId = item['id'];
                //                         var folder = `<div class="sc_share_wrapper  ${getFdrClass}" type="${type}" option="option${c}" id="${id}" data-folder-name="${item['title']}"  data-folder-Id="${folderId}">
                //                                             <div hidden class="sc_dots_img">
                //                                                     <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                //                                             </div>
                //                                             <!-- <div class="checkbox" > -->
                //                                             <div class="checkbox selectFile" type="${type}" cehckBoxValue="cehckBoxValue${c}" cehckSelect="cehckBoxValue${c}" id="${resp['userId']}" data-folder-name="${ item['title']}" droType="${ item['path_lower']}" data-folder-Id="${ folderId }"  >
                //                                                 <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                //                                                 <label for="rememberme${c}" class="option"></label>
                //                                             </div>
                //                                             <div class="sc_share_img sc_share_file">
                //                                                 <img src="${icon}" alt="icon">
                //                                             </div>
                //                                             <div class="folderDetails"></div>
                //                                             <div class="sc_share_detail">
                //                                                 <h5>${item['title']}</h5>
                //                                             </div>
                //                                         </div>`;
                //                         $('.cloudConnation').append(folder);
                //                 }else{
                //                     $('.GooglePgotos').show();
                //                     var downloadFIle = item.baseUrl;
                //                     var getFdrClass = 'selectFile';
                //                     var hide = "";
                //                     var icon = baseurl + 'assets/backend/images/file.png';
                //                     var folder = `<div class="choose-product">
                //                             <label for="Newcheck${c}" class="rdo">
                //                                 <div class="sc_share_wrapper  ${getFdrClass}" type="${type}" cehckBoxValue="cehckBoxValue${c}" cehckSelect="cehckBoxValue${c}" id="${id}" data-folder-name="${item.filename}" droType="${item.id}" data-folder-Id="${item.id}">
                //                                     <div class="checkbox" hidden>
                //                                         <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox${c}" value="" >
                //                                             <label for="rememberme${c}" class="option"></label>
                //                                     </div> 
                //                                     <div class="sc_share_img sc_share_file">
                //                                         <img src="${icon}" alt="icon">
                //                                     </div>
                //                                     <div class="sc_share_detail">
                //                                         <h5>${item.filename}</h5>
                //                                     </div> 
                //                                 </div>
                //                                  <input  type="text" name="UserId" value="${id}"">
                //                                 <input id="Newcheck${c}" type="checkbox" class="cehckBoxValue${c}"  value="">
                //                                 <img src="${baseurl}assets/backend/images/check_icon.png" class="checkbox_img">
                //                                 <span class="checkmark"></span>
                //                             </label>	
                //                         </div>`;
                //                     $('.cloudConnation').append(folder);
                //                 }
                //             });
                //         }else{
                //         $('.cloudConnation').empty();
                //             var folder =  `<div class="no_dash_box ">
                //                         <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                //                             <h4>No Files Are Available</h4>
                //                             <p>You haven't uploaded any files yet</p>
                //                             <div class="add-group">
                //                                 <!--<a href="javascript:;" class="ad-btn">Upload File</a>-->
                //                             </div>
                //                         </div>`;
                //             $('.cloudConnation').addClass('sc_nofile_avai');
                //             $('.cloudConnation').append(folder);
                //         }
                // }
                else{
                    var i = 1;
                    jQuery.each(resp['data']['data'], function(index, item) {

                        if (resp['connection'] == 'GoogleDrive') {
                            var folderId = item['id'];
                        }
                        if (resp['connection'] == 'DropBox') {
                            $('.cloudTransferBackBTN').attr('dropbox-url', '/');
                        }
                        if (resp['connection'] == 'Ddownload') {
                            var folderId = item['fld_id'];
                        }
                        if (resp['connection'] == 'OneDrive') {
                            var folderId = item['id'];
                        }
                        if (resp['connection'] == 'Box') {
                            var folderId = item['id'];
                        }
                        if (resp['connection'] == 'Backblaze') {
                            var folderId = item['fileName'];
                        }
                        if (resp['connection'] == 'Gofile') {
                            var folderId = item['id'];
                        }
                        if (resp['connection'] == 'Googlephoto') {
                            var folderId = item['id'];
                            if ( item['mimeType'] == undefined ) {
                                item['name'] = item['title']
                            }else{
                                item['name'] = item['filename']
                            }
                        }
                        if (resp['connection'] == 'Ftp') {
                            var folderId = resp['data']['currentfolder']+'/'+item['name'];
                        }
                        if (resp['connection'] == 'Pcloud') {
                            if (item['icon'] == 'folder') {
                                var folderId = item['folderid'];
                            }else{
                                var folderId = item['fileid'];
                            }
                        }

                        var c = i++;
                        var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);
                        var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sql', 'json','ico','fig'];
                        if (type == "from") {
                            
                            if(data_cloud_type == "backup"){      // for backup
                                console.log(1)
                                $('.cloudTransferSelectFile').show();
                                $('.cloudTransferSelectFile').attr('data-cloud-type',data_cloud_type);
                                if (Extension.lastIndexOf(fileExtension) == -1) {
                                    var icon = baseurl + 'assets/backend/images/folder.svg';
                                    var getFdrClass = 'cloudFileTransfer';
                                    var folder = `<div class="sc_share_wrapper">
                                                    <div class="checkbox folderSelectTo" data-cloud-type="${data_cloud_type}" getClass="${getFdrClass}"  connectionTyep="${type}" id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${item.path_lower}" data-folder-Id="${ folderId }">
                                                        <input type="radio" id="rememberme${c}" name="selectFolderTransfer" value="">
                                                        <label for="rememberme${c}"></label>
                                                    </div>
                                                    	<div class="sc_share_img sc_share_file">
                                                    		<img src="${icon}" alt="icon" class="${getFdrClass}" data-cloud-type="${data_cloud_type}" option="option${c}" id="${resp['userId']}" type="${type}"  kuldeep drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">
                                                    	</div>
                                                    	<div class="folderDetails"></div>
                                                    	<div class="sc_share_detail">
                                                    		<h5 class="${getFdrClass}" data-cloud-type="${data_cloud_type}" option="option${c}" id="${resp['userId']}" type="${type}" drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">${item['name']}</h5>
                                                    	</div>
                                                </div>`;
                                    $('.cloudConnation').append(folder);
                                }
                            }else{                           // for transfer
                                console.log(2)
                                $('.cloudTransferSelectFile').attr('data-cloud-type','');
                                if (Extension.lastIndexOf(fileExtension) == -1) {                 // folder
                                    var icon = baseurl + 'assets/backend/images/folder.svg';
                                    var getFdrClass = 'cloudFileTransfer';
                                    var folder = `<div class="sc_share_wrapper  ${getFdrClass}" type="${type}"  id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item['path_lower']}" data-folder-Id="${ folderId }">
                                                    <div class="checkbox selectFile" type="${type}" cehckBoxValue="cehckBoxValue${c}" cehckSelect="cehckBoxValue${c}" id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item['path_lower']}" data-folder-Id="${ folderId }"  >
                                                        <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                                            <label for="rememberme${c}" class="option"></label>
                                                    </div> 
                                                    <div class="sc_share_img sc_share_file">
                                                        <img src="${icon}" alt="icon">
                                                    </div>
                                                    <div class="folderDetails"></div>
                                                    <div class="sc_share_detail">
                                                        <h5>${ item['name']}</h5>
                                                    </div> 
                                                </div>`;
                                } else {                                                          // file
                                    console.log(3)
                                    var icon = baseurl + 'assets/backend/images/file.png';
                                    var getFdrClass = 'selectFile';
                                    var folder = `<div class="choose-product">
                                                    <label for="Newcheck${c}" class="rdo">
                                                        <div class="sc_share_wrapper  ${getFdrClass}" type="${type}" cehckBoxValue="cehckBoxValue${c}" cehckSelect="cehckBoxValue${c}" id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item['path_lower']}" data-folder-Id="${ folderId }">
                                                            <div class="checkbox" hidden>
                                                                <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox${c}" value="" >
                                                                    <label for="rememberme${c}" class="option"></label>
                                                            </div> 
                                                            <div class="sc_share_img sc_share_file">
                                                                <img src="${icon}" alt="icon">
                                                            </div>
                                                            <div class="sc_share_detail">
                                                                <h5>${ item['name']}</h5>
                                                            </div> 
                                                        </div>
                                                         <input  type="text" name="UserId" value="${resp['userId']}">
                                                        <input id="Newcheck${c}" type="checkbox" class="cehckBoxValue${c}"  value="">
                                                        <img src="${baseurl}assets/backend/images/check_icon.png" class="checkbox_img">
                                                        <span class="checkmark"></span>
                                                    </label>	
                                                </div>`;
                                } 
                                $('.cloudConnation').append(folder);
                            }
                        } else {
                            
                            console.log(4)
                            if (Extension.lastIndexOf(fileExtension) == -1) {
                                var icon = baseurl + 'assets/backend/images/folder.svg';
                                var getFdrClass = 'cloudFileTransfer';
                                 var folder = `<div class="sc_share_wrapper   ${getFdrClass}" type="${type}"  id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item['path_lower']}" data-folder-Id="${ folderId }">
                                                         <div class="checkbox folderSelectTo" getClass="${getFdrClass}" connectionTyep="${type}" id="${resp['userId']}" data-folder-name="${ item['name']}" drotype="${item.path_lower}" data-folder-Id="${ folderId }">
                                                            <input type="radio" id="rememberme${c}" name="selectFolderTransfer" value="">
                                                            <label for="rememberme${c}"></label>
                                                        </div>
                                                        <div class="sc_share_img sc_share_file">
                                                            <img src="${icon}" alt="icon">
                                                        </div>
                                                        <div class="folderDetails"></div>
                                                        <div class="sc_share_detail">
                                                            <h5>${ item['name']}</h5>
                                                        </div> 
                                                    </div>`;
                                $('.cloudConnation').append(folder);
                            }
                        }
                    });
                }
            }else{
                $('.cloudConnation').empty();
                $('.cloudConnation').addClass('sc_modal_nofilebox');
                var folder = `<div class="no_dash_box ">
                                <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                   ${smg}
                                </div>`;
             $('.cloudConnation').append(folder);
            }
        }
    });
}
function GetAllFolderAndFiles(type="",connectionID="",data_cloud_type=""){
    // alert(data_cloud_type);
    $.ajax({
        url: baseurl + 'home/moveFileDataget',
        type: "POST",
        data: {
            'id': connectionID
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            var i = 1;
            $('.cloudConnation').empty();
            $('.cloudTransferBackBTN').show();
            $('.cloudTransferBackBTN').attr('backPage', 'getConnection');
            $('.cloudTransferBackBTN').attr('connectionid', connectionID);
            $('.cloudTransferBackBTN').attr('transtype', type);
            $('.cloudTransferBackBTN').attr('condition', '');
            if(resp.data.connectionType == "s3"){
                $('.UploadFile').hide();
                if (type == "from") {
                    jQuery.each(resp['data']['getAllFolder']['data'][1], function(index, item) {
                        var c = j++;
                        var temp ;
                        var viewId = item['ETag'];
                        var filename = item['Key'];
                        var fileExtension =filename.substring(filename.lastIndexOf('/') + 1);
                        var name = fileExtension;
                        var icon = baseurl + 'assets/backend/images/file.png';
                        var getFdrClass = '';
                        var downloadFIle = "javascript:;";
                        var hide = "";
                        var folderId = item['Key'];
                        if(name!='test'){
                            var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                        <div class="sc_dots_img">
                                              <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                        </div>
                        				<div class="checkbox" hidden>
                        				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                        				    <label for="rememberme${c}" class="option"></label>
                        				</div>
                        				<div class="sc_share_img sc_share_file">
                        				    <img src="${icon}" alt="icon">
                        				</div>
                        				<div class="sc_share_detail">
                        				    <h5>${name}</h5>
                        				</div>
                        				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                            				<div class="checkBoxSelect">
                            				    <a  href="javascript:;" ${hide} class="DownloadFile"  folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                    </svg>
                                                    Download
                                                </a>
                                                <a href="javascript:;" ${hide} class="ShareURL"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>
                                                    Share
                                                </a>
                                                <a  href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                    </svg>
                                                    Move
                                                </a>
                                                <a  href="javascript:;"  class="DeletFileAndFolder" ${hide} user_id="${resp.userId}" fileid="${folderId}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                                <a href="javascript:;" ${hide} class="previewFile"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                    </svg>
                                                    Preview
                                                </a>
                            				</div>
                        				</div>
            		                </div>`;
                                $('.cloudConnation').append(folder);
                        }
                         i++;
                         k++;
                    });
                    jQuery.each(resp['data']['getAllFolder']['data'][0], function(index, item) {
                        var folderId = item['Prefix'];
                        temp = item.Prefix.split("/");
                        if(index==0){
                             var name = temp[0];
                        }else{
                             var name = temp[0];
                        }
                        var icon = baseurl + 'assets/backend/images/folder.svg';
                        var getFdrClassf = 'cloudFileTransfer';
                        var downloadFIle = '';
                        var getFdrClass = '';
                        var folder = `<div class="sc_share_wrapper  ${getFdrClassf}" type="${type}" option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                            <div class="sc_dots_img">
                                                  <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                            </div>
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme$${i+'f'}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                                				<div class="checkBoxSelect">
                                                    <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                				</div>
                            				</div>
            			                </div>`;
                        $('.cloudConnation').append(folder);
                         i++;
                    });
                }else{
                    jQuery.each(resp['data']['getAllFolder']['data'][0], function(index, item) {
                        temp = item.Prefix.split("/");
                        if(index==0){
                             var name = temp[1];
                        }else{
                             var name = temp[1];
                        }
                        var icon = baseurl + 'assets/backend/images/folder.svg';
                        var getFdrClassf = 'getSubFolderByFolderId';
                        var downloadFIle = '';
                        var getFdrClass = '';
                        var folderId = item['Prefix'];
                        var folder = `<div class="sc_share_wrapper  ${getFdrClassf}"  option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                            <div class="sc_dots_img">
                                                  <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                            </div>
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme$${i+'f'}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                                				<div class="checkBoxSelect">
                                                    <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                				</div>
                            				</div>
            			                </div>`;
                        $('.cloudConnation').append(folder);
                         i++;
                    });
                }
            } else {
                jQuery.each(resp['data']['getAllFolder']['data'], function(index, item) {
                    var c = i++;
                    var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);
                    var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx', 'xlsx', 'php', 'docx', 'apk', 'csv', 'mkv', 'sq', 'json'];
                    // var Extension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'txt', 'mp3', 'mp4', 'zip', 'rar', 'pdf', 'html', 'css', 'js', 'exe', 'pptx'];
                    if (type == "from") {
                        if (resp.data.connectionType == 'Google Drive') {
                            var folderId = item['id'];
                            var folderIdDelete = item['id'];
                            var viewFileId = item['id'];
                        }
                        if (resp.data.connectionType == 'DropBox') {
                            $('.cloudTransferBackBTN').attr('dropbox-url', '/');
                            var folderIdDelete = item.path_display;
                            var viewFileId = item['path_lower'];
                        }
                        if (resp.data.connectionType == 'Ddownload') {
                            var folderId = item['fld_id'];
                        }
                        if (resp.data.connectionType == 'OneDrive') {
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'Box') {
                            // console.log(item);
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'Backblaze') {
                            // console.log(item);
                            var folderId = item['fileName'];
                        }
                        if (resp.data.connectionType == 'Gofile') {
                            // console.log(item);
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'GooglePhoto') {
                            // console.log(item);
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'Pcloud') {
                            // console.log(item);
                            var folderId = item['folderid'];
                        }
                        if (resp.data.connectionType == 'Ftp') {
                            var folderId = '/'+item['name'];
                        }
                        if(data_cloud_type == "backup"){
                            $('.cloudTransferSelectFile').show();
                            $('.cloudTransferBackBTN').attr('data-cloud-type',data_cloud_type);
                            $('.cloudTransferSelectFile').attr('data-cloud-type',data_cloud_type);
                            if (Extension.lastIndexOf(fileExtension) == -1) {
                                var icon = baseurl + 'assets/backend/images/folder.svg';
                                var getFdrClass = 'cloudFileTransfer';
                                var folder = `<div class="sc_share_wrapper">
                                                <div class="checkbox folderSelectTo" getClass="${getFdrClass}" data-cloud-type="${data_cloud_type}" back-url="${(resp.data.connectionType == 'DropBox')? '/':''}" connectionTyep="${type}" id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${item.path_lower}" data-folder-Id="${ folderId }">
                                                    <input type="radio" id="rememberme${c}" name="selectFolderTransfer" value="">
                                                    <label for="rememberme${c}"></label>
                                                </div>
                                                	<div class="sc_share_img sc_share_file">
                                                		<img src="${icon}" alt="icon" data-cloud-type="${data_cloud_type}" back-url="${(resp.data.connectionType == 'DropBox')? '/':''}" class="${getFdrClass}" option="option${c}" id="${resp['userId']}" type="${type}"  kuldeep drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">
                                                	</div>
                                                	<div class="folderDetails"></div>
                                                	<div class="sc_share_detail">
                                                		<h5 class="${getFdrClass}" data-cloud-type="${data_cloud_type}" back-url="${(resp.data.connectionType == 'DropBox')? '/':''}" data-cloud-type="${data_cloud_type}" option="option${c}" id="${resp['userId']}" type="${type}" drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">${item['name']}</h5>
                                                	</div>
                                            </div>`;
                                $('.cloudConnation').append(folder);
                                if(resp['data']['getAllFolder']['data']==""){
                                    $('.cloudConnation').empty();
                                    $('.cloudConnation').addClass('sc_modal_nofilebox');
                                    var folder = `<div class="no_dash_box ">
                                                   <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                                      <h4>No Folder Are Available</h4>
                                                   </div>`;
                                    $('.cloudConnation').append(folder);
                                }
                            }
                        }else{
                            $('.cloudTransferSelectFile').show();
                             $('.cloudTransferSelectFile').attr('data-cloud-type','');
                            if (Extension.lastIndexOf(fileExtension) == -1) {
                                var icon = baseurl + 'assets/backend/images/folder.svg';
                                var getFdrClass = 'cloudFileTransfer';
                            } else {
                                var icon = baseurl + 'assets/backend/images/file.png';
                                var getFdrClass = '';
                            }
                            var folder = `<div class="sc_share_wrapper ${getFdrClass}" option="option${c}" id="${resp['userId']}" type="${type}" drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">
                                            	<div class="checkbox selectFile" type="${type}" cehckBoxValue="cehckBoxValue${c}" cehckSelect="cehckBoxValue${c}" id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item.path_display }" data-folder-Id="${ folderId }" >
                                            		<input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                            		<label for="rememberme${c}" class="option"></label>    
                                            	</div>    
                                            	<div class="sc_share_img sc_share_file">
                                            		<img src="${icon}" alt="icon">
                                            	</div>
                                            	<div class="sc_share_detail">
                                            		<h5>${item['name']}</h5>
                                            	</div>
                                            </div>`;
                            $('.cloudConnation').append(folder);
                        }
                    } else {
                        console.log(5)
                        $('.cloudTransferSelectFile').show();
                        if (resp.data.connectionType == 'Google Drive') {
                            var folderId = item['id'];
                            var folderIdDelete = item['id'];
                            var viewFileId = item['id'];
                        }
                        if (resp.data.connectionType == 'DropBox') {
                            var folderIdDelete = item['path_display'];
                            var viewFileId = item['path_lower'];
                        }
                        if (resp.data.connectionType == 'Ddownload') {
                            var folderId = item['fld_id'];
                        }
                        if (resp.data.connectionType == 'OneDrive') {
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'Box') {
                            // console.log(item);
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'Backblaze') {
                            // console.log(item);
                            var folderId = item['fileName'];
                        }
                        if (resp.data.connectionType == 'Gofile') {
                            // console.log(item);
                            var folderId = item['id'];
                        }
                        if (resp.data.connectionType == 'Pcloud') {
                            // console.log(item);
                            var folderId = item['folderid'];
                        }
                        if (resp.data.connectionType == 'Ftp') {
                            var folderId = '/'+item['name'];
                        }
                        if (Extension.lastIndexOf(fileExtension) == -1) {
                            var icon = baseurl + 'assets/backend/images/folder.svg';
                            var getFdrClass = 'cloudFileTransfer';
                            var folder = `<div class="sc_share_wrapper">
                                                        <div class="checkbox folderSelectTo" data-cloud-type="${data_cloud_type}" getClass="${getFdrClass}"  connectionTyep="${type}" id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${item.path_lower}" data-folder-Id="${ folderId }">
                                                            <input type="radio" id="rememberme${c}" name="selectFolderTransfer" value="">
                                                            <label for="rememberme${c}"></label>
                                                        </div>
                                                        	<div class="sc_share_img sc_share_file">
                                                        		<img src="${icon}" alt="icon" class="${getFdrClass}" data-cloud-type="${data_cloud_type}" option="option${c}" id="${resp['userId']}" type="${type}"  kuldeep drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">
                                                        	</div>
                                                        	<div class="folderDetails"></div>
                                                        	<div class="sc_share_detail">
                                                        		<h5 class="${getFdrClass}" data-cloud-type="${data_cloud_type}" option="option${c}" id="${resp['userId']}" type="${type}" drotype="${item.path_display}" data-folder-name="${item['name']}"  data-folder-Id="${folderId}">${item['name']}</h5>
                                                        	</div>
                                                        </div>`;
                            $('.cloudConnation').append(folder);
                        }
                    }
                 });
            }
        }
    });
}
$(document).on('click', '.selectFile', function() {
    var check = $(this).attr('cehckBoxValue');
    var cehckSelect = $(this).attr('cehckSelect');
    if (check == "select") {
        var check = $(this).attr('cehckBoxValue', cehckSelect);
        // $('.addFiled').remove();
        $(this).find('.addFiled').remove();
    } else {
        $(this).find('.addFiled').remove();
        var id = $(this).attr('id');
        var data_folder_name = $(this).attr('data-folder-name');
        var droType = $(this).attr('droType');
        var type = $(this).attr('type');
        var ConnectionID = $(this).attr('id');
        var data_folder_Id = $(this).attr('data-folder-Id');
        var fields = `<div hidden class="addFiled"><input type="text" name="FileId[]" value="${data_folder_Id}">
                    <input  type="text"  name="FileName[]" value="${data_folder_name}">
                    <input  type="text"  name="droType[]" value="${droType}">
                    <input  type="text"  name="UserId" value="${ConnectionID}">
                    <input  type="text"  name="type" value="${type}"></div>`;
        $(this).append(fields);
        var check = $(this).attr('cehckBoxValue', 'select');
    }
});
$(document).on('click', '.cloudTransferSelectFile', function() {
    var data_cloud_type = $(this).attr('data-cloud-type');
    var formdata = new FormData($(this).closest('form')[0]);
    formdata.append('data_cloud_type',data_cloud_type);
    $.ajax({
        method: "POST",
        url: baseurl + 'home/cloudtransferDataTaskAdd',
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp.connection_type == "GoogleDrive") {
                var img = baseurl + 'assets/backend/images/icon1.png';
            } else {
                var img = baseurl + 'assets/backend/images/dropbox.png';
            }
            if (resp.connection_type == 'GoogleDrive') {
                var img = baseurl + 'assets/backend/images/icon1.png';
            } else if (resp.connection_type == 'DropBox') {
                var img = baseurl + 'assets/backend/images/dropbox.png';
            } else if (resp.connection_type == 'Ddownload') {
                var img = baseurl + 'assets/backend/images/ddownload.png';
            } else if (resp.connection_type == 'OneDrive') {
                var img = baseurl + 'assets/backend/images/onedrive.png';
            } else if (resp.connection_type == 'Box') {
                var img = baseurl + 'assets/backend/images/icon8.png';
            } else if (resp.connection_type == 's3') {
                var img = baseurl + 'assets/backend/images/icon18.png';
            } else if (resp.connection_type == 'Backblaze') {
                var img = baseurl + 'assets/backend/images/icon26.png';
            } else if (resp.connection_type == 'GooglePhoto') {
                var img = baseurl + 'assets/backend/images/icon6.png';
            }else if(resp.connection_type == 'Gofile'){
                var img = baseurl + 'assets/backend/images/icon31.png';
            }else if(resp.connection_type == 'Anon'){
                var img = baseurl + 'assets/backend/images/icon_1.png';
            }else if(resp.connection_type == 'Ftp'){
                var img = baseurl + 'assets/backend/images/icon7.png';
            }else if(resp.connection_type == 'Pcloud'){
                var img = baseurl + 'assets/backend/images/icon3.png';
            }
            if(resp.data_cloud_type=="backup"){$('.BackUpClouddata').attr('data-cloud-type',data_cloud_type);}else{$('.BackUpClouddata').attr('data-cloud-type','')}
            if (resp.type == "from") {
                $('.setFileNameFrom').html("");
                if(resp.data_cloud_type=="backup"){
                    $('.setFileNameFrom').empty();
                    $('.fileNameFrom').empty();
                    var ConnectionID = `<input type="text"  name="CloudtransferConFromId" hidden  value="${resp.ConnectionID}">`;
                    var fileName = `<input type="text"  name="foldernamefrom" hidden  value="${resp.fileName}">`;
                    var fileID = `<input type="text"  name="TransferFolderFileIDfrom" hidden  value="${resp.fileID}">`;
                    $('.fileConnectionIMGfrom').attr('src', img)
                    $('.fileNameFrom').text(resp.fileName);
                    $('.setFileNameFrom').append(ConnectionID);
                    $('.setFileNameFrom').append(fileName);
                    $('.setFileNameFrom').append(fileID);
                }else{
                    var fileName = $.parseJSON(resp.fileName);
                    var fileID = $.parseJSON(resp.fileID);
                    jQuery.each(fileName, function(index, item) {
                        $('.setFileNameFrom').append('<p>' + item + '</p>');
                        $('.setFileNameFrom').append(`<input type="text"  name="FileName[]" hidden  value="${item}">`);
                    });
                    jQuery.each(fileID, function(index, item) {
                        $('.setFileNameFrom').append(`<input type="text"  name="FileId[]" hidden  value="${item}">`);
                    });
                    var ConnectionID = `<input type="text"  name="CloudtransferConFromId" hidden  value="${resp.ConnectionID}">`;
                    $('.setFileNameFrom').append(ConnectionID);
                    $('.fileConnectionIMGfrom').attr('src', img)
                }
            } else {
                if(resp.data_cloud_type=="backup"){
                    $('.setFileNameto').empty();
                    $('.fileNameto').empty();
                    var ConnectionID = `<input type="text"  name="CloudtransferConToId" hidden  value="${resp.ConnectionID}">`;
                    var fileName = `<input type="text"  name="foldernameto" hidden  value="${resp.fileName}">`;
                    var fileID = `<input type="text"  name="TransferFolderFileIDto" hidden  value="${resp.fileID}">`;
                    $('.fileConnectionIMGto').attr('src', img)
                    $('.fileNameto').text(resp.fileName);
                    $('.setFileNameto').append(ConnectionID);
                    $('.setFileNameto').append(fileName);
                    $('.setFileNameto').append(fileID);
                }else{
                    $('.setFileNameto').empty();
                    $('.fileNameTo').empty();
                    $('.fileConnectionIMGto').attr('src', img)
                    var ConnectionID = `<input type="text"  name="CloudtransferConToId" hidden  value="${resp.ConnectionID}">`;
                    var fileName = `<input type="text"  name="foldername" hidden  value="${resp.fileName}">`;
                    var fileID = `<input type="text"  name="TransferFolderFileID" hidden  value="${resp.fileID}">`;
                    $('.fileNameTo').text(resp.fileName);
                    $('.setFileNameto').append(ConnectionID);
                    $('.setFileNameto').append(fileName);
                    $('.setFileNameto').append(fileID);
                }
            }
            setTimeout(function() {
                $('#cloudtransferfrom').modal('hide');
            }, 500);
        },
        error: function(resp) {
           showNotifications('error',ltr_something_msg);
            return false;
        }
    });
});
$(document).on('click', '.folderSelectTo', function() {
    $('.folderDetails').empty();
    $(this).parents(".sc_share_wrapper").children(".folderDetails").empty();
    var FolderID = $(this).attr('data-folder-id');
    var connectionTyep = $(this).attr('connectionTyep');
    var drotype = $(this).attr('drotype');
    var folder_name = $(this).attr('data-folder-name');
    var id = $(this).attr('id');
    var folerDetilas = `<input type="text"  name="FileId" hidden  value="${FolderID}">
            <input  type="text"  name="FileName" hidden  value="${folder_name}">
            <input  type="text"  name="connectionTyep" hidden  value="${connectionTyep}">
            <input  type="text" name="droType"  hidden value="${drotype}" >`;
    $(this).parents(".sc_share_wrapper").children(".folderDetails").append(folerDetilas);
    var ConnectionID = `<input type="text"  name="CloudtransferConToId" hidden  value="${id}">`;
    $(this).parents(".sc_share_wrapper").children(".folderDetails").append(ConnectionID);
});
// $(document).on('click', '.TransferNowCLoudData,.BackUpClouddata', function() {
//     var formdata = new FormData($(this).closest('form')[0]);
//     var data_cloud_type =  $('.BackUpClouddata').attr('data-cloud-type');
//     let url = $(this).attr('data-url');
//     formdata.append('data_cloud_type',data_cloud_type);
//     $.ajax({
//         method: "POST",
//         url: baseurl + url,
//         data: formdata,
//         processData: false,
//         contentType: false,
//         success: function(resp) {
//             var resp = $.parseJSON(resp);
//             if (resp['status'] == '1') {
//                  $('.TransferNowCLoudData').attr('disabled', true);
//                  $('.BackUpClouddata').attr('disabled', true);
//                  $('.TransferNowCLoudData').text('Transfer Now Proccessing....');
//                  $('.BackUpClouddata').text('Transfer Now Proccessing....');
//                 showNotifications('success',resp.smg);
//                 setTimeout(function() {
//                     window.location.href = baseurl + 'home/taskList';
//                 }, 1000);
//             } else {
//                showNotifications('error',resp.smg);
//                setTimeout(function(){$('.plr_notification ').hide();},2000);  
//                 return false;
//             }
//         },
//         error: function(resp) {
//            showNotifications('error',ltr_something_msg);
//            setTimeout(function(){$('.plr_notification ').hide();},2000);  
//             return false;
//         }
//     });
// });
let click = 0;
$(document).on('click', '.TransferNowCLoudData,.BackUpClouddata', function() {
    
    var formdata = new FormData($(this).closest('form')[0]);
    var data_cloud_type =  $('.BackUpClouddata').attr('data-cloud-type');
    let url = $(this).attr('data-url');
    formdata.append('data_cloud_type',data_cloud_type);
    if ( $('[name="CloudtransferConFromId"]').val() == undefined || $('[name="CloudtransferConToId"]').val() == undefined ) {
        $('.plr_notification ').show(); showNotifications('error','Please Select Your Folders !');
        setTimeout(function(){$('.plr_notification ').hide();},2000);  
    }else{
        click += 1;
        if (click == 1) {
            $.ajax({
                method: "POST",
                url: baseurl + url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function(resp) {
                    var resp = $.parseJSON(resp);
                    if (resp['status'] == '1') {
                         $('.TransferNowCLoudData').attr('disabled', true);
                         $('.BackUpClouddata').attr('disabled', true);
                         $('.TransferNowCLoudData').text('Transfer Now Proccessing....');
                         $('.BackUpClouddata').text('Transfer Now Proccessing....');
                         $('.plr_notification ').show(); showNotifications('success',resp.smg);
                        setTimeout(function() {
                            window.location.href = baseurl + 'home/taskList';
                        }, 1000);
                    } else {
                       showNotifications('error',resp.smg);
                       setTimeout(function(){$('.plr_notification ').hide();},2000);  
                        return false;
                    }
                },
                error: function(resp) {
                   showNotifications('error',ltr_something_msg);
                   setTimeout(function(){$('.plr_notification ').hide();},2000);  
                    return false;
                }
            });
        }
    }
    });
$(document).on('click', '.DeleteCloudtransferData', function() {
    var id = $(this).attr('id');
    var user_id = $(this).attr('user_id');
    $('#confirmPopupBtn').addClass('confirmPopupBtn');
    $('#confirmPopupBtn').attr('id', id);
    $('.delete_popup_txt').html('You want to remove this task?');
    $('#confirmPopupBtn').attr('user_id', user_id);
    $('#img#confirmPopupBtn').removeAttr('id');
    $('#confirmPopup').modal('show');
});
$(document).on('click', '.CanceledTransfer', function() {
    var id = $(this).attr('id');
    $.ajax({
        method: "POST",
        url: baseurl + 'home/CanceledTransfer',
        data: {
            'id': id
        },
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                showNotifications('success','Cancel Transfer process');
                setTimeout(function() {
                    window.location.href = baseurl + 'home/taskList';
                }, 1000);
            } else {
               showNotifications('error',resp.smg);
               setTimeout(function(){$('.plr_notification ').hide();},2000);  
                return false;
            }
        },
        error: function(resp) {
           showNotifications('error',ltr_something_msg);
           setTimeout(function(){$('.plr_notification ').hide();},2000);  
            return false;
        }
    });
});
$(document).on('click', '.retransfer', function() {
    var id = $(this).attr('id');
    $.ajax({
        method: "POST",
        url: baseurl + 'home/retransfer',
        data: {
            'id': id
        },
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                showNotifications('success','Start Re-Transfer process');
                setTimeout(function() {
                    window.location.href = baseurl + 'home/taskList';
                }, 1000);
            } else {
               showNotifications('error',resp.smg);
               setTimeout(function(){$('.plr_notification ').hide();},2000);  
                return false;
            }
        },
        error: function(resp) {
           showNotifications('error',ltr_something_msg);
           setTimeout(function(){$('.plr_notification ').hide();},2000);  
            return false;
        }
    });
});
$(document).on('click', '.confirmPopupBtn', function() {
    var id = $(this).attr('id');
    $.ajax({
        method: "POST",
        url: baseurl + 'home/DeleteTask',
        data: {
            'id': id
        },
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                showNotifications('success',resp.smg);
                setTimeout(function() {
                    window.location.href = baseurl + 'home/taskList';
                }, 1000);
            } else {
               showNotifications('error',resp.smg);
               setTimeout(function(){$('.plr_notification ').hide();},2000);  
                return false;
            }
        },
        error: function(resp) {
           showNotifications('error',ltr_something_msg);
           setTimeout(function(){$('.plr_notification ').hide();},2000);  
            return false;
        }
    });
});
function showNotifications(type, message){
    console.log(3612)
    $('.plr_notification').removeClass('plr_success_msg');
    $('.plr_notification').removeClass('plr_error_msg');
    let img = baseurl+'assets/backend/images/'+type+'.png';
    $('.plr_happy_img img').attr('src',img);
    if( type == 'success' )
        $('.plr_yeah h5').text('Congratulations!');
    else
        $('.plr_yeah h5').text('Oops!');
    $('.plr_yeah p').text(message);
    $('.plr_notification').addClass('plr_'+type+'_msg');
}
$(document).on('click', '.optionCheckBox,.checkbox', function(e){
    e.stopPropagation()
})