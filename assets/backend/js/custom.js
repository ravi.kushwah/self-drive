/*--------------------- Copyright (c) 2020 -----------------------
[Master Javascript]
-------------------------------------------------------------------*/
(function($) {
    "use strict";

    /*-----------------------------------------------------
    	Function  Start
    -----------------------------------------------------*/
    var admin = {
        initialised: false,
        version: 1.0,
        mobile: false,
        collapseScreenSize: 991,
        sideBarSize: 1199,
        init: function() {
            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }
            /*-----------------------------------------------------
            	Function Calling
            -----------------------------------------------------*/
            this.userToggle();
            this.sideBarToggle();
            this.sideMenu();
            this.sideBarHover();
            this.searchToggle();
            this.rightSlide();
            this.tooltipHover();
            this.nubberSpin();
            this.loader();
            this.checkSiteName();
            this.copyToClipBoard();
            this.deleteWebsite();
            this.leadSettings();
            this.categorySettings();
            this.customerSettings();
            this.agencySettings();
            this.resellerUserSettings();
            this.customProductsSettings();
            this.shareButton();
            // this.ReTra_toggle();
        },

        /*-----------------------------------------------------
            Fix Header User Button
        -----------------------------------------------------*/
        
//         ReTra_toggle: function() {
				
// 			$(".sc_toggle_retra").on('click', function(e) {
// 				e.stopPropagation();
// 				$("body").toggleClass("re_open_menu");
// 			});
// 		},
		
        // loader			
        loader: function() {
            jQuery(window).on('load', function() {
                $(".loader").fadeOut();
                $(".spinner").delay(500).fadeOut("slow");
            });
        },
        // loader	
        userToggle: function() {
            var count = 0;
            $('.user-info').on("click", function() {
                if ($(window).width() <= admin.collapseScreenSize) {
                    if (count == '0') {
                        $('.user-info-box').addClass('show');
                        count++;
                    } else {
                        $('.user-info-box').removeClass('show');
                        count--;
                    }
                }
            });

            $(".user-info-box, .user-info").on('click', function(e) {
                if ($(window).width() <= admin.collapseScreenSize) {
                    event.stopPropagation();
                }
            });

            $('body').on("click", function() {
                if ($(window).width() <= admin.collapseScreenSize) {
                    if (count == '1') {
                        $('.user-info-box').removeClass('show');
                        count--;
                    }
                }
            });
        },
        sideBarToggle: function() {
            $(".toggle-btn").on('click', function(e) {
                e.stopPropagation();
                $("body").toggleClass('mini-sidebar');
                $(this).toggleClass('checked');

            });
            $('.sidebar-wrapper').on('click', function(event) {
                event.stopPropagation();
            });
        },
        sideMenu: function() {
            $('.side-menu-wrap ul li').has('.sub-menu').addClass('has-sub-menu');
            $.sidebarMenu = function(menu) {
                var animationSpeed = 300,
                    subMenuSelector = '.sub-menu';
                $(menu).on('click', 'li a', function(e) {
                    var $this = $(this);
                    var checkElement = $this.next();
                    if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
                        checkElement.slideUp(animationSpeed, function() {
                            checkElement.removeClass('menu-show');
                        });
                        checkElement.parent("li").removeClass("active");
                    } else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
                        var parent = $this.parents('ul').first();
                        var ul = parent.find('ul:visible').slideUp(animationSpeed);
                        ul.removeClass('menu-show');
                        var parent_li = $this.parent("li");
                        checkElement.slideDown(animationSpeed, function() {
                            checkElement.addClass('menu-show');
                            parent.find('li.active').removeClass('active');
                            parent_li.addClass('active');
                        });
                    }
                    if (checkElement.is(subMenuSelector)) {
                        e.preventDefault();
                    }
                });
            }
            $.sidebarMenu($('.main-menu'));
            $(function() {
                for (var a = window.location, counting = $(".main-menu a").filter(function() {
                        return this.href == a;
                    }).addClass("active").parent().addClass("active");;) {
                    if (!counting.is("li")) break;
                    counting = counting.parent().addClass("in").parent().addClass("active");
                }
            });
        },
        sideBarHover: function() {
            if ($(window).width() >= admin.sideBarSize) {
                $(".main-menu").hover(function() {
                    $('body').addClass('sidebar-hover');
                }, function() {
                    $('body').removeClass('sidebar-hover');
                });
            }
        },
        searchToggle: function() {
            $('.search-toggle').on("click", function() {
                $('.serch-wrapper').addClass('show-search');
            });
            $('.search-close, .main-content').on("click", function() {
                $('.serch-wrapper').removeClass('show-search');
            });
        },
        rightSlide: function() {
            $(".setting-info").on('click', function(e) {
                e.stopPropagation();
                $("body").toggleClass('open-setting');
            });
            $('body, .close-btn').on('click', function() {
                $('body').removeClass('open-setting');
            });
            $('.slide-setting-box').on('click', function(event) {
                event.stopPropagation();
            });

        },
        tooltipHover: function() {
            if ($('.toltiped').length > 0) {
                $(".toltiped").tooltip();
            }
            if ($('.toltiped-right').length > 0) {
                $(".toltiped-right").tooltip({
                    'placement': 'right',
                });
            }
        },
        nubberSpin: function() {
            if ($('.number-spin').length > 0) {
                $('.number-increase').on('click', function() {
                    if ($(this).prev().val() < 50000) {
                        $(this).prev().val(+$(this).prev().val() + 1);
                    }
                });
                $('.number-decrease').on('click', function() {
                    if ($(this).next().val() > 1) {
                        if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
                    }
                });
            }
        },
        checkSiteName: function() {
            $(document).on('click', '.check_site_url', function() {
                let siteURL = $.trim($('input[name="w_siteurl"]').val());
                let textObj = $('.plr_url_text');
                if (siteURL.length > 3) {
                    var regexp = new RegExp(/^[a-z0-9 ]*$/);
                    if (regexp.test(siteURL)) {
                        textObj.html('<span class="mb-0 badge">Checking</span>');
                        $.ajax({
                            url: baseurl + 'home/checksiteurl',
                            type: "POST",
                            data: {
                                'w_siteurl': siteURL,
                                'w_id': $('input[name="w_id"]').val()
                            },
                            success: function(e) {
                                if (e == 0) {
                                    textObj.html('<span class="mb-0 badge badge-danger">Not Available</span>');
                                    showNotifications('error', 'Please, enter different site url. Someone is already using this url.');
                                    setTimeout(function(){$('.plr_notification ').hide();},2000);  
                                } else{
                                    textObj.html('<span class="mb-0 badge badge-success">Available </span>');
                                    
                                }
                            }
                        });
                    } else{
                        showNotifications('error', 'Site URL can have letters (a-z) and numbers (0-9) only.');
                        setTimeout(function(){$('.plr_notification ').hide();},2000);  
                    }
                } else{
                    showNotifications('error', 'Site URL should be atleast 3 characters.');
                    setTimeout(function(){$('.plr_notification ').hide();},2000);  
                }
            });
        },
        copyToClipBoard: function() {
            $(document).on('click', '.copy_button', function() {
                navigator.clipboard.writeText($(this).data('tobecopied'));
                showNotifications('success', 'Content copied successfully to clipboard.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);  
            });
        },
        deleteWebsite: function() {
            $(document).on('click', '.deleteWebsite', function() {
                $('#deleteWebsiteBtn').data('webid', $(this).data('webid'))
                $('#deleteWebsite').modal('show');
            });

            $(document).on('click', '#deleteWebsiteBtn', function() {
                let webid = $(this).data('webid');
                $.ajax({
                    url: baseurl + 'home/deleteWebsite',
                    type: "POST",
                    data: {
                        'webid': webid
                    },
                    success: function(e) {
                        if (e == 1){
                            showNotifications('success', 'Website deleted successfully.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                        else{
                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }
                });
            });
        },
        shareButton: function() {
            $(document).on('click', '.shareButton', function() {
                $('.socialul').attr('id', $(this).data('webid'))
                $('#socialicon').modal('show');
            });
        },
        leadSettings: function() {
            $(document).on('change', '.arCls', function() {
                getARList($(this));
            });
            var parts = $(location).attr('href').split("/"), // use like uri segment
                last_part = parts[parts.length - 2];
            var queryparams = last_part.split('?')[0];
            // console.log(queryparams)
            if (queryparams == 'lead_settings') {

                getARList($('#w_signuplist'));
                getARList($('#w_buyplanlist'));
                getARList($('#w_newsletterlist'));
            }

        },
        categorySettings: function() {
            $(document).on('click', '.deleteCategory', function() {
                $('#deleteCategoryBtn').data('cateid', $(this).data('cateid'));
                $('#deleteCategory').modal('show');
            });

            $(document).on('click', '#deleteCategoryBtn', function() {
                let cateid = $(this).data('cateid');
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_categories/' + webid,
                    type: "POST",
                    data: {
                        'delete_cateid': cateid
                    },
                    success: function(e) {
                        if (e == 1){
                            showNotifications('success', 'Category deleted successfully.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                        else{
                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }
                });
            });

            $(document).on('change', '.cateCls', function() {
                let sts = $(this).is(":checked") ? 1 : 0;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_categories/' + webid,
                    type: "POST",
                    data: {
                        'webid': webid,
                        'sts': sts,
                        'cateid': $(this).attr("id")
                    },
                    success: function(e) {
                        if (e == 1){
                            showNotifications('success', 'Category status updated successfully.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }else{
                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                    }
                });
            });
            $(document).on('change', '.customcateCls', function() {
                let sts = $(this).is(":checked") ? 1 : 0;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_categories/' + webid,
                    type: "POST",
                    data: {
                        'webid': webid,
                        'sts': sts,
                        'customcateid': $(this).attr("id")
                    },
                    success: function(e) {
                        if (e == 1){
                            showNotifications('success', 'Category status updated successfully.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                        else{
                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                            setTimeout(function(){ location.reload(); }, 3000);
                        }
                    }
                });
            });

            $('#moveFile').on('hidden.bs.modal', function () {
                $('.moveFileForm').hide()
                $('.progress-bar-striped').css('width', '0%')
                $('.progress-bar-striped').text('0%')
              })

            $('#UploadFile').on('hidden.bs.modal', function () {
                $('.blobImage').attr('src', '');
                $('.progress-bar').css('width', '0%');
                $('.progress-bar').text('0%');
                $('#UploadFile_Name').val('')
            });

            $(document).on('click', '.openModal', function() {
                $('#add_cloud').modal('show');
            });

            $(document).on('click', '.dropBox', function() {
                $('#dropBox').modal('show');
            });
            $(document).on('click', '.GoogleDrive', function() {
                $('#GoogleDrive').modal('show');
            });
            $(document).on('click', '.CreateFolder', function() {
                $('#CreateFolder').modal('show');
            });
            $(document).on('click', '.moveFile', function() {
                $('#moveFile').modal('show');
            });
            $(document).on('click', '.cloudTransfer', function() {
                var type = $(this).attr('type');
                $('#cloudtransferfrom').modal('show');
               
            });
            $(document).on('click', '.Renamefiles', function() {
                $('#Renamefiles').modal('show');
            });
            
            $(document).on('click', '.ShareURLAnon', function() {
                var url = $(this).attr('url');
                $('#copyTargetanon').val(url);
                $('#ShareURLAnon').modal('show');
            });
            $(document).on('click', '.OneDrive', function() {
                $('#OneDrive').modal('show');
            });

            $(document).on('click', '.UploadFile', function() {
                var folderId = $('.UploadFile').attr('folder_id');
                $('.Upload').attr('folder_id', folderId);
                $('#UploadFile').modal('show');
                $('.progress ').hide();
                $('#progress ').attr('width','0');
                $('#progress ').html('');
            });
            $(document).on('click', '.amazonS3UploadFile', function() {
                var folderId = $(this).attr('folder_id');
                $('.s3UploadFile').attr('folder_id', folderId);
                $('#amazons3fileupload').modal('show');
                 $('.progress ').hide();
                $('#progress ').attr('width','0');
                $('#progress ').html('');
            });
            $(document).on('click', '.anonfilesCon', function() {
                $('#anonfiles').modal('show');
            });
            $(document).on('click', '.googlePhoto', function() {
                $('#googlePhoto').modal('show');
            });
            
            $(document).on('click', '.previewFile', function() {
                $('#fileView').modal('show');
            });
            $(document).on('click', '.Box', function() {
                $('#Box').modal('show');
            });
            $(document).on('click', '.Ddownload', function() {
                $('#Ddownload').modal('show');
            });
            $(document).on('click', '.CreateFolderonedrive', function() {
                $('#OneDriveCreateFolder').modal('show');
            });
            $(document).on('click', '.amazonS3', function() {
                $('#amazonS3').modal('show');
            });
            $(document).on('click', '.agencyuserClsEdit', function() {
                $('#agencyuserClsEdit').modal('show');
            });
            $(document).on('click', '.backBlaze', function() {
                $('#backBlaze').modal('show');
            });
            $(document).on('click', '.gofile', function() {
                $('#gofile').modal('show');
            });

            // for Pcloud start
            $(document).on('click', '[name="pcloud_generatecode"]', function() {
                $('#pcloud_codelink').hide()
                let appKey = $('[name="pcloud_appkey"]').val()
                if (appKey.length !== 0) {
                    $.ajax({
                        type: "POST",
                        url: baseurl + 'home/pcloudGenerateCode/' + appKey,
                        success: function (resp) {
                            resp = JSON.parse(resp);
                            if (resp.status == 1) {
                                $('#pcloud_codelink').text('Click To Open Auth Link').attr('href', resp.link).show()
                                 
                            }else{
                                $('.plr_notification ').show();
                                showNotifications('error', 'App Key is not valid');
                                setTimeout(function(){$('.plr_notification ').hide();},2000); 
                            }
                        }
                    });
                }else{
                    $('.plr_notification ').show();
                    showNotifications('error', 'Please Enter AppKey');
                    setTimeout(function(){$('.plr_notification ').hide();},2000); 
                }
                
            });
            // for Pcloud end

            $(document).on('click', '#submitCateData', function() {
                let cate_id = $.trim($('#cate_id').val());
                let cate_name = $.trim($('#cate_name').val());
                let cate_slug = $.trim($('#cate_slug').val());
                let webid = $('#webid').val();
                if (cate_name != '' && cate_slug != '') {
                    $.ajax({
                        url: baseurl + 'home/web_categories/' + webid,
                        type: "POST",
                        data: {
                            'cate_id': cate_id,
                            'cate_name': cate_name,
                            'cate_slug': cate_slug
                        },
                        success: function(e) {
                            if (e == 1){
                                showNotifications('success', 'Category updated successfully.');
                                setTimeout(function(){ location.reload(); }, 3000);
                                setTimeout(function(){ location.reload(); }, 3000);
                            }
                            else if (e == 2){
                                showNotifications('success', 'Category added successfully.');
                                setTimeout(function(){ location.reload(); }, 3000);
                            }
                            else{
                                showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                                setTimeout(function(){ location.reload(); }, 3000);
                                
                            }

                            setTimeout(function() {
                                location.reload();
                            }, 3000);
                        }
                    });
                } else
                    showNotifications('error', 'Please, fill in the details to continue.');
            });
        },
        customerSettings: function() {
            $(document).on('change', '.custCls', function() {
                let sts = $(this).is(":checked") ? 1 : 0;
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/web_customers/' + webid,
                    type: "POST",
                    data: {
                        'sts': sts,
                        'custid': $(this).attr("id")
                    },
                    success: function(e) {
                        if (e == 1)
                            showNotifications('success', 'Customer status updated successfully.');
                        else
                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                    }
                });
            });


            $(document).on('click', '#submitCustomerData', function() {
                let custId = $.trim($('#c_id').val());
                let c_name = $.trim($('#c_name').val());
                let c_email = $.trim($('#c_email').val());
                let c_password = $.trim($('#c_password').val());
                let err = 0;
                if (c_name != '' && c_email != '') {
                    if (custId == 0) {
                        if (c_password == '') {
                            showNotifications('error', 'Please, enter passsword.');
                            err++;
                        }
                    }
                    if (err == 0) {
                        $.ajax({
                            url: baseurl + 'home/web_customers/' + $('#webid').val(),
                            type: "POST",
                            data: {
                                'custid': custId,
                                'c_name': c_name,
                                'c_email': c_email,
                                'c_password': c_password
                            },
                            success: function(e) {
                                if (e == 1)
                                    showNotifications('success','success', 'Customer Data Updated Successfully.');
                                else if (e == 2)
                                     showNotifications('success','success', 'Customer Added Successfully.');
                                else if (e == 4)
                                     showNotifications('error','One of your customer already have this email.');
                                else
                                     showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');

                                setTimeout(function() {
                                    location.reload();
                                }, 3000);
                            }
                        });
                    }
                } else {
                    showNotifications('error', 'Please, fill in the details to continue.');
                }
            });
        },
        agencySettings:function(){
            $(document).on('change','.agencyuserCls',function(){
                let sts = $(this).is(":checked") ? 1 : 0 ;
                $.ajax({
                    url: baseurl + 'home/agency_users/', 
                    type: "POST",             
                    data: {'sts':sts,'userid':$(this).attr("id")},
                    success: function(e) {
                        if(e == 1)
                            showNotifications('success','Agency User Status Updated Successfully.');
                        else
                             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                    }
                });
            });

            $(document).on('click','.agencyuserClsEdit',function(){
                let userId = $(this).attr("id");
                if( userId == 0 ){
                    $('.agencyuserAddCls').val('');
                    $('#id').val(0);
                    $('#add_agencyuser_title').text('Add New Agency User');
                    $('#add_agencyuser').modal('show');
                }
                else{
                    $.ajax({
                        url: baseurl + 'home/agency_users/', 
                        type: "POST",             
                        data: {'userid':userId},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            $('#userName').val(resp.name)         
                            $('#UserEmail').val(resp.email)   
                            $('#id').val(userId);
                            if(resp.is_oto1==1){
                                $(".oto1").prop("checked", true);
                            }else{
                                $(".oto1").prop("checked", false);
                            }
                            if(resp.is_oto2==1){
                                $(".oto2").prop("checked", true);
                            }else{
                                $(".oto2").prop("checked", false);
                            }
                            $('#add_agencyuser_title').text('Edit Agency User');
                            $('#add_agencyuser').modal('show');      
                        }
                    });
                }
            });
                       
            
            $(document).on('click','#submitAgencyUserData',function(){
                let userId = $.trim($('#id').val());
                let u_name = $.trim($('#userName').val());
                let u_email = $.trim($('#UserEmail').val());
                let u_password = $.trim($('#password').val());
                let is_fe =  $('#is_fe').is(":checked");
                let is_oto1 = $('#is_oto1').is(":checked");
                let is_oto2 = $('#is_oto2').is(":checked");
                let err = 0;
                if( u_name != '' && u_email != '' ){
                    if( userId == 0 ){
                        if( u_password == '' ){
                            showNotifications('error','Please, enter passsword.');
                            setTimeout(function(){ location.reload(); }, 3000);
                            err++;
                        }
                    }
                    if( err == 0 )
                    {
                        $.ajax({
                            url: baseurl + 'home/agency_users/', 
                            type: "POST",             
                            data: {'userid':userId,'name':u_name,'email':u_email,'password':u_password,'is_fe':is_fe,'is_oto1':is_oto1,'is_oto2':is_oto2},
                            success: function(e) {
                                if(e == 1){
                                     showNotifications('success','Agency User Data Updated Successfully.');
                                     setTimeout(function(){ location.reload(); }, 3000);
                                }else if(e == 2){
                                     showNotifications('success','Agency User Added Successfully.');
                                     setTimeout(function(){ location.reload(); }, 3000);
                                }else if(e == 4 ){
                                      showNotifications('error','One of the User already have this email.');
                                    setTimeout(function(){ location.reload(); }, 3000);
                                }else if(e == 5 ){
                                      showNotifications('error','The limit for adding users has been reached.');
                                    setTimeout(function(){ location.reload(); }, 3000);
                                }else{
                                    showNotifications('Something went wrong. Please, refresh the page and try again.');
                                    setTimeout(function(){ location.reload(); }, 3000);
                                }
                               
                            }
                        });
                    }
                }
                else{
                    showNotifications('error','Please, fill in the details to continue.');
                    setTimeout(function(){$('.plr_notification ').hide();},2000);  
                }
            });
        },
        resellerUserSettings:function(){
            $(document).on('change','.changeStts',function(e){
                var _this = $(e.target);
                var status = (_this.is(':checked') ? 1 : 0);
                $.ajax({
                    url: baseurl + _this.attr('data-url'), 
                    type: "POST",             
                    data: {'sts':status},
                    success: function(resp) {
                        var resp = $.parseJSON(resp);
                        setTimeout(function(){ location.reload(); }, 3000);
                        console.log(resp);
                        if (resp['status'] == '1'){
                            showNotifications('success',resp.msg);
                            // setTimeout(function() {
                            //   window.location.href=baseurl+"home/dashboard";
                            // }, 1000);
                        }
                        else
                             showNotifications('error',resp.msg);
                             setTimeout(function(){ location.reload(); }, 3000);
                            return false;
                    }
                });
            });

            $(document).on('click','.resellerUserAdd,.reselleruserClsEdit',function(){
                console.log($(this))
                var userId = $(this).attr('id');
                // $('#resellerUserAdd').modal('show');
                if( userId != 0 ){
                    $('#add_reseller_title').text('Update Reseller User');
                    $('#resellerUserAdd').modal('show');
                    $.ajax({
                        url: baseurl + 'common/reseller_users/'+ userId, 
                        type: "POST",             
                        data: {},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            // var resp = $.parseJSON(resp);
                            // console.log(resp);
                            $('#name').val(resp.name)         
                            $('#email').val(resp.email)
                            $('#email').attr('readonly', '');
                            $('#password').removeClass('require');
                            $('#cnf_password').removeClass('require');
                            $('#user_id').val(userId);
                            $('#sendMail').closest('.form-group').addClass('d-none');
                            $('#add_reseller_title').text('Edit Reseller User');
                            $('#resellerUserAdd').modal('show');     
                        }
                    });
                }else{
                    $('#resellerUserAdd').modal('show');
                }
            });
            
            $(document).on('click','.whitelabelUserAdd,.whitelabelUserEdit',function(){
                var userId = $(this).attr('id');
                console.log(userId)
                // $('#resellerUserAdd').modal('show');
                if( userId != 0 ){
                    $('#add_reseller_title').text('Update White Label User');
                    $('#whitelabelUserAdd').modal('show');
                    $.ajax({
                        url: baseurl + 'common/white_label_setting/'+ userId, 
                        type: "POST",             
                        data: {},
                        success: function(e) {
                            let resp = JSON.parse(e);
                            // var resp = $.parseJSON(resp);
                            // console.log(resp);
                            $('#name').val(resp.name)         
                            $('#email').val(resp.email)
                            $('#email').attr('readonly', '');
                            $('#password').removeClass('require');
                            $('#cnf_password').removeClass('require');
                            $('#user_id').val(userId);
                            $('#sendMail').closest('.form-group').addClass('d-none');
                            $('#add_whitelabel_title').text('Edit White Label User');
                            $('#whitelabelUserAdd').modal('show');     
                        }
                    });
                }else{
                    $('#whitelabelUserAdd').modal('show');
                }
            });
            
            $(document).on('click', '#submitResellerUserData ,#submitWhiteLabelUserData ', function() {
                $('.loader').fadeIn();
                var userId = $('#user_id').val();
                // console.log(userId)
                let formValidate = validate($(this).closest('form'));
                if (!formValidate) {
                    var formdata = new FormData($(this).closest('form')[0]); 
                } else {
                    $('.loader').fadeOut();
                    return false;
                }
                // console.log(formdata)
                

                $.ajax({
                    method: "POST",
                    url: baseurl + 'common/save_users/' +userId,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(resp) {
                        $('.loader').fadeOut();
                        var resp = $.parseJSON(resp);
                        console.log(resp);
                        if (resp['status'] == '1') {
                            showNotifications('success',resp.msg);
                            setTimeout(function(){$('.plr_notification ').hide();},2000);    
                            setTimeout(function() {
                              window.location.href=baseurl+"common/"+resp.url;
                            }, 1000);
                        } else {
                             showNotifications('error',resp.msg);
                             setTimeout(function(){$('.plr_notification ').hide();},2000);    
                            return false;
                        }
                    },
                    error: function(resp) {
                         showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                         setTimeout(function(){$('.plr_notification ').hide();},2000);    
                        return false;
                    }
                });
                
            });
            
            $(document).on('click', '#submitWhiteLabelData', function() {
                $('.loader').fadeIn();
                var userId = $('#user_id').val();
                // console.log(userId)
                let formValidate = validate($(this).closest('form'));
                if (!formValidate) {
                    var formdata = new FormData($(this).closest('form')[0]); 
                } else {
                    $('.loader').fadeOut();
                    return false;
                }
                // console.log(formdata);
                // return false;
                

                $.ajax({
                    method: "POST",
                    url: baseurl + 'common/save_wl_setting/',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(resp) {
                        $('.loader').fadeOut();
                        var resp = $.parseJSON(resp);
                        console.log(resp);
                        if (resp.status == '1') {
                            showNotifications('success',resp.msg);
                        } else {
                             showNotifications('error',resp.msg);
                             setTimeout(function(){$('.plr_notification ').hide();},2000);    
                            return false;
                        }
                    },
                    error: function(resp) {
                         showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                         setTimeout(function(){$('.plr_notification ').hide();},2000);    
                        return false;
                    }
                });
                
            });
            
        },
        customProductsSettings: function() {
            $(document).on('click', '.custProdEdit', function() {
                let p_id = $(this).attr("id");
                let webid = $('#webid').val();
                if (p_id == 0) {
                    $('.productsAddCls').val('');
                    $('#p_id').val(0);
                    $('#add_products_title').text('Add Custom GiveAways');
                    $('#add_custom_products').modal('show');
                } else {
                    $.ajax({
                        url: baseurl + 'home/custom_products/' + webid,
                        type: "POST",
                        data: {
                            'p_id': p_id
                        },
                        success: function(e) {
                            let resp = JSON.parse(e);
                            $('#g_offer_name').val(resp.g_offer_name)
                            $('#slug').val(resp.slug)
                            $('#g_payout').val(resp.g_payout)
                            $('#g_priview').val(resp.g_priview)
                            $('#g_categories').val(resp.g_categories)
                            $('#g_countries').val(resp.g_countries)
                            $('#g_offer_url').val(resp.g_offer_url);
                            $('#g_sign_up_url').val(resp.g_sign_up_url);
                            $('#p_id').attr("p_id", resp.g_id);
                            $('#webid').val(resp.g_webid);
                            $('#add_products_title').text('Edit Custom Give Aways');
                            $('#add_custom_products').modal('show');
                        }
                    });
                }
            });


            $(document).on('click', '#deleteCustomProductBtn', function() {
                let custprodid = $(this).data('custprodid');
                let webid = $('#webid').val();
                $.ajax({
                    url: baseurl + 'home/custom_products/' + webid,
                    type: "POST",
                    data: {
                        'delete_prodid': custprodid
                    },
                    success: function(e) {
                        if (e == 1)
                            showNotifications('success', 'Product deleted successfully.');
                        else
                            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');

                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }
                });
            });
        }
    };

    admin.init();

})(jQuery);


toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
$('body').click(function(){
    // $('.sc_delete_popupbox').hide();
})
$(document).ready(function() {
    //// White Label Color
    // $("#WhiteColor1").spectrum({
    //     color: "#0180ff"
    // });
    // $("#WhiteColor2").spectrum({
    //     color: "#0180ff"
    // });
    $('.progress ').hide();
    $(document).on('change', '.checkProductImages', function(){
        var _this = $(this);
        console.log(_this)
        var fileType = _this.closest('div.ao_mediaProduct_dv').find('[name="type"]').val();
        for (var i = 0; i <  _this.get(0).files.length; ++i) {
            var file1= _this.get(0).files[i].name;
            var ext = file1.split('.').pop().toLowerCase();                            
            // if($.inArray(ext,['gif','jpg','png','jpeg','GIF','JJPG','PNG','JPEG' ,'JPG'])===-1){
            //     window.appowls.notifyMessage("error", 'Please upload images only.');
            //     _this.val('');
            //     return;                    
            // }
        }
        
    });
    
    $(document).on('click', 'body', function(e) {
        $('.tbf_IconHolter').hide();
        $('#removeConnection').hide();
    });
       // datatable
      if($('.server_datatable').length){
        let dataSrc = $('.server_datatable').attr('data-source');
        var adDatatable = $('.server_datatable').DataTable({
            responsive: true,
            ajax: dataSrc
        });
        //datatable search
        $('.server_datatable').keyup(function(){
            adDatatable.search($(this).val()).draw() ;
        })
      }
});

// $(document).on('click', '.sc_dots_img', function(e) {

//     var optionCheck = $(this).parent().attr('option');
//     console.log(optionCheck)
//     $('.'+optionCheck).show();

// });

$(document).on('click', '.sc_dots_img:not(body)', function(e) {
    // var optionCheck = $(this).attr('option');
    var optionCheck = $(this).parent().attr('option');
    $('.tbf_IconHolter').hide()
    $(this).parent().toggleClass('sc_popup_box_show');
console.log($(this).siblings('.checkOptionMenu').css('display','block'));
    // if ($('.' + optionCheck).css('display') === 'none') {
    //     $('.' + optionCheck).show();
    //     console.log('show', optionCheck)
    // }else{
    //     $('.' + optionCheck).hide();
    //     console.log('hide', optionCheck)
    // }
    if (e.button != 2) {
        document.oncontextmenu = function() {
            $('.sc_delete_popupbox').removeClass('show_delete_popup')
            console.log('a')
            return false;
        };
        
        return false;
    } else {
        document.oncontextmenu = function() {
            console.log('b')
            return true;
        };
    }
    return true;
});
$(document).on('mousedown', '#amazonS3Option', function(e) {
    var optionCheck = $(this).attr('option');
    if (e.button == 2) {
        document.oncontextmenu = function() {
            return false;
        };
         $('.' + optionCheck).show();
        return false;
    } else {
        document.oncontextmenu = function() {
            return true;
        };
    }
    return true;
});

$(document).on('click', '.removeConnection', function(e) {
    var connectionID = $(this).attr('connectionID');
    if (e.button != 2) {
        document.oncontextmenu = function() {
            return false;
        };
            $('#confirmPopupBtn').attr('connectionID',connectionID);
            $('#confirmPopup').modal('show');
         
        // $('#removeConnection').show();
        return false;
    } else {
        document.oncontextmenu = function() {
            return true;
        };
    }
    return true;
});
$(document).on('click', '#confirmPopupBtn', function() {
    var connectionID = $(this).attr('connectionID');
        url: baseurl + 'home/RemoveConnection',
     $.ajax({
        url: baseurl + 'home/RemoveConnection',
        type: "POST",
        data: {
            'id': connectionID,
        },
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                showNotifications('success','Successfully Remove Account');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }else {
                 showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }
    });
});

$(document).on('click', '.saveProfile', function() {
    var formdata = new FormData($(this).closest('form')[0]);
    $.ajax({
        method: "POST",
        url: baseurl + 'home/adminProfile',
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                showNotifications('success','Profile Updated Successfully.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
                setTimeout(function() {
                    location.reload();
                }, 3000);
            } else if (resp['status'] == '2') {
                showNotifications('success','Profile Updated Successfully.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            } else {
                 showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
            $('.edu_preloader').fadeOut();
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }
    });

});

function saveProfile() {
    let u_name = $.trim($('#name').val());
    if (u_name != '') {
        $.ajax({
            url: baseurl + 'home/profile',
            type: "POST",
            data: {
                'name': u_name,
                'pwd': $('#pwd').val(),
                'email': $('#email').val()
            },
            success: function(e) {
                if (e == 1){
                    showNotifications('success','Profile Updated Successfully.');
                    setTimeout(function(){ location.reload(); }, 3000);
                // showNotifications('success','Profile updated successfully.');
                }else{
                    showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
                    setTimeout(function(){ location.reload(); }, 3000);
                }

                setTimeout(function() {
                    location.reload();
                }, 3000);
            }
        });
    } else{
        showNotifications('error', 'Please enter the name.')
        setTimeout(function(){ location.reload(); }, 3000);
    }
}


// upload file js   
$(document).on('click', '.plr_uploade_label', function(e) {
    $(this).parent().find('.plr_fileupload').click();
    var image_uploads = $(this).parent().find('.priViewImg');

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                image_uploads.attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".plr_fileupload").change(function() {
        readURL(this);
    });
});
$(document).on('click', '.folderBack', function(e) {
     window.location.reload();
    return false;
    e.preventDefault();
    getSubFolderByFolderId( $(this).attr('tofolderid'), '', '', '', $(this).attr('id') )
})

$(document).on('click', '.UserCreateFolder', function(e) {
    var folder_name = $('#folder_name').val();
    var user_id = $('#user_id').val();
    $.ajax({
        url: baseurl + 'home/userCreateFolder',
        type: "POST",
        data: {
            'user_id': user_id,
            'folderName': folder_name
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            //   console.log(resp)
            //   return false;
            if (resp.data.status == '1' || resp.status == '1') {
                showNotifications('success','Folder Create Successfully.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
                setTimeout(function() {
                    window.location.reload();
                }, 1000);
            } else {
                let msg = 'Something went wrong. Please, refresh the page and try again.'
                if (resp.data.message != undefined) { msg = resp.data.message }
                $('.plr_notification ').show(); showNotifications('error', msg);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });

});

$(document).on('click', '.userSubCreateFolder,#userSubCreateFolder', function(e) {
    e.preventDefault();
    console.log(1204)
    var folder_name = $('#folder_name').val();
    var parentId = $(this).attr('folder_id');
    var drotype = $(this).attr('drotype');
    var user_id = $('#user_id').val();
   
    $.ajax({
        url: baseurl + 'home/userSubCreateFolder',
        type: "POST",
        data: {
            'user_id': user_id,
            'folderName': folder_name,
            'parentId': parentId,
            'drotype': drotype
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            if (resp.data.data.status == 0 && resp.data.data.message != undefined) {
                msg = resp.data.data.message
                let type = resp.data.data.status == 1 ? 'success' : 'error';
                $('.plr_notification ').show(); showNotifications(type, msg);
                setTimeout(function(){$('.plr_notification ').hide();},2000);
            }else{
                $('.plr_notification ').show();  showNotifications('success','Successfully Create Folder');
                setTimeout(function(){$('.plr_notification ').hide();},2000);
            }
             $('#CreateFolder').hide();
             
            // setTimeout(function(){
            //      window.location.reload();
            // },1000);
            // return false;
            
            // if (resp['data']['data']['path_display'] == "") {
            //     folderAccess = parentId;
            // } else if(resp['data']['userId']!=""){
            //     folderAccess = parentId;
            // }else{
            //     folderAccess = drotype;
                
            // }
            if(parentId!=""){
                 folderAccess = parentId;
            }else{
                folderAccess = drotype;
            }
            $.ajax({
                url: baseurl + 'home/getSubFolderByFolderId',
                type: "POST",
                data: {
                    'folderId': folderAccess,
                    'id': user_id
                },
                success: function(e) {
                    var resp = $.parseJSON(e);
                    if (resp != "") {
                        $('.sc_share_main_section').empty();
                        $('.folderBack').show();
                        $('.UserCreateFolder').removeClass('UserCreateFolder');
                        $('#folder_name').val('New Folder');
                        $('#CreateFolder').modal('hide');
                    }
                    var i = 1;
                    var j = 0;
                    var k = 2;
                  
                     if(resp.connection == "s3"){
                        $('.UploadFile').hide();
                        // $('.CreateFolder').show();
                        jQuery.each(resp['data'][1], function(index, item) {
                            var c = j++;
                            var temp ;
                            var viewId = item['ETag'];
                            var filename = item['Key'];
                            temp = filename.split("/");
                            var name = temp[1];
                            var icon = baseurl + 'assets/backend/images/file.png';
                            var getFdrClass = '';
                            var downloadFIle = "javascript:;";
                            var hide = "";
                            var folderId = item['Key'];
                        
                            if(name!='test'){
                            
                                var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                            <div class="sc_dots_img">
                                                  <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                            </div>
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme${c}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5>${name}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                                				<div class="checkBoxSelect">
                                				    <a  href="javascript:;" ${hide} class="DownloadFile"  folderIDShareURL="${folderId}" userID="${id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                        </svg>
                                                        Download
                                                    </a>
                                                    <a href="javascript:;" ${hide} class="ShareURL"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Share
                                                    </a>
                                                    
                                                    <a  href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                        </svg>
                                                        Move
                                                    </a>
                                                    
                                                    <a  href="javascript:;"  class="DeletFileAndFolder" ${hide} user_id="${resp.userId}" fileid="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                                    <a href="javascript:;" ${hide} class="previewFile"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                        </svg>
                                                        Preview
                                                    </a>
                                				</div>
                            				</div>
            			                </div>`;
                                    $('.sc_share_main_section').append(folder);
                            }
                             
                        });
                        jQuery.each(resp['data'][0], function(index, item) {
                            var c = i++;
                            var d = j++;
                           
                            temp = item.Prefix.split("/");
                            if(index==0){
                                 var name = temp[c];
                            }else{
                                 var name = temp[c];
                            }
                           
                            var icon = baseurl + 'assets/backend/images/folder.svg';
                            var getFdrClassf = 'getSubFolderByFolderId';
                            var downloadFIle = '';
                            var getFdrClass = '';
                            var folderId = item['Key'];
                            var folder = `<div class="sc_share_wrapper  ${getFdrClassf}"  option="option${c+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${item}">
                                                <div class="sc_dots_img">
                                                      <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                                </div>
                                				<div class="checkbox" hidden>
                                				    <input type="checkbox" id="rememberme${c+'f'}" name=""class="optionCheckBox" value="" >
                                				    <label for="rememberme$${c+'f'}" class="option"></label>
                                				</div>
                                				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${item}">
                                				    <img src="${icon}" alt="icon">
                                				</div>
                                				<div class="sc_share_detail">
                                				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${item}">${name}</h5>
                                				</div>
                                				<div class="checkOptionMenu tbf_IconHolter  option${c+'f'}" clickValue="option${c+'f'}" style="display:none;">
                                    				<div class="checkBoxSelect">
                                                        <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                            </svg>
                                                            Delete
                                                        </a>
                                                      
                                    				</div>
                                				</div>
            				                </div>`;
                            $('.sc_share_main_section').append(folder);
                             
                        });
                         
                     }else if(resp.connection == "Googlephoto"){
                        jQuery.each(resp['data']['data'], function(index, item) {
                        var c = i++;
                        var fileExtension = item['title'].substring(item['title'].lastIndexOf('.') + 1);
                        Extension = FileExtension();
                        if (Extension.lastIndexOf(fileExtension) == -1) {
                            var icon = baseurl + 'assets/backend/images/folder.svg';
                            var getFdrClass = 'getSubFolderByFolderId';
                            var downloadFIle = "javascript:;";
                            var hide = "hidden";
                        } else {
                            var icon = baseurl + 'assets/backend/images/file.png';
                            var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';
                            var getFdrClass = '';
                            var hide = "";
                        }
                        
                        folderId = item['id'];
                        var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['title']}"  data-folder-Id="${folderId}">
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme${c}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5>${item['title']}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                                				<div class="checkBoxSelect">
                                                    <a href="javascript:;" ${hide} class="ShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${user_id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Share
                                                    </a>
                                                    
                                                    <a  href="javascript:;" ${hide} class="moveFile " filename="${item['title']}" userId="${user_id}" fileId="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                        </svg>
                                                        Move
                                                    </a>
                                                    
                                                    <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp['userId']}" folderID="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                                    
                                                    <a  href="javascript:;" ${hide} class="previewFile"  folderIDShareURL="${folderId}" userID="${user_id}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                        </svg>
                                                        Preview
                                                    </a>
                                                  
                                				</div>
                            				</div>
        				                </div>`;
                        $('.sc_share_main_section').append(folder);
                    });
                }else{
                    var i = 1;
                    var connType = resp['connection']; var myArray = [];
                    $('.sc_share_main_section').removeClass('sc_nofile_avai')
                    jQuery.each(resp['data']['data'], function(index, item) {
                        myArray = [ { "delete"  : " " }, { "download": " " }, { "copy_to" : " " }, { "share"   : " " }, { "cut"     : " " }, { "rename"  : " " }, { "copy"    : " " }, { "move"    : " " },{ "preview"    : " " } ];

                        var c = i++;
                        var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);
                       
                         Extension = FileExtension();
                        if (Extension.lastIndexOf(fileExtension) == -1) {
                            var icon = baseurl + 'assets/backend/images/folder.svg';
                        } else {
                            var icon = baseurl + 'assets/backend/images/file.png';
                        }

                        if (connType == 'Backblaze') {
                            if (item['action'] == 'folder') {
                                var folderId = item['fileName'];
                                var urlByFile = baseurl + `home/downloadB2File/${folderId}/${id}/${item['name']}`;
                                myArray = [ { "delete"  : `fileid = "${item['fileName']}"` }, { "download": `hidden` }, { "copy_to" : "hidden" }, { "share"   : `hidden` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : "hidden" }, { "preview"    : "hidden" } ];
                                
                            }else{
                                var folderId = item['fileId'];
                                var urlByFile = baseurl + `home/downloadB2File/${folderId}/${id}/${item['name']}`;
                                myArray = [ { "delete"  : `fileid = "${item['fileName']}"` }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                            }
                            item['id'] = folderId;
        
                            var previewFile = 'OnePreviewFile';
                          //  checkBackBtnCond()
                        }
                        if (connType == 'Gofile') {
                            
                            var folderId = item['id'];
                            var previewFile = 'OnePreviewFile';
                            var urlByFile = item['directLink'];
                            if (item['type'] == 'file') {
                                myArray = [ { "delete"  : ' ' }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                            }else{
                                myArray = [ { "delete"  : ' ' }, { "download": `hidden` }, { "copy_to" : "hidden" }, { "share"   : `hidden ` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : "hidden" }, { "preview"    : "hidden" } ];
                            }
                          //  checkBackBtnCond()
                        }
                        if (connType == 'Pcloud') {
                            
                            if (item['isfolder'] == 1) {
                                var folderId  = item['folderid'];
                                myArray       = [ { "delete"  : `folderID = "${folderId}"` }, { "download": `hidden` }, { "copy_to" : "hidden" }, { "share"   : `hidden ` }, { "cut"     : "hidden" }, { "rename"  : ` fileCode = ${folderId} ` }, { "copy"    : "hidden" }, { "move"    : "hidden" }, { "preview"    : "hidden" } ];
                                
                            }else{
                                var folderId  = item['fileid'];
                                var urlByFile = baseurl + 'home/downloadPcloudFile/'+ id + '/' + folderId;
                                myArray       = [ { "delete"  : `fileid = "${folderId}" folderID = ""` }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : `fileCode = "" fileID = ${ folderId } ` }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                            }
                            item['id'] = folderId;
                            // checkBackBtnCond()
                        }
                        if (connType == 'Ftp') {
                
                            var folderId = resp['data']['currentfolder']+'/'+item['name'];
                            var previewFile = 'OnePreviewFile';
                            item['id'] = folderId;
                            var urlByFile = baseurl + `home/downloadFtpFile/?id=${user_id}&path=${ encodeURIComponent(folderId) }`;
                            if (item['type'] == 'file') {
                                myArray = [ { "delete"  : `fileid = "${folderId}" folderID = ""` }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                            }else{
                                myArray = [ { "delete"  : `folderID = "${folderId}" ` }, { "download": `hidden` }, { "copy_to" : "hidden" }, { "share"   : `hidden` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : "hidden" }, { "preview"    : "hidden" } ];
                            }
                           
                            if ( item['type'] != 'file') {
                                icon        = baseurl + 'assets/backend/images/folder.png';
                                getFdrClass = 'getSubFolderByFolderId';
                                hide        = "hidden";
                            } else {
                                icon        = baseurl + 'assets/backend/images/file.png';
                                getFdrClass = '';
                                hide        = "";
                            }
                          //  checkBackBtnCond()
                        }

                        // var folder = '<div class="sc_share_wrapper getSubFolderByFolderId " option="option' + c + '" id="' + resp['userId'] + '" data-folder-name="' + item['name'] + '" data-folder-Id="' + item['id'] + '"><div class="checkbox" hidden><input type="checkbox" id="rememberme' + c + '" name=""class="optionCheckBox" value="" >        <label for="rememberme' + c + '" class="option"></label>    </div>    <div class="sc_share_img"><img src="' + icon + '" alt="icon"></div><div class="sc_share_detail"><h5>' + item['name'] + '</h5></div><div class="checkOptionMenu tbf_IconHolter option' + c + '" clickValue="option' + c + '" style="display:none;"><div class="checkBoxSelect"><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px"><path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/></svg>Copy To</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px"><path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/></svg>Download</a><a  href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Share</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/></svg>Cut</a><a  href="javascript:;" class="DeletFileAndFolder" user_id="' + resp['userId'] + '" folderID="' + item['id'] + '" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/></svg>Delete</a><a  href="javascript:;" class="Renamefiles"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px"><path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/></svg>Preview</a><a  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17"><path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path></svg>Rename</a><a hidden  href="javascript:;" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/></svg>Copy</a></div></div></div>';
                        var folder = `<div class="sc_share_wrapper getSubFolderByFolderId " option="option${c}" id="${ resp['userId']}" data-folder-name="${ item['name']}" data-folder-Id="${ item['id']}">
                                        <div class="sc_dots_img">
                                                <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                        </div>
                                        <div class="checkbox" hidden>
                                            <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                                <label for="rememberme${c}" class="option"></label> 
                                        </div>
                                         <div class="sc_share_img sc_folder_img getSubFolderByFolderId" >
                                            <img src="${icon}" alt="icon">
                                        </div>
                                        <div class="sc_share_detail">
                                            <h5>${ item['name']}</h5>
                                        </div>
                                        <div class="checkOptionMenu tbf_IconHolter option${c}" clickValue="option${c}" style="display:none;">
                                            <div class="checkBoxSelect">
                                                <a ${ myArray.find(item => item.copy_to).copy_to }  href="javascript:;">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">
                                                        <path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>
                                                    </svg>
                                                    Copy To
                                                </a>
                                                <a ${ myArray.find(item => item.download).download }>
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                    </svg>Download
                                                </a>
                                                <a ${ myArray.find(item => item.share).share }  class="ShareURL ${resp['userId'] } href="javascript:;">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>Share
                                                </a>
                                                <a ${ myArray.find(item => item.cut).cut } href="javascript:;" >
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                    <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                </svg>Cut
                                                </a>
                                                <a  ${ myArray.find(item => item.move).move } href="javascript:;" class="moveFile " userId="${resp['userId'] }" fileId="${item['id'] }" drotype="${item['path_lower'] }">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                    <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                </svg>Move
                                                </a>
                                                <a ${ myArray.find(item => item.delete).delete } href="javascript:;" class="DeletFileAndFolder" user_id="${ resp['userId']}" folderID="${ item['id']}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>Delete
                                                </a>
                                                <a ${ myArray.find(item => item.preview).preview } href="javascript:;" class="Renamefiles">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                    </svg>Preview
                                                </a>
                                                <a ${ myArray.find(item => item.rename).rename } href="javascript:;" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
                                                        <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63">
                                                        </path>
                                                    </svg>Rename
                                                </a>
                                                <a ${ myArray.find(item => item.copy).copy } hidden  href="javascript:;" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>
                                                    </svg>Copy
                                                </a>
                                            </div>
                                        </div>
                                    </div>`;
                        $('.sc_share_main_section').append(folder);
                    });

                }
                }
            });
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });

});

$('.folderBack').hide();
$('.UploadFile').hide();

// let current_api = $('.cd_header_title').text();
// if (current_api == 'Gofile' || current_api == 'Backblaze' || current_api == 'Ftp') {
//     $('.UploadFile').show();
// }else{
//     $('.UploadFile').hide();
// }
$('.CreateAlbum').hide();


$(document).on('click', '.getSubFolderByFolderId', function(e) {
    var name = $(this).attr('data-folder-name');
    var folderId = $(this).attr('data-folder-Id');
    var droType = $(this).attr('drotype');
    var type = $(this).attr('type');
    var id = $(this).attr('id');
    
    getSubFolderByFolderId( folderId, name, droType, type, id )

});

function checkBackBtnCond() {
    let back_id = $('.folderBack').attr('tofolderid')
    back_id = back_id.replace(/[^a-zA-Z0-9]/g, '');
    if (back_id == '') {
        $('.folderBack').hide();
    }
}

$('.GooglePgotos').hide();
function getSubFolderByFolderId( folderId, name = '', droType = '', type = '', id = '') {
    $('#progressbar').attr('width','0');
    $('#progressbar').html('');
    $('.UploadFile').attr('folder_id', folderId);
    $('.GooglePgotos ').attr('folder_id', folderId);
    $('.UserCreateFolder').addClass('userSubCreateFolder ');
    $('.UserCreateFolder').attr('id', 'userSubCreateFolder');
    $('.UserCreateFolder').attr('folder_id', folderId);
    $('.userSubCreateFolder').attr('folder_id', folderId);
    $('.UserCreateFolder').attr('droType', droType);
    $('.CloudUploadFile').attr('folderId', folderId);
    $('.CloudUploadFile').attr('droType', droType);
    $('.RenameFile').attr('droType', droType);
    $('.folderBack').attr('droType', droType);
    $('.folderBack').attr('folderId', folderId);
    $('.folderBack').attr('id', id);
// return false;
   $.ajax({
        url: baseurl + 'home/getSubFolderByFolderId',
        type: "POST",
        data: {
            'name': name,
            'folderId': folderId,
            'id': id,
            'type': type,
            'drType': droType
        },
        success: function(e) {
            var resp = $.parseJSON(e); 
          
            if (resp.data != "") {
                $('.sc_share_main_section').empty();
                $('.folderBack').show();
                $('.amazonS3UploadFile').show();
                $('.UserCreateFolder').removeClass('UserCreateFolder');
                $('.folderBack').attr('tofolderid', resp['data']['folderid']);
            }
                var i = 1;
                var j = 0;
                var k = 2;
                if(resp.connection == "s3"){
                    $('#s3UploadFile').attr('folder_id', folderId);
                    $('.UploadFile').hide();
                        jQuery.each(resp['data'][1], function(index, item) {
                    if(resp['data'][0]=="" && resp['data'][1]==""){
                        $('.sc_share_main_section').empty();
                        var folder =  `<div class="no_dash_box ">
                                    <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                        <h4>No Files Are Available</h4>
                                        <p>You haven't uploaded any files yet</p>
                                        <div class="add-group">
                                            <!--<a href="javascript:;" class="ad-btn">Upload File</a>-->
                                        </div>
                                    </div>`;
                                    
                        $('.sc_share_main_section').addClass('sc_nofile_avai');
                        $('.sc_share_main_section').append(folder);
                    }else{
                            var c = j++;
                            var temp ;
                            var viewId = item['ETag'];
                            var filename = item['Key'];
                            
                            var fileExtension =filename.substring(filename.lastIndexOf('/') + 1);
                            var name = fileExtension;
                            
                            var icon = baseurl + 'assets/backend/images/file.png';
                            var getFdrClass = '';
                            var downloadFIle = "javascript:;";
                            var hide = "";
                            var folderId = item['Key'];
                        
                            if(name!='test'){
                            
                                var folder = `<div class="sc_share_wrapper ">
                                            <div class="sc_dots_img">
                                                  <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                            </div>
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme${c}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file ${getFdrClass}"  option="option${c}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5 class"${getFdrClass}"  option="option${c}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                                				<div class="checkBoxSelect">
                                				    <a  href="javascript:;" ${hide} class="DownloadFile"  folderIDShareURL="${folderId}" userID="${id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                        </svg>
                                                        Download
                                                    </a>
                                                    ${current_plan.length > 1 && current_plan.includes("3") ?
                                                        `<a href="javascript:;" ${hide} class="ShareURL"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                            </svg>
                                                            Share
                                                        </a>
                                                        <a  href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                            </svg>
                                                            Move
                                                        </a>`
                                                        :
                                                        ``
                                                    }
                                                    <a  href="javascript:;"  class="DeletFileAndFolder" ${hide} user_id="${resp.userId}" fileid="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                                    <a href="javascript:;" ${hide} class="previewFile"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                        </svg>
                                                        Preview
                                                    </a>
                                				</div>
                            				</div>
            			                </div>`;
                                    $('.sc_share_main_section').append(folder);
                            }
                             i++;
                             k++;
                    }
                        });
                    if(resp['data'][0]=="" && resp['data'][1]==""){
                        $('.sc_share_main_section').empty();
                        var folder =  `<div class="no_dash_box ">
                                    <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                        <h4>No Files Are Available</h4>
                                        <p>You haven't uploaded any files yet</p>
                                        <div class="add-group">
                                            <!--<a href="javascript:;" class="ad-btn">Upload File</a>-->
                                        </div>
                                    </div>`;
                                    
                        $('.sc_share_main_section').addClass('sc_nofile_avai');
                        $('.sc_share_main_section').append(folder);
                    }else{
                        jQuery.each(resp['data'][0], function(index, item) {
                      
                        temp = item.Prefix.split("/");
                        if(index==0){
                             var name = temp[1];
                        }else{
                             var name = temp[1];
                        }
                       
                        var icon = baseurl + 'assets/backend/images/folder.svg';
                        var getFdrClassf = 'getSubFolderByFolderId';
                        var downloadFIle = '';
                        var getFdrClass = '';
                        var folderId = item['Prefix'];
                        var folder = `<div class="sc_share_wrapper ">
                                            <div class="sc_dots_img">
                                                  <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                            </div>
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme$${i+'f'}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file ${getFdrClassf}"  option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5 class="${getFdrClassf}"  option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                                				<div class="checkBoxSelect">
                                                    <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                                  
                                				</div>
                            				</div>
        				                </div>`;
                        $('.sc_share_main_section').append(folder);
                         i++;
                    });
                    }
                }else if(resp.connection == "Googlephoto"){
                    $('.GooglePgotos').show();
                    $('.GooglePgotos').addClass('amazonS3UploadFile');
                    $('.UploadFile').hide();
                    $('.CreateAlbum').show();
                    $('#add_customer_title').html('Create Album');
                    $('#folder_name').html('Create Album');
                    $('.Folderame').html('Create Album');
                    $('#folder_name').val('New Album');
                    $('.userSubCreateFolder').val('Create Album');
                     if(resp['data']!=""){
                        jQuery.each(resp['data']['data'], function(index, item) {
                          
                            var c = i++;
                            if(resp.type !="Photos" && resp.type !="mediaItemSearch" || resp.type =='ShareAlbum'){
                                 
                                var fileExtension = item['title'].substring(item['title'].lastIndexOf('.') + 1);
                               
                                 Extension = FileExtension();
                              
                                if (Extension.lastIndexOf(fileExtension) == -1) {
                                    var icon = baseurl + 'assets/backend/images/folder.svg';
                                    var getFdrClass = 'getSubFolderByFolderId';
                                    var downloadFIle = "javascript:;";
                                    var hide = "hidden";
                                } else {
                                    var icon = baseurl + 'assets/backend/images/file.png';
                                    var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';
                                    var getFdrClass = '';
                                    var hide = "";
                                }
                                folderId = item['id'];
                                var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['title']}"  data-folder-Id="${folderId}">
                                                    <div hidden class="sc_dots_img">
                                                          <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                                    </div>
                                    				<div class="checkbox" hidden>
                                    				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                    				    <label for="rememberme${c}" class="option"></label>
                                    				</div>
                                    				<div class="sc_share_img sc_share_file">
                                    				    <img src="${icon}" alt="icon">
                                    				</div>
                                    				<div class="sc_share_detail">
                                    				    <h5>${item['title']}</h5>
                                    				</div>
                                    				<div hidden class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                                        				<div class="checkBoxSelect">
                                                            ${current_plan.length > 1 && current_plan.includes("3") ?
                                                            `<a href="javascript:;"  class="ShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Share
                                                            </a>
                                                            <a href="javascript:;"  class="ShareURL" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Share
                                                            </a>
                                                            <a  href="javascript:;" class="moveFile " filename="${item['title']}" userId="${id}" fileId="${folderId}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                                </svg>
                                                                Move
                                                            </a>`
                                                            :
                                                            ``
                                                            }
                                                            
                                                            <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp['userId']}" folderID="${folderId}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Delete
                                                            </a>
                                                            
                                                            <a  href="javascript:;" class="previewFile"  folderIDShareURL="${folderId}" userID="${id}">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                                    <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                                </svg>
                                                                Preview
                                                            </a>
                                                          
                                        				</div>
                                    				</div>
                				                </div>`;
                                $('.sc_share_main_section').append(folder);
                            }else{
                                $('.GooglePgotos').show();
                                $('.GooglePgotos').addClass('amazonS3UploadFile');
                                var downloadFIle = item.baseUrl;
                                var getFdrClass = '';
                                var hide = "";
                                var icon = baseurl + 'assets/backend/images/file.png';
                                
                                var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp.userId}" data-folder-name="${item.filename}"  data-folder-Id="${item.id}">
                                                    <div class="sc_dots_img">
                                                          <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                                    </div>
                                    				<div class="checkbox" hidden>
                                    				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                    				    <label for="rememberme${c}" class="option"></label>
                                    				</div>
                                    				<div class="sc_share_img sc_share_file">
                                    				    <img src="${icon}" alt="icon">
                                    				</div>
                                    				<div class="sc_share_detail">
                                    				    <h5>${item.filename}</h5>
                                    				</div>
                                    				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                                        				<div class="checkBoxSelect">
                                                            <a href="javascript:;" class="DownloadFile" fileIDShareURL="${item.id}" folderIDShareURL="${downloadFIle}" userID="${id}" Download>
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Download
                                                            </a>
                                                            <a href="javascript:;"  class="ShareURL" fileIDShareURL="${downloadFIle}" folderIDShareURL="${downloadFIle}" userID="${id}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Share
                                                            </a>
                                                            <a  href="javascript:;" class="moveFile " filename="${item.filename}" userId="${id}" fileId="${item.id}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                                </svg>
                                                                Move
                                                            </a>
                                                            
                                                            <a hideen href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" folderID="${item.id}" >
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                                    <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                                </svg>
                                                                Delete
                                                            </a>
                                                            
                                                            <a  href="javascript:;" class="previewFile"  folderIDShareURL="${downloadFIle}" userID="${id}">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                                    <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                                </svg>
                                                                Preview
                                                            </a>
                                                          
                                        				</div>
                                    				</div>
                				                </div>`;
                                $('.sc_share_main_section').append(folder);
                            }
                        });
                     }else{
                        $('.GooglePgotos').show();
                        $('.GooglePgotos').addClass('amazonS3UploadFile');
                        $('.sc_share_main_section').empty();
                        var folder =  `<div class="no_dash_box ">
                                    <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                        <h4>No Files Are Available</h4>
                                        <p>You haven't uploaded any files yet</p>
                                        <div class="add-group">
                                            <a href="javascript:;" class="ad-btn amazonS3UploadFile" folder_id="">Upload File</a>
                                        </div>
                                    </div>`;
                                    
                        $('.sc_share_main_section').addClass('sc_nofile_avai');
                        $('.sc_share_main_section').append(folder);
                    }
                }else{
                    var myArray = [ { "delete"  : " " }, { "download": " " }, { "copy_to" : " " }, { "share"   : " " }, { "cut"     : " " }, { "rename"  : " " }, { "copy"    : " " }, { "move"    : " " },{ "preview"    : " " } ];
                    $('.sc_share_main_section').removeClass('sc_nofile_avai')
                     $('.UploadFile').show();
                   if(resp['data']['data']!=""){
                        jQuery.each(resp['data']['data'], function(index, item) {
                    var c = i++;
                    var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);
                     Extension = FileExtension();
                    if (Extension.lastIndexOf(fileExtension) == -1) {
                        var icon = baseurl + 'assets/backend/images/folder.svg';
                        var getFdrClass = 'getSubFolderByFolderId';
                        var downloadFIle = "javascript:;";
                        var hide = "hidden";
                    } else {
                        var icon = baseurl + 'assets/backend/images/file.png';
                        var downloadFIle = 'https://www.googleapis.com/drive/v3/files/' + item['id'] + '?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';
                        var getFdrClass = '';
                        var hide = "";
                    }
                    // console.log(item)
                    let connType = resp.connection;
    
                    if (connType == 'GoogleDrive') {
                        var folderId = item['id'];
                    }
                    if (connType == 'DropBox') {
                        var boxPath = item['path_lower'];
                    }
                    if (connType == 'Ddownload') {
                        if (item['file_code'] == '' || item['file_code'] == "undefined") {
                            var folderId = item['fld_id'];
                        } else {
                            var folderId = item['file_code'];
                        }
                        var urlByFile = `https://ddownload.com/${folderId}/${item['name']}`;
                        var hidden = 'hidden';
                    }
                    if (connType == 'Box') {
                        var folderId = item['id'];
                        var urlByFile = `https://ddownload.com/${folderId}/${item['name']}`;
                        var hidden = '';
                    }
                    if (connType == 'OneDrive') {
                        var folderId = item['id'];
                        var previewFile = 'OnePreviewFile';
                        var urlByFile = baseurl + `home/DownloadFilesOneDrive/${folderId}/${id}/${item['name']}`;
                    }
                    if (connType == 'Backblaze') {
                        if (item['action'] == 'folder') {
                            var folderId = item['fileName'];
                        }else{
                            var folderId = item['fileId'];
                        }
    
                        var previewFile = 'OnePreviewFile';
                        var urlByFile = baseurl + `home/downloadB2File/${folderId}/${id}/${item['name']}`;
                        myArray = [ { "delete"  : `fileid = "${item['fileName']}"` }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                      //  checkBackBtnCond()
                    }
                    if (connType == 'Gofile') {
                        
                        var folderId = item['id'];
                        var previewFile = 'OnePreviewFile';
                        var urlByFile = item['directLink'];
                        myArray = [ { "delete"  : ' ' }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${item['directLink']}" ` }, { "cut"     : "hidden" }, { "rename"  : "hidden" }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                      //  checkBackBtnCond()
                    }
                    if (connType == 'Pcloud') {
                            
                        if (item['isfolder'] == 1) {
                            var folderId  = item['folderid'];
                            myArray       = [ { "delete"  : `folderID = "${folderId}"` }, { "download": `hidden` }, { "copy_to" : "hidden" }, { "share"   : `hidden ` }, { "cut"     : "hidden" }, { "rename"  : ` fileCode = ${folderId} ` }, { "copy"    : "hidden" }, { "move"    : "hidden" }, { "preview"    : "hidden" } ];
                            hide = '';
                        }else{
                            var folderId  = item['fileid'];
                            var urlByFile = baseurl + 'home/downloadPcloudFile/'+ id + '/' + folderId;
                            myArray       = [ { "delete"  : `fileid = "${folderId}" folderID = ""` }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : `fileCode = "" fileID = ${ folderId } ` }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                            
                        }
                        // checkBackBtnCond()
                    }
                    if (connType == 'Ftp') {
                    
                        var folderId = resp['data']['currentfolder']+'/'+item['name'];
                        var previewFile = 'OnePreviewFile';
                        var urlByFile = baseurl + `home/downloadFtpFile/?id=${id}&path=${ encodeURIComponent(folderId) }`;
                        if (item['type'] == 'file') {
                            myArray = [ { "delete"  : `fileid = "${folderId}" folderID = ""` }, { "download": `href="${urlByFile}" class="" target="_blank"` }, { "copy_to" : "hidden" }, { "share"   : `folderIDShareURL="${urlByFile}" ` }, { "cut"     : "hidden" }, { "rename"  : " " }, { "copy"    : "hidden" }, { "move"    : " " }, { "preview"    : "hidden" } ];
                        }else{
                            myArray = [ { "delete"  : `folderID = "${folderId}" ` }, { "download": `hidden` }, { "copy_to" : "hidden" }, { "share"   : `hidden` }, { "cut"     : "hidden" }, { "rename"  : " " }, { "copy"    : "hidden" }, { "move"    : "hidden" }, { "preview"    : "hidden" } ];
                        }
                       
                        if ( item['type'] != 'file') {
                            icon        = baseurl + 'assets/backend/images/folder.svg';
                            getFdrClass = 'getSubFolderByFolderId';
                            hide        = "hidden";
                        } else {
                            icon        = baseurl + 'assets/backend/images/file.png';
                            getFdrClass = '';
                            hide        = "";
                        }
                      //  checkBackBtnCond()
    
                    }
                   
                    if (item['path_lower'] != "undefined") {
                        var viewFileId = item['path_lower'];
                    } else {
                        var viewFileId = item['id'];
                    }
    
                    // console.log(item['path_lower']);
                    var folder = `<div class="sc_share_wrapper">
                                        <div class="sc_dots_img">
                                              <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                        </div>
                        				<div class="checkbox" hidden>
                        				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                        				    <label for="rememberme${c}" class="option"></label>
                        				</div>
                        				<div class="sc_share_img sc_share_file ${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['name']}" droType="${item['path_lower']}" data-folder-Id="${folderId}">
                        				    <img src="${icon}" alt="icon">
                        				</div>
                        				<div class="sc_share_detail">
                        				    <h5 class="${getFdrClass}"  option="option${c}" id="${resp['userId']}" data-folder-name="${item['name']}" droType="${item['path_lower']}" data-folder-Id="${folderId}">${item['name']}</h5>
                        				</div>
                        				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                            				<div class="checkBoxSelect">
                            				    ${(connType == 'Ddownload' || connType == 'OneDrive' || connType == 'Backblaze'? `<a  ${hide} href="${urlByFile}" class="" target="_blank" folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                    </svg>
                                                    Download
                                                </a>`:`<a ${ myArray.find(item => item.download).download } href="javascript:;" ${hide} class="DownloadFile" drotype = "${item['path_display']}"  folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                    </svg>
                                                    Download
                                                </a>`)}
                                				${current_plan.length > 1 && current_plan.includes("3") ? 
                                                    `<a ${ myArray.find(item => item.share).share } href="javascript:;" fileName="${item.name}" ${hide} class="ShareURL" drotype="${item['path_display']}" fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Share
                                                    </a>
                                                    
                                                    <a ${ myArray.find(item => item.move).move } ${(connType == 'Box' || connType == 'Ddownload' )?'hidden':''} href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}" drotype="${item['path_lower']}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                        </svg>
                                                        Move
                                                    </a>`
                                                    :
                                                    ``
                                				}
                                                
                                                <a ${ myArray.find(item => item.delete).delete } href="javascript:;"  class="DeletFileAndFolder" ${hidden} user_id="${resp['userId']}" folderID="${folderId}" drotyp="${item['path_display']}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                                
                                                <a ${ myArray.find(item => item.preview).preview } ${(connType == 'Box' || connType == 'Ddownload' )?'hidden':''} href="javascript:;" ${hide} class="${(connType == 'OneDrive'? previewFile : 'previewFile')}" drotype = "${item['path_display']}"  folderIDShareURL="${folderId}" userID="${id}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                    </svg>
                                                    Preview
                                                </a>
                                                
                                                <a ${ myArray.find(item => item.rename).rename } href="javascript:;" ${(connType == 'OneDrive'? 'hidden' : '')}${hide} class="Renamefiles" type="${item['type']}" fileID="${folderId}" fileCode="${folderId}" folderNAME="${item['name']}" droType="${viewFileId}"   >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
                                                        <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63">
                                                        </path>
                                                    </svg>
                                                    Rename
                                                </a>
                            				</div>
                        				</div>
    				                </div>`;
                    $('.sc_share_main_section').append(folder);
                });
                    }else{
                        $('.sc_share_main_section').empty();
                        var folder =  `<div class="no_dash_box ">
                                    <img src="${baseurl}assets/backend/images/no_file.png" alt="">
                                        <h4>No Files Are Available</h4>
                                        <p>You haven't uploaded any files yet</p>
                                        <div class="add-group">
                                            <!--<a href="javascript:;" class="ad-btn">Upload File</a>-->
                                        </div>
                                    </div>`;
                                    
                        $('.sc_share_main_section').addClass('sc_nofile_avai');
                        $('.sc_share_main_section').append(folder);
                    }
                }
            
        }
    });
}


$(document).on('click', '.ShareURL', function() {
    // console.log($('#ShareURL').find('form'))
    $('#copyButton').attr('filename', $(this).attr('filename'));
    $('#copyButton').attr('folderidshareurl', $(this).attr('folderidshareurl'));
    $('#copyButton').attr('fileIDShareURL', $(this).attr('fileIDShareURL'));
    $('#copyButton').attr('drotype', $(this).attr('drotype'));
    $('#copyButton').attr('userid' ,$(this).attr('userid'));
    $('#ShareURL').modal('show');
});

$(document).on('click', '#copyButton', function() {
    var file_type = $('input[name="url_type"]:checked').val();
    var folderId = $(this).attr('folderidshareurl');
    var fileId = $(this).attr('fileIDShareURL');
    var droType = $(this).attr('drotype');
    var filename = $(this).attr('filename');
    var id = $(this).attr('userid');
    
    console.log(filename)
    $.ajax({
        url: baseurl + 'common/shareURLGet',
        type: "POST",
        data: {
            'folderId': folderId,
            'fileId': fileId,
            'id': id,
            'drType': droType,
            'file_type': file_type,
            'filename': filename,
        },
        success: function(e) {
            var data = $.parseJSON(e);
            var resp = data.data;
            console.log(data)
            if(data.status == 1){
                
                $('.sc_share_link').addClass('sc_share_opacity');
                var Private_share_url = 'Private sharing link: '+data.shareUrl+' View password: '+resp.file_password;
                if(resp.file_type == 1){
                    $('#copyTargetUrl').closest('.form-group').removeClass('d-none');
                    $('#copyTargetUrl').val(decodeURI(data.shareUrl));
                }else{
                    $('#copyTargetUrl').closest('.form-group').removeClass('d-none');
                    $('#copyTargetPassword').closest('.form-group').removeClass('d-none');
                    $('#copyTargetUrl').val(decodeURI(data.shareUrl));
                    $('#copyTargetPassword').val(resp.file_password);
                }
                $('#copyButton').html('Copy Url');
                $('#copyButton').addClass('CopyMe');
                $('#copyButton').removeAttr('id');
                console.log($('#privareUrl').val())
                // setTimeout(function(){
                //     window.location = baseurl+'home/checkLoginUser';
                // },3000);
            }else{
                showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                setTimeout(function(){ location.reload(); }, 3000);
            }
            return false;
            // var res = copyToClipboard(document.getElementById("copyTarget"));
            // if (res) {
            //     showNotifications('success','Copy Link');
            // }
        }
    });
    
});

$(document).on('click', '.CopyMe', function() {
    var res = copyToClipboard(document.getElementById("copyTargetUrl"));
    if (res) {
        showNotifications('success','Copy Link');
        showNotifications('success','Copy url successfully.');
    }
});

$(document).on('click', '.privateFileUrl', function() {
    var password = $('#file_password').val();
    if(password == ''){
        showNotifications('error','Password is required.');
        setTimeout(function(){ location.reload(); }, 3000);
    }else{
        
        var file_id = $('#file_id').val();
        var conn_id = $('#conn_id').val();
        var file_password = $('#file_password').val();
        $.ajax({
            url: baseurl + 'common/sharePrivateFiles',
            type: "POST",
            data: {
                'file_id': file_id,
                'conn_id': conn_id,
                'password': file_password,
            },
            success: function(e) {
                var resp = $.parseJSON(e);
                if(resp.status == 1){
                    $('#ShareURL').hide();
                    window.open(baseurl + resp.url).focus();
                }
                console.log(resp);
                return false;
            },
            error: function(resp) {
                 showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
            }
        });
    }
});


$(document).on('click', '.previewFile', function() {
    var folderId = $(this).attr('folderidshareurl');
    var droType = $(this).attr('drotype');
    var id = $(this).attr('userid');

    $.ajax({
        url: baseurl + 'home/view_file',
        type: "POST",
        data: {
            'folderId': folderId,
            'id': id,
            'drType': droType
        },
        success: function(e) {
            var data = $.parseJSON(e);
            // console.log(data.data.file.data.url)
           
            if (data['data']['connection'] == 'googleDrive') {
                
                if (data.data.file.data.ex == 'jpeg' || data.data.file.data.ex == 'jpg' || data.data.file.data.ex == 'png' || data.data.file.data.ex == 'gif' || data.data.file.data.ex == 'pdf' || data.data.file.data.ex == 'html' || data.data.file.data.ex == 'mp4' || data.data.file.data.ex == 'mp3') {
                   var viewData = '<iframe src="' +data.data.file.data.url+ '" height="100%" width="100%" title="Iframe Example" class="fileReding"></iframe>';
                        $('#AllFilePreview').empty();
                        $('#AllFilePreview').append(viewData);
                } else {
                     var viewData = `<center><h5 class="modal-title" id="add_customer_title">File Not Priview Only Download</h5><br>
                                        <div class="add-group">
        									<a href="${data.data.file.data.url}" class="ad-btn">Clieck Here Download </a>
        								</div></center>
                                    `;
                        $('#AllFilePreview').empty();
                        $('#AllFilePreview').append(viewData);
                }
            }else if (data['connection'] == 'OneDrive') {
                window.open(data['shareUrl'], '_blank').focus();
            }else if(data['data']['connection'] == 's3'){
                       
                    if (data['data']['filetype'][4] == 'jpeg' || data['data']['filetype'][4] == 'jpg' || data['data']['filetype'][4] == 'png' || data['data']['filetype'][4] == 'gif' ) {
                        var viewData = '<img src="' + data['data']['url'] + '">';
                            $('#AllFilePreview').empty();
                            $('#AllFilePreview').append(viewData);
                    } else {
                         var viewData = `<center><h5 class="modal-title" id="add_customer_title">File Not Priview Only Download</h5><br>
                                            <div class="add-group">
            									<a href="${data['data']['url']}" class="ad-btn">Clieck Here Download </a>
            								</div></center>
                                        `;
                            $('#AllFilePreview').empty();
                            $('#AllFilePreview').append(viewData);
                    }
               
            }else if(data['data']['connection'] == 'GooglePhoto '){
                var viewData = '<img src="' + data['data']['url'] + '">';
            }else if(data['data']['connection'] == 'DropBox'){
                
                var ext = data.data.filename.split('.').pop().toLowerCase(); 
                
                if (ext == 'jpeg' || ext == 'jpg' || ext == 'png' || ext == 'gif' || ext == 'pdf' || ext == 'mp4' || ext == 'mp3') {
                   var viewData = '<iframe src="' +data.data.file.data.url+ '" height="100%" width="100%" title="Iframe Example" class="fileReding"></iframe>';
                        $('#AllFilePreview').empty();
                        $('#AllFilePreview').append(viewData);
                } else {
                     var viewData = `<center><h5 class="modal-title" id="add_customer_title">File Not Priview Only Download</h5><br>
                                        <div class="add-group">
        									<a href="${data.data.file.data.url}" class="ad-btn">Clieck Here Download </a>
        								</div></center>
                                    `;
                        $('#AllFilePreview').empty();
                        $('#AllFilePreview').append(viewData);
                }
            }else{
                var viewData = '<iframe src="' +data.data.file.data.url+ '" height="100%" width="100%" title="Iframe Example" class="fileReding"></iframe>';
                $('#AllFilePreview').empty();
                $('#AllFilePreview').append(viewData);
            }
        }
    });
});

$(document).on('click', '.DownloadFile', function() {
    var folderId = $(this).attr('folderidshareurl');
    var droType = $(this).attr('drotype');
    var id = $(this).attr('userid');
    $.ajax({
        url: baseurl + 'home/DownloadFiles',
        type: "POST",
        data: {
            'folderId': folderId,
            'id': id,
            'drType': droType,
            // 		'fileName': fileName
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            console.log(resp);
            // return false;
            if (resp.type == 'Backblaze') {
                window.location.replace( baseurl + '/home/downloadB2File/' + resp.fileId + '/' + resp.id + '/' + resp.fileName);
            }else{
                window.open(resp['shareUrl'], '_blank').focus();
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
        }
    });
});

$(document).on('click', '.Renamefiles', function() {
    var path = $(this).attr('drotype');
    var folderNAME = $(this).attr('folderNAME');
    var fileID = $(this).attr('fileID');
    var fileCode = $(this).attr('fileCode');
    var type = $(this).attr('type');
    var fileName = $('#fileName').val();
    $('.RenameFile').attr('path', path);
    $('.RenameFile').attr('fileID', fileID);
    $('.RenameFile').attr('folderNAME', folderNAME);
    $('.RenameFile').attr('fileCode', fileCode);
    $('.RenameFile').attr('typefile', type);

});

$(document).on('click', '.RenameFile', function() {
    var path = $(this).attr('path');
    var fileID = $(this).attr('fileID');
    var foldername = $(this).attr('foldername');
    var user_id = $('#user_id').val();
    var fileName = $('#fileName').val();
    var folderPath = $(this).attr('drotype');
    var fileCode = $(this).attr('fileCode');
    var typefile = $('.RenameFile').attr('typefile');

    $.ajax({
        url: baseurl + 'home/RenameFile',
        type: "POST",
        data: {
            'fileName': fileName,
            'path': path,
            'fileID': fileID,
            'id': user_id,
            'folderPath': folderPath,
            'foldername': foldername,
            'fileCode': fileCode,
            'typefile': typefile
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            if (resp['data'] != '' && resp['status'] == '1') {
                showNotifications('success','Successfully Change File Name ');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
                setTimeout(function() {
                    window.location.reload();
                }, 3000);
                return false;
            } else {
                 showNotifications('error',resp['Something went wrong. Please, refresh the page and try again']);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
        }
    });
});

$('.s3UploadFile').on('click', function() {
    $('.progress ').show();
    var formdata = new FormData($(this).closest('form')[0]);
    
    var user_id = $('#user_id').val();
    var folder_id = $(this).attr('folder_id');
  
    formdata.append('folder_id', folder_id);
    formdata.append('type', 'amzonS3');
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            $('.progress').removeClass('hide');
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);
                    $(".progress-bar").width(percentComplete + '%');
                    $(".progress-bar").html(percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        method: "POST",
		url: baseurl + 'home/userUploadFiles',
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            var resp = $.parseJSON(resp);
            
            if (resp['status'] == 1) {
                showNotifications('success','Successfully Upload File');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
                setTimeout(function() {
                    location.reload();
                }, 1000);
                 
                return false;
                getSubFolderByFolderId( folder_id, '', '', '', user_id )
            } else {
                 showNotifications('error',resp.smg);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
            $('.edu_preloader').fadeOut();
        },
        error: function(resp) {
             showNotifications('error',resp.smg);
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
        }
    });
});

$('.CloudUploadFile').on('click', function() {
    if ( $('#UploadFile_Name').val().length != 0  ) {
        $('.progress ').show();
        $('.sc_share_main_section.sc_nofile_avai').html('');
        $('.sc_nofile_avai').removeClass('sc_nofile_avai');
        var formdata = new FormData($(this).closest('form')[0]);
        var path = $(this).attr('drotype');
        var folderid = $(this).attr('folderid');
        var user_id = $('#user_id').val();
        console.log(formdata);
        
        formdata.append('path', path);
        formdata.append('folderid', folderid);
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                $('.progress').removeClass('hide');
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            method: "POST",
            url: baseurl + 'home/userUploadFiles',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(resp) {
                var resp = $.parseJSON(resp);
            
                if (resp['status'] == '1') {
                    $('#UploadFile').modal('hide');
                    // $('.UploadFile').hide();
                    $('.plr_notification ').show(); showNotifications('success',resp.smg);
                    getSubFolderByFolderId( folderid, name, path, '', user_id )
                    setTimeout(function(){$('.plr_notification ').hide();},2000);    
                    return false;
                } else {
                    $('.plr_notification ').show(); showNotifications('error',resp.smg);
                    setTimeout(function(){$('.plr_notification ').hide();},2000);    
                    return false;
                }
                $('.edu_preloader').fadeOut();
            },
            error: function(resp) {
                $('.plr_notification ').show(); showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);    
            }
        });
    }else{
        $('.plr_notification ').show(); showNotifications('error','Please Select File To Upload');
        setTimeout(function(){$('.plr_notification ').hide();},2000);   
    }
    
});

$('.moveFileForm').hide();
$('.MoveProgressBar').hide();
$('.MoveBackFolder').hide();

$(document).on('click', '.moveFile', function(e) {
    var fileId = $(this).attr('fileId');
    var fileName = $(this).attr('filename');
    var drotype = $(this).attr('drotype');

    var id = $(this).attr('userId');
    $.ajax({
        url: baseurl + 'home/GetAllConnection',
        type: "POST",
        data: {
            'id': id
        },
        success: function(e) {
            var data = $.parseJSON(e);
            if (data != "") {
                $('.MovingFile').empty();
                $('#fileId').attr('MoveFileId', fileId);
                $('#fileId').attr('drotype', drotype);
                $('.moveFileDriveAndDropBox').attr('MoveFileId', fileId);
                $('.MoveUploadFile').attr('MoveFileName', fileName);
                $('.MoveBackFolder').show();
                $('#UserID').attr('MoveUserId', id);
            }
            var i = 1;
            jQuery.each(data['data'], function(index, item) {
                var add = true;
                if (item['connection_type'] == 'GoogleDrive') {
                    var resp = $.parseJSON(item['user_info']);
                    if (resp['image'] != '') {
                        var img = baseurl + 'assets/backend/images/icon1.png';
                    } else {
                        var img = baseurl + 'assets/backend/images/icon1.png';
                    }
                    var name = resp['name'];
                    var email = resp['email'];
                    var usedSpace = resp['usedSpace'] / 1073741824;
                    var totalSpace = resp['totalSpace'] / 1073741824;

                } else if (item['connection_type'] == 'DropBox') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/dropbox.png';
                    var name = resp['name']['display_name'];
                    console.log(name);
                    var email = resp['email'];
                    var usedSpace = resp['usedSpace'] / 1073741824;
                    var totalSpace = resp['totalSpace'] / 1073741824;

                } else if (item['connection_type'] == 'Ddownload') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/ddownload.png';
                    var name = resp['email'].split("@");
                    console.log(name);
                    // var name = resp[0];
                    var email = resp['email'];
                    var usedSpace = resp['storage_used'] / 1073741824;
                    var totalSpace = 10;

                } else if (item['connection_type'] == 'OneDrive') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/onedrive.png';
                    var name = resp['name'];
                    var email = resp['email'];
                } else if (item['connection_type'] == 'Box') {
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon8.png';
                    var name = resp['name'];
                    var email = resp['email'];
                }else if(item['connection_type'] == 's3'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon18.png';
                    var name = resp['name'];
                    var email = resp['email']; 
                }else if(item['connection_type'] == 'Backblaze'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon26.png';
                    var name = resp['name'];
                    var email = item['email']; 
                }else if(item['connection_type'] == 'GooglePhoto'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon6.png';
                    var name = resp['name'];
                    var email = resp['email']; 
                    
                }else if(item['connection_type'] == 'Gofile'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon31.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    
               
                }else if(item['connection_type'] == 'Ftp'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon7.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    add = false;
                
                }else if(item['connection_type'] == 'Pcloud'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon7.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    add = false;
                
                }else if(item['connection_type'] == 'Anon'){
                    var resp = $.parseJSON(item['user_info']);
                    var img = baseurl + 'assets/backend/images/icon_1.png';
                    var name = resp['name'];
                    var email = item['email']; 
                    
                }
                var connection = `<a href="javascript:;" userid="${item['id']}" connectionID="${item['id']}" MoveFileId="${fileId}" class="moveFileDriveAndDropBox">
                                    <div class="ad-info-card">
                                        <div class="card-body dd-flex align-items-center">
                                            <div class="icon-box-wrapper">
                                                <img src="${img}" alt="image">
                                            </div>
                                            <div class="icon-info-text">
                                                <h5>${name}</h5>
                                                <h6>${email}</h6>
                                            </div>
                                        </div>
                                    </div>
                                </a>`;
                if(add == true)
                    $('.MovingFile').append(connection);
            });
        }
    });
});

$(document).on('click', '.moveFileDriveAndDropBox', function(e) {
    var MoveFileId = $(this).attr('MoveFileId');
    var drotype = $('#fileId').attr('drotype');
    var connectionID = $(this).attr('connectionID');
    var id = $(this).attr('userId');
    var fileName = $(this).data('MoveFileName');

    $.ajax({
        url: baseurl + 'home/moveFileDataget',
        type: "POST",
        data: {
            'id': id
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            console.log(resp)
            if (resp != "") {
                $('.MovingFile').empty();
                $('#fileId').attr('MoveFileId', MoveFileId);
                $('#fileId').attr('drotype', drotype);
                $('#fileId').attr('connectionID', connectionID);
                $('.MoveBackFolder').show();
                $('.MoveUploadFile').attr('MovingFile', MoveFileId);
                $('.MoveUploadFile').attr('MoveFileName', fileName);
                $('.MoveUploadFile').attr('drotype', drotype);
                $('.MoveUploadFile').attr('connectionID', connectionID);
                $('#UserID').attr('MoveUserId', id);
                $('.CloudUploadFile').attr('drotype', drotype);
                
            }else{
                 showNotifications('error','Data Not Found ');
                 setTimeout(function(){$('.plr_notification ').hide();},2000);  
                return false;
            }
            var i = 1;
            jQuery.each(resp['data']['getAllFolder']['data'], function(index, item) {
                if(resp['data']['connectionType']=="s3"){
                    $('.moveFileForm').show();
                   
                    jQuery.each(resp['data']['getAllFolder']['data'][0], function(index, item) {
                    console.log(item.Prefix);
                    temp = item.Prefix.split("/");
                    var name = temp[0];
                   
                    var icon = baseurl + 'assets/backend/images/folder.svg';
                    var getFdrClassf = 'MovegetSubFolderByFolderId';
                    var downloadFIle = '';
                    var getFdrClass = '';
                    var folderId = item['Prefix'];
                    var folder = `<div class="sc_share_wrapper  ${getFdrClassf}"  option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${item.Prefix}">
                                        <div class="sc_dots_img">
                                              <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                        </div>
                        				<div class="checkbox" hidden>
                        				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                        				    <label for="rememberme$${i+'f'}" class="option"></label>
                        				</div>
                        				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${item.Prefix}">
                        				    <img src="${icon}" alt="icon">
                        				</div>
                        				<div class="sc_share_detail">
                        				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${item.Prefix}">${name}</h5>
                        				</div>
                        				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                            				<div class="checkBoxSelect">
                                                <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${item.Prefix}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                              
                            				</div>
                        				</div>
    				                </div>`;
                    $('.MovingFile').append(folder);
                     i++;
                });
                }else{
                    var c = i++;
                    var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);
                    
                     Extension = FileExtension();
                    if (Extension.lastIndexOf(fileExtension) == -1) {
                        var icon = baseurl + 'assets/backend/images/folder.png';
                        var getFdrClass = 'MovegetSubFolderByFolderId';
                    } else {
                        var icon = baseurl + 'assets/backend/images/file.png';
                        var getFdrClass = '';
                    }
                    if (resp.data.connectionType == 'Google Drive') {
                        var folderId = item['id'];
                        var folderIdDelete = item['id'];
                        var viewFileId = item['id'];
                    }
                    if (resp.data.connectionType == 'DropBox') {
                        var folderIdDelete = item['path_display'];
                        var viewFileId = item['path_lower'];
                    }
                    if (resp.data.connectionType == 'Ddownload') {
                        var folderId = item['fld_id'];
                    }
                    if (resp.data.connectionType == 'OneDrive') {
                        var folderId = item['id'];
                    }
                    if (resp.data.connectionType == 'Box') {
                        // console.log(item);
                        var folderId = item['id'];
                    }
                    if (resp.data.connectionType == 'Backblaze') {
                        // console.log(item);
                        var folderId = item['fileName'];
                    }
                    if (resp.data.connectionType == 'Gofile') {
                        // console.log(item);
                        var folderId = item['id'];
                    }
                    if (resp.data.connectionType == 'Ftp') {
                        var folderId = '/'+item['name'];
                    }
    
                    var folder = `<div class="sc_share_wrapper ${getFdrClass}" option="option${c}" id="${resp['userId']}"  data-folder-name="${item['name']}" droType="${drotype}" data-folder-Id="${folderId}">
                                    
                                    <div class="checkbox" hidden>
                                		<input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                		<label for="rememberme${c}" class="option"></label>    
                                	</div>    
                                	<div class="sc_share_img sc_share_file">
                                		<img src="${icon}" alt="icon">
                                	</div>
                                	<div class="sc_share_detail">
                                		<h5>${item['name']}</h5>
                                	</div>
                                	
                                </div>`;
                    $('.MovingFile').append(folder);
                }
               
            });
        }
    });
});

$(document).on('click', '.OnePreviewFile', function() {
    var folderId = $(this).attr('folderidshareurl');
    var fileId = $(this).attr('fileIDShareURL');
    var droType = $(this).attr('drotype');
    var id = $(this).attr('userid');
    $.ajax({
        url: baseurl + 'home/PreviewFileOneDrive',
        type: "POST",
        data: {
            'folderId': folderId,
            'fileId': fileId,
            'id': id,
            'drType': droType,
        },
        success: function(e) {
            var data = $.parseJSON(e);
            window.open(data['shareUrl'], '_blank').focus();
        }
    });
});

$('.moveFileForm').hide();

$(document).on('click', '.MovegetSubFolderByFolderId', function() {
    console.log(2659)
    var name = $(this).attr('data-folder-name');
    var folderId = $(this).attr('data-folder-Id');
    var movefileid = $('#fileId').attr('movefileid');
    var connectionID = $('.MoveUploadFile').attr('connectionID');
    var droType = $(this).attr('drotype');
    var olddroType = $('.MoveUploadFile').attr('drotype');
    var type = $(this).attr('type');
    var id = $(this).attr('id');

    $.ajax({
        url: baseurl + 'home/getSubFolderByFolderId',
        type: "POST",
        data: {
            'name': name,
            'folderId': folderId,
            'id': id,
            'type': type,
            'drType': droType
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            if (resp != "") {
                $('.MovingFile').empty();
                $('.moveFileForm').show();
                $('.MoveProgressBar').show();
                $('.MoveUploadFile').attr('folderId', folderId);
                $('.MoveUploadFile').attr('movefileid', movefileid);
                $('.MoveUploadFile').attr('connectionID', connectionID);
                $('.MoveUploadFile').attr('NewDrotype', name);
                $('.MoveUploadFile').attr('userID', id);
                $('#moveFolderName').html('To '+name+' Folder');
            }
                var i = 1;
                var j = 0;
                var k = 2;
                if(resp.connection == "s3"){
                    $('.UploadFile').hide();
                    jQuery.each(resp['data'][1], function(index, item) {
                        var c = j++;
                        var temp ;
                        var viewId = item['ETag'];
                        var filename = item['Key'];
                        
                        var fileExtension =filename.substring(filename.lastIndexOf('/') + 1);
                        var name = fileExtension;
                        
                        var icon = baseurl + 'assets/backend/images/file.png';
                        var getFdrClass = '';
                        var downloadFIle = "javascript:;";
                        var hide = "";
                        var folderId = item['Key'];
                    
                        if(name!='test'){
                        
                            var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  option="option${c}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                        <div class="sc_dots_img">
                                              <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                        </div>
                        				<div class="checkbox" hidden>
                        				    <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                        				    <label for="rememberme${c}" class="option"></label>
                        				</div>
                        				<div class="sc_share_img sc_share_file">
                        				    <img src="${icon}" alt="icon">
                        				</div>
                        				<div class="sc_share_detail">
                        				    <h5>${name}</h5>
                        				</div>
                        				<div class="checkOptionMenu tbf_IconHolter  option${c}" clickValue="option${c}" style="display:none;">
                            				<div class="checkBoxSelect">
                            				    <a  href="javascript:;" ${hide} class="DownloadFile"  folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                    </svg>
                                                    Download
                                                </a>
                                                <a href="javascript:;" ${hide} class="ShareURL"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>
                                                    Share
                                                </a>
                                                
                                                <a  href="javascript:;" ${hide} class="moveFile " filename="${item['name']}" userId="${id}" fileId="${folderId}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                    </svg>
                                                    Move
                                                </a>
                                                
                                                <a  href="javascript:;"  class="DeletFileAndFolder" ${hide} user_id="${resp.userId}" fileid="${folderId}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                                <a href="javascript:;" ${hide} class="previewFile"  fileIDShareURL="${fileId}" folderIDShareURL="${folderId}" userID="${id}" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                        <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                    </svg>
                                                    Preview
                                                </a>
                            				</div>
                        				</div>
        			                </div>`;
                                 $('.MovingFile').append(folder);
                        }
                         i++;
                         k++;
                    });
                    jQuery.each(resp['data'][0], function(index, item) {
                      
                        temp = item.Prefix.split("/");
                        if(index==0){
                             var name = temp[1];
                        }else{
                             var name = temp[1];
                        }
                       
                        var icon = baseurl + 'assets/backend/images/folder.svg';
                        var getFdrClassf = 'MovegetSubFolderByFolderId';
                        var downloadFIle = '';
                        var getFdrClass = '';
                        var folderId = item['Prefix'];
                        var folder = `<div class="sc_share_wrapper  ${getFdrClassf}"  option="option${i+'f'}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                                            <div class="sc_dots_img">
                                                  <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                            </div>
                            				<div class="checkbox" hidden>
                            				    <input type="checkbox" id="rememberme${i+'f'}" name=""class="optionCheckBox" value="" >
                            				    <label for="rememberme$${i+'f'}" class="option"></label>
                            				</div>
                            				<div class="sc_share_img sc_share_file ${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">
                            				    <img src="${icon}" alt="icon">
                            				</div>
                            				<div class="sc_share_detail">
                            				    <h5 class="${getFdrClassf}" id="${resp.userId}" data-folder-name="${name}" data-folder-Id="${folderId}">${name}</h5>
                            				</div>
                            				<div class="checkOptionMenu tbf_IconHolter  option${i+'f'}" clickValue="option${i+'f'}" style="display:none;">
                                				<div class="checkBoxSelect">
                                                    <a  href="javascript:;"  class="DeletFileAndFolder"  user_id="${resp.userId}" fileid="${folderId}" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                                  
                                				</div>
                            				</div>
        				                </div>`;
                        $('.MovingFile').append(folder);
                         i++;
                    });
                }else{
                    jQuery.each(resp['data']['data'], function(index, item) {
                    var c = i++;
                    var fileExtension = item['name'].substring(item['name'].lastIndexOf('.') + 1);
                    
                     Extension = FileExtension();
                    if (Extension.lastIndexOf(fileExtension) == -1) {
                        var icon = baseurl + 'assets/backend/images/folder.png';
                        var getFdrClass = 'MovegetSubFolderByFolderId';
                    } else {
                        var icon = baseurl + 'assets/backend/images/file.png';
                        var getFdrClass = '';
                    }
                    if (item['path_display'] != "") {
                        var folderIdDelete = item['path_display'];
                    } else {
                        var folderIdDelete = item['id'];
                    }
                    if (item['path_lower'] != "undefined") {
                        var viewFileId = item['path_lower'];
                    } else {
                        var viewFileId = item['id'];
                    }
                   var folder = `<div class="sc_share_wrapper  ${getFdrClass}"  id="${resp['userId']}" data-folder-name="${ item['name']}" droType="${ item['path_lower']}'" data-folder-Id="${ item['id']}">
                                    <div class="sc_dots_img">
                                            <img src="${ baseurl + 'assets/backend/images/dots.svg' }" alt="">
                                    </div>
                                    <div class="checkbox" hidden>
                                        <input type="checkbox" id="rememberme${c}" name=""class="optionCheckBox" value="" >
                                            <label for="rememberme${c}" class="option"></label>
                                    </div> 
                                    <div class="sc_share_img sc_share_file">
                                        <img src="${icon}" alt="icon">
                                    </div>
                                    <div class="sc_share_detail">
                                        <h5>${ item['name']}</h5>
                                    </div>
                                    <div class="checkOptionMenu tbf_IconHolter option${c}" clickValue="option${c}" style="display:none;">
                                        <div class="checkBoxSelect">
                                            <a hidden href="javascript:;">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">
                                                    <path fill-rule="evenodd"d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>
                                                </svg>Copy To
                                            </a>
                                            <a  href="javascript:;">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                    <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                </svg>Download
                                            </a>
                                            <a href="javascript:;">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                    <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                </svg>Share
                                            </a>
                                            <a  href="javascript:;" class="moveFile " userId="' + id + '" fileId="${ item['id']}">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                    <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                </svg>Move
                                            </a>
                                            <a  href="javascript:;" class="DeletFileAndFolder" user_id="${resp['userId']}" folderID="${folderIdDelete }" >
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                    <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                </svg>Delete
                                            </a>
                                            <a  href="${baseurl}home/view_file/${ item['id']}/${id}" >
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                    <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                </svg>Preview
                                            </a>
                                            <a  href="javascript:;" >
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
                                                    <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>
                                                </svg>Rename
                                            </a>
                                            <a hidden href="javascript:;" >
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                    <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>
                                                </svg>Copy
                                            </a>
                                        </div>
                                    </div>
                                </div>`;
                    $('.MovingFile').append(folder);
            });
                }


        }
    });
});

$(document).on('click', '.MoveUploadFile', function(e) {
    var MoveFolderID = $(this).attr('folderid');
    var MoveFileID = $(this).attr('movefileid');
    var movefilename = $(this).attr('movefilename');
    var connectionID = $(this).attr('connectionid');
    var oldPath = $(this).attr('drotype');
    var NewFolderPath = $(this).attr('newdrotype');
    var userID = $('#user_id').val();
    $('.MoveProgressBar').show();
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            $('.progress').removeClass('hide');
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);
                    $(".progress-bar").width(percentComplete + '%');
                    $(".progress-bar").html(percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        url: baseurl + 'home/SinkMoveFileUpload',
        type: "POST",
        data: {
            'MoveFolderID': MoveFolderID,
            'MoveFileID': MoveFileID,
            'oldPath': oldPath,
            'NewFolderPath': NewFolderPath,
            'connectionID': connectionID,
            'oldUserId': userID,
            'movefilename': movefilename,
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            console.log(resp);
            if (resp.status == '1') {
                showNotifications('success',resp.smg);
                //  getSubFolderByFolderId( folderId, name = '', droType = '', type = '', id = '');
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                 showNotifications('error',resp.smg);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });


});

$(document).on('click', '.DeletFileAndFolder', function(e) {
    var drotyp = $(this).attr('drotyp');
    var folderID = $(this).attr('folderID');
    var user_id = $(this).attr('user_id');
    var fileid = $(this).attr('fileid');
   
    $.ajax({
        url: baseurl + 'home/userDeleteFolder',
        type: "POST",
        data: {
            'folderID': folderID,
            'drotyp': drotyp,
            'user_id': user_id,
            'fileid': fileid
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            console.log(resp);
            // if(resp.data.connType == 'Anon'){
            //     window.open(resp.data.url, '_blank').focus();
            // }
          
            showNotifications('success',resp.smg);
            setTimeout(function() {
                location.reload();
            }, 1000);
            console.log(resp)
            return false;
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });

});
$(document).on('click', '#ShareURLAnon', function() {
    var res = copyToClipboard(document.getElementById("copyTargetanon"));
    if (res) {
        showNotifications('success','Copy Link');
        setTimeout(function(){$('.plr_notification ').hide();},2000);  
    }
});

function copyToClipboard(elem) {
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}



//////////////////// DDownload  JS ///////////////////
$(document).on('click', '#ddownload_submit', function() {
    var key = $('#ddownload_key').val();
    $.ajax({
        url: baseurl + 'api/ddownload',
        type: "POST",
        data: {
            'key': key,
        },
        success: function(e) {
            var resp = $.parseJSON(e);
            console.log(resp);
            if (resp['status'] == '1') {
                showNotifications('success','Connection Successfully.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);  
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                 showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });

});
///////////////////// GoFile  ///////////////////////////////
// $(document).on('click', '#goFileConnection', function() {
//     $('.loader').fadeIn();
//     let formValidate = validate($(this).closest('form'));
    
//     if (formValidate == 0) {
//         var formdata = new FormData($(this).closest('form')[0] ); 
//     } else {
//         $('.loader').fadeOut();
//         return false;
//     }
//     $.ajax({
//         method: "POST",
//         url: baseurl + 'home/goFileAuth',
//         data: formdata,
//         processData: false,
//         contentType: false,
//         success: function(resp) {
//             $('.loader').fadeOut();
//             var resp = $.parseJSON(resp);
//             console.log(resp);
//             if (resp['status'] == '1') {
//                 showNotifications('success',resp.smg);
//                 setTimeout(function() {
//                   window.location.href=baseurl+"home/dashboard";
//                 }, 1000);
//             } else {
//                  showNotifications('error',resp.smg);
//                 return false;
//             }
//         },
//         error: function(resp) {
//              showNotifications('error','Something went wrong. Please, refresh the page and try again.');
//             return false;
//         }
//     });
// });

///////////////////// BackBlaze b2 ///////////////////////////////
// $(document).on('click', '#backBlazeB2Connectoin', function() {
//     $('.loader').fadeIn();
//     let formValidate = validate($(this).closest('form'));
//     if (formValidate == 0) {
//         var formdata = new FormData($(this).closest('form')[0] ); 
//     } else {
//         $('.loader').fadeOut();
//         return false;
//     }
//     $.ajax({
//         method: "POST",
//         url: baseurl + 'home/backBlazeB2Auth',
//         data: formdata,
//         processData: false,
//         contentType: false,
//         success: function(resp) {
//             $('.loader').fadeOut();
//             var resp = $.parseJSON(resp);
//             if (resp['status'] == '1') {
//                 showNotifications('success',resp.smg);
//                 setTimeout(function() {
//                    window.location.href=baseurl+"home/dashboard";
//                 }, 1000);
//             } else {
//                  showNotifications('error',resp.smg);
//                 return false;
//             }
//         },
//         error: function(resp) {
//              showNotifications('error','Something went wrong. Please, refresh the page and try again.');
//             return false;
//         }
//     });
// });

// ///////////////////// Amazon S3 ///////////////////////////////
$(document).on('click', '#amazonS3Connectoin', function() {
    var formdata = new FormData($(this).closest('form')[0]);
    $.ajax({
        method: "POST",
        url: baseurl + 'home/AmazonS3Auth',
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            var resp = $.parseJSON(resp);
            console.log(resp);
            if (resp['status'] == '1') {
                showNotifications('success',resp.smg);
                setTimeout(function() {
                    window.location.href=baseurl+"home/dashboard";
                }, 1000);
            } else {
                 showNotifications('error',resp.smg);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });

});

$(document).on('click', '.makeConnection', function() {
    $('.loader').fadeIn();
    let formValidate = validate($(this).closest('form'));
    if (formValidate == 0) {
        var formdata = new FormData($(this).closest('form')[0] ); 
        var url = $(this).closest('form').attr('action');
    } else {
        $('.loader').fadeOut();
        return false;
    }
    $.ajax({
        method: "POST",
        url: baseurl + url,
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            $('.loader').fadeOut();
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                showNotifications('success',resp.smg);
                setTimeout(function() {
                   window.location.href=baseurl+"home/dashboard";
                }, 1000);
            } else {
                 showNotifications('error',resp.smg);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }
    });
});



/////////////anonfilesConnection //////////////////////
$(document).on('click', '#anonfilesConnection', function() {
    var formdata = new FormData($(this).closest('form')[0]);
    $.ajax({
        method: "POST",
        url: baseurl + 'home/anonfiles',
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            var resp = $.parseJSON(resp);
            console.log(resp);
            if (resp['status'] == '1') {
                showNotifications('success',resp.smg);
                setTimeout(function() {
                   window.location.href=baseurl+"home/dashboard";
                }, 1000);
            } else {
                 showNotifications('error',resp.smg);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });

});

/////////////////// Google Photos Connection //////////////////
$(document).on('click', '#GooglePhotosCon', function() {
    var google_phpto_display_name = $('#displayName').val();
    var displayEmail = $('#displayEmail').val();
    let formValidate = validate($(this).closest('form'));
    if (!formValidate) {
        var formdata = new FormData($(this).closest('form')[0]); 
    } else {
        $('.loader').fadeOut();
        return false;
    }
    //
    $.ajax({
        method: "POST",
        url: baseurl + 'home/GooglePhotoCon',
        data: {'displayName' : google_phpto_display_name,'Email':displayEmail},
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                //  window.open(resp['AuthURL']).focus();
                 window.location.href=resp['AuthURL'];
            } else if(resp['status'] == '0'){
                 showNotifications('error',resp.sms);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
          
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });
    
});

////////////////// BoxConnection //////////////////
$(document).on('click', '#ConnetionBOX', function() {
    // var name = $('#name').val();
    // var email = $('#email').val();
    // var accountId = $('#accountId').val();
    // console.log(name)
    // console.log(email)
    $('.loader').fadeIn();
    let formValidate = validate($(this).closest('form'));
    if (!formValidate) {
        var formdata = new FormData($(this).closest('form')[0]); 
    } else {
        $('.loader').fadeOut();
        return false;
    } 
    $.ajax({
        method: "POST",
        url: baseurl + 'Api/box_data',
        data: formdata,
        processData: false,
        contentType: false,
        success: function(resp) {
            var resp = $.parseJSON(resp);
            if (resp['status'] == '1') {
                 window.location.href=resp['AuthURL'];
            } else if(resp['status'] == '0'){
                 showNotifications('error',resp.sms);
                 setTimeout(function(){$('.plr_notification ').hide();},2000);    
                return false;
            }
          
        },
        error: function(resp) {
             showNotifications('error','Something went wrong. Please, refresh the page and try again.');
             setTimeout(function(){$('.plr_notification ').hide();},2000);    
            return false;
        }

    });
    
});
// $(document).on('click', '#ConnetionBOXtest', function() {
//   alert('Box API Under Development ..');
// });

function readURL(input) {
    if (input.files && input.files[0]) {
        let type = input.files[0].type.split('/')
        if ( type[0] == 'image' ) {
            var reader = new FileReader();
            reader.onload = function (e) { 
                $(input).parent().find('img').attr("src",e.target.result);
            };
            reader.readAsDataURL(input.files[0]); 
        } else {
            $(input).parent().find('img').attr("src",baseurl+"assets/backend/images/file.png");
        }
    }else{
        $(input).parent().find('img').attr("src","");
    }
}

function regularExpression(type) {
        let regEpx = {
            url: /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/,
            email: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/,
            mobile: /((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/,
            youtube : /(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|dailymotion.com)\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/
        };
        return regEpx[type] ? regEpx[type] : "";
}

function validate(target){
     var check = 0;
        target.find("input , textarea , select").each(function() {
            if ($(this).hasClass("require")) {
                if ((typeof $(this).val() == "object" && isEmpty($(this).val()) == true) || (typeof $(this).val() != "object" && $(this).val().trim() == "")) {
                    check = 1;
                    $(this).addClass("ap_error").focus();
                     showNotifications('error',"You have missed out some required fields.");
                     setTimeout(function(){$('.plr_notification ').hide();},2000);  
                    return false;
                } else if (
                    (typeof $(this).val() == "object" &&
                        isEmpty($(this).val()) == true) ||
                    (typeof $(this).val() != "object" &&
                        $(this).val().trim() != "" &&
                        typeof $(this).attr("data-match") != "undefined")
                ) {
                    var fieldToMatch = $(this).attr("data-match");
                    if ($(this).val() != $("#" + fieldToMatch).val()) {
                        check = 1;
                        $(this).addClass("ap_error").focus();
                         showNotifications('error',$(this).attr("data-error"));
                         setTimeout(function(){$('.plr_notification ').hide();},2000);  
                        return false;
                    } else {
                        $(this).removeClass("ap_error");
                    }
                } else {
                    $(this).removeClass("ap_error");
                }
            }
            if (
                (typeof $(this).val() == "object" &&
                    isEmpty($(this).val()) == true) ||
                (typeof $(this).val() != "object" && $(this).val().trim() != "")
            ) {
                // let errorMsg = $(this).data('message');
                // errorMsg = (typeof errorMsg != 'undefined') ? errorMsg : 'You miss out some require field.';
                var valid = $(this).data("valid");
                let email = regularExpression("email");
                let mobile = regularExpression("mobile");
                let youtube = regularExpression("youtube");
                if (typeof valid != "undefined") {
                    if (!eval(valid).test($(this).val().trim())) {
                        $(this).addClass("ap_error").focus();
                        check = 1;
                         showNotifications('error',$(this).attr("data-error"));
                         setTimeout(function(){ location.reload(); }, 3000);
                        return false;
                    } else {
                        $(this).removeClass("ap_error");
                    }
                }
            }
        });
        return check;
}

$(document).on('click','.sc_toggle_retra',function(){
    var classcheckBox = $(this).attr('checkBOxClass');
    console.log(classcheckBox)
    // $('#'+classcheckBox).toggleClass('show_delete_popup');
    if($('#'+classcheckBox).hasClass('show_delete_popup')){
        $('#'+classcheckBox).removeClass('show_delete_popup');
    }else{
        $('.sc_delete_popupbox.show_delete_popup').removeClass('show_delete_popup');
        $('#'+classcheckBox).addClass('show_delete_popup');
    }
});
$(document).on('click', function (e) {
    if (!$(e.target).closest('.sc_toggle_retra').length) {
        $('.sc_delete_popupbox.show_delete_popup').removeClass('show_delete_popup')
    }
})


// var parts = $(location).attr('href').split("/"),// use like uri segment
// last_part = parts[parts.length-1];
// console.log(last_part);
// if (($('.ao_colorPicker').length) && (last_part == 'auto_generate_app' || last_part == 'white_label')) {
if (($('.ao_colorPicker').length) ) {
    var _this = $('.ao_colorPicker');
    var a = [];
    $('.ao_colorPicker').closest('.ao_colorpicker_dv').each(function () {
        var id = '#' + $(this).attr('data-id');
        // console.log(id);
        a.push(id)
    })
    $.each(a, function (index, id) {
        $(id).spectrum({
            preferredFormat: "hex",
            color: $(id).val(),
            showInput: true,
            showAlpha: true,
            showButtons: false,
            move: function (color) {
                var colors = color.toRgbString();
                // apps.updateColorOnChange(colors, id);
            },
            hide: function (color) {
                var formId = $(id).closest('form').attr('id');
                if (formId == 'appColorForms') {
                    var formData = new FormData($('#' + formId)[0]);
                    formData.append('appId', apps.appId);
                    $.ajax({
                        url: _this.closest('form').attr('action'),
                        method: "post",
                        data: formData,
                        formData: 1,
                        isShowLoader: 1
                    },
                        (resp) => {
                            // window.appowls.notifyMessage("success", resp.message);
                        }
                    );
                }
            }
        }).on("dragstop.spectrum", function (e, color) {
            var colors = color.toRgbString();
            apps.updateColorOnChange(colors, id);
        })
    })
}

function showNotifications(type, message){
    $('.plr_notification').removeClass('plr_success_msg');
    $('.plr_notification').removeClass('plr_error_msg');
    let img = baseurl+'assets/backend/images/'+type+'.png';
    $('.plr_happy_img img').attr('src',img);
    if( type == 'success' )
        $('.plr_yeah h5').text('Congratulations!');
    else
        $('.plr_yeah h5').text('Oops!');
    $('.plr_yeah p').text(message);
    $('.plr_notification').addClass('plr_'+type+'_msg');
}
$('.cloudBackup').on('click',function(){
    alert('will be available soon');
});
function FileExtension(){
    return  Extension = ['gz','css','CSS','xml','XML','js','JS','py','PY','php','PHP','aac','adt','adts','accdb','accde','accdr','accdt','aif','aifc','aiff','aspx','ASP','avi','bat','bin','bmp','cab','cda','csv','dif','dll','doc','docm','mkv', 'sql', 'json','docx','dot','dotx','eml','eps','exe','flv','gif','htm','html','ini','iso','jar','jpg','jpeg','m4a','mdb','mid','midi','mov','mp3','mp4','MPEG','mp4','mpeg','mpg','msi','mui','pdf','png','pot','potm','potx','ppam','pps','ppsm','ppsx','ppt','pptm','pptx','psd','pst','pub','rar','rtf','sldm','sldx','swf','sys','tif','tiff','tmp','txt','vob','vsd','vsdm','vsdx','vss','vssm','vst','vstm','vstx','wav','wbk','wks','wma','wmd','wmv','wmz','wms','wpd','wp5','xla','xlam','xll','xlm','xls','xlsm','xlsx','xlt','xltm','xltx','xps','zip','JPEG','AAC','ADT','ADTS','ACCDB','ACCDE','ACCDR','ACCDT','AIF','AIFC','AIFF','ASPX','ASP','AVI','BAT','BIN','BMP','CAB','CDA','CSV','DIF','DLL','DOC','DOCM','MKV', 'SQL', 'JSON','DOCX','DOT','DOTX','EML','EPS','EXE','FLV','GIF','HTM','HTML','INI','ISO','JAR','JPG','JPEG','M4A','MDB','MID','MIDI','MOV','MP3','MP4','MPEG','MP4','MPEG','MPG','MSI','MUI','PDF','PNG','POT','POTM','POTX','PPAM','PPS','PPSM','PPSX','PPT','PPTM','PPTX','PSD','PST','PUB','RAR','RTF','SLDM','SLDX','SWF','SYS','TIF','TIFF','TMP','TXT','VOB','VSD','VSDM','VSDX','VSS','VSSM','VST','VSTM','VSTX','WAV','WBK','WKS','WMA','WMD','WMV','WMZ','WMS','WPD','WP5','XLA','XLAM','XLL','XLM','XLS','XLSM','XLSX','XLT','XLTM','XLTX','XPS','ZIP'];
}


$(document).on('click', '.checkbox', function(e){
    e.stopPropagation();
});

function createPagination(pageNum){
    $.ajax({
        url: baseurl+'home/loadTaskList/'+pageNum,
        type: 'get',
        dataType: 'json',
        success: function(responseData){
            $('#pagination').html(responseData.pagination);
            paginateTaskListHtml(responseData.empData, responseData.record);
        }
    });
}

$(document).ready(function() {
    if( $('#pagination').length ){
	    createPagination(0);
    }
	$('#pagination').on('click','a',function(e){
		e.preventDefault(); 
		var pageNum = $(this).attr('data-ci-pagination-page');
		createPagination(pageNum);
	});
})

let paginateTaskListHtml = function (data_ar, s_no) {
    let html  = retransfer = '';
    let count = s_no;
    if (data_ar.length > 0) {
        data_ar.forEach(item => {
            count ++;
            if( item.transfer_status ==0 ){
                retransfer = `<a href="javascript:;" class="ad-btn retransfer" user_id="${item.user_id}" id="${item.id}">Re-Transfer</a>`
            }else if( item.transfer_status == 2 ){
                retransfer = `<div class="sc_task_filebox"><a href="javascript:;" class="Canceled CanceledTransfer" user_id="${item.user_id}" id="${item.id}"><i class="fa fa-times-circle" aria-hidden="true" ></i> Cancel</a></div>`
            }else{
                retransfer = ``
            }
            let state = (item.transfer_status == 1 && item.cron_status == 1)? 'done': (item.transfer_status == 0)? 'Cancelled' : 'Transferring' ;
            html += `<div class="sc_task_list_wrapper">
                        <div class="sc_task_filebox">
                            <img src="${baseurl}assets/auth/images/favicon.png">
                            <span>${count}</span>
                        </div>
                        <div class="sc_task_filebox">
                            <h5>Cloud ${item.transfer_type} : <a href="javascript:;">Task ${count}</a></h5>
                            <p>From: <a href="javascript:;">${item.from_name}</a></p>
                        </div>
                        <div class="sc_task_filebox">
                            <p>To: <a href="javascript:;">${item.to_name}</a></p>
                        </div>
                    <div class="sc_task_filebox">
                        <a href="javascript:;" class="success"><i class="fa fa-times-circle" aria-hidden="true"></i> ${state}</a>
                    </div>
                    <div class="Re-Transfer">
                        ${retransfer}
                        <div class="sc_toggle_retra" checkboxclass="popupchek${count}">
                            <span class=""><i class="fa fa-bars" aria-hidden="true"></i></span>
                                <div class="sc_delete_popupbox" id="popupchek${count}">
                                    <div class="checkBoxSelect">
                                        <a hidden="" href="javascript:;" class="" drotype="" folderidshareurl="" userid="">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M1 .5v11h10V.5H1zm8 8H3v-1h6v1zm0-2H3v-1h6v1zm0-2H3v-1h6v1z" fill-rule="evenodd" clip-rule="evenodd" fill=" "></path></svg>
                                            View Logs
                                        </a>
                                        <a hidden="" href="javascript:;" class="" drotype="" fileidshareurl="" folderidshareurl="" userid="">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M11.1 4.7h-.6c-.1-.3-.2-.7-.4-1l.5-.5c.3-.3.3-.9 0-1.2l-.7-.5c-.3-.3-.9-.3-1.2 0l-.4.4c-.3-.2-.7-.3-1-.4V.9c0-.5-.4-.9-.9-.9h-.8c-.5 0-.9.4-.9.9v.6c-.3.1-.7.2-1 .4l-.4-.4c-.3-.3-.9-.3-1.2 0l-.6.6c-.3.3-.3.9 0 1.2l.5.5c-.3.2-.4.6-.5.9H.9c-.5 0-.9.4-.9.9v.9c0 .5.4.9.9.9h.6c.1.4.2.7.4 1l-.4.3c-.3.3-.3.9 0 1.2l.6.6c.3.3.9.3 1.2 0l.4-.4c.3.2.7.3 1 .4v.6c0 .5.4.9.9.9h.9c.5 0 .9-.4.9-.9v-.6c.4-.1.7-.2 1-.4l.4.4c.3.3.9.3 1.2 0l.6-.6c.3-.3.3-.9 0-1.2l-.4-.4c.2-.3.3-.6.4-1h.6c.5 0 .9-.4.9-.9v-.8c-.1-.5-.5-.9-1-.9zM6 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z" fill-rule="evenodd" clip-rule="evenodd" fill=" "></path></svg>
                                            Options
                                        </a>
                                        <a hidden="" href="javascript:;" class=" " filename="faq.jpg" userid="" fileid="" drotype="undefined">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M8.5 6h-2v1h2V6zm0 2h-2v1h2V8zm-3 0h-2v1h2V8zm0-2h-2v1h2V6zM11 1H9.5v2h-7V1H1c-.3 0-.5.2-.5.5v10c0 .3.2.5.5.5h10c.3 0 .5-.2.5-.5v-10c0-.3-.2-.5-.5-.5zm-1.5 9h-7V5h7v5zm-1-10h-5v2h5V0z" fill-rule="evenodd" clip-rule="evenodd" fill=" "></path></svg>
                                            Schedule
                                        </a>
                                        <a href="javascript:;" class="DeleteCloudtransferData" user_id="${item.user_id}" id="${item.id}">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"></path>
                                            </svg>
                                            Delete
                                        </a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`
        });
    }else{
        html = `<div class="sc_task_list_wrapper">
                    <div class="no_dash_box ">
                        <img src="https://selfcloud.co.in/assets/backend/images/no_file.png" alt="">
                        <h4>No Task Are Available</h4>
                    </div>
                </div>`
    }
    

    $('.sc_task_list').html(html)


}