toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
$(document).ready(function(){

  // datatable
  if($('.ad_datatable_wrapper').length){
    let dataSrc = $('.ad_datatable_wrapper .ad_table').data('source');
    var adDatatable = $('.ad_datatable_wrapper .ad_table').DataTable({
        responsive: true,
        ajax: dataSrc
    });
    //datatable search
    $('.ad_datatableSearch').keyup(function(){
        adDatatable.search($(this).val()).draw() ;
    })
  }
  

  // profile dorpdown
  $(document).on('click','.ad_profile_wrapper',function(){
      $(this).find('.ad_profileDropdown').slideToggle()
  })

  // body click
  $(document).on('click', function(e){
      if(!$(e.target).closest('.ad_profile_wrapper').length){
         $('.ad_profileDropdown').slideUp()
      }
  })

  // toggle menu
  $(document).on('click','.ad_toggle', function(){
    $(this).closest('.admin_main_wrapper').toggleClass('ad_mainMenuOpen')
    $(this).closest('.admin_main_wrapper').removeClass('ad_submenuOpen')
  })
  $(document).on('click','.ad_main_menu>ul>li>a[href="#"]', function(){
     $(this).closest('.admin_main_wrapper').toggleClass('ad_submenuOpen')
  })

  $(document).on('click','.ad_plus',function(){
    let html = $('.repDiv').html();
    $('.parentDiv').append(html);
  });

  $(document).on('change','#imgfile',function(){
    if ( $(this).val() != '' )
      $(this).addClass('validate');
  });

})


function submitNiche(){
let tempArr = [];
let descArr = [];
let widgetArr = [];
let err = 0;
$('.nicheTitle').each(function(){
  if( $(this).val() != '' ){
    if( $(this).val().length < 21 )
      tempArr.push($(this).val());
    else{
        showNotifications('error', 'Category title should not be more than 20 Characters.');
        setTimeout(function(){$('.plr_notification ').hide();},2000);
    //   toastr.error('Category title should not be more than 20 Characters.');
      $(this).css('border-color','red');
      $(this).focus();
      err++;
    }
  }
});
$('.nicheDesc').each(function(){
  if( $(this).val() != '' ){
    descArr.push($(this).val());
  }
});
$('.widgetTitle').each(function(){
  if( $(this).val() != '' ){
    widgetArr.push($(this).val());
  }
});
if( err == 0 ) {
  $.ajax({
    url: baseurl + 'admin/submitNiche', 
    type: "POST",             
    data: {'title':tempArr,'desc':descArr,'widget':widgetArr},      
    success: function(e) {
        if(e == 1)
            // toastr.success('Category saved successfully.');
            showNotifications('success', 'Category saved successfully.');
        else
            showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
            // toastr.error('Something went wrong. Please, refresh the page and try again.');
          
        setTimeout(function(){  window.location.href = baseurl + "admin/categories"; }, 3000);
        setTimeout(function(){$('.plr_notification ').hide();},2000);
    }
  });
}
else{
  setTimeout(function(){ $('.nicheTitle').css('border-color',''); }, 5000);
}
}


function submitArticles(){
  var art_id = $('#art_id').val();
  var err = 0;
  let obj = {};
  
  $('.form-control').each(function(){
    var idd = $(this).attr('id');
    var v = $.trim($(this).val());
    if( v != '' )
      obj[idd] = v;
    else
      err++;
  });
  if( art_id == 0 )
    var str = 'Article added successfully';
  else
    var str = 'Article updated successfully';
 
  if( err != 0 ){
    // toastr.error('You can\'t leave the fields empty.')
    showNotifications('error', 'You can\'t leave the fields empty..');
    setTimeout(function(){$('.plr_notification ').hide();},2000);
  }
  else{
    obj.art_id = art_id;
    
    $.ajax({
      url: baseurl + 'admin/articles/postreq', 
      type: "POST",             
      data: obj,      
      success: function(e) {
          if(e == 1)
            //   toastr.success(str);
              showNotifications('success', str);
              
          else
            //   toastr.error('Something went wrong. Please, refresh the page and try again.');
              showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
          
          setTimeout(function(){  window.location.href = baseurl + "admin/articles"; }, 3000);
          setTimeout(function(){$('.plr_notification ').hide();},2000);
      }
    });
  }
}


function submitNiche_com(){
  let tempArr = [];
  let err = 0;
  $('.nicheTitle').each(function(){
    if( $(this).val() != '' ){
      if( $(this).val().length < 21 )
        tempArr.push($(this).val());
      else{
        // toastr.error('Sharing Niche title should not be more than 20 Characters.');
        showNotifications('error', 'Sharing Niche title should not be more than 20 Characters.');
        
        $(this).css('border-color','red');
        $(this).focus();
        err++;
        
      }
    }
  });
  if( err == 0 ) {
    $.ajax({
      url: baseurl + 'admin/submitNicheCom', 
      type: "POST",             
      data: {'title':tempArr},      
      success: function(e) {
          if(e == 1)
            //   toastr.success('Sharing Niche saved successfully.');
              showNotifications('success', 'Sharing Niche saved successfully.');
          else
            //   toastr.error('Something went wrong. Please, refresh the page and try again.');
              showNotifications('error', 'Something went wrong. Please, refresh the page and try again.');
            
          setTimeout(function(){  window.location.href = baseurl + "admin/com_niche"; }, 3000);
      }
    });
  }
  else{
    setTimeout(function(){ $('.nicheTitle').css('border-color',''); }, 5000);
  }
  }

function validateAndSubmit(pageType){
var tempArr = {};
let err = 0;
$('.form-control').each(function(){
  let labelText = $(this).parents('div[class="form-group"]').children('label').text();
  let inputValue = $(this).val();
  let URLpattern = new RegExp('^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&amp;a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$','i');
  $(this).css('border-color','');

  if( $(this).hasClass('validate') ) {
    if (err == 0) {
      if (inputValue == '') {
        // toastr.error('Please, enter the ' + labelText + '.');
        showNotifications('error', labelText + 'Please, enter the ' + labelText + '.');
        $(this).css('border-color','red');
        $(this).focus();
        err++;
      }
    }

    if (err == 0) {
      if ($(this).data('maxlength') !== undefined) {
        if( inputValue.length > $(this).data('maxlength') ){
          let validLength = parseInt($(this).data('maxlength')) - 1;
        //   toastr.error(labelText + ' should not be more than '+ validLength +' Characters.');
          showNotifications('error', labelText + 'should not be more than '+ validLength +' Characters.');
          $(this).css('border-color','red');
          $(this).focus();
          err++;
        }
      }
    }

    if (err == 0) {
      if ($(this).data('minlength') !== undefined) {
        if( inputValue.length < $(this).data('minlength') ){
          let validLength = parseInt($(this).data('minlength'));
          showNotifications(labelText + ' should not be less than '+ validLength +' Characters.');
        //   toastr.error(labelText + ' should not be less than '+ validLength +' Characters.');
          $(this).css('border-color','red');
          $(this).focus();
          err++;
        }
      }
    }

    if (err == 0) {
      if ($(this).data('url') !== undefined) {
        if ( !URLpattern.test(inputValue) ) {
        //   toastr.error('Please, enter the valid URL for ' + labelText + '.');
          showNotifications('Please, enter the valid URL for ' + labelText + '.');
          $(this).css('border-color','red');
          $(this).focus();
          err++;
        }
      }
    }

    if (err == 0) {
      if ($(this).data('isselect') !== undefined) {
        if ( inputValue == 0 ) {
        //   toastr.error('Please, choose valid option for ' + labelText + '.');
          showNotifications('Please, choose valid option for ' + labelText + '.');
          $(this).css('border-color','red');
          $(this).focus();
          err++;
        }
      }
    }

  }
  
  tempArr[$(this).attr('name')] = $(this).val();
  setTimeout(function(){$('.plr_notification ').hide();},2000);

  
});
if( err == 0 ) {
  if( $('#uploadForm').length > 0 ){
    // toastr.success('Upload Started Wait......');
    showNotifications('success', 'Upload Started Wait......');
    setTimeout(function(){$('.plr_notification ').hide();},2000);
    let myForm = document.getElementById('uploadForm');
    var obj = new FormData(myForm);
    $.ajax({
      url: baseurl + 'admin/submitCommonData', 
      type: "POST",             
      data: obj,
      contentType: false,
      cache: false,
      processData:false,  
      success: function(e) {
        console.log(e)
        handlingSubmitResponse(e);
      }
    });
  }
  else{
    var obj = {};
    obj.tempArr = tempArr;
    obj.pageType = pageType;
    obj.uniqeid = $('input[name="uniqeid"]').val();

    $.ajax({
      url: baseurl + 'admin/submitCommonData', 
      type: "POST",             
      data: obj, 
      success: function(e) {
          handlingSubmitResponse(e);
      }
    });
  }
}
}

function handlingSubmitResponse(e){
  if(e == 1){
    if( $('input[name="uniqeid"]').val() == 0 )
        showNotifications('success', 'Data saved successfully.');
    //   toastr.success('Data saved successfully.');
    else
    //   toastr.success('Data updated successfully.');
        showNotifications('success', 'Data saved successfully.');
  }
  else
    toastr.error(e);
    showNotifications('error', e);
    setTimeout(function(){$('.plr_notification ').hide();},2000);
  //setTimeout(function(){ location.reload(); }, 3000);
}

function updateData(id,type){
var objData = {};
if( type == 'niche' ){
  var u = 'submitNiche';
  objData.id = id;
  objData.title = $('#nicheTitle').val();
  objData.desc = $('#nicheDescription').val();
  objData.widget = $('#nicheWidget').val();
}

$.ajax({
  url: baseurl + 'admin/'+u, 
  type: "POST",             
  data: objData,
  success: function(e) {
      if(e == 1)
          showNotifications('success','Data updated successfully.');
      else
          showNotifications('error','Something went wrong. Please, refresh the page and try again.');
        
      setTimeout(function(){ location.reload(); }, 3000);
  }
});

$('#editDataModal').modal('hide');
}

$(document).on('click','.deleteUser',function(){
    var id = $(this).attr('uid');
    $('.deleteData').attr('id',id);
});
$(document).on('click','.deleteData',function(){
   var id = $(this).attr('id');
  var objData = {};
  objData.deleteid = id;
  objData.delete = 'delete';
  $.ajax({
    url: baseurl + 'admin/submitUserRecords', 
    type: "POST",             
    data: objData,
    success: function(e) {
        var resp = $.parseJSON(e);
        if(resp.status == 1){
            // toastr.success(resp.smg);
            showNotifications('success',resp.smg);
            setTimeout(function(){ location.reload(); }, 3000);
        }else{
            showNotifications('error',resp.smg);
            setTimeout(function(){$('.plr_notification ').hide();},2000);
            // toastr.error(resp.smg);
        }
    },
    error: function(resp) {
            // toastr.error('Duplicate User Please Change Email');
            showNotifications('error','Duplicate User Please Change Email');
            setTimeout(function(){$('.plr_notification ').hide();},2000);
            return false;
    }
  });
});

// upload file js   
$(document).on('click', '.plr_uploade_label', function(e){  
    $(this).parent().find('.plr_fileupload').click();  
    var image_uploads= $(this).parent().find('.priViewImg');
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
               image_uploads.attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".plr_fileupload").change(function(){
        readURL(this);
    });
}); 
 $(document).on('click', '.saveProfile', function() {
    var formdata = new FormData($(this).closest('form')[0]);
        $.ajax({
            method: "POST",
            url: baseurl + 'admin/adminProfile', 
            data: formdata,
            processData: false,
            contentType: false,
            success: function(resp) {
                var resp = $.parseJSON(resp);
                if (resp['status'] == '1') {
                    showNotifications('success','Profile updated successfully.');
                    setTimeout(function(){ location.reload(); }, 3000);
                } else if (resp['status'] == '2') {
                    showNotifications('success','Profile updated successfully.');
                    setTimeout(function(){$('.plr_notification ').hide();},2000);
                    return false;
                } else {
                     showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                     setTimeout(function(){$('.plr_notification ').hide();},2000);
                    return false;
                }
                $('.edu_preloader').fadeOut();
            },
            error: function(resp) {
                showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);
                return false;
            }
        });
    
});
 $(document).on('click', '.submitUserRecords', function() {
    var formdata = new FormData($(this).closest('form')[0]);
        $.ajax({
            method: "POST",
            url: baseurl + 'admin/submitUserRecords', 
            data: formdata,
            processData: false,
            contentType: false,
            success: function(resp) {
                var resp = $.parseJSON(resp);
                console.log(resp.smg);
                if (resp['status'] == '1') {
                    showNotifications('success',resp.smg);
                  setTimeout(function(){  window.location.href = baseurl + resp['url']; }, 3200);
                } else if (resp['status'] == '2') {
                    showNotifications('success',resp['smg']);
                    setTimeout(function(){$('.plr_notification ').hide();},2000);
                    return false;
                } else {
                     showNotifications('error',resp['smg']);
                     setTimeout(function(){$('.plr_notification ').hide();},2000);
                    return false;
                }
                $('.edu_preloader').fadeOut();
            },
            error: function(resp) {
                showNotifications('error','Something went wrong. Please, refresh the page and try again.');
                setTimeout(function(){$('.plr_notification ').hide();},2000);
                return false;
            }
        });
    
});
function submitUserRecords(){
var u_id = $('#u_id').val();
var err = 0;
let obj = {};
if( u_id == 0 ){
  $('.form-control').each(function(){
    var idd = $(this).attr('id');
    var v = $.trim($(this).val());
    if( v != '' )
      obj[idd] = v;
    else
      err++;
  });
  var str = 'User added successfully';
}
else{
  $('.form-control').each(function(){
    var idd = $(this).attr('id');
    var v = $.trim($(this).val());
    if( v != '' )
      obj[idd] = v;
    else if(idd != 'u_password')
      err++;
  });
  var str = 'User updated successfully';
}
if( err != 0 ){
  showNotifications('error','You can\'t leave the fields empty.')
  setTimeout(function(){$('.plr_notification ').hide();},2000);
}
else{
  obj.u_id = u_id;
  
  $.ajax({
    url: baseurl + 'admin/submitUserRecords', 
    type: "POST",             
    data: obj,      
    success: function(e) {
        if(e == 1)
            showNotifications('success',str);
        else
            showNotifications('error','Something went wrong. Please, refresh the page and try again.');
        
        // setTimeout(function(){  window.location.href = baseurl + "admin/users"; }, 3000);
            setTimeout(function(){$('.plr_notification ').hide();},2000);
    }
  });
}
}

function submitCommonData(pageType){
  let tempArr = [];
  let err = 0;
  $('.dataTitle').each(function(){
    if( $(this).val() != '' ){
      if( $(this).val().length < 251 )
        tempArr.push($(this).val());
      else{
        showNotifications('error','It should not be more than 250 Characters.');
        setTimeout(function(){$('.plr_notification ').hide();},2000);
        $(this).css('border-color','red');
        $(this).focus();
        err++;
      }
    }
      //tempArr.push($(this).val());
  });

  if( err == 0 ) {
    let obj = {};
    obj.title = tempArr;
    obj.pageType = pageType;
    obj.nicheid = $('#nicheid').val();
    $.ajax({
      url: baseurl + 'admin/com_submitCommonData', 
      type: "POST",             
      data: obj,      
      success: function(e) {
          if(e == 1)
              showNotifications('success','Data saved successfully.');
          else
              showNotifications('error','Something went wrong. Please, refresh the page and try again.');
          
          setTimeout(function(){ location.reload(); }, 3000);
      }
    });
  }
  else{
    setTimeout(function(){ $('.nicheTitle').css('border-color',''); }, 5000);
  }
}


function generatestring(type) {
  let length = 8;
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {  
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  $('#'+type).val(result);
}
function showNotifications(type, message){
    console.log(3612)
    $('.plr_notification').removeClass('plr_success_msg');
    $('.plr_notification').removeClass('plr_error_msg');
    let img = baseurl+'assets/backend/images/'+type+'.png';
    $('.plr_happy_img img').attr('src',img);
    if( type == 'success' )
        $('.plr_yeah h5').text('Congratulations!');
    else
        $('.plr_yeah h5').text('Oops!');
    $('.plr_yeah p').text(message);
    $('.plr_notification').addClass('plr_'+type+'_msg');
}