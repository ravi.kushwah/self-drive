<?php

namespace App\Controllers;

class Ajax extends BaseController
{		
    public function index(){
		die('There is nothing here..');
	}
	
	function getBotAnswers(){
		if( !isset($_POST) || !isset($_POST['post_unique_key']) )
			die('Unauthorize Access!!');

		if( $_POST['botCounter'] == 2){
			// Get Website Data
			$url = $_POST['botTextField'];
			$checkMeta = @get_meta_tags($url);
			$metas = $checkMeta === false ? array() : get_meta_tags($url);
			
			$metaDesc = isset($metas['description']) ? ( $metas['description'] != "" ? $metas['description'] : "" ) : "" ;

			if( $metaDesc != "" )
				$responseText = $metas['description'];
			else
				$responseText = "webifybot";
			
			$browserResponse['status'] = "success" ;
			$browserResponse['bottext'] = $responseText ;
			$browserResponse['session'] = array('firsttime',1);
			return json_encode($browserResponse);
			die();
		}
		else{
			$checkPreviousAns = $this->model->get_data('bot_answers',array('user_id'=> $_POST['userId'] ), 'id');
			$bot_answers = json_decode($_POST['userResponse'],true);
			$bot_answers['user_id'] = $_POST['userId'];

			if(empty($checkPreviousAns)){
				$this->model->put_data( 'bot_answers' , $bot_answers);
				$location = $bot_answers['countries'];
				$location = explode(',',$location);
				$cityname = $location[0].' , '.$location[1];
				$country = substr( trim($location[2]) , 0 , 2);

				$keyword = $bot_answers['sectors'];
				$searchQuery = array(
					'location'	=> $country,
					'cityname'	=> $cityname,
					'keyword'	=> $keyword,
				);
				$response = $this->initiateSearchLeads($searchQuery);
				if(isset($response['searchid'])) {
					$dArray = array(
						'user_id'	=>	$_POST['userId'],
						'date_added'	=>	date("Y-m-d h:m:s"),
						'campaign_name'	=>	$bot_answers['sectors'].' - campaign',
						'keywords'	=>	$keyword,
						'location'	=>	$cityname,
						'country'	=>	$country,
						'search_id'	=>	$response['searchid'],
						'wait_time'	=>	$response['wait_seconds']
					);
					$this->model->put_data( 'campaign_list' , $dArray);
				}
			}
			else
				$this->model->set_data( 'bot_answers' , $bot_answers , array('id'=>$checkPreviousAns[0]['id']));
			
			$this->model->set_data( 'usertbl' , array('u_firsttime'=>1) , array('u_id'=>$_POST['userId']) );
			$browserResponse['status'] = "success" ;
			$browserResponse['message'] = 'Great! We are preparing your dashboard.' ;
			$browserResponse['redirect'] = '/campaigns' ;
            $browserResponse['session'] = array('firsttime',1);
			return json_encode($browserResponse);
			die();
		}
	}

	function initiateSearchLeads($searchQuery=''){
		if(!empty($searchQuery)){
			$ch = curl_init();  
			curl_setopt($ch, CURLOPT_URL, 'https://dash.d7leadfinder.com/app/api/search/?keyword='.$searchQuery['keyword'].'&country='.$searchQuery['location'].'&location='.$searchQuery['cityname'].'&key='.SEARCH_KEY.'');  
			curl_setopt($ch, CURLOPT_HEADER, FALSE); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$response = curl_exec($ch); 
			curl_close($ch);
			return json_decode($response,true);
		}
		die();
	}

	function createCampaign(){
		if( !isset($_POST) || !isset($_POST['post_unique_key']) )
			die('Unauthorize Access!!');

		$checkCampaigns = $this->model->get_data('campaign_list',array('user_id'=> $_POST['userId'] ), 'id',array(1));
		if(empty($checkCampaigns)){
			$getBotAnswers = $this->model->get_data('bot_answers',array('user_id'=> $_POST['userId'] ), 'countries,sectors');
			if(empty($getBotAnswers)){
				$this->model->set_data( 'usertbl' , array('u_firsttime'=>0) , array('u_id'=>$_POST['userId']) );
				$browserResponse['status'] = "error" ;
				$browserResponse['message'] = 'It seems something went wrong.' ;
				$browserResponse['redirect'] = '/welcome-on-board' ;
				$browserResponse['session'] = array('firsttime',0);
				return json_encode($browserResponse);
				die();
			}
			else
				$preSearchQuery = $getBotAnswers[0];
		}
		else
			$preSearchQuery = $_POST;

		$location = $preSearchQuery['countries'];
		$location = explode(',',$location);
		$cityname = $location[0].' , '.$location[1];
		$country = substr( trim($location[2]) , 0 , 2);

		$keyword = $preSearchQuery['sectors'];
		$searchQuery = array(
			'location'	=> $country,
			'cityname'	=> $cityname,
			'keyword'	=> $keyword,
		);

		$insertArray = array(
			'keywords'	=>	$keyword,
			'location'	=>	$cityname,
			'country'	=>	$country
		);

		// Check for same campaign with same search terms
		$sameCampaign = $this->model->get_data('campaign_list', $insertArray , 'search_id,wait_time',array(1),"date_added DESC");
		if(empty($sameCampaign)) {
			$response = $this->initiateSearchLeads($searchQuery);
			if(isset($response['searchid'])) {
				$insertArray['search_id']=$response['searchid'];
				$insertArray['wait_time']=$response['wait_seconds'];
			}
		}
		else{
			$insertArray['search_id']=$sameCampaign[0]['search_id'];
			$insertArray['wait_time']=$sameCampaign[0]['wait_time'];
		}
		$insertArray['user_id']=$_POST['userId'];
		$insertArray['date_added']=date("Y-m-d h:m:s");
		$insertArray['campaign_name']=$_POST['campaign_name'];
		
		$this->model->put_data( 'campaign_list' , $insertArray);

		$browserResponse['status'] = "success" ;
		$browserResponse['message'] = 'Congratulation! Your campaign got created. We will notify you once we find leads for you.' ;
		$browserResponse['redirect'] = 'reload' ;
		return json_encode($browserResponse);
		die();
	}	
}