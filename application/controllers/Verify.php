<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Verify extends CI_Controller {
	public $isAppPreview = 0;
	function __Construct(){
		parent :: __Construct();
		// echo '<pre>';
		// print_r($this->agent);
		// exit;
		if(!in_array($_SERVER['HTTP_HOST'] , MAIN_DOMAIN_LIST)){
	        echo 'hello';
	     print_r($_SESSION['wlDetails']); exit;   
	    }

		if(isset($_GET['mode']) && ($_GET['mode'] == 'standalone' || $_GET['mode'] == 'mobileApp')){
			$_SESSION['siteAsApp'] = 1;
		}
	}
	public $respStatus = 0;
    public $respMessage = 'Something went wrong.';
    public $respData = array();

    private function show_result(){
        echo json_encode(array(
            'status' => $this->respStatus,
            'message' => $this->respMessage,
            'data' => $this->respData
        ));
        die();
    }
	function index(){
	//  echo"<pre>";print_r($_SESSION);
		$checkApp = $this->my_model->select_data(array(
			'field' => 'id,status',
			'table' => 'user',
			'where' => array(
				'wl_domain' => $_SERVER['HTTP_HOST'],
			),
			'limit' => 1,
			// 'join' => [
			// 	'user' , 'user.id =  apps.user_id',
			// ]
		)); 

		if(!empty($checkApp)){
			if($checkApp[0]['status']){
				// continue;
			}else{

				echo 'Access denied!';
			}
		}else{
			$this->live_site();
		}
		
	}

	function live_site($currentPage = ''){
		$checkApp = $this->my_model->select_data(array(
			'field' => 'apps.*, user.plan, user.per_month_installation,user.parent,external_script',
			'table' => 'apps',
			'where' => array(
				'domain_name' => $_SERVER['HTTP_HOST'],
				'apps.status' => 1,
				'user.status' => 1,
			),
			'limit' => 1,
			'join' => [
				'user' , 'user.id =  apps.user_id',
			]
		)); 

		if(!empty($checkApp)){
			$this->loadPages([
				'applicationData' => $checkApp[0],
				'currentPage' => $currentPage,
				'previewType' => 'application', 
				'menuUrl' => base_url(), 
				'ratingDetails' => json_encode($this->checkAppRatings($checkApp[0]['id'])),
			]);
		}else{
			// echo 'Bad Request.  Website is no longer available :(';
			echo $this->load->view('404' , '' , true);
		}
	}
	function app_preview($uniqueId , $pageSlug = ''){
		$this->isAppPreview = 1;
		$this->template_preview($uniqueId , $pageSlug);
	}
	function template_preview($uniqueId , $pageSlug = ''){
		$checkTemplate = $this->my_model->select_data(array(
			'field' => 'apps.*,user.plan,user.parent,per_month_installation',
			'table' => 'apps',
			'where' => array(
				'apps.unique_id' => $uniqueId,
			),
			'limit' => 1,
			'join' => [
				'user' , 'user.id =  apps.user_id',
			]
		)); 

		if(!empty($checkTemplate)){
			// print_r($checkTemplate[0]);
			$this->loadPages([
				'applicationData' => $checkTemplate[0],
				'currentPage' => $pageSlug,
				'previewType' => $this->isAppPreview == 1?'app-preview':'template', 
				'menuUrl' => base_url(($this->isAppPreview == 1?'app':'template').'-preview/'.$uniqueId.'/'), 
				'ratingDetails' => json_encode($this->checkAppRatings($checkTemplate[0]['id'])),
			]);
		}else{
			echo 'Bad Request. (template not found)';
		}
	}

	private function loadPages($params){
		// echo '<pre>';
		// print_r($params);
		// die;
		// print_r($_SESSION);
		extract($params);
		$appId = $applicationData['id'];
		$_SESSION['app_id'] = $appId;
		$_SESSION['appUniqueId'] = $applicationData['unique_id'];
		// if(!isset($_SESSION['appCurrency'])){
			$checkAppDetail = $this->my_model->select_data([
				'table' => 'apps_details use index(app_id)',
				'where' => [ 'app_id' => $appId],
				'field' => 'currency,timezone,install_data,is_login,is_registration,is_email_verification,is_cookie_consent,cookie_consent_msg',
				'limit' => 1
			]);

			if(!empty($checkAppDetail)){
				$appCurData = explode('-' , $checkAppDetail[0]['currency']);
				$_SESSION['appCurrency'] = $appCurData[1];
				$_SESSION['appCurrencyCode'] = $appCurData[0];
				$_SESSION['isEnableLogin'] = $checkAppDetail[0]['is_login'];
				$_SESSION['isEnableRegistration'] = $checkAppDetail[0]['is_registration'];
				$_SESSION['isEnableEmailVerification'] = $checkAppDetail[0]['is_email_verification'];
				$_SESSION['appVendorId'] = $applicationData['user_id'];
				if($checkAppDetail[0]['timezone'] != ''){
					$_SESSION['appTimezone'] = $checkAppDetail[0]['timezone'];
				}
			}
		// }
// 		if(isset($_GET['test'])){
// 		    echo 'currentPage : '.$currentPage;
// 			print_r($_SESSION);
// 		}
		// print_r($_SESSION);
		if(isset($_SESSION['appTimezone'])){ //set app timezone 
			date_default_timezone_set($_SESSION['appTimezone']);
		}
		// echo date('Y-m-d H:i:s');
		$userId = $applicationData['user_id'];
		$appPath = MY_APP_PATH."user_".$userId.'/app_'.$appId."/";
        $checkPage = [];
		$isProfilePage = ($currentPage == 'profile-'.$applicationData['unique_id'])?1:0;
		$isCartPage = ($currentPage == 'cart-'.$applicationData['unique_id'])?1:0;
		$isCheckOutPage = ($currentPage == 'checkout-'.$applicationData['unique_id'])?1:0;
		$isSubscriptionPage = ($currentPage == 'subscriptions-'.$applicationData['unique_id'])?1:0;
		$isOrderPage = ($currentPage == 'order-'.$applicationData['unique_id'])?1:0;
		$isUserSubscriptionPage = ($currentPage == 'subscription-'.$applicationData['unique_id'])?1:0;
		$paymentStatusPage = ($currentPage == 'status-'.$applicationData['unique_id'])?1:0;
		$manageOrderPage = ($currentPage == 'manage_order-'.$applicationData['unique_id'])?1:0;
		$notificationPage = ($currentPage == 'notification-'.$applicationData['unique_id'])?1:0;
		$bookingsPage = ($currentPage == 'bookings-'.$applicationData['unique_id'])?1:0;
		$loginPage = ($currentPage == 'login-'.$applicationData['unique_id'])?1:0;
		$changePasswordPage = ($currentPage == 'reset-password-'.$applicationData['unique_id'])?1:0;
		$emailVerificationPage = ($currentPage == 'email-verification-'.$applicationData['unique_id'])?1:0;
		$isPrivacyPolicyPage = ($currentPage == 'privacy-policy-'.$applicationData['unique_id'])?1:0;
		// if(isset($_GET['test'])){
		// 	echo '<?br>'.'email-verification-'.$applicationData['unique_id'].'</br>'.$currentPage.$isCheckOutPage;exit;
		// }
		$pageFields = 'slug,header_script,footer_script,after_login,after_purchase,product_id,after_subscription_purchase,subscription_id';
		if(trim($currentPage) == ''){
			$checkPage = $this->my_model->select_data(array( //check the default page
				'field' => $pageFields,
				'table' => 'app_pages use INDEX(app_id)',
				'where' => 'app_id = "'.$appId.'" AND is_default = 1',
				'limit' => 1,
			));
			if(empty($checkPage)){
			    $checkPage = $this->my_model->select_data(array( //check the default page
    				'field' => $pageFields,
    				'table' => 'app_pages use INDEX(app_id)',
    				'where' => 'app_id = "'.$appId.'"',
    				'limit' => 1,
    				'order' => ['page_order' , 'ASC']
    			));
			}
			// echo "<pre>";
            // print_r($checkPage); exit;
			$currentPage = (!empty($checkPage))?$checkPage[0]['slug']:$currentPage;
			$isHomePage = 1; 
		}else{
		    $checkPage = $this->my_model->select_data(array( //check the default page
				'field' => $pageFields,
				'table' => 'app_pages use INDEX(app_id)',
				'where' => 'app_id = "'.$appId.'" AND slug = "'.$currentPage.'"',
				'limit' => 1,
			));
			$isHomePage = 0;
		}	
		// echo $currentPage; exit;
		$CampaHtml = '';
		//it's use for registration/profile/add-sheeping-address
		$country = $this->my_model->select_data([
			'table' => 'country',
			'field' => 'id, name, phonecode'
		]);
		$userPath = 'assets/apps/user_'.$userId.'/';
		$imgPath = $userPath.'app_'.$applicationData['id'].'/draft/icon/'; 
		$this->load->library('live_app');
		$pageData = !empty($checkPage)?$checkPage[0]:[];
		// echo '<pre>';
		// print_r($pageData); exit;
		/**check restriction for show the install button start */
		$isInstallActive = 1;
		$isPoweredByLabel = 1;
		
		if($applicationData['parent'] != 0){//remove branding for WL and Commercial user's Apps
			$isPoweredByLabel = 0;
		}
		
        if(isset($applicationData['plan'])){
			$getPlan = ($applicationData['plan'] != '' ? json_decode($applicationData['plan'], true) : []);
            if(in_array('2', $getPlan)  //check lite plan
			&& count($getPlan) == 1 //check user have only lite plan not another 
			&& $applicationData['per_month_installation'] >= $this->common->commercialInstallLimit){ //check highest install count as per plan
				$isInstallActive = 0;
            }

            if(!empty(array_intersect(["3","6","7","9","11"], $getPlan))){
				$isPoweredByLabel = 0;
			}
        }

		/**check restriction for show the install button end **/
		$data = [
			'userPath' => $userPath,
			'appData' => $applicationData,
			'menuUrl' => $menuUrl,
			'pageData' => $pageData,
			'ratingDetails' => $ratingDetails,
			'countryData' => $country,
			'previewType' => $previewType,
			'appLogo' => ($applicationData['logo'] != '')?base_url($imgPath.$applicationData['logo'].'?'.time()):$this->common->siteLogo,
			'imgPath' => $imgPath,
			'menus' => $this->live_app->menuManager($applicationData , $menuUrl , $previewType),
			'isHomePage' => $isHomePage,
			'externalScript' => $applicationData['external_script'] != ''?json_decode($applicationData['external_script'] , true):[],
			'isInstallActive' => $isInstallActive,
			'appFolderPath' => $appPath,
		];
		// echo "<pre>";
		// echo"<pre>";print_r($checkAppDetail); exit;
		if($isHomePage == 1){//get install bar data 
		}
		
		$data['intallPopupData'] = !empty($checkAppDetail) && $checkAppDetail[0]['install_data'] != ''?json_decode($checkAppDetail[0]['install_data'] , true):[];
		$data['cookieConsentData'] = !empty($checkAppDetail) && $checkAppDetail[0]['is_cookie_consent'] == 1?$checkAppDetail[0]['cookie_consent_msg']:'';

		if(isset($pageData['after_login']) && ($pageData['after_login'] == 0 || ($pageData['after_login'] == 1 && isset($_SESSION['isLogin']))) || $isCartPage  || $isCheckOutPage || $isProfilePage || $isOrderPage || $paymentStatusPage || $manageOrderPage || $notificationPage || $bookingsPage || $loginPage || $changePasswordPage || $emailVerificationPage || $isSubscriptionPage || $isUserSubscriptionPage || $isPrivacyPolicyPage){ //check page for login user
			if($currentPage != '' && !empty($checkPage) || $isProfilePage || $isCartPage || $isCheckOutPage || $isOrderPage || $paymentStatusPage || $manageOrderPage || $notificationPage || $bookingsPage || $loginPage || $changePasswordPage || $emailVerificationPage || $isSubscriptionPage || $isUserSubscriptionPage || $isPrivacyPolicyPage){
				if($isProfilePage){ //check profile page
					$this->check_url_for_login($menuUrl , 'profile-');
					$data['pageTitle'] =  'Profile';
					$data['loyaltyPoint'] = $this->my_model->select_data([
					    'table' => 'manage_loyalty_points use index(user_id)',
					    'field' => 'points',
					    'where' => [
					        'user_id' => $_SESSION['id']
					    ],
					    'limit' => 1
					]);
					$CampaHtml = $this->load->view('templates/profile' , $data , true);
				}else if($isCartPage){
					$data['pageTitle'] =  'Cart';
					$CampaHtml = $this->load->view('templates/cart' , $data , true);
				}else if($isPrivacyPolicyPage){
					$data['pageTitle'] =  'Privacy Policy';
					$CampaHtml = '';
					$privacyPath = $appPath.'privacy-policy.html';
					if(file_exists($privacyPath)){
						
						$CampaHtml = '<div class="container">
										<div class="pt-5">
											'.file_get_contents($privacyPath).'
										</div>
									</div>';
					}
				}else if($isCheckOutPage){
					$this->load->library('live_app');
					$addCrdData = $this->live_app->getAddToCartData($appId);
					$backUrl = $menuUrl;
					
					if(!empty($_GET)){
						if(isset($_GET['order-details'])){
							$backUrl .= 'checkout-'.$applicationData['unique_id'];
						}else if(isset($_GET['select-payment'])){
							$backUrl .= 'checkout-'.$applicationData['unique_id'].'?order-details';
						}
					}else{
						$backUrl .= 'cart-'.$applicationData['unique_id'];
					}
					
					$data['backUrl'] = $backUrl; 
					// if(!isset($_SESSION['isLogin']) || empty($addCrdData['item'])){ //check cart is not empty and user logged in
					if(empty($addCrdData['item'])){ //check cart is not empty and user logged in
						redirect($menuUrl.'cart-'.$applicationData['unique_id']); 
					}
					$data['pageTitle'] =  'Checkout';
					$data['countriesData'] =  $this->my_model->select_data(array( //check the default page
						'field' => 'rating , count(rating) as count',
						'table' => 'country',
						'field' =>  'id,name'
					));
					$CampaHtml = $this->load->view('templates/checkout' , $data , true);
				}else if($isSubscriptionPage){
					$this->load->library('live_app');
					$backUrl = $menuUrl;
				
					if(!isset($_COOKIE['_'.$appId.'subCrt'])){
						redirect($menuUrl); 
					}
					$data['subscription_detail'] = $this->my_model->select_data([
						'table' => 'service',
						'where' => [
							'service.app_id' => $appId,
							'service.id' => $_COOKIE['_'.$appId.'subCrt']
						],
						'field' => 'service.*, payment_credentials.pay_cred',
						'join' => ['payment_credentials', 'payment_credentials.id = service.payment_account'],
						'limit' => 1
					]);
					
					$data['countriesData'] =  $this->my_model->select_data(array( 
						'field' => 'rating , count(rating) as count',
						'table' => 'country',
						'field' =>  'id,name,phonecode'
					));
					
					$data['backUrl'] = $backUrl; 
					$data['pageTitle'] =  'Subscription';
					
					$CampaHtml = $this->load->view('templates/subscription' , $data , true);
				}else if($isUserSubscriptionPage){
					$this->check_url_for_login($menuUrl , 'subscription-');
					$data['pageTitle'] =  'My Subscriptions';
					$CampaHtml = $this->load->view('templates/user_subscription' , $data , true);
				}else if($isOrderPage){
					$this->check_url_for_login($menuUrl , 'order-');
					$data['pageTitle'] =  'Order';
					$CampaHtml = $this->load->view('templates/order' , $data , true);
				}else if($manageOrderPage){
					$this->check_url_for_login($menuUrl , 'manage_order-');
					$data['pageTitle'] =  'Manage Order';
					$data['delivery_manager'] = $this->my_model->select_data([
					                                'table' => 'sub_user_details',
					                                'where' => [
					                                    'app_id' => $appId,
					                                    'role' => 1
					                                ],
					                                'field' => 'user_id, name, last_name'
					                            ]);
					$CampaHtml = $this->load->view('templates/manage_order' , $data , true);
				}else if($notificationPage){
					$this->check_url_for_login($menuUrl , 'notification-');
					$data['pageTitle'] =  'Notification';
					$CampaHtml = $this->load->view('templates/notification' , $data , true);
				}else if($bookingsPage){
					$this->check_url_for_login($menuUrl , 'bookings-');
					$data['pageTitle'] =  'Bookings';
					$CampaHtml = $this->load->view('templates/booking' , $data , true);
				}else if($loginPage){
					$data['pageTitle'] =  'Login';
					$CampaHtml = $this->load->view('templates/login' , $data , true);
				}else if($changePasswordPage){
					$data['pageTitle'] =  'Change Password';
					$data['checkUser'] =  $this->my_model->select_data(array( //check the default page
						'field' => 'name',
						'table' => 'sub_user_details use INDEX(app_id)',
						'where' =>  [
							'app_id' => $appId,
							'pass_varification_code' => $_GET['v-code']
						],
						'limit' =>  1
					));
					$CampaHtml = $this->load->view('templates/reset_password' , $data , true);
				}else if($paymentStatusPage){
					$msg = $this->session->flashdata('message'); 
					if(!isset($_SESSION['isLogin']) || empty($msg)){ //check user is login or not for showing the order page
						redirect($menuUrl);
					}
					$data['message'] =  $msg;
					$CampaHtml = $this->load->view('templates/message' , $data , true);
				}else if($emailVerificationPage){
					$data['pageTitle'] =  'Email Verification';
					$checkProduct = $this->my_model->select_data(array( //check the default page
						'field' => 'user_id',
						'table' => 'sub_user_details',
						'where' => [
							'app_id' => $appId,
							'verification_code' => $_GET['t']
						],
						'limit' => 1
					));
					if(!empty($checkProduct)){
						$this->my_model->update_data(array( //check the default page
							'table' => 'sub_user_details',
							'where' => [
								'app_id' => $appId,
								'verification_code' => $_GET['t']
							],
							'limit' => 1,
							'data' => [
								'verification_code' => '',
								'status' => 1
							]
						));
						$data['message'] =  [
							'status' => 'success',
							'title' => 'Congratulations!!',
							'sub_title' => 'Your email is verified and your '.$applicationData['title'].' account is activated.',
							'message' => 'Please <a data-modal="#_apo_loginModal" class="openMyModal">login</a> to your account.'
						];
					}else{
						$data['message'] =  [
							'status' => 'erorr',
							'title' => 'Ohhhh!!',
							'sub_title' => 'This link is already used or invalid.',
							'message' => ''
						];
					}
					$CampaHtml = $this->load->view('templates/message' , $data , true);
				}else{
					if($pageData['after_purchase'] == 1 && $pageData['product_id'] != 0){
						$isPurchasedProduct = 0;
						$checkProduct = $this->my_model->select_data(array( //check the default page
							'field' => 'id,title,description,amount',
							'table' => 'product use INDEX(app_id)',
							'where' => [
								'app_id' => $appId,
								'id' => $pageData['product_id'],
								'status' => 1
							],
							'limit' => 1
						));
						if(!empty($checkProduct)){
							$checkOrder = $this->my_model->select_data(array( //check the default page
								'field' => 'order_details.id,cart_data',
								'table' => 'orders use INDEX(user_id)',
								'where' => [
									'user_id' => $_SESSION['id'],
									'orders.status' => 3
								],
								'join' => [
									'order_details' , 'order_details.order_id = orders.id'
								]
							));
							if(!empty($checkOrder)){
								foreach($checkOrder as $cOrder){
									$crtData = $cOrder['cart_data'] != ''?(array) json_decode($cOrder['cart_data']):[];
									if(isset($crtData[$pageData['product_id']])){
										$isPurchasedProduct = 1;
										break;
									}
								}
							}
							if(!$isPurchasedProduct){
								$data['productData'] =  $checkProduct[0];
								$data['featureProductMedia'] =  $this->common->get_product_image($checkProduct[0]['id'] , '' , $cData['title']);
								$CampaHtml = $this->load->view('templates/product_restriction' , $data , true);
							}else{
								$CampaHtml = $this->getPageData($appPath , $currentPage , $previewType , $appId , $menuUrl);
							}
						}else{//in case of product is in-active/not-found
							echo $this->load->view('404' , '' , true);
							exit;
						}
					}else if($pageData['after_subscription_purchase'] == 1 && $pageData['subscription_id'] != '' && !empty($pageData['subscription_id'])){
						$isPurchasedSubscription = 0;
						$subscriptionId = json_decode($pageData['subscription_id'], true);
						$ids = '';
						for($s=0;$s<sizeof($subscriptionId); $s++){
							$ids .= $subscriptionId[$s].',';
						}
						$sIds = rtrim($ids,',');
						$checkSubscription = $this->my_model->select_data(array( //check the default page
							'field' => 'id,title,description,amount,image',
							'table' => 'service use INDEX(app_id)',
							'where' => "app_id = $appId AND id IN($sIds) AND status = 1"
						));
						
						if(!empty($checkSubscription)){
							$checkSubs = $this->my_model->select_data(array( //check the default page
								'field' => 'service.id',
								'table' => 'subscription use INDEX(customer_id)',
								'where' => [
									'customer_id' => $_SESSION['id'],
									'subscription.status' => 1
								],
								'join' => [
									'service' , 'service.paypal_plan_id = subscription.subs_plan_id'
								]
							));
							if(!empty($checkSubs)){
								// print_R($checkSubs);
								// print_r($pageData['subscription_id']);
								foreach($checkSubs as $cSubs){
									$subsId = json_decode($pageData['subscription_id'], true);
									
									if(in_array($cSubs['id'], $subsId)){
										$isPurchasedSubscription = 1;
										break;
									}
								}
							}
							if(!$isPurchasedSubscription){
								$data['subscriptionData'] =  $checkSubscription;
								$CampaHtml = $this->load->view('templates/subscription_restriction' , $data , true);
							}else{
								$CampaHtml = $this->getPageData($appPath , $currentPage , $previewType , $appId , $menuUrl);
							}
						}else{//in case of product is in-active/not-found
							echo $this->load->view('404' , '' , true);
							exit;
						}
					}else{
					   // echo "<pre>";
					   // print_r($currentPage); exit;
						$CampaHtml = $this->getPageData($appPath , $currentPage , $previewType , $appId , $menuUrl);
					}
				}
			}/*else{
				echo 'Bad Request.  (current page not found)';
			} */
		}else if(isset($pageData['after_login']) && $pageData['after_login'] == 1 && !isset($_SESSION['isLogin'])){
			if($_SESSION['isEnableLogin'] == 1){
				redirect($menuUrl.'login-'.$_SESSION['appUniqueId'].'?redirect='.$menuUrl.$currentPage);
			}else{
				echo $this->load->view('404' , '' , true); exit;
			}
		}	
		// echo '<pre>';
		// print_r($data); exit;
		$header = $this->load->view('templates/header' , $data , true);
		$footer = $this->load->view('templates/footer' , $data , true);
		$authPopupHtml = $this->load->view('templates/authentication' , [ 'previewURL' => $menuUrl , 'appTitle' => $applicationData['title'], 'appId' => $appId] , true);
		$popupPageName = 'popup'.$appId;
		$popupFilePath = $appPath.$popupPageName.'.html';
		if(file_exists($popupFilePath)){
		    $customPopupData = $this->getPageData($appPath , $popupPageName , $previewType , $appId , $menuUrl);
			$authPopupHtml .= $customPopupData;
// 			$authPopupHtml .= file_get_contents($popupFilePath);
		}
		
		$authPopupHtml .= $isPoweredByLabel == 1 ? '<div class="ao_poweredBy">
								<p>Powered By <img src="'.base_url('/assets/images/appowls.png').'" /></p>
							</div>' : '';
		echo $header.$CampaHtml.$authPopupHtml.$footer;
		exit;
	}
	function getPageData($appPath , $currentPage , $previewType , $appId , $menuUrl){
		$currentFilePath = $appPath.$currentPage.'.html';
		if($previewType == 'template' && !file_exists($currentFilePath)){ //check page in draft in case of live/template preview
			$currentFilePath = $appPath.'draft/'.$currentPage.'.html';
		}
		if(file_exists($currentFilePath)){
			if($previewType == 'application'){ //++ site view count
				$this->incrementViewCount($appId);
			}
			$pageHtml = str_replace('data-interval' , 'data-enterval' , file_get_contents($currentFilePath));
			return str_replace('MY_APP_URL/' , $menuUrl , $pageHtml) ;
		}
	}
	function check_url_for_login($menuUrl , $fromSec){
		if($_SESSION['isEnableLogin'] == 1 && !isset($_SESSION['isLogin'])){
		    $checkFunc = explode('/', $fromSec);
			redirect($menuUrl.'login-'.$_SESSION['appUniqueId'].'?redirect='.$menuUrl.$fromSec.($checkFunc[0] == 'loyalty-program' ? '' : $_SESSION['appUniqueId']));
		}else if(!isset($_SESSION['isLogin'])){
		     $checkFunc = explode('/', $fromSec);
		     if($checkFunc[0] == 'loyalty-program'){
		         $checkApp = $this->my_model->select_data(array(
        			'field' => 'unique_id',
        			'table' => 'apps',
        			'where' => array(
        				'domain_name' => $_SERVER['HTTP_HOST'],
        				'apps.status' => 1,
        			),
        			'limit' => 1
        		)); 
        		if(!empty($checkApp)){
		            redirect($menuUrl.'login-'.$checkApp[0]['unique_id'].'?redirect='.$menuUrl.$fromSec);
        		}else{
        		    echo $this->load->view('404' , '' , true); exit;
        		}
		     }else{
			    echo $this->load->view('404' , '' , true); exit;
		     }
		}
	}	
	function checkAppRatings($appId){
		$ratingData = $this->my_model->select_data(array( //check the default page
			'field' => 'rating , count(rating) as count',
			'table' => 'rating_data use INDEX(app_id)',
			'where' => [
				'app_id' => $appId
			],
			'group' => 'rating'
		));
		$ratingDetails = [
			'1' => 0,
			'2' => 0, 		
			'3' => 0,
			'4' => 0,
			'5' => 0,
			'avg' => 0,
			'total' => 0
		];
		if(!empty($ratingData)){
			$ratingSum = 0;
			$ratUserCount = 0;
			foreach($ratingData as $rData){
				$ratingDetails[$rData['rating']] = $rData['count'];
				$ratingSum += $rData['rating']*$rData['count'];
				$ratUserCount += $rData['count'];
			}
			// $rList = [];
			// foreach($ratingDetails as $key => $val){
			// 	if(is_numeric($key)){
			// 		array_push($rList , $val);
			// 	}
			// }
			// $ratingDetails['max'] = max($rList);
			$ratingDetails['avg'] = round($ratingSum/$ratUserCount , 1);
			$ratingDetails['total'] = $ratUserCount;
		}
		return $ratingDetails;
	}
	private function incrementViewCount($appId){
		// echo $appId;
		// echo"<pre>";print_r($getApp);die;
		$cookieKey = '_view'.$appId;
		if(!isset($_COOKIE[$cookieKey])){
			setcookie($cookieKey , 1 , time() + 86400 , "/"); // 86400 = 1 day
			$this->my_model->update_data(array(
				'table' => 'apps',
				'where' => [
					'id' => $appId,
				],
				'data' => [
					'view' , 'view+1' , false
				],
				'limit' => 1,
			));
			$newAppId = $this->my_model->insert_data(array(
				'table' => 'apps_views',
				'data' => [
					'app_id' 	  => $appId
				]
			));
		}
	}
	function incrementInstallCount($appUId , $appId){
		$this->my_model->update_data(array(
			'table' => 'apps',
			'where' => [
				'unique_id' => $appUId,
			],
			'data' => [
				'install' , 'install+1' , false
			],
			'limit' => 1,
		));
		$this->my_model->update_data(array(
			'table' => 'user',
			'where' => [
				'id' => $_SESSION['appVendorId'],
			],
			'data' => [
				'per_month_installation',
				'per_month_installation+1' ,
				false
			],
			'limit' => 1,
		));
		
		$this->my_model->insert_data(array(
			'table' => 'apps_installs',
			'data' => [
				'app_id' => $appId
			]
		));
	}
	function check_mails(){
		$data = [
			'appLogo' => $this->common->siteLogo,
			'appName' => 'Test',
			'appUrl' => base_url(),
			'title' => 'this is test title',
			'salutation' => 'Hey PK',
			'body' => 'Hey ',
		];
		$this->load->view('email_template' , $data);
	}    
	function SetCurrentToken(){
		$rows = [];
		$rules = array(
			array( 'field' => 'firebase_token', 'label' => 'Firebase Token', 'rules' => 'trim|required'),
			array( 'field' => 'app_id', 'label' => 'App', 'rules' => 'trim|required'),
			array( 'field' => 'app_user_id', 'label' => 'App', 'rules' => 'trim'),
		);
		$this->load->library('form_validation');
		$this->form_validation->set_rules($rules);
		if($this->form_validation->run()){
			$firebase_token = trim($_POST['firebase_token']);
			$app_id 		= $_POST['app_id'];
			$app_user_id 	= $_POST['app_user_id'];
			$IP 			= $this->common->get_client_ip();
			$check_ip = $this->my_model->select_data([
				'field' => 'token',
				'table' => 'firebase_tokens',
				'where' => ['ip_address' => $IP,'app_id'=> $app_id]
			]);
			if(!empty($check_ip)){
				$update = $this->my_model->update_data([
					'table' => 'firebase_tokens',
					'data' => [
						'token' 	=> $firebase_token,
						'app_user_id' => $app_user_id
					],
					'where' => [
						'ip_address' => $IP,'app_id'=> $app_id
					]
				]);
				// echo $this->db->last_query();die;
				if($update){
					$this->common->statusCode = 1;
					$this->common->respMessage = 'Token updated1.';
				}   
			}else{
				$check_token = $this->my_model->select_data([
					'field' => 'ip_address',
					'table' => 'firebase_tokens',
					'where' => ['token' => $firebase_token,'app_id'=> $app_id]
				]);
				if(!empty($check_token)){
					$update = $this->my_model->update_data([
						'table' => 'firebase_tokens',
						'data' => [
							'ip_address' 	=> $IP,
							'app_user_id' => $app_user_id
						],
						'where' => [
							'token' => $firebase_token,'app_id'=> $app_id
						]
					]);
					if($update){
						$this->common->statusCode = 1;
						$this->common->respMessage = 'Token updated2.';
					}
				}else{
					$insert = $this->my_model->insert_data([
						'table' => 'firebase_tokens',
						'data' 	=> [
							'app_id' 		=> $app_id, 
							'app_user_id' 	=> $app_user_id, 
							'token' 		=> $firebase_token,
							'ip_address' 	=> $IP,
						]
					]);
					if($insert){
						$this->common->statusCode = 1;
						$this->common->respMessage = 'Token added1.';
					}
				}
			}
		}else{
			$this->common->respMessage  =  $this->common->form_validation_errors();
		}
		$this->common->show_my_response($rows);
	}
	function setPerMonthLimit(){
	    $update = $this->my_model->update_data([
	        'table' => 'user',
	        'data' => ['per_month_installation' => 0, 'per_month_notification' => 0]
	    ]);
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect($this->agent->referrer());
	}
	function make_payment($orderId){
		$resp = [
			'status' => 'erorr',
			'title' => 'Ohhhh!!',
			'sub_title' => 'Something went wrong.',
			'message' => 'Please try after some time.'
		];
		$isPay = 0;
		if(isset($_SESSION['app_id']) && isset($_COOKIE['_'.$_SESSION['app_id'].'Crt']) && isset($_POST['payType']) && is_numeric($orderId)){
			$this->load->library('live_app');
            $appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
			$checkOrder = $this->my_model->select_data([
				'table' => 'orders',
				'where' => "orders.id = ".$orderId,
				'field' => 'orders.order_u_id,price,products,price_data',
				'limit' => 1,
				'join' => [
					'order_details' , 'order_details.order_id = orders.id' 
				]
			]);
			// print_r($checkOrder);
			// print_r($_POST);
			if(!empty($checkOrder)){
				$oderData = $checkOrder[0];
				$priceData = json_decode($oderData['price_data'] , true);
				if( $_POST['payType'] == 'paypal' && $appDetail['is_paypal'] ) {
					$getPaypalData =  $this->my_model->select_data([
						'table' => 'payment_credentials use index(user_id)',
						'where' => "id = '".$appDetail['paypal_pay_id']."' AND payment_type = '1' AND status = 1 AND (user_id = '".$appDetail['user_id']."' OR app_id = '".$_SESSION['app_id']."')",
						'field' => 'credentials',
						'limit' => 1,
					]);
					if(!empty($getPaypalData)){
						$isPay = 1;
						echo $formData =
							'<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="pay_form">
								<input type="hidden" name="business" value="'.$getPaypalData[0]['credentials'].'">
								<input type="hidden" name="item_name" value="'.$oderData['products'].'">
								<input type="hidden" name="amount" value="'.$priceData['grandTotal'].'">
								<input type="hidden" name="item_number" value="'.$orderId.'">
								<input type="hidden" name="no_shipping" value="1">
								<input type="hidden" name="currency_code" value="'.$priceData['currencyCode'].'">
								<input type="hidden" name="cmd" value="_xclick">
								<input type="hidden" name="handling" value="0">
								<input type="hidden" name="no_note" value="1">
								<input type="hidden" name="cpp_logo_image" value="'.$appDetail['appLogo'].'">
								<input type="hidden" name="custom" value="'.$_SESSION['app_id'].'">
								<input type="hidden" name="cancel_return" value="'.base_url().'verify/check_paypal_payment/cancel/'.$orderId.'">
								<input type="hidden" name="return" value="'.base_url().'verify/check_paypal_payment/success/'.$orderId.'">
								<input type="hidden" name="notify_url" value="'.base_url().'verify/check_paypal_payment/notify/'.$orderId.'">
							</form>';
						echo "Redirecting...<script type=\"text/javascript\"> 
								window.onload=function(){
									document.forms['pay_form'].submit();
								}
							</script>";
					}
				} elseif( $_POST['payType'] == 'cod' && $appDetail['is_cod'] || (isset($_POST['isPaid']) && $_POST['isPaid'] == 1 && $_POST['payType'] == 'cod'))  {
					$this->manageUserWithoutLogin($_SESSION['app_id'], $orderId);					
					$this->manageUserLoyaltyPoint($_SESSION['app_id'], $orderId);
					$this->live_app->removeAddToCartCookie($_SESSION['app_id']);
					$isPay = 1;
					$orderPaymentData = [
						'order_id'=> $orderId, 
						'amount' => $priceData['grandTotal'],
						'currency' => $priceData['currency'],
						'currency_code' => $priceData['currencyCode'],
					];
					$ordersData = [
						'status' => 1,
						'payment_mode' => 3,
					];
					if(isset($_POST['isPaid']) && $_POST['isPaid'] == 1){
					    $orderPaymentData['mode'] = 3;
					    $orderPaymentData['status'] = 1;
					    $ordersData['pament_status'] = 1;
					}
					$this->my_model->insert_data([
						'table' => 'order_payment',
						'data' 	=> $orderPaymentData
					]);
					$this->my_model->update_data([
						'table' => 'orders',
						'data' 	=> $ordersData,
						'where' => [
							'id' => $orderId
						],
						'limit' => 1
					]);
					$this->load->Library('setup_mail');
					$this->setup_mail->setEmailAndNotification([
						'emailFor' => 'appPlacedOrder', 
						'orderId' => $orderId,
						'payMode' => 'COD'
					]);
					$resp = [
						'status' => 'success',
						'title' => 'Thank You!!',
						'sub_title' => 'Your Order has been placed successfully.',
						'message' => ''
					];
				}
			}else{
				$resp['sub_title'] = "Order details not found.";
			}		
		}
		if(!$isPay || $_POST['payType'] == 'cod'){
			$this->session->set_flashdata('message', $resp);
			$ref = $this->agent->referrer();
			if($ref != ''){
				$redUrl = explode('checkout' , $ref);
				if(count($redUrl) > 1){
					$rediUrl = $redUrl[0].'status-'.$appDetail['unique_id'].'?'.$resp['status'];
				}else{
					$rediUrl = base_url();
				}
			}else{
				$rediUrl = base_url();
			}
			redirect($rediUrl);
		}
	}

	function manageUserWithoutLogin($appId, $orderId){
		$checkOrder = $this->my_model->select_data([
			'table' => 'orders',
			'where' => [
				'id' => $orderId,
				'app_id' => $appId,
				'user_id' => 0
			],
			'field' => 'user_id, cart_id',
			'limit' => 1
		]);
		
		if(!empty($checkOrder)){ // user without login
			$getUserData = $this->my_model->select_data([
				'table' => 'add_to_cart',
				'where' => [
					'id' => $checkOrder[0]['cart_id'],
					'app_id' => $appId
				],
				'field' => 'user_details',
				'limit' => 1
			]);
		
			if(!empty($getUserData) && $getUserData[0]['user_details'] != ''){
				$shipData = json_decode($getUserData[0]['user_details'], true);
				$password = $this->common->getRandomNumber(8);
				$addSubUser = $this->my_model->insert_data([
					'table' => 'sub_user_details',
					'data' => [
						'role' => 0,
						'name' => $shipData['first_name'],
						'last_name' => $shipData['last_name'],
						'email' => $shipData['email'],
						'pwd' => md5($password),
						'country' => $shipData['country'],
						'country_code' => $shipData['country_code'],
						'contact' => $shipData['contact'],
						'app_id' => $shipData['app_id'],
						'status' => 1
					]
				]);
				
				if($addSubUser){ // sub user added now update user id in all the required tables
					//sent mail to the user
					$shipData['user_id'] = $addSubUser;
					unset($shipData['country_code']);
					$this->my_model->insert_data([
						'table' => 'shipping_address',
						'data' => $shipData
					]);

					$this->my_model->update_data([
						'table' => 'orders',
						'where' => [
							'id' => $orderId,
							'app_id' => $appId
						],
						'data' => [
							'user_id' => $addSubUser
						],
						'limit' => 1
					]);
				}
			}
		}
	}

	function make_booking_payment($bookingDraftId){
		$resp = [
			'status' => 'erorr',
			'title' => 'Ohhhh!!',
			'sub_title' => 'Something went wrong.',
			'message' => 'Please try after some time.'
		];
		$isPay = 0;
		$this->load->library('live_app');
        $appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
		$payType = $_POST['payType'];
		
		if(!empty($appDetail)){
			if(isset($_SESSION['app_id']) && isset($_POST['payType']) && is_numeric($bookingDraftId)){
				
				$checkDraft = $this->my_model->select_data([
					'table' => 'booking_draft use INDEX(user_id)',
					'field' => 'data',
					'where' => [
						'id' => $bookingDraftId,
						'user_id' => $_SESSION['id'],
					],
					'limit' => 1,
				]);
				// echo '<pre>';
				if(!empty($checkDraft)){
					$bookingData = $checkDraft[0];				
					$bookingDetails = json_decode($bookingData['data'] , true);
					$appBookingDetail = $this->my_model->select_data([
						'table' => 'bookings use INDEX(app_id)',
						'field' => 'is_paypal,paypal_id,is_stripe,stripe_id,is_cod,user_id',
						'where' => [
							'app_id' => $bookingDetails['app_id'],
						],
						'limit' => 1,
					]);
				// echo"<pre>";print_r($appBookingDetail);die;

					if(!empty($appBookingDetail)){
						$appBookingDetail = $appBookingDetail[0];
						$bookingDetails['is_payable'] = 1;
						// print_r($_POST); 
						// echo"<pre>";print_r($appDetail['is_cod']); exit; 
						if( $_POST['payType'] == 'paypal' && $appBookingDetail['is_paypal'] ) {
							// print_r($appDetail); 
							$getPaypalData =  $this->my_model->select_data([
								'table' => 'payment_credentials use index(user_id)',
								'where' => "id = '".$appBookingDetail['paypal_id']."' AND payment_type = '1' AND status = 1 AND (user_id = '".$appBookingDetail['user_id']."' OR app_id = '".$bookingDetails['app_id']."')",
								'field' => 'credentials',
								'limit' => 1,
							]);
							if(!empty($getPaypalData)){
								$isPay = 1;
								echo $formData =
									'<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="pay_form">
										<input type="hidden" name="business" value="'.$getPaypalData[0]['credentials'].'">
										<input type="hidden" name="item_name" value="Booking Charge">
										<input type="hidden" name="amount" value="'.$bookingDetails['pay_amount'].'">
										<input type="hidden" name="item_number" value="'.$bookingDraftId.'">
										<input type="hidden" name="no_shipping" value="1">
										<input type="hidden" name="currency_code" value="'.$bookingDetails['currency_code'].'">
										<input type="hidden" name="cmd" value="_xclick">
										<input type="hidden" name="handling" value="0">
										<input type="hidden" name="no_note" value="1">
										<input type="hidden" name="cpp_logo_image" value="'.$appDetail['appLogo'].'">
										<input type="hidden" name="custom" value="'.$_SESSION['app_id'].'">
										<input type="hidden" name="cancel_return" value="'.base_url().'verify/check_booking_paypal_payment/cancel">
										<input type="hidden" name="return" value="'.base_url().'verify/check_booking_paypal_payment/success">
										<input type="hidden" name="notify_url" value="'.base_url().'verify/check_booking_paypal_payment/notify">
									</form>';
								echo "Redirecting...<script type=\"text/javascript\"> 
										window.onload=function(){
											document.forms['pay_form'].submit();
										}
									</script>";
							}
						} elseif( $_POST['payType'] == 'cod' && $appDetail['is_cod'])  {
							$bookingDetails['pay_amount'] = $bookingDetails['currency'].$bookingDetails['pay_amount'];
							unset($bookingDetails['currency'] , $bookingDetails['currency_code']);
							$bookingDetails['pay_mode'] = 3;
							$isPay = 1;
							$this->my_model->delete_data([
								'table' => 'booking_draft',
								'where' 	=> [
									'id'=> $bookingDraftId, 
								],
								'limit' => 1
							]);
							$bookingId = $this->my_model->insert_data([
								'table' => 'bookings_details',
								'data' 	=> $bookingDetails
							]);
							$resp = [
								'status' => 'success',
								'title' => 'Thank You!!',
								'sub_title' => 'Your booking has been completed successfully.',
								'message' => ''
							];
							$this->load->Library('setup_mail');
							$this->setup_mail->setEmailAndNotification([
								'emailFor' => 'appBooking',
								'bookingId' => $bookingId,
								'status' => 0
							]);
						}
					}else{
						$resp['sub_title'] = "Booking details not found.";
					}
				}else{
					$resp['sub_title'] = "Booking details not found.";
				}		
			}else{
				$resp['sub_title'] = "Payment method not added.";
			}
		}else{
			$resp['sub_title'] = "Application details not found.";
		}
		// echo $isPay .' '.$_POST['payType'];
		// echo"<pre>";print_r($resp); exit; 
		if(!$isPay || $payType == 'cod' && !empty($appDetail)){
			$this->session->set_flashdata('message', $resp);
			$rediUrl = base_url().'status-'.$appDetail['unique_id'].'?'.$resp['status'];
			redirect($rediUrl);
		}else if(empty($appDetail)){
			redirect(base_url());
		}
	}
	function check_mail(){
		$this->load->library('liva_app');
		$this->liva_app->manage_order_mails_and_notifications();
	}
	function check_paypal_payment($type , $orderId){
		if($type == 'notify'){
			if($_POST['payment_status'] == 'Completed'){
				$this->load->library('live_app');
				$this->manageUserWithoutLogin($_POST['custom'], $orderId);
				$this->manageUserLoyaltyPoint($_POST['custom'], $orderId);
				$this->live_app->removeAddToCartCookie($_POST['custom']);			
			}
			$this->my_model->insert_data([
				'table' => 'order_payment',
				'data' 	=> [
					'order_id'=> $_POST['item_number'], 
					'mode' => 1, 
					'amount' => $_POST['mc_gross'],
					'currency_code' => $_POST['mc_currency'],
					'status' => ($_POST['payment_status'] == 'Completed')?1:0,
					'data' => json_encode($_POST)
				]
			]);
			$this->load->Library('setup_mail');
			$this->setup_mail->setEmailAndNotification([
				'emailFor' => 'appPlacedOrder', 
				'payMode' => 'paypal',
				'orderId' => $_POST['item_number']
			]);
			$this->my_model->update_data([
				'table' => 'orders',
				'data' 	=> [
					'status' => 1,
					'payment_mode' => 1,
					'pament_status' => 1,
				],
				'where' => [
					'id' => $_POST['item_number']
				],
				'limit' => 1
			]);
		}else if($type == 'success'){
			$this->redirect_after_order_completed($orderId , '');
		}else{
			$resp = [
				'status' => 'cancel',
				'title' => 'Oppps!!',
				'sub_title' => 'Your payment has been cancelled.',
				'message' => ''
			];
			$this->session->set_flashdata('message', $resp);
			$appUniqueId = isset($_SESSION['appUniqueId'])?$_SESSION['appUniqueId']:''; 
			if($appUniqueId == ''){
				$getApp = $this->my_model->select_data([
					'table' => 'orders',
					'where' => ['orders.id' => $orderId],
					'field' => 'unique_id',
					'limit' => 1,
					'join' => [
						'apps' , 'apps.id = orders.app_id'
					]
				]);
				$appUniqueId = $getApp[0]['unique_id'];
			}
			redirect(base_url().'status-'.$appUniqueId.'?'.$type);
		}
	}
	function redirect_after_order_completed($orderId , $appUniqueId = ''){
		$orderDetasil = $this->my_model->select_data([
			'table' => 'order_details use INDEX(order_id)',
			'field' => 'products_data',
			'where' => [
				'order_id' => $orderId
			]
		]);
		$redirectUrl = '';
		if(!empty($orderDetasil)){
			$pData = $orderDetasil[0]['products_data'] != ''? json_decode($orderDetasil[0]['products_data'] , true):[];
			if(count($pData) == 1 && isset($pData[0]['thanks_url']) && filter_var($pData[0]['thanks_url'] , FILTER_VALIDATE_URL)){
				// $redirectUrl = $pData[0]['thanks_url'];
			}
		}
		if($redirectUrl != ''){
			header('location:'.$redirectUrl);
		}else{
			$resp = [
				'status' => 'success',
				'title' => 'Thank You!!',
				'sub_title' => 'Your Order has been placed successfully.',
				'message' => ''
			];
			$this->session->set_flashdata('message', $resp);
			if($appUniqueId == ''){
				$getApp = $this->my_model->select_data([
					'table' => 'apps',
					'where' => ['id' => $_SESSION['app_id']],
					'field' => 'unique_id',
					'limit' => 1,
				]);
				$appUniqueId = $getApp[0]['unique_id'];
			}
			$rediUrl = base_url().'status-'.$appUniqueId.'?'.$type;
			redirect($rediUrl);
		}
	}
	function check_booking_paypal_payment($type){
		$resp = [
			'status' => 'cancel',
			'title' => 'Oppps!!',
			'sub_title' => 'Your payment has been cancelled.',
			'message' => ''
		];
		if($type == 'notify'){
			$checkDraft = $this->my_model->select_data([
				'table' => 'booking_draft use INDEX(user_id)',
				'field' => 'data',
				'where' => [
					'id' => $_POST['item_number'],
				],
				'limit' => 1,
			]);
			if(!empty($checkDraft)){
				$bookingData = $checkDraft[0];				
				$bookingDetails = json_decode($bookingData['data'] , true);
				if($_POST['mc_currency'] == $bookingDetails['currency_code']){
					$bookingDetails['is_payable'] = 1;
					$bookingDetails['pay_amount'] = $bookingDetails['currency'].$bookingDetails['pay_amount'];
					unset($bookingDetails['currency'] , $bookingDetails['currency_code']);
					$bookingDetails['pay_mode'] = 1;
					$bDetailId = $this->my_model->insert_data([
						'table' => 'bookings_details',
						'data' 	=> $bookingDetails
					]);
					$this->my_model->delete_data([
						'table' => 'booking_draft',
						'where' => [
							'id' => $_POST['item_number'],
						],
						'limit' => 1
					]);
					$this->my_model->insert_data([
						'table' => 'order_payment',
						'data' 	=> [
							'booking_id'=> $bDetailId, 
							'amount' => $_POST['mc_gross'],
							'currency' => $_POST['mc_currency'],
							'status' => ($_POST['payment_status'] == 'Completed')?1:0,
							'data' => json_encode($_POST)
						]
					]);
					$this->load->Library('setup_mail');
					$this->setup_mail->setEmailAndNotification([
						'emailFor' => 'appBooking',
						'bookingId' => $bDetailId,
						'status' => 1
					]);
				}
			}			
		}else if($type == 'success'){
			$resp = [
				'status' => 'success',
				'title' => 'Thank You!!',
				'sub_title' => 'Your booking has been completed successfully.',
				'message' => ''
			];
		}
		if($type != 'notify'){
			$this->session->set_flashdata('message', $resp);
			$getApp = $this->my_model->select_data([
				'table' => 'apps',
				'where' => ['id' => $_SESSION['app_id']],
				'field' => 'unique_id',
				'limit' => 1,
			]);
			$rediUrl = base_url().'status-'.$getApp[0]['unique_id'].'?'.$type;
			redirect($rediUrl);
		}else{
			redirect(base_url());
		}
	}
	function stripe_checkout(){
		$this->load->library('live_app');
        $appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
		$resp = [
			'status' => 'cancel',
			'title' => 'Oppps!!',
			'sub_title' => 'Your payment has been cancelled.',
			'message' => ''
		];
		$this->session->set_flashdata('message', $resp);
		if(!isset($_SESSION['stripeSession']) || (isset($_SESSION['stripeSession']) && $_SESSION['stripeSession'] == '')) {
			redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
		}else{
			$getStripeData =  $this->my_model->select_data([
				'table' => 'payment_credentials use index(user_id)',
				'where' => "id = '".$appDetail['stripe_pay_id']."' AND payment_type = '2' AND status = 1 AND (user_id = '".$appDetail['user_id']."' OR app_id = '".$_SESSION['app_id']."')",
				'field' => 'credentials',
				'limit' => 1,
			]);
			if(!empty($getStripeData)){
				$getStripeData = json_decode($getStripeData[0]['credentials'] , true);
				try {
					require_once APPPATH."third_party/stripe/init.php";
					\Stripe\Stripe::setApiKey($getStripeData['secret_key']); //Replace with your Secret Key
					if( $_SESSION['stripeSession'] != '' ) {
						$stripeSessionArr = explode('@#',$_SESSION['stripeSession']);
						$charge =  \Stripe\Charge::create(array(
							"amount" => $stripeSessionArr[1]*100,
							"currency" => strtolower($appDetail['currencyCode']),
							"card" => $_POST['stripeToken'],
							"description" => $stripeSessionArr[0]
						));
						//retrieve charge details
						$chargeJson = $charge->jsonSerialize();
						$_SESSION['stripeSession'] = '';
						$this->manageUserWithoutLogin($_SESSION['app_id'], $stripeSessionArr[2]);
                        $this->manageUserLoyaltyPoint($_SESSION['app_id'], $stripeSessionArr[2]);
						$this->live_app->removeAddToCartCookie($_SESSION['app_id']);
						$this->my_model->insert_data([
							'table' => 'order_payment',
							'data' 	=> [
								'order_id'=> $stripeSessionArr[2], 
								'mode' => 2, 
								'amount' => $stripeSessionArr[1],
								'currency_code' => $appDetail['currencyCode'],
								'status' => 1,
								'data' => json_encode($chargeJson)
							]
						]);
						$this->my_model->update_data([
							'table' => 'orders',
							'data' 	=> [
								'status' => 1,
								'payment_mode' => 2,
								'pament_status' => 1,
							],
							'where' => [
								'id' => $stripeSessionArr[2],
							],
							'limit' => 1
						]);
						$this->load->Library('setup_mail');
						$this->setup_mail->setEmailAndNotification([
							'emailFor' => 'appPlacedOrder', 
							'orderId' => $stripeSessionArr[2], 
							'payMode' => 'stripe',
							// 'toEmail' => $_SESSION['email'], 
							// 'toName' => $_SESSION['name'],
							// 'appId' => $_SESSION['app_id'],
							// 'appDetail' => $appDetail,
							// 'orderId' => $stripeSessionArr[2],
						]);
						$this->redirect_after_order_completed($orderId , $appDetail['unique_id']);
					}else {
						redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
					}
				}
				catch(Stripe_CardError $e) {
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				}
				catch (Stripe_InvalidRequestError $e) {
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				} catch (Stripe_AuthenticationError $e) {
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				} catch (Stripe_ApiConnectionError $e) {
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				} catch (Stripe_Error $e) {
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				} catch (Exception $e) {
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				}
			}else{
			}
		}
	}
	function square_checkout(){
	    $this->load->library('live_app');
        $appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
		$resp = [
			'status' => 'cancel',
			'title' => 'Oppps!!',
			'sub_title' => 'Your payment has been cancelled.',
			'message' => ''
		];
		$this->session->set_flashdata('message', $resp);
		if(isset($_POST['token']) && $_POST['token'] != '' && isset($_POST['card_detail']) && !empty($_POST['card_detail'])){
			$getSquareData =  $this->my_model->select_data([
				'table' => 'payment_credentials use index(user_id)',
				'where' => "id = '".$appDetail['square_pay_id']."' AND payment_type = '4' AND status = 1 AND (user_id = '".$appDetail['user_id']."' OR app_id = '".$_SESSION['app_id']."')",
				'field' => 'id, credentials',
				'limit' => 1,
			]);
			if(!empty($getSquareData)){
				$SquareData = json_decode($getSquareData[0]['credentials'] , true);
				$square_customer_id = '';
				$access_token = $SquareData['access_token'];
	            if(!isset($SquareData['square_customer_id']) && $SquareData['square_customer_id'] == ''){
    	            $fullname = $this->session->userdata('fullName');
                    $user_email = $this->session->userdata('email');
    				$name = explode(' ', $fullname);
    				$curl = curl_init();
                    $curlArr = [
                        CURLOPT_URL => 'https://connect.squareup.com/v2/customers',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_HTTPHEADER => [
                            "Authorization: Bearer $access_token",
                            "Content-Type: application/json" 
                        ],
                        CURLOPT_POSTFIELDS => json_encode([
                            "given_name" => $name[0],
                            "family_name" => (isset($name[1]) ? $name[1] : ''),
                            "email_address" => $user_email
                        ])
                    ];
                    curl_setopt_array($curl, $curlArr);
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                        $this->respMessage = $err;
                        $this->respData = [
                            'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                        ];
                    } else {
                        $resp = json_decode($response ,true);
                        if(isset($resp['errors'])){
                            $this->respMessage = $resp['errors'][0]['detail'];
                            $this->respData = [
                                'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                            ];
                        }else{
                            $square_customer_id = $resp['customer']['id'];
                            $SquareData['square_customer_id'] = $square_customer_id;
                            $updateCustomerId = $this->my_model->update_data([
                                'table' => 'payment_credentials',
                                'where' => [ 'id' => $getSquareData[0]['id'] ],
                                'data' => [ 'credentials' => json_encode($SquareData) ],
                                'limit' => 1
                            ]);
                        }
                    }
	            }else{
	                $square_customer_id = $SquareData['square_customer_id'];
	            }
                $squareSessionArr = explode('@#',$_SESSION['squareSession']);
                $idempotency_key = $this->common->getRandomNumber(35);
                $paymentData = [
                    'source_id' => $_POST['token'],
                    'amount' => $squareSessionArr[1],
                    'idempotency_key' => $idempotency_key,
                    'customer_id' => $square_customer_id,
                    'access_token' => $access_token,
                    'currency_code' => $appDetail['currencyCode']
                ];
                $createPayment = $this->create_square_payment($paymentData);
                if($createPayment['status'] == 1){
                    $payment_data = json_decode($createPayment['data'], true);
                    $allPaymentData = $payment_data['payment'];
                    $paidAmount = floatVal($allPaymentData['amount_money']['amount'])/100;
                    if($allPaymentData['status'] == 'COMPLETED'){
                        $_SESSION['squareSession'] = '';
						$this->manageUserWithoutLogin($_SESSION['app_id'], $squareSessionArr[2]);
                        $this->manageUserLoyaltyPoint($_SESSION['app_id'], $squareSessionArr[2]);
						$this->live_app->removeAddToCartCookie($_SESSION['app_id']);
						$this->my_model->insert_data([
							'table' => 'order_payment',
							'data' 	=> [
								'order_id'=> $squareSessionArr[2], 
								'mode' => 4, 
								'amount' => $squareSessionArr[1],
								'currency_code' => $appDetail['currencyCode'],
								'status' => 1,
								'data' => json_encode($allPaymentData)
							]
						]);
						$this->my_model->update_data([
							'table' => 'orders',
							'data' 	=> [
								'status' => 1,
								'payment_mode' => 4,
								'pament_status' => 1,
							],
							'where' => [
								'id' => $squareSessionArr[2],
							],
							'limit' => 1
						]);
						$this->load->Library('setup_mail');
						$this->setup_mail->setEmailAndNotification([
							'emailFor' => 'appPlacedOrder', 
							'orderId' => $squareSessionArr[2], 
							'payMode' => 'square',
						]);
                    	$resp = [
            				'status' => 'success',
            				'title' => 'Thank You!!',
            				'sub_title' => 'Your Order has been placed successfully.',
            				'message' => ''
            			];
            			$this->session->set_flashdata('message', $resp);
            			$rediUrl = base_url().'status-'.$appDetail['unique_id'];
			            $this->respStatus = 1;
			            $this->respMessage = '';
			            $this->respData = [
			                'url' => $rediUrl
			            ];
                    }else{
                        $this->respMessage = 'Your payment has been cancelled.';
                        $this->respData = [
                            'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                        ];
                    }
                }else{
                    $this->respMessage = 'Your payment has been cancelled.';
                    $this->respData = [
                        'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                    ];
                }
			}else{
			    $this->respMessage = 'Your payment has been cancelled.';
                $this->respData = [
                    'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                ];
			}
		}
        $this->show_result();
	}
	function create_square_payment($param){
        extract($param);
        $curl = curl_init();
            // https://connect.squareupsandbox.com/v2/payments
        $curlArr = [
            CURLOPT_URL => "https://connect.squareup.com/v2/payments",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer $access_token",
                "Content-Type: application/json" 
            ],
            CURLOPT_POSTFIELDS => json_encode([
                "idempotency_key" => $idempotency_key,
                "amount_money" => [
                    "amount" => intVal($amount*100),
                    "currency" => $currency_code,
                ],
                "source_id" => $source_id,
                "autocomplete" => true,
                "customer_id" => $customer_id                
            ])
        ];
        curl_setopt_array($curl, $curlArr);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $Resp = $err;
        } else {
            $resp = json_decode($response ,true);
            if(isset($resp['errors'])){
                $Resp = ['status' => 0, 'msg' => $resp['errors'][0]['detail'] ];
            }else{
                $Resp = ['status' => 1, 'data' => $response];
            }
        }
        return $Resp;
    }
	function paystack_checkout(){
		$this->load->library('live_app');
        $appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
		$resp = [
			'status' => 'cancel',
			'title' => 'Oppps!!',
			'sub_title' => 'your payment has been cancelled.',
			'message' => ''
		];
		$this->session->set_flashdata('message', $resp);
        if(!isset($_POST['trxref']) && (!isset($_SESSION['paystackSession']) || (isset($_SESSION['paystackSession']) && $_SESSION['paystackSession'] == ''))){
            $this->respMessage = 'your payment has been cancelled.';
            $this->respData = [
                'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
            ];
		}else{
		    if(isset($_POST['trxref'])){
		        $getPaystackData =  $this->my_model->select_data([
    				'table' => 'payment_credentials use index(user_id)',
    				'where' => "id = '".$appDetail['paystack_pay_id']."' AND payment_type = '5' AND status = 1 AND (user_id = '".$appDetail['user_id']."' OR app_id = '".$_SESSION['app_id']."')",
    				'field' => 'credentials',
    				'limit' => 1,
    			]);
		        $reference = $_POST['trxref'];
		        if(!empty($getPaystackData)){
		            $paystackCred = json_decode($getPaystackData[0]['credentials'] , true);
		            $secretKey = $paystackCred['secret_key'];
    		        $curl = curl_init();
                    $curlArr = [
                        CURLOPT_URL => "https://api.paystack.co/transaction/verify/".urlencode($reference),
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_HTTPHEADER => [
                            "accept: application/json",
                            "authorization: Bearer $secretKey",
                            "cache-control: no-cache"
                        ],
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    ];
                    curl_setopt_array($curl, $curlArr);
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    if ($err) {
                        $this->respMessage = 'your payment has been cancelled.';
                        $this->respData = [
                            'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                        ];
                    }else {
                        $resp = json_decode($response ,true);
                        if(isset($resp['status']) && !$resp['status']){
                            $this->respMessage = 'your payment has been cancelled.';
                            $this->respData = [
                                'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                            ];
                        }else{
                            if( $_SESSION['paystackSession'] != '' ) {
            					$paystackSessionArr = explode('@#',$_SESSION['paystackSession']);
            					$_SESSION['paystackSession'] = '';
								$this->manageUserWithoutLogin($_SESSION['app_id'], $paystackSessionArr[2]);
                                $this->manageUserLoyaltyPoint($_SESSION['app_id'], $paystackSessionArr[2]);
            					$this->live_app->removeAddToCartCookie($_SESSION['app_id']);
            					$this->my_model->insert_data([
            						'table' => 'order_payment',
            						'data' 	=> [
            							'order_id'=> $paystackSessionArr[2], 
            							'mode' => 5, 
            							'amount' => $paystackSessionArr[1],
            							'currency_code' => $appDetail['currencyCode'],
            							'status' => 1,
            							'data' => json_encode($resp['data'])
            						]
            					]);
            					$this->my_model->update_data([
            						'table' => 'orders',
            						'data' 	=> [
            							'status' => 1,
            							'payment_mode' => 5,
            							'pament_status' => 1,
            						],
            						'where' => [
            							'id' => $paystackSessionArr[2],
            						],
            						'limit' => 1
            					]);
            					$this->load->Library('setup_mail');
            					$this->setup_mail->setEmailAndNotification([
            						'emailFor' => 'appPlacedOrder', 
            						'orderId' => $paystackSessionArr[2], 
            						'payMode' => 'paystack',
            					]);
            					$resp = [
                    				'status' => 'success',
                    				'title' => 'Thank You!!',
                    				'sub_title' => 'Your Order has been placed successfully.',
                    				'message' => ''
                    			];
                    			$this->session->set_flashdata('message', $resp);
                                $this->respStatus = 1;
                                $this->respMessage = '';
                                $this->respData = [
                                    'url' => base_url().'status-'.$appDetail['unique_id']
                                ];
            				}else {
            				    $this->respMessage = 'your payment has been cancelled.';
                                $this->respData = [
                                    'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                                ];
            				}
                		}
                    }
		        }else{
		            $this->respMessage = 'your payment has been cancelled.';
                    $this->respData = [
                        'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                    ];
		        }
		    }else{
		        $this->respMessage = 'your payment has been cancelled.';
                $this->respData = [
                    'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                ];
		    }
		}
		$this->show_result();
	}
	function razorpay_checkout(){
		$this->load->library('live_app');
        $appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
        // if($_SESSION['app_id'] == '5357'){
        //   print_r($_POST);die; 
        // }
		$resp = [
			'status' => 'cancel',
			'title' => 'Oppps!!',
			'sub_title' => 'your payment has been cancelled.',
			'message' => ''
		];
		$this->session->set_flashdata('message', $resp);
        if(!isset($_POST['razorpay_payment_id']) && !isset($_POST['totalAmount'])){
			$this->respData = [
                'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
            ];
		}else{
	        $getRazorpayData =  $this->my_model->select_data([
				'table' => 'payment_credentials use index(user_id)',
				'where' => "id = '".$appDetail['razorpay_pay_id']."' AND payment_type = '6' AND status = 1 AND (user_id = '".$appDetail['user_id']."' OR app_id = '".$_SESSION['app_id']."')",
				'field' => 'credentials',
				'limit' => 1,
			]);
	        if(!empty($getRazorpayData) && $_SESSION['razorpaySession'] != ''){
	            $razorpaySessionArr = explode('@#',$_SESSION['razorpaySession']);
	            $razorpayCred = json_decode($getRazorpayData[0]['credentials'] , true);
	            $secretKey = $razorpayCred['key_secret'];
        		$ch = curl_init();
        		curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/payments/'.$_POST['razorpay_payment_id'].'/capture');
        		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        		curl_setopt($ch, CURLOPT_POST, 1);
        		curl_setopt($ch, CURLOPT_POSTFIELDS, 'amount='.$razorpaySessionArr[1]);
        		curl_setopt($ch, CURLOPT_USERPWD, $razorpayCred['key_id'] . ':' . $razorpayCred['key_secret']);
        		$headers = array();
        		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
        		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        		$response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);
                if ($err) {
                    $this->respData = [
                        'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                    ];
                }else {
                    $resp = json_decode($response ,true);
                    if(isset($resp['error'])){
                        $this->respData = [
                            'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                        ];
                    }else{
                        if(isset($resp['status']) && $resp['status'] == 'captured'){
                            $_SESSION['razorpaySession'] = '';
							$this->manageUserWithoutLogin($_SESSION['app_id'], $razorpaySessionArr[2]);
                            $this->manageUserLoyaltyPoint($_SESSION['app_id'], $razorpaySessionArr[2]);
        					$this->live_app->removeAddToCartCookie($_SESSION['app_id']);
        					$this->my_model->insert_data([
        						'table' => 'order_payment',
        						'data' 	=> [
        							'order_id'=> $razorpaySessionArr[2], 
        							'mode' => 6, 
        							'amount' => $razorpaySessionArr[1]/100,
        							'currency_code' => $appDetail['currencyCode'],
        							'status' => 1,
        							'data' => json_encode($resp)
        						]
        					]);
        					$this->my_model->update_data([
        						'table' => 'orders',
        						'data' 	=> [
        							'status' => 1,
        							'payment_mode' => 6,
        							'pament_status' => 1,
        						],
        						'where' => [
        							'id' => $razorpaySessionArr[2],
        						],
        						'limit' => 1
        					]);
        					$this->load->Library('setup_mail');
        					$this->setup_mail->setEmailAndNotification([
        						'emailFor' => 'appPlacedOrder', 
        						'orderId' => $razorpaySessionArr[2], 
        						'payMode' => 'razorpay',
        					]);
                            $resp = [
                				'status' => 'success',
                				'title' => 'Thank You!!',
                				'sub_title' =>  ($_POST['razorpay_payment_type'] == 'order')?'Your Order has been placed successfully.':'Your booking has been completed successfully.',
                				'message' => ''
                			];
                			$this->session->set_flashdata('message', $resp);
    			            $this->respStatus = 1;
    			            $this->respMessage = '';
    			            $this->respData = [
    			                'url' => base_url().'status-'.$appDetail['unique_id']
    			            ];
        				}else{
        				    $this->respData = [
                                'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                            ];
        				}
                    }
                }
	        }else{
                $this->respData = [
                    'url' => base_url().'status-'.$appDetail['unique_id'].'?cancel'
                ];
	        }
		}
		$this->show_result();
	}
	function manageUserLoyaltyPoint($appId, $orderId){
	    $cartKey = '_'.$appId.'Crt';
		if(isset($_COOKIE[$cartKey])){
		    $getOrderDetail = $this->my_model->select_data([
                'table' => 'orders',
                'field' => 'user_id, order_details.price_data',
                'where' => [
                    'orders.id' => $orderId
                ],
                'limit' => 1,
                'join' => ['order_details', 'orders.id = order_details.order_id', 'left']
            ]);
            if(!empty($getOrderDetail)){
    			$cartId = $_COOKIE[$cartKey];
    			$checkCartData = $this->my_model->select_data([
    			    'table' => 'add_to_cart use index(app_id)',
    			    'field' => 'loyalty',
    			    'where' => [
    			        'app_id' => $appId,
    			        'id' => $cartId
    			    ],
    			    'limit' => 1
    			]); 
    			if(!empty($checkCartData) && $checkCartData[0]['loyalty'] != ''){
        		    $loyaltyData = json_decode($checkCartData[0]['loyalty'], true);
        		    $pointToDeduct = (isset($loyaltyData['redeem_points']) ? $loyaltyData['redeem_points'] : 0);
        		    if($pointToDeduct != '' || $pointToDeduct != 0){
        		        $this->my_model->update_data([
            		        'table' => 'manage_loyalty_points',
            		        'where' => [
            		            'user_id' => $getOrderDetail[0]['user_id'],
            		            'app_id' => $appId,
            		            'points > ' => 0
            		        ],
            		        'data' => ['points', "points-$pointToDeduct", FALSE],
            		        'limit' => 1
        		        ]);
        		        $this->my_model->insert_data([
            		        'table' => 'online_loyalty_data',
            		        'data' => [
            		            'user_id' => $getOrderDetail[0]['user_id'],
            		            'app_id' => $appId,
            		            'order_id' => $orderId,
            		            'redeemed_points' => $pointToDeduct
            		        ]
        		        ]);
    		        }
        		}
        		$checkAvailPoint = $this->my_model->select_data([
        		    'table' => 'manage_loyalty_points use index(user_id)',
        		    'field' => 'points, id',
        		    'where' => [
        		        'user_id' => $getOrderDetail[0]['user_id']
        		    ],
        		    'limit' => 1
        		]);
        		 $getMinimumAmount = $this->my_model->select_data([
    		        'table' => 'loyalty_program use index(app_id)',
    		        'field' => 'min_amnt_gain_point',
    		        'where' => [
    		            'app_id' => $appId,
    		            'type' => 1
    		        ],
    		        'limit' => 1
    		    ]);
                $totalPoint = $purchasePrice = 0;    	
                $priceData = (!empty($getOrderDetail[0]['price_data']) ? json_decode($getOrderDetail[0]['price_data'], true) : '');
                if($priceData != ''){
                    $purchasePrice = $priceData['sellingPrice'];
                }
    		    if(!empty($getMinimumAmount) && $purchasePrice != 0 && $getMinimumAmount[0]['min_amnt_gain_point'] <= $purchasePrice){
    		        $totalPoint = intdiv($purchasePrice, $getMinimumAmount[0]['min_amnt_gain_point']);
    		    }
    		    if(!empty($checkAvailPoint)){ //update
    		        $this->my_model->update_data([
    		            'table' => 'manage_loyalty_points',
    		            'data' => ['points', "points+$totalPoint", FALSE],
    		            'where' => [
    		                'id' => $checkAvailPoint[0]['id'],
    		                'app_id' => $appId,
    		            ],
    		            'limit' => 1
    		        ]);
        		}else{ //insert
        		    $this->my_model->insert_data([
    		            'table' => 'manage_loyalty_points',
    		            'data' => [
    		                'points' => $totalPoint,
    		                'user_id' => $getOrderDetail[0]['user_id'],
    		                'app_id' => $appId
    		            ]
    		        ]);
        		}
            }
		}
	}
	function stripe_booking_checkout(){
		$this->load->library('live_app');
		$appDetail = $this->live_app->getAppDetails($_SESSION['app_id']);
		$resp = [
			'status' => 'cancel',
			'title' => 'Oppps!!',
			'sub_title' => 'your payment has been cancelled.',
			'message' => ''
		];
		$this->session->set_flashdata('message', $resp);
		if(!isset($_SESSION['stripeBookingSession'])) {
			redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
		}else {
			
			$stripeSessionArr = explode('@#',$_SESSION['stripeBookingSession']);
			if(count($stripeSessionArr) > 2){
				$checkDraft = $this->my_model->select_data([
					'table' => 'booking_draft use INDEX(user_id)',
					'field' => 'data,is_stripe,stripe_id',
					'where' => [
						'booking_draft.id' => $stripeSessionArr[2],
					],
					'limit' => 1,
					'join' => [
						'bookings', 'bookings.app_id = booking_draft.app_id'
					]
				]);

				
				if(!empty($checkDraft)){
					$bookingData = $checkDraft[0];				
					$bookingDetails = json_decode($bookingData['data'] , true);
					$getStripeData =  $this->my_model->select_data([
						'table' => 'payment_credentials use index(user_id)',
						'where' => "id = '".$bookingData['stripe_id']."' AND payment_type = '2' AND status = 1 AND (user_id = '".$appDetail['user_id']."' OR app_id = '".$_SESSION['app_id']."')",
						'field' => 'credentials',
						'limit' => 1,
					]);
					// print_r($getStripeData); exit;

					if(!empty($getStripeData)){
						$getStripeData = json_decode($getStripeData[0]['credentials'] , true);
						try {
							require_once APPPATH."third_party/stripe/init.php";
							$secretKey = $getStripeData['secret_key'];
							\Stripe\Stripe::setApiKey($secretKey); //Replace with your Secret Key
							if( $_SESSION['stripeBookingSession'] != '' ) {
								$charge =  \Stripe\Charge::create(array(
									"amount" => $stripeSessionArr[1]*100,
									"currency" => strtolower($appDetail['currencyCode']),
									"card" => $_POST['stripeToken'],
									"description" => $stripeSessionArr[0],
								));
								$_SESSION['stripeBookingSession'] = '';
								//retrieve charge details
								$chargeJson = $charge->jsonSerialize();

								


 								$bookingDetails['is_payable'] = 1;
								$bookingDetails['pay_amount'] = $bookingDetails['currency'].$bookingDetails['pay_amount'];
								unset($bookingDetails['currency'] , $bookingDetails['currency_code']);
								$bookingDetails['pay_mode'] = 2;
								$bookingDetails['status'] = 1;
								$bookingDetails['pay_status'] = 1;
								$bDetailId = $this->my_model->insert_data([
									'table' => 'bookings_details',
									'data' 	=> $bookingDetails
								]);
								$this->my_model->delete_data([
									'table' => 'booking_draft',
									'where' => [
										'id' => $stripeSessionArr[2],
									],
									'limit' => 1
								]);
								$this->my_model->insert_data([
									'table' => 'booking_payment',
									'data' 	=> [
										'booking_id'=> $bDetailId, 
										'amount' => $stripeSessionArr[1],
										'currency' => $appDetail['currencyCode'],
										'status' => 1,
										'data' => json_encode($charge)
									]
								]);
								$resp = [
									'status' => 'success',
									'title' => 'Thank You!!',
									'sub_title' => 'Your booking has been completed successfully.',
									'message' => ''
								];
								$this->load->Library('setup_mail');
                                $this->setup_mail->setEmailAndNotification([
                                    'emailFor' => 'appBooking',
                                    'bookingId' => $bDetailId,
                                    'status' => 1
                                ]);
								$this->session->set_flashdata('message', $resp);
								$rediUrl = base_url().'status-'.$appDetail['unique_id'].'?success';
								redirect($rediUrl);
							}else {
								redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
							}
						}
						catch(Stripe_CardError $e) {
							redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
						}
						catch (Stripe_InvalidRequestError $e) {
							redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
						} catch (Stripe_AuthenticationError $e) {
							redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
						} catch (Stripe_ApiConnectionError $e) {
							redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
						} catch (Stripe_Error $e) {
							redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
						} catch (Exception $e) {
							redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
						}
					}else{
						redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
					}
				}else{
					redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
				}
			}else{
				redirect(base_url().'status-'.$appDetail['unique_id'].'?cancel');
			}
		}
	}
    function my_invoice($orderDetail){
		$orderData = explode('-' , str_replace('#ORD' , '' , urldecode($orderDetail)));
		if(count($orderData) > 1){
			$orderId = $orderData[0];
			$appId = $orderData[1];
			$checkOrder = $this->my_model->select_data([
				'table' => 'orders use INDEX(app_id)',
				'where' => [
					'orders.id' => $orderId,
					'app_id' => $appId,
				],
				'field' => 'price_data,shipping_data,products_data,orders.status as order_status,coupon_data,loyalty_data,cart_data,payment_mode',
				'limit' => 1  ,
				'join' => ['order_details', 'order_details.order_id = orders.id'],    
			]);	
			if(!empty($checkOrder)){
				$this->load->library('live_app');
            	$prStatus = $this->live_app->orderStatus();
				$orderData = $checkOrder[0];
				$checkOrderStatus = $this->my_model->select_data([
				    'table' => 'order_status',
				    'field' => 'order_status.*, orders.order_on, orders.pament_status',
				    'where' => [
				        'order_id' => $orderId
				    ],
				    'join' => ['orders', 'order_status.order_id = orders.id'],
				    'order' => ['id', 'desc'],
				    'limit' => 1
				]);
				$statusData = [];
                if(!empty($checkOrderStatus)){
                    $orderStatus = $checkOrderStatus[0];
                        $dbName = $this->my_model->select_data([
                            'table' => 'sub_user_details',
                            'where' => [
                                'user_id' => $orderStatus['db_id']
                            ],
                            'field' => 'name, last_name',
                            'limit' => 1
                        ]);
                        $delivery_manager = '';
                        if(!empty($dbName)){
                            if($orderStatus['role'] == 1){
                                $assignDbName = $this->my_model->select_data([
                                   'table' => 'sub_user_details',
                                    'where' => [
                                        'user_id' => $orderStatus['added_by']
                                    ],
                                    'field' => 'name, last_name',
                                    'limit' => 1
                                ]);
                                if(!empty($assignDbName)){
                                    $delivery_manager = $assignDbName[0]['name'].' '.$assignDbName[0]['last_name'].' assigned this order to '.$dbName[0]['name'].' '.$dbName[0]['last_name'].' on '.date('d M Y h:i A', strtotime($orderStatus['added_on']));
                                }
                            }else{
                                $delivery_manager = $dbName[0]['name'].' '.$dbName[0]['last_name'];
                            }
                        }
                        $statusData = [
                            'order_date' => date('d M Y h:i A', strtotime($orderStatus['order_on'])),
                            'date_of_placement' => date('d M Y h:i A', strtotime($orderStatus['added_on'])),
                            'payment_status' => ($orderStatus['pament_status'] == 1 ? 'Paid' : 'In-complete'),
                            'delivery_status' => $prStatus[$orderStatus['order_status']],
                            'assign_delivery_manager' => $delivery_manager
                        ];
                }
				$checkPayment = $this->my_model->select_data([
					'table' => 'order_payment use INDEX(order_id)',
					'where' => [
						'order_id' => $orderId,
					],
					'field' => 'amount,currency',
					'limit' => 1,
				]);	
				$this->load->library('live_app');
            	$appDetail = $this->live_app->getAppDetails($appId); 
				$appsdetails = $this->my_model->select_data([
					'table' => 'apps_details use INDEX(app_id)',
					'where' => [
						'app_id' => $appId,
					],
					'field' => 'seller_address',
					'limit' => 1  , 
				]);
				$appsData = $this->my_model->select_data([
					'table' => 'apps use INDEX(user_id)',
					'where' => [
						'id' => $appId,
					],
					'field' => 'app_color',
					'limit' => 1  , 
				]);
				$sellerAddress = '';
				if(!empty($appsdetails)){
					$appsdetails = ($appsdetails[0]['seller_address'] != '')?json_decode($appsdetails[0]['seller_address'] , true):[];
					if(!empty($appsdetails)){
						$sellerAddress = $appsdetails['address_line_1'].'</br>'.$appsdetails['address_line_2'].'</br><b>Email:</b> '.$appsdetails['email_address'].'</br><b>Contact:</b> '.$appsdetails['phone_number'];
						if(is_numeric($appsdetails['state'])){
							$countrySData = $this->my_model->select_data([
								'table' => 'states use INDEX(country_id)',
								'where' => "states.id = '".$appsdetails['state']."'",
								'field' => 'states.name as state_name, country.name as country_name',
								'join' => [
									'country' , 'country.id = states.country_id'
								],
								'limit' => 1
							]);
							$countrySData = !empty($countrySData)?$countrySData[0]:[];							
						}else{
							$countrySData = [];
						}
						$appsdetails['state'] = isset($countrySData['state_name'])?$countrySData['state_name']:'';
						$appsdetails['country'] = isset($countrySData['country_name'])?$countrySData['country_name']:'';
						$sellerAddress = '<p style="margin: 0">'.$appsdetails['address_line_1'].', '.$appsdetails['address_line_2'].', <br>'.$appsdetails['city'].', '.$appsdetails['state'].' ('.$appsdetails['country'].')<br>'.$appsdetails['zip'].'</p>';
					}
				}
				// echo"<pre>";print_r($appsData);die;
				$shippingDatas = $orderData['shipping_data'] != '' ? json_decode($orderData['shipping_data'] , true) : [];
				$data = [
					'priceData' => json_decode($orderData['price_data'] , true),
					'shippingData' => isset($shippingDatas[0])?$shippingDatas[0]:$shippingDatas,
					'productData' => json_decode($orderData['products_data'] , true),
					'couponData' => json_decode($orderData['coupon_data'] , true),
					'loyaltyData' => json_decode($orderData['loyalty_data'] , true),
					'cartData' => json_decode($orderData['cart_data'] , true),
					'colorData' => json_decode($appsData[0]['app_color'] , true),
					'orderId' => $orderDetail,
					'status' => $prStatus[$orderData['order_status']],
					'appLogo' => $appDetail['appLogo'],
					'appTitle' => $appDetail['appTitle'],
					'sellerAddress' => $sellerAddress,
					'paymentData' => $checkPayment,
					'statusData' => $statusData
				];
				// if(isset($_GET['test'])){
				// 	echo '<pre>';
				// 	print_r($orderData['shipping_data']);
				// }
				$this->load->view('templates/order_details' , $data);
			}
		}
    }
    function loyalty_program($loyaltyId){
        $todayDate = date('Y-m-d');
        $_GET['date'] = base64_decode($_GET['date']);
        if(isset($_GET['date']) && $_GET['date'] == $todayDate){
            $this->check_url_for_login(base_url() , 'loyalty-program/'.$loyaltyId.'?date='.base64_encode(date('Y-m-d')));
            $appId = $_SESSION['app_id'];
            $checkLoyalty = $this->my_model->select_data([
                'table' => 'loyalty_program use index(uuid, app_id)',
                'field' => '*',
                'where' => "uuid = '$loyaltyId' AND app_id = '$appId' AND (start_date <= '$todayDate' OR end_date >= '$todayDate' )",
                'limit' => 1
            ]);
            if(!empty($checkLoyalty)){
                $todayDate = date('Y-m-d');
                //checking per day validity for loyalty card
                $checkAsValidity = $this->my_model->select_data([
                    'table' => 'manage_loyalty_data',
                    'field' => 'id',
                    'where' => "added_on LIKE '%$todayDate%' AND loyalty_uuid = '$loyaltyId'",
                    'limit' => $checkLoyalty[0]['per_day_validity']
                ]);
                if(!empty($checkAsValidity) && sizeof($checkAsValidity) == $checkLoyalty[0]['per_day_validity']){ // if total cards are already scanned
                    $this->session->set_flashdata('message_type', 'error');
                    $this->session->set_flashdata('loyalty_message', 'You have already exceeded the scan limit.');
                }else{
                    $countLoyaltyData = $this->my_model->aggregate_data([
                        'function' => 'COUNT',
            			'field_name' => 'id',
            			'table' => 'manage_loyalty_data',
                        'where' => [
                            'loyalty_uuid' => $loyaltyId,
                            'app_id' => $appId,
                            'user_id' => $_SESSION['id']
                        ]
                    ]);    
                    if($countLoyaltyData == 0 || ($countLoyaltyData != 0 && $checkLoyalty[0]['no_of_cards'] != $countLoyaltyData)){ // insert when no data in table
                        $addScratchData = $this->my_model->insert_data([
                            'table' => 'manage_loyalty_data',
                            'data' => [
                                'loyalty_uuid' => $loyaltyId,
                                'app_id' => $appId,
                                'user_id' => $_SESSION['id']
                            ]
                        ]);
                        $checkCountTable = $this->my_model->select_data([
                            'table' => 'manage_loyalty_count',
                            'field' => 'id',
                            'where' => [
                                'loyalty_uuid' => $loyaltyId,
                                'user_id' => $_SESSION['id']
                            ],
                            'limit' => 1
                        ]);
                        if(empty($checkCountTable)){ //insert
                            $this->my_model->insert_data([
                                'table' => 'manage_loyalty_count',
                                'data' => [
                                    'loyalty_uuid' => $loyaltyId,
                                    'user_id' => $_SESSION['id'],
                                    'redeem_count' => 1
                                ]
                            ]);
                        }else{ // update
                            $this->my_model->update_data([
                                'table' => 'manage_loyalty_count',
                                'data' => [ 'redeem_count', 'redeem_count + 1', FALSE ],
                                'where' => [ 'id' => $checkCountTable[0]['id'] ],
                                'limit' => 1
                            ]);
                        }
                        $this->session->set_flashdata('message_type', 'success');
                        $this->session->set_flashdata('loyalty_message', 'Congratulations, you have successfully scanned your stamps.');
                    }else{
                        $this->session->set_flashdata('message_type', 'success');
                        $this->session->set_flashdata('loyalty_message', $checkLoyalty[0]['winning_text']);
                    }
                }
            }else{
                $this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('loyalty_message', 'Loyalty Program Doesn\'t exist.');
            }
        }else{
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('loyalty_message', 'Something went wrong.');
        }
        redirect(base_url());
    }
}