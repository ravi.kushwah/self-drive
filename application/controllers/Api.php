<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index(){
        echo 'We are here..';
    }
    
	public function dropbox(){
	    if (isset($_GET['code'])) {
            $code = $_GET['code'];    
            $this->load->library('Dropbox_libraries'); 
            $result = $this->dropbox_libraries->getAccesToken($code);
            $res = json_decode( $result, true );
            if(isset($res['access_token'])){
                // echo"<pre>";print_r($res);
                $userInfo = $this->dropbox_libraries->getUserDetails($res['account_id'], $res['access_token']);
                $userSpaceUsage = $this->dropbox_libraries->getSpaceUsage($res['access_token']);
                $userSpace = json_decode( $userSpaceUsage, true );
                $totalSpace = $userSpace['allocation']['allocated'];
                $usedSpace = $userSpace['used'];
            //   echo"<pre>";print_r($userInfo);
                $userInfo = json_decode( $userInfo, true );
                $userInfo['usedSpace'] = $usedSpace;
                $userInfo['totalSpace'] = $totalSpace;
                if(isset($userInfo)){
                    $data = array(
                        'user_id' => $_SESSION['id'],
                        'email' => $userInfo['email'],
                        'access_token' => $result,
                        'user_info' => json_encode( $userInfo ),
                        'status' => 1,
                        'connection_type' =>'DropBox'
                    );
                    $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'email'=>$userInfo['email'],'connection_type'=>'DropBox'));
                    if(!empty($checkValue)){
                        $data = array(
                            'email' => $userInfo['email'],
                            'access_token' => $result,
                            'user_info' => json_encode( $userInfo ),
                            'status' => 1
                        );
                        
                        $result = $this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$userInfo['email'],'connection_type'=>'DropBox'));
                        if($result==true){
                             redirect( 'home/dashboard', 'refresh' );
                        }else{
                             redirect( 'home/connection', 'refresh' );
                        }
                    }else{
                        $result = $this->DBfile->put_data('connection',$data);
                        if($result==true){
                             redirect( 'home/dashboard', 'refresh' );
                        }else{
                             redirect( 'home/connection', 'refresh' );
                        }
                   }
                }
                echo"<pre>";print_r($userInfo);die;
            }
        }
	}
	
	public function box_data(){
	    $info = [
	        'name' => $_POST['name'],
	        'email' => $_POST['email'],
	        'accountId' => $_POST['accountId'],
	    ];
	    
        setcookie('box_data', json_encode($info), time() + (86400 * 30), "/");
            // echo"<pre> user";print_r($_COOKIE['box_data']);die;
        $this->load->library('box_api');
	    $BoxUrl = $this->box_api->getAuthUrl();
	    $res = array('status'=>1,'AuthURL'=>$BoxUrl);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	   // header('Location: '.$BoxUrl);
    }
    
    public function box(){
       
	    if (isset($_GET['code'])) {
            $code = $_GET['code'];    
            $this->load->library('Box_api'); 
            $result = $this->box_api->getAccesToken($code);
            $res = json_decode( $result, true );
                                    
            if(isset($res['access_token'])){
                $refToken = $this->box_api->getAccesTokenFromRefreshToken($res['refresh_token']);
       
                $refreshToken = json_decode( $refToken, true );
                if($refreshToken){
                    // $userInfo = $this->box_api->getUserDetails(BOXACCOUNTID, $refreshToken['access_token']);
                    $userDetials = json_decode($_COOKIE['box_data'], true );
                    // echo"<pre> user";print_r(json_decode($_COOKIE['box_data'], true));die;
                    
                    if(!empty($userDetials)){
                        $data = array(
                            'user_id' => $_SESSION['id'],
                            'email' => $userDetials['email'],
                            'access_token' => $refToken,
                            'refresh_token' => $refreshToken['refresh_token'],
                            'user_info' => json_encode($userDetials),
                            'status' => 1,
                            'connection_type' =>'Box'
                        );
                        //  echo"<pre> user";print_r($data);die;
                        $checkValue = $this->DBfile->get_data('connection',['user_id'=>$this->session->userdata('id'),'email'=> $userDetials['email'],'connection_type'=>'Box']);
                        if(!empty($checkValue)){
                            $data = array(
                                'email' => $userDetials['email'],
                                'access_token' => $refToken,
                                'refresh_token' => $refreshToken['refresh_token'],
                                'user_info' => json_encode( $userDetials ),
                                'status' => 1
                            );
                            $result = $this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$userDetials['email'],'connection_type'=>'Box'));
         
                            if($result==true){
                                 redirect( 'home/dashboard', 'refresh' );
                            }else{
                                 redirect( 'home/connection', 'refresh' );
                            }
                        }else{
                            $result = $this->DBfile->put_data('connection',$data);
                            if($result==true){
                                 redirect( 'home/dashboard', 'refresh' );
                            }else{
                                 redirect( 'home/connection', 'refresh' );
                            }
                       }
                    }else{
                        
                    }
                }
            }else{
                redirect( 'home/dashboard', 'refresh' );
          
            }
        }
    }
    
    // public function box(){
       
	   // if (isset($_GET['code'])) {
    //         $code = $_GET['code'];    
    //         $this->load->library('Box_api'); 
    //         $result = $this->box_api->getAccesToken($code);
    //         $res = json_decode( $result, true );
                                    
    //         if(isset($res['access_token'])){
    //             $refToken = $this->box_api->getAccesTokenFromRefreshToken($res['refresh_token']);
       
    //             $refreshToken = json_decode( $refToken, true );
    //             if($refreshToken){
    //                 // $userInfo = $this->box_api->getUserDetails(BOXACCOUNTID, $refreshToken['access_token']);
    //                 $userDetials = json_decode($_COOKIE['box_data'], true );
    //                 // echo"<pre> user";print_r(json_decode($_COOKIE['box_data'], true));die;
                    
    //                 if(!empty($userDetials)){
    //                     $data = array(
    //                         'user_id' => $_SESSION['id'],
    //                         'email' => $userDetials['email'],
    //                         'access_token' => $refToken,
    //                         'refresh_token' => $refreshToken['refresh_token'],
    //                         'user_info' => json_encode($userDetials),
    //                         'status' => 1,
    //                         'connection_type' =>'Box'
    //                     );
    //                     //  echo"<pre> user";print_r($data);die;
    //                     $checkValue = $this->DBfile->get_data('connection',['user_id'=>$this->session->userdata('id'),'email'=> $userDetials['email'],'connection_type'=>'Box']);
    //                     if(!empty($checkValue)){
    //                         $data = array(
    //                             'email' => $userDetials['email'],
    //                             'access_token' => $refToken,
    //                             'refresh_token' => $refreshToken['refresh_token'],
    //                             'user_info' => json_encode( $userDetials ),
    //                             'status' => 1
    //                         );
    //                         $result = $this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$userDetials['email'],'connection_type'=>'Box'));
         
    //                         if($result==true){
    //                              $resp = array('status'=>'1','msg'=> 'Connection create successfully.', 'url'=>'home/dashboard');
    //                         }else{
    //                              $resp = array('status'=>'0','msg'=> 'Please try agein later.', 'url'=>'home/dashboard');
    //                         }
    //                     }else{
    //                         $result = $this->DBfile->put_data('connection',$data);
    //                         if($result==true){
    //                              $resp = array('status'=>'1','msg'=> 'Connection create successfully.', 'url'=>'home/dashboard');
    //                         }else{
    //                              $resp = array('status'=>'0','msg'=> 'Please try agein later.', 'url'=>'home/connection');
    //                         }
    //                   }
    //                 }else{
    //                      $resp = array('status'=>'0','msg'=> 'Please try agein later.', 'url'=>'home/connection');
    //                 }
    //             }
    //         }else{
    //             $resp = array('status'=>'0','msg'=> 'Please try agein later.', 'url'=>'home/connection');
    //         }
    //         echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
	   //     die();
    //     }
    // }
    
    public function ddownload(){
        
        if (isset($_POST['key'])) {
            
            $this->load->library('Ddownload_api'); 
            $result = $this->ddownload_api->getUserDetails($_POST['key']);
		    $json = json_decode( $result, true );
            if(isset($json) && $json['status'] === 200){
                $json['key'] = $_POST['key'];
                $userInfo = $json['result'];
                $data = array(
                    'user_id' => $_SESSION['id'],
                    'email' => $json['result']['email'],
                    'access_token' => json_encode( $json ),
                    'user_info' => json_encode( $userInfo ),
                    'status' => 1,
                    'connection_type' =>'Ddownload'
                );
                $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'email'=>$userInfo['email'],'connection_type'=>'Ddownload'));
                if(!empty($checkValue)){
                    $data = array(
                        'email' => $userInfo['email'],
                        'access_token' => json_encode( $json ),
                        'user_info' => json_encode( $userInfo ),
                        'status' => 1
                    );
                    
                    $result = $this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$userInfo['email'],'connection_type'=>'Ddownload'));
                // echo"<pre>";print_r($data);die;
                    
                    if($result==true){
                         $resp = array('status'=>'1');
                        //  redirect( 'home/dashboard', 'refresh' );
                    }else{
                         $resp = array('status'=>'0');
                         echo "test";
                        //  redirect( 'home/connection', 'refresh' );
                    }
                    
                }else{
                    $result = $this->DBfile->put_data('connection',$data);
                    if($result==true){
                         $resp = array('status'=>'1');
                    }else{
                         $resp = array('status'=>'0');
                    }
               }
            echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
    	    die();
            }
		    
        }
    }
    
}