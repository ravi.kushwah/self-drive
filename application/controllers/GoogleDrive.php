<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class GoogleDrive extends CI_Controller {
    
    function __Construct(){
        parent::__Construct();
        $this->load->library('Googledrive');
        $this->load->library('Dropbox_libraries');
    }
    
	public function index(){
		$this->checkLoginUser();
		$data['pageName'] = 'Login';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/login');
		$this->load->view('auth/footer');
	}	

	public function forgotpassword(){
		$data['pageName'] = 'Forgot Password';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/forgotpassword');
		$this->load->view('auth/footer');
	}	

    public function signup(){
		$data['pageName'] = 'Sign Up';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/signup');
		$this->load->view('auth/footer');
	}
	public function Page_not_found(){
		$data['pageName'] = '404 Page Not found';
		$this->load->view('frontend/404',$data);

	}
	
	function checkLoginUser(){
		if(isset($this->session->userdata['loginstatus']) && $this->session->userdata['type'] == 1 )
			redirect(base_url('admin/dashboard'));
		if(isset($this->session->userdata['loginstatus']) && $this->session->userdata['type'] == 2 )
			redirect(base_url('home/dashboard'));
	}

	private function checkUserLoginStatus(){
		if(!isset($this->session->userdata['loginstatus']) || $this->session->userdata['type'] != 2 )
			redirect(base_url());
	}

	private function checkValidAJAX(){
		$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		if( strpos($ref,base_url()) === false )
			die('Unauthorize Access!!');
		if( !isset($_POST) )
			die('Unauthorize Access!!');
		
		$postData = array();
		foreach( $_POST as $key=>$soloProd ){
			if( $key != 'w_seoextscript' ){
				$temp = $this->input->post($key,TRUE);
				//$temp = $this->db->escape($temp);
				$postData[$key] = $temp;
			}
			else
				$postData[$key] = $soloProd;
		}
		return json_encode($postData);
	}

	function logout(){
		setcookie('check_seo_login', 1 , time() - (86400 * 30 * 24), "/");
		$this->session->sess_destroy();
			setcookie('uem', '', time() - (86400 * 30), "/");
			setcookie('upwd', '', time() - (86400 * 30), "/");
		redirect(base_url());
	}
	
	function profile(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Profile';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/profile',$data);
		$this->load->view('backend/footer',$data);
	}
 function adminProfile(){
        if(isset($_POST['name'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$data_arr['name']=$postData['name'];
			if( $postData['password'] != '' ) {
				$pwd = $postData['password'];
				unset($postData['password']);
				$data_arr['password'] = md5($pwd);
			}
			else{
				unset($postData['pwd']);
			}
			$config['upload_path'] ='./assets/admin/images/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|txt|';
            $config['max_size']    = '0';		
            $this->load->library('upload', $config);
            if(isset($_FILES['profile_image']) && !empty($_FILES['profile_image']['name'])){
                if ($this->upload->do_upload('profile_image')){
                    $uploaddata = $this->upload->data();
                    $pic = $uploaddata['raw_name'];
                    $pic_ext = $uploaddata['file_ext'];
                    $image_name = $pic.'_'.date('ymdHis').$pic_ext;
                    rename('./assets/admin/images/'.$pic.$pic_ext,'./assets/admin/images/'.$image_name);
                    $data_arr['profile_image'] = $image_name;
                }else{
                    $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                    echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                    die();
                }
            }
           
		    $res=$this->DBfile->set_data( 'usertbl', $data_arr , array('id'=>$this->session->userdata['id']) );
		    if($res==true){
		        $resp = array('status'=>'0');
            }else{
               $user_detail = array(
				'c_name' =>explode(' ', $_POST['name']),
				'c_email' => $_POST['email'],
				'profile_image' => $_FILES['profile_image']['name'],
			  );
			   $this->session->set_userdata($user_detail);
		        $resp = array('status'=>'1');
            }
         echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
		}
    }
	function dashboard(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Dashboard';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
		$data['my_cloud'] = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata['id'],'status'=>1));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/dashboard',$data);
		$this->load->view('backend/footer',$data);
	
	}
	function connection(){
	    
	    $this->checkUserLoginStatus();
	    $data['googleAuthUrl'] = $this->googledrive->getAuthUrl();
	    $data['dropboxAuthUrl'] = $this->dropbox_libraries->getAuthUrl();
    	$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Connection';
    	$data['checkUserConnectCloud'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata['id'],'status'=>1,'connection_type'=>'GoogleDrive'),'connection_type','',1);
    	$data['DropBox'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata['id'],'status'=>1,'connection_type'=>'DropBox'),'connection_type','',1);
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/connection',$data);
		$this->load->view('backend/footer',$data);
	}

	function CloudTransfer(){
	    $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Cloud Transfer';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloudtransfer',$data);
		$this->load->view('backend/footer',$data);
	}
	function CloudSync(){
	    $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Cloud Sync';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloudsync',$data);
		$this->load->view('backend/footer',$data);
	}
	function CloudBackup(){
	   $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Cloud Backup';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloudbackup',$data);
		$this->load->view('backend/footer',$data);
	}
	function Share(){
	   $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Share';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/share',$data);
		$this->load->view('backend/footer',$data);
	}
	function cloud_details($id){
	    $this->checkUserLoginStatus();
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        $data['id'] = $id;
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $data['getAllFolder'] = $this->googledrive->getAllFolders($token,null);
                $data['connectionType']='Google Drive';
                $data['email']=$result[0]['email'] ;
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $accessToken = $token['access_token'];
                $data['getAllFolder'] = $this->dropbox_libraries->getAllFolders($accessToken);
                $data['connectionType']='DropBox';
                $data['email']=$result[0]['email'] ;
                // echo"<pre>";print_r($data['getAllFolder']);die;
            }
        }
        
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata['id']));
    	$data['pageName'] = 'Cloud Details ';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloud_details',$data);
		$this->load->view('backend/footer',$data);
	}
	function getSubFolderByFolderId(){
	    if(empty($_POST['folderId'])){
	         $folderid ='';
	    }else{
	         $folderid =$_POST['folderId'];
	    }
	    if(!empty($_POST['drType']) && $_POST['drType']!='undefined'){
	         $folderid =$_POST['drType'];
	    }
	    
	    $id =$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
       
        $token = json_decode($result[0]['access_token'],true);
        //  print_r($_POST);die;
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                 $ar = $this->googledrive->getAllFolders($token, $folderid);
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $accessToken = $token['access_token'];
                
                $ar = $this->dropbox_libraries->getAllFolders($accessToken,$folderid);
                // print_r($ar);die;
                // echo"<pre>";
            }
        }
        
	    $res = array('userId'=>$result[0]['id'],'data'=>$ar);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function userCreateFolder(){
	   
	    $id =$_POST['user_id'];
	    $folderName =$_POST['folderName'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
       
        $token = json_decode($result[0]['access_token'],true);
        
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                 $connection_type = 'GoogleDrive';
                 $data = $this->googledrive->createFolder($token, $folderName);
	             $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $accessToken = $token['access_token'];
                $ar = $this->dropbox_libraries->createFolder($accessToken,$folderName);
                $dropBoxData = json_decode($ar, true);
                $connection_type = 'DropBox';
                if(isset($dropBoxData['error_summary'])){
                    $data['status'] = 0; 
                }else{
                    $data['status'] = 1; 
                }
	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
        }
        
        
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function userSubCreateFolder(){
	   // echo"<pre>";print_r($_POST);die;
	    $id =$_POST['user_id'];
	    $folderName = $_POST['folderName'];
	    $parentId =$_POST['parentId'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
       
        $token = json_decode($result[0]['access_token'],true);
        
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                 $ar = $this->googledrive->createSubFolder($folderName, $parentId,$token);
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $accessToken = $token['access_token'];
	            $drotype = $_POST['drotype'];
	            $subFolderName = $drotype."/".$folderName; 
                $ar = $this->dropbox_libraries->createFolder($accessToken,'',$subFolderName );
            }
        }
        
	    $res = array('userId'=>$result[0]['id'],'data'=>$ar);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function userUploadFiles(){
	    
	    $fileName = $_FILES['file']['name'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$_POST['user_id']));
        
        $token = json_decode($result[0]['access_token'],true);
		$access_token = $token['access_token'];
		   
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
			 
                $config['upload_path'] = 'uploads/googleDrive';
    			$config['allowed_types'] = 'jpeg|jpg|png|gif|SVG|svg|pdf|zip|mp4|mp3|html|css|js|php|sql';
                $config['max_size']    = '0';
                $this->load->library('upload', $config);
                
                if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
                    if ($this->upload->do_upload('file')){
                        $uploaddata = $this->upload->data();
                        $pic = $uploaddata['raw_name'];
                        $pic_ext = $uploaddata['file_ext'];
                        $image_name = $pic.'_'.date('ymdHis').$pic_ext;
                        rename('./uploads/googleDrive/'.$pic.$pic_ext,'./uploads/googleDrive/'.$image_name);
                        $filename = $image_name;
                    }else{
                        $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                        echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                        die();
                    }
                }  
                
                $target_file = 'uploads/googleDrive/'.$filename; 
                $file_content = file_get_contents($target_file); 
                $mime_type = mime_content_type($target_file);  
                
                $google = $this->uploadFileOnGoogleDrive( $target_file, $filename, $_FILES['file']['type'],$token,$_POST['folderid'] );
				
				$fileurl = $google['url'];
				$drivekey[] = $google['fileID'];
				// unlink($fileurl);
				// unlink(base_url().$target_file);
				// print_r($google);
				// die();
                if($google['fileID']){
                    $res = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>1);
                }else{
                     $res = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>0);
                }
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $accessToken = $token['access_token'];
                if(!empty($_POST['path'])){
                    $path = $_POST['path'];
                }else{
                     $path = '';
                }
                $ar = $this->dropbox_libraries->uploadFileDropbox($accessToken,$fileName,$path);
                if(isset($dropBoxData['error_summary'])){
                    $data['status'] = 0; 
                }else{
                    $data['status'] = 1; 
                }
	            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'res'=>$data);
            }
        }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	} 

	private function uploadFileOnGoogleDrive($file, $filename, $type,$accessToken, $folderID = ''){

		$result = array();
		$folderName = 'New Folder ';
		if(empty($folderID)){
			$rf = $this->googledrive->createFolder($accessToken, $folderName);
			if(isset($rf['status']) && $rf['status']){
				$data = array(
					'folder_id' => $rf['folderid'],
					'datetime' => date('Y-m-d H:i:s')
				);
			}
			
		}
		$gu = $this->googledrive->uploadFile( $filename, $folderID, $file, $type, $accessToken );
		
		if($gu['status']){
			$result = array( 
				'fileID' => $gu['filedata']['id'],
				'url' => "https://drive.google.com/uc?export=view&id={$gu['filedata']['id']}&ezex={$gu['ex']}"
			);
		}
		$result['folderID'] = $folderID;

		return $result;
	}
	function userDeleteFolder(){
	   
	    $id =$_POST['user_id'];
	    $folderID =$_POST['folderID'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
       
        $token = json_decode($result[0]['access_token'],true);
        
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                 $ar = $this->googledrive->deleteFiles($folderID, $token);
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $accessToken = $token['access_token'];
                $ar = $this->dropbox_libraries->deleteFile($accessToken,$folderID);
            }
        }
        
	    $res = array('userId'=>$result[0]['id'],'data'=>$ar);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
    function get_info_google_drive(){
        $this->load->library('Googledrive');
    
        if (isset($_GET['code'])) {
            $client = $this->googledrive->getClientForAuth();
            $client->authenticate($_GET['code']);	
            $token = $client->getAccessToken();
            
            $userInfo = $this->googledrive->userInformation($token);
            
            $totalSpace = $userInfo['totalStorage'];
            $usedSpace = $userInfo['useStorage'];
            
            $userInfo['usedSpace'] = $usedSpace;
            $userInfo['totalSpace'] = $totalSpace;
            $data = array(
                'user_id' => $_SESSION['id'],
                'access_token' => json_encode( $token ),
                'user_info' => json_encode( $userInfo ),
                'connection_type' => 'GoogleDrive',
                'email' => $userInfo['email'],
                'status' => 1
            );
            $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata['id'],'email'=>$userInfo['email'],'connection_type'=>'GoogleDrive'));
           if(!empty($checkValue)){
               $data = array(
                    'email' => $userInfo['email'],
                    'access_token' => json_encode( $token ),
                    'user_info' => json_encode( $userInfo ),
                    'status' => 1
                );
               $result =$this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata['id'],'email'=>$userInfo['email'],'connection_type'=>'DropBox'));
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                }else{
                     redirect( 'home/connection', 'refresh' );
                }
           }else{
               $result = $this->DBfile->put_data('connection',$data);
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                }else{
                     redirect( 'home/connection', 'refresh' );
                }
           }
            
        }
        
    }
}
?>