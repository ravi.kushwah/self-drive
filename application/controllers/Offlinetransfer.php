<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class Offlinetransfer extends CI_Controller {
    
    function __Construct(){
        parent::__Construct();
      
    }
    function index(){
        die;
        //we re not using it now its just for reference
        $data = $this->DBfile->get_data('task_list',array('status'=>0,'transfer_status'=>2),'',array('id','ASC'));
        $this->load->library('Googledrive');
        $this->load->library('Dropbox_libraries'); 
        
        if(!empty($data)){
            
            $transfer_from_id = isset($data[0]['transfer_to_id']) && !empty($data[0]['transfer_to_id']) ? $data[0]['transfer_to_id'] : '';
        
            $moveCLoudToken = $this->DBfile->get_data( 'connection', array('id'=>$transfer_from_id));
            
            $CloudToken =  json_decode($moveCLoudToken[0]['access_token'],true);
           
            $from_files_url = json_decode($data[0]['from_files_url']);
            
            $from_file_name = json_decode($data[0]['from_file_name']);
            
            $to_file_id = json_decode($data[0]['to_file_id']);
           
            foreach($data as $value){
                 
                if($value['to_name']=="GoogleDrive"){
                    
                    foreach($from_files_url as $key=>$files){
                        
                       
                        $resp = $this->uploadFileOnGoogleDrive( $files, $from_file_name[$key], mime_content_type($files),$CloudToken,$to_file_id[0]);
                        
                        
                        $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$from_file_name[$key];
                        
                        if(isset($resp['fileID']) && !empty($resp['fileID'])){
                             $data_arr = array(
                                        'status'=>1,
                                        'transfer_status'=>1,
                                    );
                            $this->DBfile->set_data( 'task_list', $data_arr , array('id'=>$value['id']));
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                        }
                    }
                }else if($value['to_name']=="DropBox"){
                    
                   foreach($from_files_url as $key=>$files){
                         
                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($CloudToken['refresh_token']);
                
                        $ntoken = json_decode($access_token,true);
                        $accessToken = $ntoken['access_token'];
                        
                        $resp = $this->dropbox_libraries->uploadFile($accessToken, $to_file_id[$key], $files , $from_file_name[$key]);
                     
                        $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$from_file_name[$key];
                        
                        if(isset($resp['name']) && !empty($resp['name'])){
                             $data_arr = array(
                                        'status'=>1,
                                        'transfer_status'=>1,
                                    );
                            
                            $this->DBfile->set_data( 'task_list', $data_arr , array('id'=>$value['id']));
                            unlink($newfname);
                        }else{
                           echo $resp['error_summary'];
                        }
                    }
                }else if($value['to_name']=="Box"){
                    print_r();
                    die($moveCLoudToken);
                    $this->load->library('Box_api');
                    $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box');
                    
                    if($access_token){
                        $folderId = $_POST['MoveFolderID'];
                        $fileName = $fname;
                        $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fname);
                       
                       if(isset($ar['entries']) != ''){
                            $ar = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'');
                          
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'');
                            
                        }
                    }
                }else if($value['to_name'] == 's3'){
                       
                    $this->load->helper('aws');
                    $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
              
                    if(isset($data['url'])){
                        $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                         $data = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                        unlink($newfname);
                    }else{
                        unlink($newfname);
                        $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0);
                    }
                }else if($value['to_name'] == 'Backblaze'){
                    print_r($_POST);
                    die();
                    $this->load->helper('back_blaz');
                    
                    $ar = b2_uploadFile( $_POST['folderid'], $newfname, $fname, $CloudToken['key_id'], $CloudToken['application_key']);
                    $connection = 'Backblaze';
              
                    if(isset($ar['fileName'])){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1);
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0);
                      unlink($newfname);
                    }
    	           
                }else if($value['to_name'] == 'Gofile'){
                    print_r($_POST);
                    die();
                    $this->load->library('Gofile_lib');
                    
                    $ar = $this->gofile_lib->gofile_upload_file( $token['token'], $_POST['folderid'], $newfname );
                    
                    $connection = 'Gofile';
              
                    if( $ar['status'] == 'ok'){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1);
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0);
                      unlink($newfname);
                    }
    	           
                }else if($value['to_name'] == 'GooglePhoto'){
                    print_r($_POST);
                    die();
                    $this->load->library('Googlephoto');
                    $clientData = $this->googlephoto->Getbatch($token,$_POST['MoveFileID']);
                    $GooglePhotos = json_decode($clientData,true);
                    $data =  $GooglePhotos['mediaItemResults'][0]['mediaItem'];
                    $mime_type = $data['mimeType'];
                    $productUrl = $data['productUrl'];
                    $baseUrl = $data['baseUrl'];
                    $url = $baseUrl;
                }else {
                    die;
                }
            }
            
        }else{
            echo "No Any Task ....";
        }
       
    }
    function transfer(){
     
       $data = $this->DBfile->get_data('task_list',array('status'=>0,'transfer_status'=>2),'',array('id','ASC'));
        $this->load->library('Googledrive');
        $this->load->library('Dropbox_libraries'); 
        
        if(!empty($data)){
            
            $transfer_from_id = isset($data[0]['transfer_to_id']) && !empty($data[0]['transfer_to_id']) ? $data[0]['transfer_to_id'] : '';
        
            $moveCLoudToken = $this->DBfile->get_data( 'connection', array('id'=>$transfer_from_id));
            
            $CloudToken =  json_decode($moveCLoudToken[0]['access_token'],true);
           
            $from_files_url = json_decode($data[0]['from_files_url']);
            
            $from_file_name = json_decode($data[0]['from_file_name']);
            
            $to_file_id = json_decode($data[0]['to_file_id']);
            
            foreach($data as $value){
                  
                if($value['to_name']=="GoogleDrive"){
                    
                    foreach($from_files_url as $key=>$files){
                        
                        $resp = $this->uploadFileOnGoogleDrive( $files, $from_file_name[$key], mime_content_type($files),$CloudToken,$to_file_id[0]);
                        
                        // echo "<pre>"; print_r($resp);die;
                        
                        $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$from_file_name[$key];
                        
                        if(isset($resp['fileID']) && !empty($resp['fileID'])){
                             $data_arr = array(
                                        'status'=>1,
                                        'transfer_status'=>1,
                                    );
                            $this->DBfile->set_data( 'task_list', $data_arr , array('id'=>$value['id']));
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                        }
                    }
                }else if($value['to_name']=="DropBox"){
                    
                   foreach($from_files_url as $key=>$files){
                         
                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($CloudToken['refresh_token']);
                
                        $ntoken = json_decode($access_token,true);
                        $accessToken = $ntoken['access_token'];
                        
                        $resp = $this->dropbox_libraries->uploadFile($accessToken, $to_file_id[$key], $files , $from_file_name[$key]);
                     
                        $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$from_file_name[$key];
                        
                        if(isset($resp['name']) && !empty($resp['name'])){
                             $data_arr = array(
                                        'status'=>1,
                                        'transfer_status'=>1,
                                    );
                            
                            $this->DBfile->set_data( 'task_list', $data_arr , array('id'=>$value['id']));
                            unlink($newfname);
                        }else{
                           echo $resp['error_summary'];
                        }
                    }
                }else if($value['to_name']=="Box"){
                    print_r($moveCLoudToken);
                    die();
                    $this->load->library('Box_api');
                    $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box');
                    
                    if($access_token){
                        $folderId = $_POST['MoveFolderID'];
                        $fileName = $fname;
                        $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fname);
                       
                       if(isset($ar['entries']) != ''){
                            $ar = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'');
                          
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'');
                            
                        }
                    }
                }else if($value['to_name'] == 's3'){
                       
                    $this->load->helper('aws');
                    $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
              
                    if(isset($data['url'])){
                        $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                         $data = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                        unlink($newfname);
                    }else{
                        unlink($newfname);
                        $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0);
                    }
                }else if($value['to_name'] == 'Backblaze'){
                      echo"<pre>"; print_r($value);
                       die();
                    $this->load->helper('back_blaz');
                    
                    $ar = b2_uploadFile( $_POST['MoveFolderID'], $newfname, $fname, $CloudToken['key_id'], $CloudToken['application_key']);
                    $connection = 'Backblaze';
              
                    if(isset($ar['fileName'])){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1);
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0);
                      unlink($newfname);
                    }
    	           
                }else if($value['to_name'] == 'Gofile'){
                      
                    $this->load->library('Gofile_lib');
                    
                    $ar = $this->gofile_lib->gofile_upload_file( $CloudToken['token'], $_POST['MoveFolderID'], $newfname );
                    
                    $connection = 'Gofile';
              
                    if( $ar['status'] == 'ok'){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1);
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0);
                      unlink($newfname);
                    }
    	           
                }
            }
            
        }else{
            echo "No Any Task ....";
        }
    }
	private function uploadFileOnGoogleDrive($file, $filename, $type,$accessToken, $folderID = ''){

		$result = array();
		$folderName = 'New Folder ';
		  $this->load->library('Googledrive');
		if(empty($folderID)){
			$rf = $this->googledrive->createFolder($accessToken, $folderName);
			if(isset($rf['status']) && $rf['status']){
				$data = array(
					'folder_id' => $rf['folderid'],
					'datetime' => date('Y-m-d H:i:s')
				);
			}
			
		}
		$gu = $this->googledrive->uploadFile( $filename, $folderID, $file, $type, $accessToken );
		
		if($gu['status']){
			$result = array( 
				'fileID' => $gu['filedata']['id'],
				'url' => "https://drive.google.com/uc?export=view&id={$gu['filedata']['id']}&ezex={$gu['ex']}"
			);
		}
		$result['folderID'] = $folderID;

		return $result;
	} 
	
}