<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
		
	public function index(){
		if(isset($_SESSION['loginstatus']) && $this->session->userdata('type') == 1 )
			redirect(base_url('admin/dashboard'));
		$this->load->view('admin/login');
	}

	private function checkAdminLogin(){
		if(!isset($_SESSION['loginstatus']) || $this->session->userdata('type') != 1 )
			redirect(base_url());
	}

	private function checkValidAJAX(){
		$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		if( strpos($ref,base_url()) === false )
			die('Unauthorize Access!!');
		if( !isset($_POST) )
			die('Unauthorize Access!!');
			
		$postData = array();
		foreach( $_POST as $key=>$value ){
			$temp = $this->input->post($key,TRUE);
			//$temp = $this->db->escape($temp);
			$postData[$key] = $temp;
		}
		return json_encode($postData);
	}

	function logout(){
		$this->session->sess_destroy();
		setcookie('em', '', time() - (86400 * 30), "/");
		setcookie('pwd', '', time() - (86400 * 30), "/");
		redirect(base_url());
	}
	
	function dashboard(){
		$this->checkAdminLogin();
		$data['pagename'] = 'Dashboard';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['userCount'] = count($this->DBfile->get_data('usertbl',array('role'=>2),'id'));
		$this->load->view('admin/header',$data);
		$this->load->view('admin/dashboard',$data);
		$this->load->view('admin/footer',$data);
	}

	function addnew($pagetype='categories',$dbid=0){
		$this->checkAdminLogin();
		$data['pagename'] = 'Add New '.ucfirst($pagetype);
		$data['pagetype'] = $pagetype;
		if( $pagetype == 'users' ){
			$data['userData'] = array();
			if( $dbid != 0 ){
				$u = $this->DBfile->get_data('usertbl',array('id'=>$dbid));
				if(!empty($u)){
					$data['userData'] = $u;
					$data['pagename'] = 'Edit User';
				}
			}
		}

		$viewPage = 'addnew_'.$pagetype;
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/'.$viewPage,$data);
		$this->load->view('admin/footer',$data);
	}
	function submitCommonData(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$type = $postData['pageType'];
		if(isset($postData['delete'])){
			$initials = substr($type,0,1);
			$dbidCol = $initials.'_id';
			if($type=='bonus'){
				$res = $this->DBfile->get_data($type.'tbl',array($dbidCol=>$postData['deleteid']));
				if(!empty($res)){
					$upPath = (explode('/application/',__DIR__)[0]).'/assets/upload/';
					unlink($upPath.$res[0]['b_image']);
				}
			}
			$this->DBfile->delete_data( $type.'tbl', array($dbidCol=>$postData['deleteid']) );
		}
		elseif($postData['uniqeid'] != 0){
			if($type == 'product'){

				$this->load->library('Dropbox');
				$upPath = (explode('/application/',__DIR__)[0]).'/assets/products/';
				
				if(!empty($_FILES['p_imagelink']['name'])){
					$config['upload_path'] = $upPath;
					$config['allowed_types'] = '*';
					$config['file_name'] = generateString().getImageExtension($_FILES['p_imagelink']['name']);
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('p_imagelink')){
						$uploadData = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadData['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_imagelink'] = substr($finalResp['url'],0,-4).'raw=1';
						$delPath['p_imagelink'] = $uploadData['full_path'];
					}
					else{
						echo "Failed to upload Image. The reason is : " . $this->upload->display_errors();
						die();
					}
				}

				if(!empty($_FILES['p_finalprod']['name'])){
					$confi1['upload_path'] = $upPath;
					$confi1['allowed_types'] = '*';
					$confi1['file_name'] = generateString().getImageExtension($_FILES['p_finalprod']['name']);
					
					$this->load->library('upload',$confi1);
					$this->upload->initialize($confi1);
					if($this->upload->do_upload('p_finalprod')){
						$uploadDat1 = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadDat1['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_finalprod'] = substr($finalResp['url'],0,-1).'1';
						$delPath['p_finalprod'] = $uploadDat1['full_path'];
					}
					else{
						echo "Failed to upload Zip. The reason is : " . $this->upload->display_errors();
						die();
					}
				}
				
				$uniqeid = $postData['uniqeid'];
				unset($postData['uniqeid']);
				unset($postData['pageType']);

				if(!isset($postData['p_imagelink']) && $postData['p_imagelink_url'] != ''){
					$u = $postData['p_imagelink_url'];
					$postData['p_imagelink'] = substr($u,0,-4).'raw=1';
					unset($postData['p_imagelink_url']);
				}
				else
					unset($postData['p_imagelink_url']);

				if(!isset($postData['p_finalprod']) && $postData['p_finalprod_url'] != ''){
					$u = $postData['p_finalprod_url'];
					$postData['p_finalprod'] = substr($u,0,-1).'1';
					unset($postData['p_finalprod_url']);
				}
				else
					unset($postData['p_finalprod_url']);
					
				$this->DBfile->set_data( 'products',$postData, array('p_id'=>$uniqeid) );

				if(isset($delPath['p_imagelink']))
					unlink($delPath['p_imagelink']);

				if(isset($delPath['p_finalprod']))
					unlink($delPath['p_finalprod']);
			}
			else{
				$initials = substr($type,0,1);
				$dbidCol = $initials.'_id';
				$this->DBfile->set_data( $type.'tbl',$postData['tempArr'], array($dbidCol=>$postData['uniqeid']) );
			}
		}
		elseif($postData['uniqeid'] == 0){
			if($type == 'product'){
				$this->load->library('Dropbox');
				$upPath = (explode('/application/',__DIR__)[0]).'/assets/products/';
				
				if(!empty($_FILES['p_imagelink']['name'])){
					$config['upload_path'] = $upPath;
					$config['allowed_types'] = '*';
					$config['file_name'] = generateString().getImageExtension($_FILES['p_imagelink']['name']);
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('p_imagelink')){
						$uploadData = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadData['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_imagelink'] = substr($finalResp['url'],0,-4).'raw=1';
						$delPath['p_imagelink'] = $uploadData['full_path'];
					}
					else{
						echo "Failed to upload Image. The reason is : " . $this->upload->display_errors();
						die();
					}
				}

				if(!empty($_FILES['p_finalprod']['name'])){
					$confi1['upload_path'] = $upPath;
					$confi1['allowed_types'] = '*';
					$confi1['file_name'] = generateString().getImageExtension($_FILES['p_finalprod']['name']);
					$this->load->library('upload',$confi1);
					$this->upload->initialize($confi1);
					if($this->upload->do_upload('p_finalprod')){
						$uploadDat1 = $this->upload->data();
						$resp = $this->dropbox->uploadFile(ACCESS_TOKEN,DP_FOLDER_NAME,$uploadDat1['full_path']);
						$finalResp = $this->dropbox->getSharedLink(ACCESS_TOKEN,$resp['path_lower']);
						$postData['p_finalprod'] = substr($finalResp['url'],0,-1).'1';
						$delPath['p_finalprod'] = $uploadDat1['full_path'];
					}
					else{
						echo "Failed to upload Zip. The reason is : " . $this->upload->display_errors();
						die();
					}
				}
				unset($postData['uniqeid']);
				unset($postData['pageType']);

				if(!isset($postData['p_imagelink']) && $postData['p_imagelink_url'] != ''){
					$u = $postData['p_imagelink_url'];
					$postData['p_imagelink'] = substr($u,0,-4).'raw=1';
					unset($postData['p_imagelink_url']);
				}
				else
					unset($postData['p_imagelink_url']);

				if(!isset($postData['p_finalprod']) && $postData['p_finalprod_url'] != ''){
					$u = $postData['p_finalprod_url'];
					$postData['p_finalprod'] = substr($u,0,-1).'1';
					unset($postData['p_finalprod_url']);
				}
				else
					unset($postData['p_finalprod_url']);

				$this->DBfile->put_data( 'products', $postData);

				if(isset($delPath['p_imagelink']))
					unlink($delPath['p_imagelink']);

				if(isset($delPath['p_finalprod']))
					unlink($delPath['p_finalprod']);
			}
			else
				$this->DBfile->put_data( $type.'tbl', $postData['tempArr']);
		}
		
		echo '1';
		die();
	}
	
	function users($list=''){
		$this->checkAdminLogin();
		if($list == 'postreq'){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$this->DBfile->put_data( 'usertbl', $postData);
			echo '1';
			die();
		}
		if($list != ''){
			$list = $this->DBfile->get_data('usertbl',array('role'=>2),'',array('id'=>'DESC'),'');
			$i=0;
			$getPlans = $this->DBfile->get_data('plan','','id,name',array('id'=>'DESC'),'');
			$planArr = [];
    		foreach($getPlans as $plans){
    			$planArr[$plans['id']] = $plans['name'];
    		}
			$data = array();
			if(!empty($list)){
				foreach($list as $sList){
				    $userPlan = json_decode($sList['plan'], true);
    				$planName = '';
    				if(!empty($userPlan) && $userPlan != ''){
    					$userPlan = array_unique($userPlan);
    					foreach($userPlan as $uPlan){
    						$planName .= (isset($planArr[$uPlan]))? $planArr[$uPlan].' , ' :'';
    					}
    				}
					$i++;
					$t = array();
					array_push($t,$i);
					array_push($t,$sList['name']);
					array_push($t,$sList['email']);
					array_push($t,$sList['source']);
					array_push($t,rtrim($planName,' , '));
					array_push($t,'<a href="'.base_url().'admin/addnew/users/'.$sList['id'].'">'.EDIT_ICON.'</a>');
					array_push($t,'<a data-toggle="modal" data-target="#delete" style="cursor:pointer;" uid="'.$sList['id'].'" class="deleteUser">'.DELETE_ICON.'</a>');
					array_push($data,$t);
				}
			}
			echo json_encode(array('data'=>$data));
			die();
		}
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['pagename'] = 'Users';
		$this->load->view('admin/header',$data);
		$this->load->view('admin/users',$data);
		$this->load->view('admin/footer',$data);
	}

	function profile(){
		$this->checkAdminLogin();
		$data['pagename'] = 'Profile';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$this->load->view('admin/header',$data);
		$this->load->view('admin/profile',$data);
		$this->load->view('admin/footer',$data);
	}

    function adminProfile(){
        if(isset($_POST['name'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$data_arr['name']=$postData['name'];
			if( $postData['password'] != '' ) {
				$pwd = $postData['password'];
				unset($postData['password']);
				$data_arr['password'] = md5($pwd);
			}
			else{
				unset($postData['pwd']);
			}
			$config['upload_path'] ='./assets/admin/images/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|txt|';
            $config['max_size']    = '0';		
            $this->load->library('upload', $config);
            if(isset($_FILES['profile_image']) && !empty($_FILES['profile_image']['name'])){
                if ($this->upload->do_upload('profile_image')){
                    $uploaddata = $this->upload->data();
                    $pic = $uploaddata['raw_name'];
                    $pic_ext = $uploaddata['file_ext'];
                    $image_name = $pic.'_'.date('ymdHis').$pic_ext;
                    rename('./assets/admin/images/'.$pic.$pic_ext,'./assets/admin/images/'.$image_name);
                    $data_arr['profile_image'] = $image_name;
                }else{
                    $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                    echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                    die();
                }
            }
           
		    $res=$this->DBfile->set_data( 'usertbl', $data_arr , array('id'=>$this->session->userdata('id')) );
		    if($res==true){
               $user_detail = array(
				'c_name' =>explode(' ', $_POST['name']),
				'c_email' => $_POST['email'],
				'profile_image' => $_FILES['profile_image']['name'],
			  );
			   $this->session->set_userdata($user_detail);
		        $resp = array('status'=>'1');
            }else{
		        $resp = array('status'=>'0');
            }
         echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
		}
    }
	function submitUserRecords(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);

		if(isset($postData['delete'])){
		 $res=$this->DBfile->delete_data( 'usertbl', array('id'=>$postData['deleteid']));
    	    if($res){
    	        $resp = array('status'=>'1','smg'=>'Successfully Delete User ');
            }else{
    	        $resp = array('status'=>'0','smg'=>'Something went wrong. Please, refresh the page and try again.');
            }
            echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
		}
		
	    if(isset($postData['id'])){ 
    			$uid = $postData['id'];
    			unset($postData['id']);
			    $resp = $this->DBfile->get_data('usertbl',array('email'=>$postData['email']));
    			if(isset($postData['password']) && !empty($postData['password'])){
    				$postData['password'] = md5($postData['password']);
    			}else{
    				$postData['password'] = $resp[0]['password'];
    			}
    			    
    			if( $uid == 0 ){
    				$postData['role'] = 2;
    				$postData['status'] = 1;
    				
        			if(!empty($resp)){
        			     $resp = array('status'=>'0','smg'=>'Email already exists :- '.$resp[0]['email']);
        			}else{
            			if(empty($postData['name'])){
                		     $resp = array('status'=>'0','smg'=>'Name Is Blank');
                		     echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
                		}else if( empty($postData['email'])){
                		     $resp = array('status'=>'0','smg'=>'Email Is Blank');
                		     echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
                		}else if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $postData['email'])){
                            $resp = array('status'=>0,'smg'=>' Email format is not valid.');
                             echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
                        }else if( empty($postData['password'])){
                		     $resp = array('status'=>'0','smg'=>'password Is Blank');
                		     echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
                		}else{
                		    $data = array(
                		            'source'=>'admin',
            		                'name'=>$postData['name'],
            		                'email'=>$postData['email'],
            		                'password'=>$postData['password'],
            		                'status'=>$postData['status'],
            		                'plan'=>json_encode($postData['OTO']),
            		                'role'=>2,
            		                'user_type'=>$postData['user_type'],
            		        );
        		         
                		    $res=$this->DBfile->put_data( 'usertbl', $data );
            			    if($res){
                    	        $resp = array('status'=>'1','smg'=>'Add User Successfully.','url'=>'admin/users');
                            }else{
                    	        $resp = array('status'=>'0','smg'=>'Something went wrong. Please, refresh the page and try again.');
                            }
                		}
        			}
		    	}else{
		    	       $data = array(
    		                'name'=>$postData['name'],
    		                'email'=>$postData['email'],
    		                'password'=>$postData['password'],
    		                'status'=>$postData['status'],
    		                'user_type'=>$postData['user_type'],
    		                'plan'=>json_encode($postData['OTO']),
        		        );
        		    	$res=$this->DBfile->set_data( 'usertbl', $data , array('id'=>$uid) );
        		    	
        		    	if($res){
                	        $resp = array('status'=>'1','smg'=>'User Updated Successfully.','url'=>'admin/users');
                        }else{
                	        $resp = array('status'=>'0');
                        }
        			}
    			
        		
                echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
		}
		
	 
	}

}
?>