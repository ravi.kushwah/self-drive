<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {
    
    function __Construct(){
        parent::__Construct();
    }
    
    private function checkUserLoginStatus(){
		if(!isset($_SESSION['loginstatus']) || $this->session->userdata('type') != 2 )
			redirect(base_url());
	}
    
    function reseller_users($id =""){
	    $this->checkUserLoginStatus();
	   // echo !empty(array_intersect(["7"], $_SESSION['current_plan']));die;
    	if(!empty(array_intersect(["7"], $_SESSION['current_plan']))){
    	    if($id != ''){
    	        $custDetails = $this->DBfile->get_data('usertbl',array('id'=>$_SESSION['id']));
    			$temp = array(
    				'name'	=>	$custDetails[0]['name'],
    				'email'	=>	$custDetails[0]['email'],
    			);
    			echo json_encode($temp);
    			die();
    	    }else{
        		$data['pageName'] = 'Reseller Users';
        		$data['user_id'] = $_SESSION['id'];
        		
        		$this->load->view('backend/header',$data);
        		$this->load->view('backend/reseller_users',$data);
        		$this->load->view('backend/footer',$data);
    	    }
        }else{
        	redirect(base_url());
        }  
	
	}
    
    function white_label_setting($id =""){
        // print_r($_SESSION);die;
	    $this->checkUserLoginStatus();
    	$custDetails = $this->DBfile->get_data('usertbl',array('id'=>$_SESSION['id']));
    	if(!empty(array_intersect(["8"], $_SESSION['current_plan']))){
    	    if($id != ''){
    			$temp = array(
    				'name'	=>	$custDetails[0]['name'],
    				'email'	=>	$custDetails[0]['email'],
    			);
    			echo json_encode($temp);
    			die();
    	    }else{
            	$data['whiteLabelData'] = $this->DBfile->get_data('wl_setting',array('user_id'=>$_SESSION['id']));
        		$data['pageName'] = 'White Label';
        		$data['user_id'] = $_SESSION['id'];
        		
        		$this->load->view('backend/header',$data);
        		$this->load->view('backend/white_label',$data);
        		$this->load->view('backend/footer',$data);
    	    }
    	}else{
        	redirect(base_url());
        }
	
	}
	
	function save_users($id=''){
	    $this->checkUserLoginStatus();
		$email = $this->input->post('email');
		if(!empty($email)){
			$password = $this->input->post('password');
			$name = $this->input->post('name');
			if($password == $this->input->post('cnf_password')){
				if($id == ''){
					$cond = ['email' => $email ];
				}else{
					$cond = ['email' => $email, 'id !=' => $id ];
				}

				$checkEmail = $this->DBfile->get_data('usertbl',$cond, 'id');
				//  print_r($checkEmail);die;
				if(empty($checkEmail)){
					$data = [
						'name' => $this->input->post('name'),
						'user_type' => (isset($_POST['type']) && $_POST['type'] == 'reseller')? 1 : 2,
						'parent_id' => $this->session->userdata('id')
					];
					
				    $userData = $this->DBfile->get_data('usertbl',['id' =>$this->session->userdata('id')], 'role, plan', '', 1);
				    // $data[] = $userData[0];
			     //   print_r($userData);die;
			        $md = array_merge($data,$userData[0]);
					if($id == ''){
						$md['password'] = md5($password);
						$md['status'] = 1;
						$md['email'] = $this->input->post('email');

					    $insUpdate = $this->DBfile->put_data('usertbl',$md);
					}else{
						if($password != ''){
							$md['password'] = md5($password);
						}
                    	$where = [
                    	    'id' => $id,
    						'user_type' => (isset($_POST['type']) && $_POST['type'] == 'reseller')? 1 : 2,
    						'parent_id' => $this->session->userdata('id')
    					];
    					
                        $insUpdate = $this->DBfile->set_data( 'usertbl', $md , $where );
                        if($insUpdate && isset($_POST['send_on_mail']) && $_POST['send_on_mail'] == 'on'){
                            $this->load->Library('Common_lib');
                            
                            $data = [
                                'appName' => $this->Common_lib->siteName,
                                'appUrl' => base_url(),
                                'title' => '',
                                'salutation' => 'Hi '. ucfirst(explode(' ', $name)[0]).',',
                                'body' => 'Welcome to <a href="'.base_url().'">'.$this->Common_lib->siteName.'</a>.<br/><br/>  
                                            Please use below details to access your account.<br/><br/> 
                                            
                                            Login URL : <a href="'.base_url().'">'.base_url().'</a><br/>
                                            Email : '.$email.'<br/>
                                            Password : '.$password.'<br/>
                                            ',
                                "isSupportFooter" => true
                            ];
                
                            $mailSubject = 'Welcome to '. $this->Common_lib->siteName;
                            $templateMessage = $this->load->view('email_text_template' , $data , true);
                            sendUserEmailMandrill($email, $mailSubject, $templateMessage);
                        }
					}
					if($insUpdate){
						$msg = ($id == '' ? 'added' : 'updated').' successfully.';
						$resp = array('status'=>'1', 'msg' => $msg, 'url' => $_POST['type'] == 'reseller' ? 'reseller_users' : 'white_label_setting' );
						
				// 		if(isset($_POST['type'])){
				// 			$this->respData =[
				// 			    'url' => $_POST['type'] == 'client' ? 'client_manager' : ($_POST['type'] == 'reseller' ? 'sub_user' : 'white_label_user')    
				// 			];
				// 		}
					}else{
						$resp = array('status'=>'0', 'msg' => 'Something went wrong.');
					}
				}else{
				    $resp = array('status'=>'0', 'msg' => 'Email already exist.');
				}
			}else{
			    $resp = array('status'=>'0', 'msg' => 'Confirm password doesn\'t match.');
			}
		}else{
		    $resp = array('status'=>'0', 'msg' => 'Email is required.');
		}
		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
        die();

	}
	
	function changeStatus($type ,  $id){
	    $this->checkUserLoginStatus();
	   // print_r($_POST);die;
	    if($type == 'users'){
		    $table = 'usertbl';
	    }
	    $sts = $_POST['sts'] == 1 ? 1 : 2 ;
		$isUpdate = $this->DBfile->set_data( $table , array('status' => $sts) , array('id' => $id) );
		
		if($isUpdate){
		    $res = array('status' => 1, 'msg' => 'Reseller user status updated successfully.');
		}else{
		   	$res = array('status'=>'0', 'msg' => 'Something went wrong.');
		}
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	    
	}
   
	function save_wl_setting(){
	    $this->checkUserLoginStatus();
		
		$this->load->Library('Manage_cpanel_domain',['domain' => C_MAINDOMAIN, 'cPanelUserName' => C_USERNAME, 'cPanelPassword' => C_PASSWORD, 'mainDomainIP' => C_MAINDOMAINIP]);
		$title = $this->input->post('title');
		$id = (isset($_POST['id'])? $_POST['id'] : '');
		$this->load->Library('Common_lib');
		if(!empty($title)){
			// $domainName = $this->common->slugify($this->input->post('domian_name'));
			$domainName = $this->input->post('domain_name');
			$domain = explode('.' , $domainName);
			if(count(explode('.' , $domainName)) > 1){
				if($id == ''){
					$cond = ['wl_setting.domain_name' => $domainName, 'wl_setting.user_id !=' => $_SESSION['id'] ];
				}else{
					$cond = ['wl_setting.domain_name' => $domainName, 'wl_setting.user_id !=' => $_SESSION['id'], 'wl_setting.id !=' => $id  ];
				}
				
				$checkDomain = $this->DBfile->get_data('wl_setting',$cond, 'id');
				// echo $this->db->last_query();
				
				if(!empty($checkDomain)){
					$resp = array( 'status' => 0, 'msg' => 'Domain already exist, please choose another.');
				    json_encode($resp,JSON_UNESCAPED_SLASHES); 
					exit();
				}else{
					$data = [
						'title' => $_POST['title'],
						'keyword' => $_POST['keyword'],
						'author_name' => $_POST['author_name'],
						'domain_name' => $_POST['domain_name'],
						'description' => $_POST['description'],
						'wl_primary_color' => isset($_POST['wl_primary_color']) ? $_POST['wl_primary_color'] : '',
						'wl_secondary_color' => isset($_POST['wl_secondary_color']) ? $_POST['wl_secondary_color'] : '',
						'smtp_details' => json_encode([
							'from_name' => $_POST['from_name'],
							'from_email' => $_POST['from_email'],
							'smtp_host' => $_POST['smtp_host'],
							'port' => $_POST['port'],
							'smtp_user' => $_POST['smtp_user'],
							'smtp_pass' => $_POST['smtp_pass'],
						]),
					];
					$data['user_id'] = $_SESSION['id'];
					if($id != ''){
					    
						$DomainData = $this->DBfile->get_data('wl_setting', ['id' => $id, 'user_id' => $_SESSION['id']], '', '',1);
				// 		echo"<pre>";print_r($DomainData);die;
						if($DomainData[0]['domain_name'] == $_POST['domain_name']){
							
							if(($id != '' || $id == '') && !empty($_FILES['wl_logo']['name'])){
								
								$uImage = $this->common_lib->uploadFile([
									'path' => '/assets/backend/images/white_label/',
									'name' => 'wl_logo',
									'postFix' =>  '_'.$_SESSION['id']
								]);
								
								if($uImage['status'] == 1){
									$data['wl_logo'] = $uImage['name'];
								}else{
									$this->respMessage = $uImage['message'];
									$this->show_result();
								}							
								
							}
							if(($id != '' || $id == '') && !empty($_FILES['wl_fav_icon']['name'])){
								$uImage = $this->common_lib->uploadFile([
									'path' => '/assets/backend/images/white_label/',
									'name' => 'wl_fav_icon',
									'postFix' =>  '_'.$_SESSION['id']
								]);
								
								if($uImage['status'] == 1){
									$data['wl_fav_icon'] = $uImage['name'];
								}else{
									$this->respMessage = $uImage['message'];
									$this->show_result();
								}							
							}
							if(!empty($id)){
						    	$insUpdate = $this->DBfile->set_data( 'wl_setting', $data, ['id' => $id, 'user_id' => $_SESSION['id']], 1);
							}else{
							    $insUpdate = $this->DBfile->put_data( 'wl_setting', $data );
							}
							// print_r($insUpdate);
							if($insUpdate){
							    $resp = array( 'status' => 1, 'msg' => 'White label setting '.(isset($id) == '' ? 'added' : 'updated').' successfully.');
							}else{
							    $resp = array( 'status' => 0, 'msg' => 'Something went wrong.');
							}
							
						}else{
						    if($DomainData[0]['domain_name'] != ''){
								$existDomain = explode('.' , $DomainData[0]['domain_name']);
								
								$existDomainData = [
									"domainName" => $DomainData[0]['domain_name'],
									"subDomainName" => $existDomain[0].$_SESSION['id'],
									"dir" => "public_html",
								];
								$removeDomin = $this->manage_cpanel_domain->removeAddonDomain($existDomainData);
						    
								if(!empty($removeDomin) && !empty($removeDomin['status'])){
									$domainData = [
                                        "domainName" => $domainName,
                                        "subDomainName" =>  $domain[0].$_SESSION['id'],
                                        "dir" =>  'public_html',
                                        "checkIP" => false
                                    ];
                                    
            					    $addDomain = $this->manage_cpanel_domain->addAddonDomain($domainData);
									
									if(!empty($addDomain) && !empty($addDomain['status'])){
										$data['user_id'] = $_SESSION['id'];
										
										if(($id != '' || $id == '') && !empty($_FILES['wl_logo']['name'])){
											
											$uImage = $this->common_lib->uploadFile([
												'path' => '/assets/backend/images/white_label/',
												'name' => 'wl_logo',
												'postFix' =>  '_'.$_SESSION['id']
											]);
											
											if($uImage['status'] == 1){
												$data['wl_logo'] = $uImage['name'];
											}else{
												$this->respMessage = $uImage['message'];
												$this->show_result();
											}							
											
										} 
										if(($id != '' || $id == '') && !empty($_FILES['wl_fav_icon']['name'])){
											$uImage = $this->common_lib->uploadFile([
												'path' => '/assets/backend/images/white_label/',
												'name' => 'wl_fav_icon',
												'postFix' =>  '_'.$_SESSION['id']
											]);
											
											if($uImage['status'] == 1){
												$data['wl_fav_icon'] = $uImage['name'];
											}else{
												$this->respMessage = $uImage['message'];
												$this->show_result();
											}							
										}
										if(!empty($id)){
            						    	$insUpdate = $this->DBfile->set_data( 'wl_setting', $data, ['id' => $id, 'user_id' => $_SESSION['id']], 1);
            							}else{
            							    $insUpdate = $this->DBfile->put_data( 'wl_setting', $data );
            							}
            							// print_r($insUpdate);
            							if($insUpdate){
            							    $resp = array( 'status' => 1, 'msg' => 'White label setting '.(isset($id) == '' ? 'added' : 'updated').' successfully.');
            							}else{
            							    $resp = array( 'status' => 0, 'msg' => 'Something went wrong.');
            							}
									
									}else{
										 $resp = array( 'status' => 0, 'msg' => isset($addDomain)? $addDomain['message']:'Something went wrong.');
									}
								}else{
									 $resp = array( 'status' => 0, 'msg' => isset($removeDomin)? $removeDomin['message']:'Something went wrong.');
								}
							}else{
								 $resp = array( 'status' => 0, 'msg' => isset($removeDomin)? $removeDomin['message']:'Something went wrong.');
							}
						}
					}else{
                        $domainData = [
                            "domainName" => $domainName,
                            "subDomainName" =>  $domain[0].$_SESSION['id'],
                            "dir" =>  'public_html',
                            "checkIP" => true
                        ];
					    $addDomain = $this->manage_cpanel_domain->addAddonDomain($domainData);
					 
					    if(!empty($addDomain) && !empty($addDomain['status'])){
					       
							if(($id != '' || $id == '') && !empty($_FILES['wl_logo']['name'])){
							
								$uImage = $this->common_lib->uploadFile([
									'path' => '/assets/backend/images/white_label/',
									'name' => 'wl_logo',
									'postFix' =>  '_'.$_SESSION['id']
								]);
								
								if($uImage['status'] == 1){
									$data['wl_logo'] = $uImage['name'];
								}else{
								    $resp = array( 'status' => 0, 'msg' => $uImage['message']);
            					    json_encode($resp,JSON_UNESCAPED_SLASHES); 
            						exit();
								}							
								
							}
							if(($id != '' || $id == '') && !empty($_FILES['wl_fav_icon']['name'])){
								
								$uImage = $this->common_lib->uploadFile([
									'path' => '/assets/backend/images/white_label/',
									'name' => 'wl_fav_icon',
									'postFix' =>  '_'.$_SESSION['id']
								]);
								
								if($uImage['status'] == 1){
									$data['wl_fav_icon'] = $uImage['name'];
								}else{
									$resp = array( 'status' => 0, 'msg' => $uImage['message']);
            					    json_encode($resp,JSON_UNESCAPED_SLASHES); 
            						exit();
								}							
							}
							
							if(!empty($id)){
								$insUpdate = $this->DBfile->set_data( 'wl_setting', $data, ['id' => $id], 1);
							}else{
							    $insUpdate = $this->DBfile->put_data( 'wl_setting', $data );
							 //   echo $this->db->last_query();
							 //   echo"<pre>427 - ";print_r($insUpdate);die;
							}
							// print_r($insUpdate);
							if($insUpdate){
							    $resp = array( 'status' => 1, 'msg' => 'White label setting '.(isset($id) == '' ? 'added' : 'updated').' successfully.');
							}else{
							    $resp = array( 'status' => 0, 'msg' => 'Something went wrong.');
							}
						
					    }else{
					        $resp = array( 'status' => 0, 'msg' => isset($addDomain)? $addDomain['message']:'Something went wrong.');
					    }
					}
				}

			}else{
				$resp = array( 'status' => 0, 'msg' => 'Invalid Domain name.');
			    json_encode($resp,JSON_UNESCAPED_SLASHES); 
				exit();
			}
		}else{
			$resp = array( 'status' => 0, 'msg' => 'Title is required.'); 	
		}
    	echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
        die();
	}
	
	function getRandomNumber($n){
		$seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789!@#$%^&*()'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, $n) as $k) $rand .= $seed[$k];
	    return $rand;
	}
	
	function shareURLGet(){
	    $this->checkUserLoginStatus();
	   // echo"<pre>";print_r($_POST);die;
	    if(empty($_POST['folderId'])){
	         $folderid ='';
	    }else{
	         $folderid = $_POST['folderId'];
	    }
	    if(!empty($_POST['drType']) && $_POST['drType']!='undefined'){
	         $folderid =$_POST['drType'];
	    }
	    $id = $_POST['id'];
        $result = $this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
      
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $ar = $this->googledrive->getFileInfo($token,$folderid);
                $res = array('shareUrl'=>$ar['data']['url']);
            }else if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $folderid);
                $shareFileLink = json_decode( $shareFileData, true );
                if(empty($shareFileLink['links'])){
                    $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $folderid);
                    $ru = json_decode( $ru, true );
                    if(isset($ru['url'])){
                        $shareUrl = $ru['url'];
                    }
                }else{
                   $shareUrl =$shareFileLink['links'][0]['url'];
                }
             $res = array('shareUrl'=>$shareUrl);
            }else if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $ar = $this->ddownload_api->getFileInfo($accessKey, $_POST['folderId']);
                $connection = 'Ddownload';
                $ru = json_decode( $ar, true );
                $fileName = $ru['result'][0]['name'];
                // $shareUrl = "https://ddownload.com/'.$ru['result'][0]['filecode'].'/'.".$fileName.";
                $shareUrl = "https://ddownload.com/".$ru['result'][0]['filecode']."/"."$fileName";
                // if($_SESSION['id']== 1){
                //     echo"<pre>";print_r($shareUrl);die;
                // }
                $res = array('shareUrl'=>$shareUrl);
            }else if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                // $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $access_token = $ntoken['access_token'];
                if($access_token){
                    $file = $this->box_api->getFileDetails($access_token, $folderid);
                    $fileData = json_decode($file, true);
                    if(!empty($fileData['shared_link'])){
                        $shareURL = $fileData['shared_link']['url'];
                        // $shareURL = $fileData['shared_link']['download_url'];
                    }else{
                        $shareURL = 'https://app.box.com/file/'.$folderid;
                    }
                    $connection_type = 'Box';
                    $res = array('shareUrl'=>$shareURL);
                    // if(isset($DdownloadData) && ($DdownloadData['type'] == 'error')){
                        
    	               // $res = array('status'=>0, 'userId'=>$result[0]['id'], 'data'=>$data, 'connection_type'=>$connection_type);
                    // }else{
    	               // $res = array('status'=>1, 'userId'=>$result[0]['id'], 'data'=>$data, 'connection_type'=>$connection_type);
                    // }
                }
            }else if($result[0]['connection_type'] == 'OneDrive'){
                $id =$_POST['id'];
                $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
                
                $this->load->library('onedrive');
                 
                $tk = json_decode($result[0]['access_token'],true);
        	   // $ntk = json_decode($tk['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                
                $this->load->library('onedrive');
                       
                $ar = $this->onedrive->ShareURL($accessToken,$_POST['folderId']);
                $res = array('shareUrl'=>$ar['webUrl']);
            }else if($result[0]['connection_type'] == 's3'){
                $this->load->helper('aws');
                $res =  getUrl($token['BucketName'],$folderid,$token['key'],$token['secret'],$token['region']);
                $ar['data']['url']=$res;
                $res = array('shareUrl'=>$ar['data']['url']);
            }else if($result[0]['connection_type'] == 'GooglePhoto'){
                 $this->load->library('Googlephoto');
                 $clientData = $this->googlephoto->shareURLAlbum($token,$_POST['folderId']);
                 print_r($clientData);die;
                //  $shareUrl = $ru['url'];
                //  $res = array('shareUrl'=>$shareUrl);
            }else if($result[0]['connection_type'] == 'Gofile'){
                
                $res = array('shareUrl'=>$_POST['folderId']);
				
            }else if($result[0]['connection_type'] == 'Backblaze'){
                $res = array('shareUrl'=>$_POST['folderId']);

            }else if($result[0]['connection_type'] == 'Pcloud'){
                $res = array('shareUrl'=>$_POST['folderId']);

            }else if($result[0]['connection_type'] == 'Anon'){
                $shareUrl = "https://anonfiles.com/account/file/".$folderid."/remove?p=1";
                $res = array('shareUrl'=>$shareUrl);
            
            }else if($result[0]['connection_type'] == 'Ftp'){
                $shareUrl = base_url().'home/downloadFtpFile/?id='.$id.'&path='.$folderid;
                $res = array('shareUrl'=>$shareUrl);
            }
        }
        if($res['shareUrl'] !=''){
            $enId =  encryptor('encrypt',$id);
            $enFileId =  encryptor('encrypt',$folderid);
            $shareUrl = ($_POST['file_type'] == 2)? base_url('share/'.$enId.'/'.$enFileId) : $res['shareUrl'];
            $where = [
                'user_id'=> $_SESSION['id'],
                'conn_id'=> $id,
                'connection_type' => $result[0]['connection_type'],
            ];
            
            $checkUrl = $this->DBfile->get_data( 'share_files', $where);
            $fileData = [
                'file_name' => $_POST['filename'],
                'file_id' => $folderid,
                'file_url' => $res['shareUrl']
            ];
        	$data = [
        	    'file_id' => $folderid,
				'file_type' => ($_POST['file_type'] != '')? $_POST['file_type'] : 1,
				'file_password' => ($_POST['file_type'] == 2)? $this->getRandomNumber(5) : '',
				'file_data' => json_encode($fileData),
				'shared' => ($_POST['file_type'] == 2)? 0 : 1
			];
			
            $margeData = array_merge($data, $where);
            
            if(empty($checkUrl)){
			    $insUpdate = $this->DBfile->put_data( 'share_files', $margeData );
            }else{
		    	$insUpdate = $this->DBfile->set_data( 'share_files', $margeData, $where, 1);
            }
            // $data['private_file_url' => 'Private sharing link: '.$shareUrl.' View password: 6veM'];
            
			// print_r($insUpdate);
			if($insUpdate){
			    $resp = array( 'status' => 1, 'msg' => '', 'data' => $data, 'shareUrl' => $shareUrl);
			}else{
			    $resp = array( 'status' => 0, 'msg' => 'Something went wrong.');
			}
        }else{
            $resp = array( 'status' => 0, 'msg' => 'Something went wrong.');
        }
        echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	    die();
	}
	
    function share_file(){
        $exData = explode('/' , $_SERVER['REQUEST_URI']);
        // print_r($file_id);die;
	    $data['conn_id'] =  $exData[2];
	    $data['file_id'] =  $exData[3];
        $data['pageName'] = 'Share File';
    	$this->load->view('backend/share_file',$data);
	
	}
	
    function sharePrivateFiles(){
        
	    $file_id =  encryptor('decrypt',$_POST['file_id']);
	    $conn_id =  encryptor('decrypt',$_POST['conn_id']);
	    
        //  if($_SESSION['id'] == 1){
	   //     echo"<pre>";print_r($_POST);
	   //     echo"<pre>";print_r($file_id);
	   //     echo"<pre>";print_r($conn_id);
	   //     die;
	   // }
	    if($file_id != ''){
	        $where = ['conn_id'=>$conn_id, 'file_id' =>$file_id];
			$custDetails = $this->DBfile->get_data('share_files',$where);
			if($custDetails[0]['file_password'] == $_POST['password']){
			    
			    $insUpdate = $this->DBfile->set_data( 'share_files', ['shared' => 1], $where, 1);
            	if($insUpdate){
            	    $res = array( 'status' => 1, 'msg' => '', 'url' => 'home/share');
            	}else{
            	    $res = array( 'status' => 0, 'msg' => 'Something went wrong.');
            	}
            }else{
	    	    $res = array('status'=>'0', 'msg' => 'Password doesn\'t match.');
			}
    	}else{
		    $res = array('status'=>'0', 'msg' => 'Something went wrong.');
    	}
	    echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
    function testing(){
        //  $ip = gethostbyname('jobsinindore.net');
        //  print_r($ip);
        //  die;
        	$this->load->Library('Manage_cpanel_domain',['domain' => C_MAINDOMAIN, 'cPanelUserName' => C_USERNAME, 'cPanelPassword' => C_PASSWORD, 'mainDomainIP' => C_MAINDOMAINIP]);
		   	$domainData = [
                    "domainName" => 'jobsinindore.net',
                    "subDomainName" =>  'jobsinindore.net123',
                    "dir" =>  'public_html',
                    "checkIP" => true
                ];
		    $addDomain = $this->manage_cpanel_domain->testingIP('jobsinindore.net');
		    print_r($addDomain);
		    
    }
}
?>