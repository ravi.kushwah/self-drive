<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ipn extends CI_Controller {
		
	public function index(){
		die('Unauthorize Access!!');
	}
		
    function set_ipn(){
		if(isset($_POST['WP_BUYER_NAME'])){

			/*
			
			FE   (wso_nq3pc5)
            OTO1 Unlimited (wso_rpk5w1)
            OTO2 Max (wso_yfgqrh)
            OTO3 Turbo  (wso_zvl32d)
            OTO4 Prime (wso_vhw9lv)
            OTO5 Agency (wso_n9d8x6)
            OTO6 Reseller (wso_z60j9p)
            OTO7 Whitelabel (wso_r7vyv9)
            
            Pro VIP Pack (wso_hxmr4b)
            Unlimited Ninja (wso_krwh8k)
            Max 20x(wso_jvf526)
            Turbo VIP(wso_j2whks)
            Prime Golden Member(wso_k3bgpg)

			*/
            $this->DBfile->put_data('user_records' , array('recdata'=>json_encode($_POST),'recemail'=>$_POST['WP_BUYER_EMAIL'],'recdate'=>date('Y-m-d H:i:s'),'product_id'=>$_POST['WP_ITEM_NUMBER']));
			
            $userDetails = $this->DBfile->get_data('usertbl' , array('email' => $_POST['WP_BUYER_EMAIL']),'*');			
            
            $planDetails = $this->DBfile->get_data('plan' , array('product_id' => $_POST['WP_ITEM_NUMBER']),'*');		
            
            // print_r($planDetails);die;

            if(empty($planDetails)){
                echo $_POST['WP_ITEM_NUMBER'] , ' , Product is not available.';
                die();
            }
            
            $planData = $planDetails[0];
            $planName = $planData['name'];
            $planDbId = $planData['id'];
            
            $this->load->Library('Common_lib'); 
              
            $name = $_POST['WP_BUYER_NAME'];
            
            $user_email = strtolower($_POST['WP_BUYER_EMAIL']);

// 			print_r($userDetails);die;
			
			if(empty($userDetails)) { // NEW USER
				$password = substr(md5(date('his')), 0, 8);
				$user_details = array( 
					'parent_id' => '3',
					'name' => $name,
					'email' => $_POST['WP_BUYER_EMAIL'],
					'password' => md5($password),
					'source' => 'warriorplus',
					'status' => 1,
					'role' => 2,
					'plan' => json_encode([$planDbId]),
				);
				$this->DBfile->put_data('usertbl' , $user_details);
				
                /* GetResponse */

			    $listID = 'P3lfA'; // PLR Site Builder List Token
				$args = array(
					'campaign' => array('campaignId'=>$listID),
					'email' => $_POST['WP_BUYER_EMAIL'],
					'name'  =>  $_POST['WP_BUYER_NAME'],
					'dayOfCycle'=>0,
				);                                                                
				$data_string = json_encode($args);                                                                                   
				$ch = curl_init('https://api.getresponse.com/v3/contacts');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
					'X-Auth-Token:api-key 7p55ps3l2j6rvww0e7xyi0ir94pzv6j5',                                                                      
					'Content-Type: application/json',                                                                                
					'Content-Length: ' . strlen($data_string))                                                                       
				);
				$result = curl_exec($ch);
				
				/* GetResponse */
    			
    			$data = [
                    'appName' => $this->common_lib->siteName,
                    'appUrl' => base_url(),
                    'title' => 'Welcome to <a href="'.base_url().'">'.ucfirst($this->common_lib->siteName).'</a>,',
                    'salutation' => 'Hi '.$name.',',
                    'body' => 'Congratulations on your <a href="'.base_url().'">'.ucfirst($this->common_lib->siteName).'</a> purchase, we look forward<br/>
                    to seeing you using Self Cloud and start getting results.<br/><br/>Here are your login details.<br/>
                    Login URL : '.base_url().'<br/>
                    Email : '.$user_email.'<br/>
                    Password : '.$password.'<br/><br/>
                    You can change your password from your <a href="'.base_url().'">profile page</a>.<br/><br/>',
                    "isSupportFooter" => true
                ];
                $mailSubject = "Welcome to ".ucfirst($this->common_lib->siteName).' - Login Details';
                


			}else { // OLD USER

		        $userPlan = $userDetails[0]['plan'] != ''?json_decode($userDetails[0]['plan'] , true):[];
                $userPlan[] = "$planDbId";

				$uid = $userDetails[0]['id'];
				$user_details = array(
					'status' => 1,
					'plan' =>json_encode(array_unique($userPlan))
				);
				$password = "EXISTING password will work"; 
                $this->DBfile->set_data('usertbl' , $user_details ,array('id' => $uid));
			
                $data = [
                    'appName' => $this->common_lib->siteName,
                    'appUrl' => base_url(),
                    'title' => 'Thank you for purchasing - '.$planName,
                    'salutation' => 'Hi '.$name.',',
                    'body' => 'Your account has been upgraded successfully  .<br/><br/> Your login details are same as before.<br/><br/>',
                    "isSupportFooter" => true
                ];
    
                $mailSubject = ucfirst($this->common_lib->siteName).' Upgrade Plan';
                
			}
            if($mailSubject != '' && isset($data)){
                $templateMessage = $this->load->view('email_text_template' , $data , true);
                sendUserEmailMandrill($user_email, $mailSubject, $templateMessage);
            }
		}
	}
}
?>