<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
    function __Construct(){
        parent::__Construct();
      
    }
    private function checkUserLoginStatus(){
		if(!isset($_SESSION['loginstatus']) || $this->session->userdata('type') != 2 ){
			redirect(base_url());
		}
	}
	function checkLoginUser(){
		if(isset($_SESSION['loginstatus']) && $this->session->userdata('type') == 1 ){
			redirect(base_url('admin/dashboard'));
		}
		if(isset($_SESSION['loginstatus']) && $this->session->userdata('type') == 2 ){
			redirect(base_url('home/dashboard'));
		}
	}
	private function checkUserTaskLimit(){
	    
		if(count($_SESSION['current_plan']) == 1){
		  //  echo"<pre>";print_r($_SESSION);die;
		   if(isset($_SESSION['task_count']) && $_SESSION['task_count'] >= 100 ){
		       	redirect(base_url('home/dashboard'));
		   }
		}
	}
	function task_limit_over(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Dashboard';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['my_cloud'] = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'status'=>1));
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/dashboard',$data);
		$this->load->view('backend/footer',$data);
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
	}
	public function index(){
		$this->checkLoginUser();
		$data['pageName'] = 'Login';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/login');
		$this->load->view('auth/footer');
	}	
	public function forgotpassword(){
		$data['pageName'] = 'Forgot Password';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/forgotpassword');
		$this->load->view('auth/footer');
	}	
    public function signup(){
		$data['pageName'] = 'Sign Up';
		$this->load->view('auth/header',$data);
		$this->load->view('auth/signup');
		$this->load->view('auth/footer');
	}
	public function Page_not_found(){
		$data['pageName'] = '404 Page Not found';
		$this->load->view('frontend/404',$data);
	}
	function about_us(){
		$this->load->view('backend/about_us');
	}
	function privacy(){
		$this->load->view('backend/privacy');
	}
	function terms(){
		$this->load->view('backend/terms');
	}
	private function checkValidAJAX(){
		$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		if( strpos($ref,base_url()) === false )
			die('Unauthorize Access!!');
		if( !isset($_POST) )
			die('Unauthorize Access!!');
		$postData = array();
		foreach( $_POST as $key=>$soloProd ){
			if( $key != 'w_seoextscript' ){
				$temp = $this->input->post($key,TRUE);
				//$temp = $this->db->escape($temp);
				$postData[$key] = $temp;
			}
			else
				$postData[$key] = $soloProd;
		}
		return json_encode($postData);
	}
	function logout(){
		setcookie('check_seo_login', 1 , time() - (86400 * 30 * 24), "/");
		$this->session->sess_destroy();
			setcookie('uem', '', time() - (86400 * 30), "/");
			setcookie('upwd', '', time() - (86400 * 30), "/");
		redirect(base_url());
	}
	function profile(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Profile';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/profile',$data);
		$this->load->view('backend/footer',$data);
	}
    function adminProfile(){
        if(isset($_POST['name'])){
			$jsonPost = $this->checkValidAJAX();
			$postData = json_decode($jsonPost,TRUE);
			$data_arr['name']=$postData['name'];
			if( $postData['password'] != '' ) {
				$pwd = $postData['password'];
				unset($postData['password']);
				$data_arr['password'] = md5($pwd);
			}
			else{
				unset($postData['pwd']);
			}
			$config['upload_path'] ='./assets/admin/images/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|txt|';
            $config['max_size']    = '0';		
            $this->load->library('upload', $config);
            if(isset($_FILES['profile_image']) && !empty($_FILES['profile_image']['name'])){
                if ($this->upload->do_upload('profile_image')){
                    $uploaddata = $this->upload->data();
                    $pic = $uploaddata['raw_name'];
                    $pic_ext = $uploaddata['file_ext'];
                    $image_name = $pic.'_'.date('ymdHis').$pic_ext;
                    rename('./assets/admin/images/'.$pic.$pic_ext,'./assets/admin/images/'.$image_name);
                    $data_arr['profile_image'] = $image_name;
                }else{
                    $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                    echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                    die();
                }
            }
		    $res=$this->DBfile->set_data( 'usertbl', $data_arr , array('id'=>$this->session->userdata('id')) );
		    if($res==true){
               $user_detail = array(
    				'c_name' =>explode(' ', $_POST['name']),
    				'c_email' => $_POST['email'],
    				'profile_image' => $_FILES['profile_image']['name'],
			    );
    		    $this->session->set_userdata($user_detail);
    		    $resp = array('status'=>'1');
            }else{
		        $resp = array('status'=>'0');
            }
         echo json_encode($resp,JSON_UNESCAPED_SLASHES); 
		}
    }
	function dashboard(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Dashboard';
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['my_cloud'] = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'status'=>1));
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/dashboard',$data);
		$this->load->view('backend/footer',$data);
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
	}
	function taskList(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Task List';
		$data['my_cloud'] = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'status'=>1));
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
		$this->load->view('backend/header',$data);
		$this->load->view('backend/taskList',$data);
		$this->load->view('backend/footer',$data);
	}
    function loadTaskList($record = 0){
        $this->load->helper('url');
        $this->load->library('pagination');
        $recordPerPage = 5;
		if($record != 0){
			$record = ($record-1) * $recordPerPage;
		}      	
      	$recordCount = $this->db->where('user_id',$this->session->userdata('id'))->from("task_list")->count_all_results();
        $empRecord = $this->DBfile->select_data('*' , 'task_list' , array( 'task_list.user_id' => $this->session->userdata('id') ) , [ $recordPerPage, $record ] , $order = ['task_list.id', 'desc'] , $like = '' , $join_array = '' , $group = '', $or_like = '',$where_in ='');

      	// $empRecord = $this->DBfile->get_data('task_list',array('user_id'=>$this->session->userdata('id')));
      	
        //pagination settings
        $config['base_url'] = site_url('home/loadTaskList');
        $config['total_rows'] = $recordCount;
        $config['per_page'] = $recordPerPage;
        // $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 1;

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';



        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        
        
        // $config['base_url'] = base_url().'home/loadTaskList';
      	$config['use_page_numbers'] = TRUE;
        //   $config['num_links'] = 2;
		// $config['next_link'] = 'Next';
		// $config['prev_link'] = 'Previous';
		// $config['total_rows'] = $recordCount;
		// $config['per_page'] = $recordPerPage;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['empData'] = $empRecord;
		$data['record'] = ($record != 'undefined')? $record : 0;
        // print_r('<pre>'); 
        // print_r($data);die;
		echo json_encode($data);		
    }
	function connection(){
	    $this->checkUserLoginStatus();
	    $this->checkUserTaskLimit();
    	$data['pageName'] = 'Connection';
	    $this->load->library('Googledrive');
	    $data['googleAuthUrl'] = $this->googledrive->getAuthUrl();
    	$data['checkUserConnectCloud'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'GoogleDrive'),'connection_type','',1);
	    $this->load->library('Dropbox_libraries');
	    $data['dropboxAuthUrl'] = $this->dropbox_libraries->getAuthUrl();
    	$data['DropBox'] = $this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'DropBox'),'connection_type','',1);
	   //echo"<pre>";print_r($data['DropBox']);die;
	    $this->load->library('box_api');
	   // $data['Box']  = $this->box_api->getAuthUrl();
	    $data['Box']  = '#';
    	$data['BoxCon'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Box'),'connection_type','',1);
    	$this->load->library('onedrive');
        $data['Url'] = $this->onedrive->getAuthUrl();
        $data['OneDrive'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'OneDrive'),'connection_type','',1);
        $data['Ddownload'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Ddownload'),'connection_type','',1);
        $data['amazomS3'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'s3'),'connection_type','',1);
        $data['Anon'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Anon'),'connection_type','',1);
        $data['googlePhptos'] =$this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'GooglePhoto'),'connection_type','',1);

        $data['Backblaze']  = $this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Backblaze'),'connection_type','',1);
        $data['Gofile']  = $this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Gofile'),'connection_type','',1);
        $data['ftp']  = $this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Ftp'),'connection_type','',1);
        $data['Pcloud']  = $this->DBfile->get_data( 'connection',array('user_id'=>$this->session->userdata('id'),'status'=>1,'connection_type'=>'Pcloud'),'connection_type','',1);

    	$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
    	$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/connection',$data);
		$this->load->view('backend/footer',$data);
	}
	function CloudTransfer(){
	    $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
    	$data['pageName'] = 'Cloud Transfer';
    	$data['id'] = $this->session->userdata('id');
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloudtransfer',$data);
		$this->load->view('backend/footer',$data);
	}
	function CloudSync(){
	    $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
    	$data['pageName'] = 'Cloud Sync';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloudsync',$data);
		$this->load->view('backend/footer',$data);
	}
	function CloudBackup(){
	   $this->checkUserLoginStatus();
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
		$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
    	$data['pageName'] = 'Cloud Backup';
        $data['id'] = $this->session->userdata('id');
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloudbackup',$data);
		$this->load->view('backend/footer',$data);
	}
	function Share(){
	    $this->checkUserLoginStatus();
		$data['shareList'] = $this->DBfile->get_data('share_files',array('user_id'=>$this->session->userdata('id'), 'shared' =>1));
    	$data['pageName'] = 'Share';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/share',$data);
		$this->load->view('backend/footer',$data);
	}
	function support(){
	    $this->checkUserLoginStatus();
		$data['agency_users'] = $this->DBfile->get_data('usertbl',array('parent_id'=>$this->session->userdata('id')));
    	$data['pageName'] = 'Support Desk';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/support',$data);
		$this->load->view('backend/footer',$data);
	}
	function tutorials(){
		$this->checkUserLoginStatus();
		$data['pageName'] = 'Tutorials';
		$this->load->view('backend/header',$data);
		$this->load->view('backend/tutorials',$data);
		$this->load->view('backend/footer',$data);
	}
	function agency_users(){
	   
		$this->checkUserLoginStatus();
		$custDetails = $this->DBfile->get_data('usertbl',array('id'=>$_SESSION['id']));
	
		if(!empty(array_intersect(["6"], $_SESSION['current_plan']))){
    		if(isset($_POST['sts'])){
    			$jsonPost = $this->checkValidAJAX();
    			$postData = json_decode($jsonPost,TRUE);
    	    	$data['userCount'] = $this->DBfile->getCount('task_list',array('user_id'=>$_SESSION['id']));
                $sts = $postData['sts'] == 0 ? 2 : 1 ;
    			$this->DBfile->set_data( 'usertbl', array('status'=>$sts) , array('id'=>$_POST['userid']) );
    			echo '1';
    			die();
    		}
    		if( isset($_POST['name']) ){
    			$jsonPost = $this->checkValidAJAX();
    			$postData = json_decode($jsonPost,TRUE);
    			$userId = $postData['userid'];
    			unset($postData['userid']);
    			$checkEmail = $this->DBfile->get_data('usertbl',array('email'=>$postData['email']));		
    			if( $userId == 0 ){
    			    $userCount = $this->DBfile->aggregate_data('usertbl','parent_id','SUM',array('parent_id'=>$_SESSION['id']));
        		    if($userCount<250){
        			    if($postData['is_fe']=='true'){
        			        $fe = 1;
        			    }else{
        			        $fe = 0; 
        			    }
        			    if($postData['is_oto1']=='true'){
        			        $is_oto1 = 1;
        			    }else{
        			        $is_oto1 = 0;
        			    }
        			    if($postData['is_oto2']=='true'){
        			        $is_oto2 = 1;
        			    }else{
        			        $is_oto2 = 0;
        			    }
        			 //   $userPlan = $checkEmail[0]['plan'] != ''?json_decode($checkEmail[0]['plan'] , true):[];
        			    $userPlan[] = ($_POST['is_fe']=='true')? "1":"";
        			    $userPlan[] = ($_POST['is_oto1']=='true')? "2":"";
        			    $userPlan[] = ($_POST['is_oto2']=='true')? "3":"";
        			   
        				if( empty($checkEmail) ){
        			        $postData['plan'] = json_encode(array_unique($userPlan));
        					$postData['parent_id'] = $this->session->userdata('id');
        					$postData['password'] = md5($postData['password']);
        					$postData['role'] = 2;
        					$postData['status'] = 1;
        					$postData['is_fe'] = $fe;
        					$postData['is_oto1'] = $is_oto1;
        					$postData['is_oto2'] = $is_oto2;
        					$postData['access_level'] = 250;
        				
        					$this->DBfile->put_data('usertbl',$postData);
        					echo 2;
        				}
        				else{
        					echo 4;
        				}
        		    }else{
        		        	echo 5;
        		    }
    			}else{
    				if( $postData['password'] != '' ){
    					$postData['password'] = md5($postData['password']);
    				}else{
    					unset($postData['password']);
    				}
    			    $userPlan = $checkEmail[0]['plan'] != ''?json_decode($checkEmail[0]['plan'] , true):[];
    				if($postData['is_fe']=='true'){
    			        $fe = 1;
    			        $userPlan[] = "1";     
    			    }else{
    			        $fe = 0; 
    			    }
    			    if($postData['is_oto1']=='true'){
    			        $is_oto1 = 1;
    			        $userPlan[] = "2"; 
    			    }else{
    			        $is_oto1 = 0;
    			    }
    			    if($postData['is_oto2']=='true'){
    			        $is_oto2 = 1;
    			        $userPlan[] = "3"; 
    			    }else{
    			        $is_oto2 = 0;
    			    }
                    $postData['plan'] = json_encode(array_unique($userPlan));
    			
    				$postData['is_fe'] = $fe;
    				$postData['is_oto1'] =$is_oto1;
    				$postData['is_oto2'] = $is_oto2;
    				if( $checkEmail[0]['id'] == $userId ){
    					$this->DBfile->set_data('usertbl',$postData,array('id'=>$userId));
    					echo 1;
    				}
    				else
    					echo 4;
    			}
    			die();
    		}
    		if( isset($_POST['userid']) ){
    			$custDetails = $this->DBfile->get_data('usertbl',array('id'=>$_POST['userid']));
    			$temp = array(
    				'name'	=>	$custDetails[0]['name'],
    				'email'	=>	$custDetails[0]['email'],
    				'is_oto1'	=>	$custDetails[0]['is_oto1'],
    				'is_oto2'	=>	$custDetails[0]['is_oto2'],
    			);
    			echo json_encode($temp);
    			die();
    		}
    		$data['agency_users'] = $this->DBfile->get_data('usertbl',array('parent_id'=>$this->session->userdata('id')));
    		$data['pageName'] = 'Agency Users';
    		$data['user_id'] =$_SESSION['id'];
    		$this->load->view('backend/header',$data);
    		$this->load->view('backend/agency_users',$data);
    		$this->load->view('backend/footer',$data);
    	}else{
	    	redirect(base_url());
    	}
	}
	function agencyUserTable(){
		$this->checkUserLoginStatus();
	    $agency_users = $this->DBfile->get_data('usertbl',array('parent_id'=>$this->session->userdata('id')));
		$i=0;
		$data = array();
		if(!empty($agency_users)){
			foreach($agency_users as $sList){
		        $checked = $sList['status'] == 2 ? '' : 'checked';
		        $checkbox = '<label class="switch"><input type="checkbox" class="switch-inpt agencyuserCls" '.$checked.' id="'.$sList['id'].'"><span class="slider round"></span></label>';
                $editbtn = '<div class="plr_action_icon"><a href="javascript:;" class="agencyuserClsEdit" id="'.$sList['id'].'"><i class="fa fa-pencil"></i><div class="plr_tooltip_show"><p>Edit</p></div></a></div>';
				$i++;
				$t = array();
				array_push($t,$i);
				array_push($t,$sList['name']);
				array_push($t,$sList['email']);
				array_push($t,$checkbox);
				array_push($t,$editbtn);
                array_push($data,$t);
			}
		}
		echo json_encode(array('data'=>$data));
		die();
	}
	function user_data($type){
		$this->checkUserLoginStatus();
    	$user_type  = (isset($type) && $type == 'reseller')? 1 : 2;
	    $reseller_users = $this->DBfile->get_data('usertbl',array('parent_id'=>$this->session->userdata('id'), 'user_type'=>$user_type));
	   // $this->db->last_query($reseller_users);die;
       $class = (isset($type) && $type == 'reseller')? 'reselleruserClsEdit': 'whitelabelUserEdit';
		$i=0;
		$data = array();
		if(!empty($reseller_users)){
			foreach($reseller_users as $sList){
		        $checked = $sList['status'] == 2 ? '' : 'checked';
		        $checkbox = '<label class="switch"><input type="checkbox" class="switch-inpt changeStts" data-url="common/changeStatus/users/'.$sList['id'].'" '.$checked.' id="'.$sList['id'].'"><span class="slider round"></span></label>';
                $editbtn = '<div class="plr_action_icon"><a href="javascript:;" class="'.$class.'" id="'.$sList['id'].'"><i class="fa fa-pencil"></i><div class="plr_tooltip_show"><p>Edit</p></div></a></div>';
				$i++;
				$t = array();
				array_push($t,$i);
				array_push($t,$sList['name']);
				array_push($t,$sList['email']);
				array_push($t,$checkbox);
				array_push($t,$editbtn);
                array_push($data,$t);
			}
		}
		echo json_encode(array('data'=>$data));
		die();
	}
	function RemoveConnection(){
	    $reusult=$this->DBfile->delete_data( 'connection', array('id'=>$_POST['id']));
		if($reusult==true){
		    $res = array('status'=>1);
		}else{
	       $res = array('status'=>0);
		}
		echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function view_file(){
	   // if($_SESSION['id']==1){
	        $this->checkUserLoginStatus();
	   // }
    	$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
	    if(empty($_POST['folderId'])){
	         $folderid ='';
	    }else{
	         $folderid =$_POST['folderId'];
	    }
	    if(!empty($_POST['drType']) && $_POST['drType']!='undefined'){
	         $folderid =$_POST['drType'];
	    }
	    $id =$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $data['file'] = $this->googledrive->getFileInfo($token,$folderid);
                $data['connection'] = 'googleDrive';
            }else if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $folderid);
                $shareFileLink = json_decode( $shareFileData, true );
                $url = $shareFileLink['links'][0]['url'];
                $shareURL = str_replace( 'dl=0','raw=1', $url);
                if(empty($shareFileLink['links'])){
                    $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $folderid);
                    $dataUrl = json_decode( $ru, true );
                    $data['file']['data']['url'] = str_replace( 'dl=0','raw=1',  $dataUrl['url'] );
                    $data['filename'] = $dataUrl['name'];
                    $data['connection'] = 'DropBox';
                }else{
                    $data['file']['data']['url'] =$shareURL;
                    $data['filename'] = $shareFileLink['links'][0]['name'];
                    $data['connection'] = 'DropBox';
                }
               
            }else if($result[0]['connection_type'] == 's3'){
                if(empty($_POST['folderId'])){
        	         $folderid ='';
        	    }else{
        	         $folderid = $_POST['folderId'];
        	    }
        	    $id =$_POST['id'];
                $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
                $token = json_decode($result[0]['access_token'],true);
        	    $this->load->helper('aws');
                $res =  getUrl($token['BucketName'],$folderid,$token['key'],$token['secret'],$token['region']);
                $typeCheck =  explode('.', $res);
                $data['url'] =$res;
                $data['filetype'] = $typeCheck;
                $data['connection'] = 's3';
            }else{
                $data['connection'] = 'GooglePhoto';
                $data['file']['data']['url'] =$folderid;
            }
        }
        $res = array('data'=>$data);
        // if($_SESSION['id']==1){
        //     if($res['data']['status'] == 1){
    		  //  $this->DBfile->custom_query("UPDATE `usertbl` SET `task_count` = task_count + 1 WHERE `id` = ".$_SESSION['id']);
    		  //  $_SESSION['task_count'] = $_SESSION['task_count']+1;
        //     } 
        // }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function PreviewFileOneDrive(){
        // if($_SESSION['id']==1){
	        $this->checkUserLoginStatus();
	   // }
        $id =$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $this->load->library('onedrive');
        $tk = json_decode($result[0]['access_token'],true);
	   // $ntk = json_decode($tk['access_token'],true);
	    $refresh_token=$tk['refresh_token'];
	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
	   // if($_SESSION['id']==1){
    	    $this->DBfile->custom_query("UPDATE `usertbl` SET `task_count` = task_count + 1 WHERE `id` = ".$_SESSION['id']);
            $_SESSION['task_count'] = $_SESSION['task_count']+1;
	   // }
        $tk = json_decode($res,true);
        $accessToken = $tk['access_token'];
        $ar = $this->onedrive->ShareURL($accessToken,$_POST['folderId']);
        $res = array('shareUrl'=>$ar['webUrl']);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    function getFileView(){
        $data['fileView']= $file;
    	$data['pageName'] = 'View File ';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/view_file',$data);
		$this->load->view('backend/footer',$data);
    }
	function checkBoxAccessToken($token, $emailId, $connectionType, $createTime){
	    $accessToken = '';
	    $this->load->library('Box_api');
	    $currentTime = strtotime($createTime);
	    $tokenDataExpireTime = $token['expires_in'];
	    $totalTime = $currentTime + $tokenDataExpireTime;
	   // echo $totalTime.' '.$currentTime; exit;
		if($totalTime > $currentTime){
			$accessToken = $token['access_token'];
		}
		if($accessToken == ''){
		    $refToken = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
            $refreshToken = json_decode( $refToken, true );
	   // echo"<pre>";print_r($refreshToken);die;
            if(!empty($refreshToken)){
                $data = array(
                    'email' => $emailId,
                    'access_token' => $refToken,
                    'refresh_token' => $refreshToken['refresh_token'],
                    'status' => 1
                );
                $result = $this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$emailId,'connection_type'=>$connectionType));
                if($result){
                    return $refreshToken['access_token'];
                }
            }
		}else{
		    return $accessToken;
		}
	}
	function cloud_details($id){
	    
	    $this->checkUserLoginStatus();
	    $this->checkUserTaskLimit();
	    $data = [];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        if(isset($result) && empty($result)){
            redirect( 'home/dashboard', 'refresh' );
        }
        $token = json_decode($result[0]['access_token'],true);
        $data['id'] = $id;
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $data['getAllFolder'] = $this->googledrive->getAllFolders($token,null);
                $data['connectionType']='Google Drive';
                $data['email']=$result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $data['getAllFolder'] = $this->dropbox_libraries->getAllFolders($accessToken);
                $data['connectionType']='DropBox';
                $data['email']=$result[0]['email'] ;
                // echo"<pre>";print_r($data['getAllFolder']);die;
            }else if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $data['getAllFolder'] = $this->ddownload_api->getAllFolders($accessKey, 0);
                $data['connectionType'] = 'Ddownload';
                $data['email'] = $result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'OneDrive'){
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                $data['getAllFolder'] = $this->onedrive->getFiles($accessToken,null);
                $data['connectionType']='OneDrive'; 
                $data['email']=$result[0]['email'];
            }else if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                if( $_SESSION['id'] == 1 ){
                    $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                    // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                    // $ntoken = json_decode($access_token,true);
                    // $access_token = $ntoken['access_token'];
                    // print_r($access_token);die;
                }else{
                    $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                }
                if($access_token){
    			    $folderId = 0;
                    $data['getAllFolder'] = $this->box_api->getAllFolders($access_token, $folderId);
                    $data['connectionType'] = 'Box';
                    $data['email'] = $result[0]['email'] ;
                    if( $_SESSION['id'] == 1 ){
                        // print_r($data['getAllFolder']);die;
                    }
                }
            }else if($result[0]['connection_type'] == 's3'){
                $this->load->helper('aws');
                $data['getAllFolder']['data'] =s3_get_bucket_folder($token['BucketName'], $token['key'],$token['secret'],$token['region']);
                $data['connectionType'] = 's3';
                $data['email'] = $result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'GooglePhoto'){
                $data['getAllFolder']['data'] = array(
                                            array('name'=>'Albums','id'=>1),
                                            array('name'=>'Sharing','id'=>2),
                                            array('name'=>'Photo','id'=>3),
                                        );
                $data['connectionType']='GooglePhoto';
                $data['email']=$result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'Backblaze'){
                $this->load->helper('back_blaz');
                $data['getAllFolder']['data'] = b2_get_folder('', $token['key_id'], $token['application_key'] );
                // echo"<pre>";print_r($data);die;
                $data['connectionType'] = 'Backblaze';
                $data['email'] = $result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'Gofile'){
                $this->load->library('Gofile_lib');
                $connection = $this->gofile_lib->gofile_get_folder( $token['token'] , $token['root_id']);
                if ($connection['status'] == 'ok' ) {
                    $data['getAllFolder']['data'] = $connection['data']['contents'];
                    $data['email'] = $result[0]['email'] ;
                }else{
                    $data['getAllFolder'] = [];
                    $data['email'] = 'Hmm ! Seems you have no Premium Account or Your Auth Token is changed' ;
                }
                $data['connectionType'] = 'Gofile';
            }else if($result[0]['connection_type'] == 'Anon'){
                $fileDetails = $this->DBfile->get_data('files_details', ['user_id'=>$_SESSION['id'], 'connection_key'=>$token['Apikey'], 'status'=>1]);
                $data = [];
                // echo"<pre>";print_r($fileDetails);die;
                if($fileDetails){
                    $this->load->library('AnonFile_lib');
                    foreach($fileDetails as $key => $folderData){
                        $ar = $this->anonfile_lib->getFileInfo($folderData['file_id']);
                        $resp = json_decode($ar,true);
                        if($resp['status'] == 1){
                            $files = json_decode($folderData['file_data'], true);
                            $FileData = [
                                'id'=> $folderData['id'],
                                'conn_id'=> $folderData['conn_id'],
                                'file_id'=> $folderData['file_id'],
                                'file_name'=> $files['data']['file']['metadata']['name'],
                                'file_url'=> $files['data']['file']['url']['full'],
                            ];
                            array_push($data, $FileData);
                        }else{
                            $where = array('user_id'=>$this->session->userdata['id'], 'connection_key'=>$token['Apikey'], 'file_id' =>$folderData['file_id']);
                            $data['status'] = 0;
                            $this->DBfile->set_data( 'files_details', $data, $where);
                        }
                    }
                }
                // echo"<pre>";print_r($data);die;
                $data['getAllFolder']['data'] = $data;
                $data['connectionType'] = 'Anon';
                $data['email'] = $result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'Ftp'){
                $this->load->library('ftp');
                $ftp            = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp, $token['username'], $token['password']);
                if ($login_result == 1) { 
                    $res = ftp_mlsd($ftp, "");
                    foreach ($res as $key => $value) {
                        if ( $value['type'] == 'cdir' || $value['type'] == 'pdir') {
                            unset($res[$key]);
                        }
                    }
                    $this->ftp->close();
                    $data['getAllFolder']['data'] = $res;
                    $data['email']                = $result[0]['email'] ;
                }else{
                    $data['getAllFolder']['data'] = [];
                    $data['email']                = 'Invalid Details ! Your Connection details might be incorrect' ;
                }
                $data['connectionType'] = 'Ftp';
                ftp_close($ftp);
            }else if($result[0]['connection_type'] == 'Pcloud'){
                $this->load->library('Pcloud_lib');
                $res = $this->pcloud_lib->getFolderById(0, $token['token']);
                if ($res['status'] == 1) { 
                    $data['getAllFolder']['data'] = $res['folder_data'];
                    $data['email']                = $result[0]['email'] ;
                }else{
                    $data['getAllFolder']['data'] = [];
                    $data['email']                = 'Invalid Details ! Your Connection details might be incorrect' ;
                }
                $data['connectionType'] = 'Pcloud';
            }
        }
		$data['userList'] = $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
    	$data['pageName'] = 'Cloud Details ';
    	$this->load->view('backend/header',$data);
		$this->load->view('backend/cloud_details',$data);
		$this->load->view('backend/footer',$data);
	}
	function getSubFolderByFolderId(){
	    $this->checkUserTaskLimit();
	    if(empty($_POST['folderId'])){
	         $folderid ='';
	    }else{
	         $folderid = $_POST['folderId'];
	    }
	    $id =$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $ar = $this->googledrive->getAllFolders($token, $folderid);
                $connection = 'GoogleDrive';
                $type ="";
            }else if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
        	    if(!empty($_POST['drType']) && $_POST['drType']!='undefined'){
        	         $folderid =$_POST['drType'];
        	    }
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                    // print_r($folderid);die;
                $ar = $this->dropbox_libraries->getAllFolders($accessToken,$folderid);
                $connection = 'DropBox';
                 $type ="";
            }else if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $ar = $this->ddownload_api->getAllFolders($accessKey, $folderid);
                $connection = 'Ddownload';
                 $type ="";
            }else if($result[0]['connection_type'] == 'OneDrive'){
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	   // $ntk = json_decode($tk['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                $driveId = explode('!', $_POST['folderId']);
                $ar = $this->onedrive->getFiles($accessToken,$folderid,$driveId[0]);
                $connection = 'OneDrive';
                 $type ="";
            }else if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                if($access_token){
                    $ar = $this->box_api->getAllFolders($access_token, $_POST['folderId']);
                    $connection = 'Box';
                }
                 $type ="";
            }else if($result[0]['connection_type'] == 'GooglePhoto'){
                //  print_r($_POST);
                //  die;
                $this->load->library('Googlephoto');
                if($folderid!=1 && $folderid!=2 && $folderid!=3){
                    $file = $this->googlephoto->getAlbumItem($token,$folderid);
                    $ar = isset($file['mediaItems']) && !empty($file['mediaItems']) ? $file['mediaItems'] : '' ;
                    $type ="mediaItemSearch";
                }else{
                    if($_POST['folderId']==1){
                        $clientData = $this->googlephoto->ListAlbum($token);
                        $type = 'album';
                        $data = json_decode($clientData,true);
                        $ar = isset($data['albums']) && !empty($data['albums']) ? $data['albums'] : '' ;
                        $type = 'Album';
                    }else if($_POST['folderId']==2){
                        $clientData = $this->googlephoto->listShare($token);
                        $data = json_decode($clientData,true);
                        $ar = isset($data['sharedAlbums']) && !empty($data['sharedAlbums']) ? $data['sharedAlbums'] : '' ;
                        $type = 'ShareAlbum';
                    }else if($_POST['folderId']==3){
                        $clientData = $this->googlephoto->MediaItem($token);   
                        $type = 'mediaitem';
                        $data = json_decode($clientData,true);
                        $ar = isset($data['mediaItems']) && !empty($data['mediaItems']) ? $data['mediaItems'] : '' ;
                        $type = 'Photos';
                    }
                }
                $ar = ['data' => $ar];
                $connection = 'Googlephoto';
            }else if($result[0]['connection_type'] == 'Backblaze'){
                $this->load->helper('back_blaz');
                $tk = json_decode($result[0]['access_token'],true);
                $data_ = b2_get_folder($folderid, $tk['key_id'], $tk['application_key'] );
                $connection = 'Backblaze';
                // echo"<pre>";print_r($ar);die;
                foreach($data_ as $key => $value){
                    // print_r($value);
                    $name = basename( $value['fileName'] );
                    $data_[$key]['name'] = $name;
                    if( $data_[$key]['fileName'] == $folderid.'.bzEmpty' ){ unset($data_[$key]); }
                }
                $string = rtrim($folderid,"/");
                $folder_back = preg_replace('#[^/]*$#', '', $string);
                $ar = array(
                    'status' => 1,
                    'data' => $data_,
                    'folderid' => $folder_back
                );
                 $type ="";
            }else if($result[0]['connection_type'] == 'Gofile'){
                $this->load->library('Gofile_lib');
                $folderID = empty($_POST['folderId']) ? $_POST['folderId'] : $_POST['folderId'] ;
                $data_ = $this->gofile_lib->gofile_get_folder( $token['token'] ,$folderID);
                if (isset($data_['data']['parentFolder'])) {
                    $folder_back = $data_['data']['parentFolder'];
                }else{
                    $folder_back = $data_['data']['id'];
                }
                $ar = array(
                    'status' => 1,
                    'data' => $data_['data']['contents'],
                    'folderid' => $folder_back
                );
                $connection = 'Gofile';
                 $type ="";
            }else if($result[0]['connection_type'] == 's3'){
                  $this->load->helper('aws');
                  $ar =s3_get_object($folderid,$token['BucketName'], $token['key'],$token['secret'],$token['region']);
                    // echo"<pre>";print_r($ar);die;
                  $connection = 's3';
                   $type ="";
            }else if($result[0]['connection_type'] == 'Ftp'){
                $this->load->library('ftp');
                $ftp            = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp, $token['username'], $token['password']);
                $data_          = ftp_mlsd($ftp, $this->input->post('folderId').'/');
                foreach ($data_ as $key => $value) {
                    if ( $value['type'] == 'cdir' || $value['type'] == 'pdir') {
                        unset($data_[$key]);
                    }
                }
                $this->ftp->close();
                $ar = array(
                    'status' => 1,
                    'data' => $data_,
                    'folderid' => $this->input->post('folderId').'/..',
                    'currentfolder' => $this->input->post('folderId')
                );
                $connection = 'Ftp';
                ftp_close($ftp);
                 $type ="";
            }else if($result[0]['connection_type'] == 'Pcloud'){
                $this->load->library('Pcloud_lib');
                require_once( APPPATH.'libraries/pcloud/autoload.php');
                $pCloudFolder = new pCloud\Folder();
                $folder_info = $pCloudFolder->getMetadata( (int)$this->input->post('folderId'), $token['token']);

                $data_ = $this->pcloud_lib->getFolderById( (int)$this->input->post('folderId'), $token['token']);

                // print_r($data_);die;
                $ar = array(
                    'status' => 1,
                    'data' => $data_['folder_data'],
                    'folderid' => $folder_info->metadata->parentfolderid
                );

                $connection = 'Pcloud';
                $type ="";
            }
        }
	    $res = array('userId'=>$result[0]['id'],'data'=>$ar, 'connection'=>$connection,'type'=>$type);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
    function unShare(){
        $id =$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        $this->load->library('Googlephoto');
        $clientData = $this->googlephoto->unShare($token,$_POST['folderId']);
        // print_r($clientData);die;
    }
	function DownloadFiles(){
	       $this->checkUserLoginStatus();
	    if(empty($_POST['folderId'])){
	         $folderid ='';
	    }else{
	         $folderid = $_POST['folderId'];
	    }
	    if(!empty($_POST['drType']) && $_POST['drType']!='undefined'){
	         $folderid =$_POST['drType'];
	    }
	    $id =$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        // if($_SESSION['id']==1){
            // print_r($result);
            // die;
        // }
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $ar = $this->googledrive->getFileInfo($token,$folderid);
                $URl = 'https://www.googleapis.com/drive/v3/files/'.$folderid.'?alt=media&supportsAllDrives=True&includeItemsFromAllDrives=True&key=AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';
                $res = array('shareUrl'=>$URl);
                // if($_SESSION['id']){
                //     print_r($ar);
                //     die;
                // }
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $folderid);
                $shareFileLink = json_decode( $shareFileData, true );
                if(empty($shareFileLink['links'])){
                    $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $folderid);
                    $ru = json_decode( $ru, true );
                    if(isset($ru['url'])){
                        $shareUrl = $ru['url'];
                    }
                }else{
                   $shareUrl =$shareFileLink['links'][0]['url'];
                }
             $shareURL = str_replace( 'dl=0','dl=1', $shareUrl);
             $res = array('shareUrl'=>$shareURL);
            }
            if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $parent_id = (isset($_POST['folderId'])? $_POST['folderId'] : 0 );
                $ar = $this->ddownload_api->createFolder($accessKey, $folderName, $parent_id);
                // echo"<pre>";print_r($ar);die;
                $DdownloadData = json_decode($ar, true);
                $connection_type = 'Ddownload';
                if(isset($DdownloadData['error'])){
                    $data['status'] = 0; 
                }else{
                    $data['status'] = 1; 
                }
	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
            if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                // $ntoken = json_decode($access_token,true);
                // $access_token = $ntoken['access_token'];
                if($access_token){
                    $file = $this->box_api->getFileDetails($access_token, $folderid);
                    $fileData = json_decode($file, true);
                    if(!empty($fileData['shared_link'])){
                        $shareURL = $fileData['shared_link']['download_url'];
                    }else{
                        $shareURL = 'https://app.box.com/file/'.$folderid;
                    }
                    // echo"<pre>";print_r($shareURL);die;
                    $connection_type = 'Box';
                    $res = array('shareUrl'=>$shareURL);
                    // if(isset($DdownloadData) && ($DdownloadData['type'] == 'error')){
    	               // $res = array('status'=>0, 'userId'=>$result[0]['id'], 'data'=>$data, 'connection_type'=>$connection_type);
                    // }else{
    	               // $res = array('status'=>1, 'userId'=>$result[0]['id'], 'data'=>$data, 'connection_type'=>$connection_type);
                    // }
                }
            }
            if($result[0]['connection_type'] == 's3'){
                $this->load->helper('aws');
                $res =  getUrl($token['BucketName'],$folderid,$token['key'],$token['secret'],$token['region']);
                $data['url'] =$res;
                $res = array('shareUrl'=>$res);
            }
            if($result[0]['connection_type'] == 'GooglePhoto'){
               $res = array('shareUrl'=>$folderid);
            }
            if($result[0]['connection_type'] == 'Anon'){
                $shareUrl = "https://anonfiles.com/account/file/".$folderid."/remove?p=1";
                $res = array('shareUrl'=>$shareUrl);
            }
            
            if(!empty($res['shareUrl'])){
    		    $this->DBfile->custom_query("UPDATE `usertbl` SET `task_count` = task_count + 1 WHERE `id` = ".$_SESSION['id']);
    		    $_SESSION['task_count'] = $_SESSION['task_count']+1;
            } 
        }
	     echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die(); 
	}

    function downloadPcloudFile($conn_id, $file_id){
        if (isset($file_id)) {
            $result =$this->DBfile->get_data( 'connection', array('id'=>$conn_id));
            $token = json_decode($result[0]['access_token'],true);
            if($result[0]['connection_type'] == 'Pcloud'){
                require_once(APPPATH.'libraries/pcloud/autoload.php');
                try {
                    
                    $pCloudFile = new pCloud\File();
                    
                    $info = $pCloudFile->download((int)$file_id, $token['token']);
                    
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                die();
            }
        }
    }

    function downloadFtpFile(){
        if (isset($_GET['id']) && $_GET['path']) {
            $decod_path = rawurldecode($_GET['path']);
            $result =$this->DBfile->get_data( 'connection', array('id'=>$_GET['id']));
            $token = json_decode($result[0]['access_token'],true);
            if($result[0]['connection_type'] == 'Ftp'){
                $this->load->helper('download');
                $ftp_conn   = ftp_connect( $token['hostname'] );
                $login      = ftp_login($ftp_conn, $token['username'], $token['password']);
                $connection = 'Ftp';
                $filename   = basename( $decod_path );
                $newfname   = $_SERVER['DOCUMENT_ROOT']."/uploads/Gofile/".$filename;
                // try to download $server_file and save to $local_file
                if (ftp_get($ftp_conn, $newfname, $decod_path, FTP_BINARY )) {
                    ftp_close($ftp_conn);
                    $file_data = file_get_contents($newfname);
                    force_download( $filename , $file_data); 
                }
                if (file_exists($newfname)) {
                    // last resort setting
                    // chmod($oldPicture, 0777);
                    chmod($newfname, 0644);
                    $a = unlink($newfname);
                    print_r($a);
                } 
                die();
            }
        }else{
            die();
        }
    }
    function downloadB2File($folderid, $id, $filename){
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if($result[0]['connection_type'] == 'Backblaze'){
            $this->load->helper('back_blaz');
            $this->load->helper('download');
            $file_data = b2_download_file_by_id( $folderid, $token['key_id'], $token['application_key'] );
            force_download( $filename , $file_data);    
            // $res = '';
            die();
        }
    }
	function DownloadFilesOneDrive($fileId,$id,$fName){
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $this->load->library('onedrive');
        $tk = json_decode($result[0]['access_token'],true);
	   // $ntk = json_decode($tk['access_token'],true);
	    $refresh_token=$tk['refresh_token'];
	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
        $tk = json_decode($res,true);
        $accessToken = $tk['access_token'];
        $this->load->helper('download');
        $ar = $this->onedrive->DownloadFile($accessToken,$fileId);
    	force_download($fName,$ar);
        $res = array('shareUrl'=>$ar);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
	function userCreateFolder(){
	   //  if($_SESSION['id']==1){
	        $this->checkUserLoginStatus();
	   // }
	    $id =$_POST['user_id'];
	    $folderName = $_POST['folderName'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $connection_type = 'GoogleDrive';
                $data = $this->googledrive->createFolder($token, $folderName);
	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $ar = $this->dropbox_libraries->createFolder($accessToken,$folderName);
                $dropBoxData = json_decode($ar, true);
                $connection_type = 'DropBox';
                if(isset($dropBoxData['error_summary'])){
                    $data['status'] = 0; 
                }else{
                    $data['status'] = 1; 
                }
	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
            if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $parent_id = (isset($_POST['folderId'])? $_POST['folderId'] : 0 );
                $ar = $this->ddownload_api->createFolder($accessKey, $folderName, $parent_id);
                // echo"<pre>";print_r($ar);die;
                $DdownloadData = json_decode($ar, true);
                $connection_type = 'Ddownload';
                if(isset($DdownloadData['error'])){
                    $data['status'] = 0; 
                }else{
                    $data['status'] = 1; 
                }
	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
            if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                // $ntoken = json_decode($access_token,true);
                // $access_token = $ntoken['access_token'];
                if($access_token){
                    $ar = $this->box_api->createFolder($access_token, $folderName, 0);
                    $BoxData = json_decode($ar, true);
                    $connection_type = 'Box';
                    if(isset($BoxData['error_summary'])){
                        $data['status'] = 0; 
                    }else{
                        $data['status'] = 1; 
                    }
    	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
                }
            }
            if($result[0]['connection_type'] == 'Gofile'){
                $this->load->library('Gofile_lib');
                $tk = json_decode($result[0]['access_token'],true);
                $data_ = $this->gofile_lib->gofile_create_folder( $tk['token'], $tk['root_id'], $folderName );
                    $connection_type = 'Gofile';
                    if( $data_['status'] == 'ok' ) {
                        $data['status'] = 1; 
                    }else{
                        $data['status'] = 0; 
                    }
    	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }
            if($result[0]['connection_type'] == 'OneDrive'){
                $id = $_POST['user_id'];
                $my_folder=$_POST['folderName'];
                $result =$this->DBfile->get_data( 'connection', array('id'=>$id));$this->load->library('onedrive');
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	   // $ntk = json_decode($tk['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                if(!empty($_POST['folderId'])){
                    $drive = explode('!', $_POST['folderId']);
                    $driveId = $drive[0];
                    $folderId = $_POST['folderId'];
                }else{
                    $driveId = null;
                    $folderId = null;
                }
                $oneDrive = $this->onedrive->CreateFolder($accessToken,$my_folder,$driveId,$folderId);
                if(isset($oneDrive['error'])){
                    $data['status'] = 0; 
                }else{
                    $data['status'] = 1; 
                }
	            $res = array('userId'=>$result[0]['id'],'data'=>$data);
            }
            if($result[0]['connection_type'] == 'Backblaze'){
                $this->load->helper('back_blaz');
                $tk = json_decode($result[0]['access_token'],true);
                $folderName = clean($folderName);
                $data_ = b2_create_folder( '', $folderName.'/', $tk['key_id'], $tk['application_key'] );
                    $connection_type = 'Backblaze';
                    if( isset($data_['fileId']) ){
                        $data['status'] = 1; 
                    }else{
                        $data['status'] = 0; 
                    }
    	            $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
            }else if($result[0]['connection_type'] == 's3'){
              $this->load->helper('aws');
                $data = upload_s3('test','test',$_POST['folderName'].'/',null,$token['key'],$token['secret'],$token['region'],$token['BucketName']);
                if(isset($data['url'])){
                    $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                }else{
                     $res = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>0);
                }
            }else if($result[0]['connection_type'] == 'Ftp'){
                $folderName     = $this->input->post('folderName');
                $ftp_conn       = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp_conn, $token['username'], $token['password']);
                $connection_type = 'Ftp';
                if (ftp_mkdir($ftp_conn, $folderName)){
                    $data['status'] = 1; 
                }
                else{
                    $data['status'] = 0; 
                }
                ftp_close($ftp_conn);
                $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);
              }else if($result[0]['connection_type'] == 'Pcloud'){
                require_once(APPPATH.'libraries/pcloud/autoload.php');
                $folderName     = $this->input->post('folderName');

                try {
                    $pCloudFolder = new pCloud\Folder();
                    $pCloudFolder->create( $folderName, $token['token'], 0 ); 
                    $data['status'] = 1;
                } 
                catch (Exception $e) {
                    $err_msg = $e->getMessage();
                    $data['status'] = 0;
                    $data['message'] = $err_msg;
                }
                $res = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>'Pcloud');
              }
            //   if($_SESSION['id']==1){
                   if($res['data']['status'] == 1){
        			    $this->DBfile->custom_query("UPDATE `usertbl` SET `task_count` = task_count + 1 WHERE `id` = ".$_SESSION['id']);
                    }
        	   // }
        }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function userSubCreateFolder(){
	   // echo"<pre>";print_r($_POST);die;
	    $this->checkUserTaskLimit();
	    $id =$_POST['user_id'];
	    $folderName = $_POST['folderName'];
	    $parentId =$_POST['parentId'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $ar = $this->googledrive->createSubFolder($folderName, $parentId,$token);
                $ar = array(
                        'status' => 1,
                        'data' => $ar
                    );
            }else if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
	            $drotype = $_POST['drotype'];
	            $subFolderName = $drotype."/".$folderName; 
                $ar = $this->dropbox_libraries->createFolder($accessToken,'',$subFolderName );
                $ar = array(
                        'status' => 1,
                        'data' => $ar
                    );
            }else if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                // $parent_id = (isset($parentId)? $_POST['folderId'] : 0 );
                $ar = $this->ddownload_api->createFolder($accessKey, $folderName, $parentId);
                // echo"<pre>";print_r($ar);die;
                $ar = array(
                        'status' => 1,
                        'data' => $ar
                    );
            }else if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                // $ntoken = json_decode($access_token,true);
                // $access_token = $ntoken['access_token'];
                if($access_token){
                    $ar = $this->box_api->createFolder($access_token, $folderName, $parentId);
                    $ar = array(
                        'status' => 1,
                        'data' => $ar
                    );
                }
            }else if($result[0]['connection_type'] == 'Gofile'){
                $this->load->library('Gofile_lib');
                $tk = json_decode($result[0]['access_token'],true);
                $data_ = $this->gofile_lib->gofile_create_folder( $tk['token'], $parentId, $folderName );
                    if( $data_['status'] == 'ok' ) {
                        $status = 1; 
                    }else{
                        $status = 0; $data_ = [];
                    }
                    $data_['path_display'] = '';
                    $ar = array(
                        'status' => $status,
                        'data' => $data_
                    );
            }else if($result[0]['connection_type'] == 'OneDrive'){
                $id = $_POST['user_id'];
                $my_folder=$_POST['folderName'];
                $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	   // $ntk = json_decode($tk['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                if(!empty($parentId)){
                    $drive = explode('!', $parentId);
                    $driveId = $drive[0];
                    $folderId = $parentId;
                }else{
                    $driveId = null;
                    $folderId = null;
                }
                $ar = $this->onedrive->CreateFolder($accessToken,$my_folder,$driveId,$folderId);
                $ar = array(
                    'status' => 1,
                    'data' => $ar
                );
            }else if($result[0]['connection_type'] == 'GooglePhoto'){
                $this->load->library('Googlephoto');
                $data = $this->googlephoto->CreateAlbum($_POST['folderName'],$token);
                if(isset($data['userId'])){
                    $ar = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                }else{
                     $ar = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0);
                }
            }else if($result[0]['connection_type'] == 'Backblaze'){
                $this->load->helper('back_blaz');
                $tk = json_decode($result[0]['access_token'],true);
                $folderName = clean($folderName);
                $data_ = b2_create_folder( $parentId, $folderName.'/', $tk['key_id'], $tk['application_key'] );
                $data_['path_display'] = '';
                $ar = array(
                    'status' => 1,
                    'data' => $data_
                );
            }else if($result[0]['connection_type'] == 's3'){
                $this->load->helper('aws');
                if(!empty($folderName)){
                    $fldrnm = $_POST['parentId'].$folderName.'/';
                }else{
                    $fldrnm = null;
                }
                $data = upload_s3('test','test',$fldrnm,null,$token['key'],$token['secret'],$token['region'],$token['BucketName']);
                if(isset($data['url'])){
                    $ar = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                }else{
                     $ar = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>0);
                }
            }else if($result[0]['connection_type'] == 'Ftp'){
                $folderName     = $this->input->post('folderName');
                $ftp_conn       = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp_conn, $token['username'], $token['password']);
                $connection_type = 'Ftp';
                if (ftp_mkdir($ftp_conn, $parentId.'/'.$folderName)){
                    $data['status'] = 1; 
                }
                else{
                    $data['status'] = 0; 
                }
                ftp_close($ftp_conn);
                $ar = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>$connection_type);

              }else if($result[0]['connection_type'] == 'Pcloud'){
                require_once(APPPATH.'libraries/pcloud/autoload.php');
                $folderName     = $this->input->post('folderName');

                try {
                    $pCloudFolder = new pCloud\Folder();
                    $pCloudFolder->create( $folderName, $token['token'], $this->input->post('parentId') ); 
                    $data['status'] = 1;
                } 
                catch (Exception $e) {
                    $err_msg = $e->getMessage();
                    $data['status'] = 0;
                    $data['message'] = $err_msg;
                }
                $ar = array('userId'=>$result[0]['id'],'data'=>$data,'connection_type'=>'Pcloud');
              }

        }else{
            $res = array('status'=>0,'sms'=>'User Connection Are not Found');
        }
	    $res = array('userId'=>$result[0]['id'],'data'=>$ar);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	public function userUploadFiles(){
	   $this->checkUserLoginStatus();
	 
	//    if($_SESSION['id']==1){
    //        print_r(phpinfo());
	//        print_r($_FILES);
    //        die;
	//    }
	    if(!empty($_FILES['file']['name'])){
    	    $fileName = $_FILES['file']['name'];
            $result =$this->DBfile->get_data( 'connection', array('id'=>$_POST['user_id']));
            $token = json_decode($result[0]['access_token'],true);
            if(isset($result)){
                if($result[0]['connection_type'] == 'GoogleDrive'){
    		        $access_token = $token['access_token'];
    			    if($_POST['folderid']!='undefined'){
    			        $folderid = $_POST['folderid'];
    			    }else{
    			         $folderid='';
    			    }
                    $google = $this->uploadFileOnGoogleDrive( $_FILES['file']['tmp_name'], $fileName, $_FILES['file']['type'],$token,$folderid );
    				$fileurl = $google['url'];
    				$drivekey[] = $google['fileID'];
                    if($google['fileID']){
                        $res = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>1,'smg'=>'Successfully Upload File');
                    }else{
                         $res = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>0);
                    }
                }
                else if($result[0]['connection_type'] == 'DropBox'){
                    $this->load->library('Dropbox_libraries');
                    $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                    $ntoken = json_decode($access_token,true);
                    $accessToken = $ntoken['access_token'];
                    if(!empty($_POST['path'])){
                        $path = $_POST['path'];
                    }else{
                         $path = '';
                    }
                    $path = isset($_POST['path'])? $_POST['path'] : '/';
                    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $ar = $this->dropbox_libraries->uploadFile($accessToken, $path, $_FILES['file']['tmp_name'], $fileName);
                    // print_r($ar);die;
                    if(isset($ar['error_summary'])){
                         $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0);
                    }else{
                        $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Upload File');
                    }
                }
                else if($result[0]['connection_type'] == 'Ddownload'){
                    $config['upload_path'] = 'uploads/Ddownload';
    			    $config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
                        if ($this->upload->do_upload('file')){
                            $uploaddata = $this->upload->data();
                            $pic = $uploaddata['raw_name'];
                            $pic_ext = $uploaddata['file_ext'];
                            $fileData = $pic.'_'.date('ymdHis').$pic_ext;
                            // rename('./uploads/Ddownload/'.$pic.$pic_ext,'./uploads/Ddownload/'.$fileData);
                            $filename = $fileData;
                        }else{
                            $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                            die();
                        }
                    }  
                    $target_file = $_SERVER['DOCUMENT_ROOT'].'/uploads/Ddownload/'.$_FILES['file']['name']; 
                    $this->load->library('Ddownload_api');
                    $accessKey = $token['key'];
                    $ar = $this->ddownload_api->uploadFile($accessKey, $target_file);
                    // print_r($target_file);die;
                    $uploadFileData = json_decode($ar, true);
                    if($uploadFileData[0]['file_status'] == 'OK'){
                        $file_code = $uploadFileData[0]['file_code'];
                        $Rname = $this->ddownload_api->fileRename($accessKey, $file_code, $_FILES['file']['name']);
                        $fileR = json_decode($Rname,true);
                        if($fileR['status'] == '200'){
                            $moveFile = $this->ddownload_api->setFileInFolder($accessKey, $file_code, $_POST['folderid']);
                            $fileM = json_decode($moveFile,true);
                            if($fileM['status'] == '200'){
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Upload File');
                                unlink($target_file);
                            }else{
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                            }
                        }else{
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                        }
                    }else{
                        $res = array('status'=> 0,'smg'=>'SSL certificate problem: unable to get local issuer certificate{userId: "151');
                    }
                }
                else if($result[0]['connection_type'] == 'OneDrive'){
                    $type = $_FILES['file']['type'];
                    $fileName = $_FILES['file']['name'];
                    $this->load->library('onedrive');
                    $tk = json_decode($result[0]['access_token'],true);
            	   // $ntk = json_decode($tk['access_token'],true);
            	    $refresh_token=$tk['refresh_token'];
            	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                    $tk = json_decode($res,true);
                    $accessToken = $tk['access_token'];
                    $driveId = explode('!', $_POST['folderid']);
                    $config['upload_path'] = 'uploads/OneDrive';
            		$config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
                        if ($this->upload->do_upload('file')){
                            $uploaddata = $this->upload->data();
                            $pic = $uploaddata['raw_name'];
                            $pic_ext = $uploaddata['file_ext'];
                            $image_name = $pic.'_'.date('ymdHis').$pic_ext;
                            rename('./uploads/OneDrive/'.$pic.$pic_ext,'./uploads/OneDrive/'.$image_name);
                            $data_arr['profile_image'] = $image_name;
                            $filename = $image_name;
                        }else{
                            $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                            die();
                        }
                    }  
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/OneDrive/".$filename;
                    $ar = $this->onedrive->UPloadFile($accessToken,$driveId[0],$_POST['folderid'],$newfname,$filename,$type);
                    $connection = 'OneDrive';
                    if(!isset($ar['error'])){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Upload File');
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=> isset($ar['error']['message'])?$ar['error']['message']:'There are some issues at your drive\'s end, Please try again' );
                      unlink($newfname);
                    }
                }
                else if($result[0]['connection_type'] == 'Box'){
                    $this->load->library('Box_api');
                    $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                    // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                    // $ntoken = json_decode($access_token,true);
                    // $access_token = $ntoken['access_token'];
                    if($access_token){
                        $folderId = $_POST['folderid'];
                        $config['upload_path'] = 'uploads/Box';
        			    $config['allowed_types'] = '*';
                        $config['max_size']    = '0';
                        $this->load->library('upload', $config);
                        if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
                            if ($this->upload->do_upload('file')){
                                $uploaddata = $this->upload->data();
                                $pic = $uploaddata['raw_name'];
                                $pic_ext = $uploaddata['file_ext'];
                                $fileData = $pic.'_'.date('ymdHis').$pic_ext;
                                // rename('./uploads/Ddownload/'.$pic.$pic_ext,'./uploads/Ddownload/'.$fileData);
                                $filename = $fileData;
                            }else{
                                $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                                echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                                die();
                            }
                        }  
                        $target_file = $_SERVER['DOCUMENT_ROOT'].'/uploads/Box/'.$_FILES['file']['name']; 
                        // $file_content = file_get_contents($target_file);
                        // $mime_type = mime_content_type($target_file);  
                        $ar = $this->box_api->uploadFile($access_token, $folderId, $target_file, $fileName);
                        $tt = json_decode($ar,true);
                        // print_r($tt);die;
                       if(isset($tt) && ($tt['total_count'] == '1')){
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1, 'msg' => 'File upload successfully.' );
                        }else{
                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0 , 'msg' => $tt['message']);
                        }
                    }
                }
                else if($result[0]['connection_type'] == 's3'){
                    $this->load->helper('aws');
                    $data = upload_s3($_FILES['file']['tmp_name'],$_FILES['file']['name'],$_POST['folder_id'],null,$token['key'],$token['secret'],$token['region'],$token['BucketName']);
                    if(isset($data['url'])){
                        $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                    }else{
                         $res = array('userId'=>$result[0]['id'],'data'=>$google,'status'=>0);
                    }
                }else if($result[0]['connection_type'] == 'Backblaze'){
                    $this->load->helper('back_blaz');
                    $config['upload_path'] = 'uploads/backBlaze';
    			    $config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    $_FILES['file']['name'] = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['file']['name'] );
                    if(isset($_FILES['file']) && !empty( $_FILES['file']['name'] )){
                        if ($this->upload->do_upload('file')){
                            $uploaddata = $this->upload->data();
                            $pic = $uploaddata['raw_name'];
                            $pic_ext = $uploaddata['file_ext'];
                            $fileData = $pic.'_'.date('ymdHis').$pic_ext;
                            $filename = $fileData;
                        }else{
                            $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                            die();
                        }
                    } 
                    
                    $folderid   = $this->input->post('folderid');
                    if ($this->input->post('folderid') == 'undefined') {
                        $folderid = '';
                    }else{
                        $folderid = $this->input->post('folderid');
                    }

                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backBlaze/".$_FILES['file']['name'];
                    $ar = b2_uploadFile( $folderid, $newfname, $_FILES['file']['name'], $token['key_id'], $token['application_key']);
                    $connection = 'Backblaze';
                    if(isset($ar['fileName'])){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Upload File');
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue....');
                      unlink($newfname);
                    }
                }else if($result[0]['connection_type'] == 'Gofile'){
                    $this->load->library('Gofile_lib');
                    $config['upload_path'] = 'uploads/Gofile';
    			    $config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    $_FILES['file']['name'] = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['file']['name'] );
                    if(isset($_FILES['file']) && !empty( $_FILES['file']['name'] )){
                        if ($this->upload->do_upload('file')){
                            $uploaddata = $this->upload->data();
                            $pic = $uploaddata['raw_name'];
                            $pic_ext = $uploaddata['file_ext'];
                            $fileData = $pic.'_'.date('ymdHis').$pic_ext;
                            $filename = $fileData;
                        }else{
                            $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                            die();
                        }
                    }  

                    $folderid   = $this->input->post('folderid');
                    if ($this->input->post('folderid') == 'undefined') {
                        $folderid = $token['root_id'];
                    }else{
                        $folderid = $this->input->post('folderid');
                    }

                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/Gofile/".$_FILES['file']['name'];
                    $ar = $this->gofile_lib->gofile_upload_file( $token['token'], $folderid, $newfname );
                    $connection = 'Gofile';
                    if( $ar['status'] == 'ok'){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Upload File');
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue....');
                      unlink($newfname);
                    }
                }else if($result[0]['connection_type'] == 'Anon'){
                    $path = $_SERVER['DOCUMENT_ROOT'].'/uploads/Anon';
                    if(!file_exists($path)){
                        mkdir($path);
                    }
                    $config['upload_path'] = 'uploads/Anon';
    			    $config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    if(isset($_FILES['file']) && !empty($_FILES['file']['name'])){
                        if ($this->upload->do_upload('file')){
                            $uploaddata = $this->upload->data();
                            $pic = $uploaddata['raw_name'];
                            $pic_ext = $uploaddata['file_ext'];
                            $fileData = $pic.'_'.date('ymdHis').$pic_ext;
                            // rename('./uploads/Anon/'.$pic.$pic_ext,'./uploads/Anon/'.$fileData);
                            $filename = $fileData;
                        }else{
                            $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                            die();
                        }
                    }
                    $target_file = $path.'/'.$_FILES['file']['name'];
                    // print_r(file_exists($target_file));die;
                    $this->load->library('AnonFile_lib');
                    $ar = $this->anonfile_lib->uploadFile($token['Apikey'], $target_file);
                    $json_response = json_decode( $ar, true);
                    // echo"<pre>";print_r($json_response);die;
                    if(isset($json_response) && $json_response['status'] == '1' ){
                        $data =array(
                            'user_id'=> $_SESSION['id'],
                            'conn_id'=> $_POST['user_id'],
                            'file_id'=> $json_response['data']['file']['metadata']['id'],
                            'connection_key'=> $token['Apikey'],
                            'upload_on'=> 'Anon',
                            'file_name'=> $fileName,
                            'file_size'=> $_FILES['file']['size'],
                            'file_data'=> $ar,
                            'status'=> 1,
                        );
                        $result = $this->DBfile->put_data('files_details',$data);
                        if($result){
                            $res = array('status'=>1, 'data'=>$result,'smg'=>'File Uploaded Successfully');
                            unlink($target_file);
                        }else{
                            $res = array('status'=>0);
                        }
                    }else{
                        $res = array('status'=>0);
                    }
                }else if($result[0]['connection_type'] == 'Ftp'){
                    $this->load->library('Gofile_lib');
                    $config['upload_path'] = 'uploads/Gofile';
                    $config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    $_FILES['file']['name'] = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['file']['name'] );
                    // if(isset($_FILES['file']) && !empty( $_FILES['file']['name'] )){
                    //     if ($this->upload->do_upload('file')){
                    //         $uploaddata = $this->upload->data();
                    //     }else{
                    //         $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                    //         echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                    //         die();
                    //     }
                    // }  

                    $folderid   = $this->input->post('folderid').'/';
                    if ($this->input->post('folderid') == 'undefined') {
                        $folderid = '';
                    }else{
                        $folderid = $this->input->post('folderid').'/';
                    }

                    $ftp_conn   = ftp_connect( $token['hostname'] );
                    $login      = ftp_login($ftp_conn, $token['username'], $token['password']);
                    $connection = 'Ftp';
                    // upload file
                    if (ftp_put($ftp_conn, $folderid.$_FILES['file']['name'], $_FILES['file']['tmp_name'], FTP_BINARY  )){
                        $res = array( 'data'=> [], 'connection'=> $connection,'status'=> 1 );
                    }
                    else{
                        $res = array( 'data'=> [], 'connection'=> $connection,'status'=> 0 );
                    }
                    // close connection
                    ftp_close($ftp_conn);
                }
                else if($result[0]['connection_type'] == 'GooglePhoto'){
                     $this->load->library('Googlephoto');
                     $parentId = $_POST['folder_id'];
                    $clientData = $this->googlephoto->uploadMedia($token,$parentId,$_FILES['file']['tmp_name'],$fileName, $_FILES['file']['type']);
                    $res = json_decode($clientData);
                }
                else if($result[0]['connection_type'] == 'Pcloud'){
                    
                    $config['upload_path'] = 'uploads/pcloud';
    			    $config['allowed_types'] = '*';
                    $config['max_size']    = '0';
                    $this->load->library('upload', $config);
                    $_FILES['file']['name'] = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['file']['name'] );
                    if(isset($_FILES['file']) && !empty( $_FILES['file']['name'] )){
                        if ($this->upload->do_upload('file')){
                            $uploaddata = $this->upload->data();
                            $pic = $uploaddata['raw_name'];
                            $pic_ext = $uploaddata['file_ext'];
                            $fileData = $pic.'_'.date('ymdHis').$pic_ext;
                            $filename = $fileData;
                        }else{
                            $resp = array('status'=>'0', 'msg' => $this->upload->display_errors());
                            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
                            die();
                        }
                    }  

                    $folderid   = $this->input->post('folderid');
                    require_once( APPPATH.'libraries/pcloud/autoload.php' );
                    
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/pcloud/".$_FILES['file']['name'];
                    $url_path  = base_url( 'uploads/pcloud/'.$_FILES['file']['name'] );
                    try {
                        $pCloudFile = new pCloud\File();
                        // $pCloudFile->upload($newfname, $token['token'], $folderid);
                        $res = $pCloudFile->upload($url_path, $token['token'], $_FILES['file']['name'], (int)$folderid);
                        $ar['status']   = 1;

                    } catch (Exception $e) {
                        $ar['status']   = 0;
                    
                    }
                    
                    $connection = 'Pcloud';
                    if( $ar['status'] == 1){
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Upload File');
                      unlink($newfname);
                    }else{
                      $res = array('data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue....');
                      unlink($newfname);
                    }

                }
          
              if($_SESSION['id']==9){
                //   print_r($res['status']);
                //   die;
                    if($res['status'] == 1){
            		    $this->DBfile->custom_query("UPDATE `usertbl` SET `task_count` = task_count + 1 WHERE `id` = ".$_SESSION['id']);
            		    $_SESSION['task_count'] = $_SESSION['task_count']+1;
                    } 
        	    }
            }
	    }else{
            $res = array('status'=>0,'smg'=>'File is required.');	        
	    }     
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
	} 
    public function SinkMoveFileUpload(){
        // print_r($_POST);die;
        $this->load->library('Googledrive');
        $this->load->library('Dropbox_libraries');
        $this->load->library('Ddownload_api');
        $result =$this->DBfile->get_data( 'connection', array('id'=>$_POST['oldUserId']));
        $token = json_decode($result[0]['access_token'],true);
		$moveCLoudToken = $this->DBfile->get_data( 'connection', array('id'=>$_POST['connectionID']));
		$CloudToken =  json_decode($moveCLoudToken[0]['access_token'],true);
		
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $ar = $this->googledrive->getFileInfo($token,$_POST['MoveFileID']);
                $url = $ar['data']['url'];
        	    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/googleDrive/".$ar['data']['name'];
        	    if(file_put_contents($newfname, file_get_contents($url))){
                    $ext = pathinfo($ar['data']['name'], PATHINFO_EXTENSION);
                    $type = $ar['data']['type'];
        	        $folderid=$_POST['MoveFolderID'];
        	        if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
        	             $google = $this->uploadFileOnGoogleDrive( $newfname, $ar['data']['name'],$type,$CloudToken,$folderid );
        	            if($google['fileID']){
                                if($result[0]['connection_type'] == 'GoogleDrive'){
                                     $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                                }
                                $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Move File');
                            unlink($newfname);
                        }else{
                             $res = array('status'=>0,'data'=>$google,'smg'=>'Technical Issue Please Refresh Page ');
                             unlink($newfname);
                        }
        	        }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($CloudToken['refresh_token']);
                        $ntoken = json_decode($access_token,true);
                        $accessToken = $ntoken['access_token'];
                        $moveFileToken = $CloudToken['access_token'];
                        if(!empty($_POST['NewFolderPath'])){
                            $path = $_POST['NewFolderPath'];
                        }else{
                            $path = '';
                        }
                        $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                        $fileName = $ar['data']['name'];
                        $google = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                        //  print_r($google);
                        // die();
                        if(isset($google['path_display']) && !empty($google['path_display']) && !isset($google['error_summary'])){
                            $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Move File ');
                                if($result[0]['connection_type'] == 'GoogleDrive'){
                                     $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                                }
                             unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$google,'smg'=>$google['error_summary']);
                            unlink($newfname);
                        }
        	        }else if($moveCLoudToken[0]['connection_type']=='OneDrive'){
        	            $this->load->library('onedrive');
                        $tk = json_decode($moveCLoudToken[0]['access_token'],true);
                	    $refresh_token=$tk['refresh_token'];
                	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                        $tk = json_decode($res,true);
                        $accessToken = $tk['access_token'];
        	            $driveId = explode('!', $_POST['MoveFolderID']);
        	            $fileName = $ar['data']['name'];
        	            $type = $ar['data']['type'];
                        $data = $this->onedrive->UPloadFile($accessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fileName,$type);
                        if(isset($data['id'])){
                            $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                            $res = array('status'=>1,'data'=>$data);
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0, 'data'=>$data);
                            unlink($newfname);
                        }
        	        }else if($moveCLoudToken[0]['connection_type'] == 'Ddownload'){
                        $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                        $this->load->library('Ddownload_api');
                        $accessKey = $dDownloadKey['key'];
                        $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                        $uploadFileData = json_decode($ar, true);
                        if($uploadFileData[0]['file_status'] == 'OK'){
                            $file_code = $uploadFileData[0]['file_code'];
                            $Rname = $this->ddownload_api->fileRename($accessKey, $file_code, $ar['data']['name']);
                            $fileR = json_decode($Rname,true);
                            if($fileR['status'] == '200'){
                                $moveFile = $this->ddownload_api->setFileInFolder($accessKey, $file_code, $_POST['MoveFolderID']);
                                $fileM = json_decode($moveFile,true);
                                if($fileM['status'] == '200'){
                                    $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Upload File');
                                    unlink($target_file);
                                }else{
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                                }
                            }else{
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                            }
                        }else{
                            $res = array('status'=> 0,'smg'=>'SSL certificate problem: unable to get local issuer certificate{userId: "151');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Box'){
                        $this->load->library('Box_api');
                        $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box', $moveCLoudToken[0]['created']);
                        // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                        // $ntoken = json_decode($access_token,true);
                        // $access_token = $ntoken['access_token'];
                        if($access_token){
                            $folderId = $_POST['MoveFolderID'];
                            $fileName = $ar['data']['name'];
                            $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $ar['data']['name']);
                           if(isset($ar['entries']) != ''){
                                $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'');
                            }else{
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'');
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 's3'){
                        $this->load->helper('aws');
                        $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
                        if(isset($data['url'])){
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1);
                             $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Backblaze'){
                        $this->load->helper('back_blaz');
                        $ar = b2_uploadFile( $_POST['MoveFolderID'], $newfname, $ar['data']['name'], $CloudToken['key_id'], $CloudToken['application_key']);
                        $connection = 'Backblaze';
                        if(isset($ar['fileName'])){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                          $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Gofile'){
                        $this->load->library('Gofile_lib');
                        $ar = $this->gofile_lib->gofile_upload_file( $CloudToken['token'], $_POST['MoveFolderID'], $newfname );
                        $connection = 'Gofile';
                        if( $ar['status'] == 'ok'){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                           $ar = $this->googledrive->deleteFiles($_POST['MoveFileID'], $token);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }
                }else{
                    echo 17;
                }
            }
            else if($result[0]['connection_type'] == 'DropBox'){
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
	            $oldPath = $_POST['oldPath'];
                if(!empty($_POST['NewFolderPath'])){
                    $path = $_POST['NewFolderPath'];
                }else{
                    $path = '';
                }
                $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                // $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $oldPath);
                $shareFileLink = json_decode( $shareFileData, true );
                if(empty($shareFileLink['links'])){
                    $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $oldPath);
                    $url = json_decode( $ru, true );
                }else{
                   $url = $shareFileLink['links'][0]['url'];
                }
                // print_r($url);
                // die;
                // $fileName = basename($_POST['oldPath']);
                $fileName =$_POST['movefilename'];
                $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/Dropbox/".$fileName;
                $shareURL = str_replace( 'dl=0','raw=1', $url);
                if(file_put_contents($newfname, file_get_contents($shareURL))){
                    if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
                        $folderid=$_POST['MoveFolderID'];
        	             $google = $this->uploadFileOnGoogleDrive( $newfname, $fileName, mime_content_type($newfname),$CloudToken,$folderid );
        	            if($google['fileID']){
        	                    if($result[0]['connection_type'] == 'DropBox'){
                                    $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                                    $ntoken = json_decode($access_token,true);
                                    $accessToken = $ntoken['access_token'];
                                    $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                }
                           $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Move File' );
                           unlink($newfname);
                        }else{
                             $res = array('status'=>0,'data'=>$google,'smg'=>'Technical Issue Please Refresh Page. ');
                             unlink($newfname);
                        }
        	        }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                        $ar = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                        if(isset($ar['path_display']) && !empty($ar['path_display']) && !isset($dropBoxData['error_summary'])){
                            $res = array('status'=>1,'data'=>$ar,'smg'=>'Successfully Move File');
                                if($result[0]['connection_type'] == 'DropBox'){
                                    $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                                    $ntoken = json_decode($access_token,true);
                                    $accessToken = $ntoken['access_token'];
                                    $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                }
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$ar,'smg'=>'Technical Issue Please Refresh Page. ');
                            unlink($newfname);
                        }
        	        }else if($moveCLoudToken[0]['connection_type'] == 'Ddownload'){
                        $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                        $this->load->library('Ddownload_api');
                        $accessKey = $dDownloadKey['key'];
                        $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                        $uploadFileData = json_decode($ar, true);
                        if($uploadFileData[0]['file_status'] == 'OK'){
                            $file_code = $uploadFileData[0]['file_code'];
                            $Rname = $this->ddownload_api->fileRename($accessKey, $file_code, $fname);
                            $fileR = json_decode($Rname,true);
                            if($fileR['status'] == '200'){
                                $moveFile = $this->ddownload_api->setFileInFolder($accessKey, $file_code, $_POST['MoveFolderID']);
                                $fileM = json_decode($moveFile,true);
                                if($fileM['status'] == '200'){
                                    $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Upload File');
                                    unlink($target_file);
                                }else{
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                                }
                            }else{
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                            }
                        }else{
                            $res = array('status'=> 0,'smg'=>'SSL certificate problem: unable to get local issuer certificate{userId: "151');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Box'){
                        $this->load->library('Box_api');
                        $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box', $result[0]['created']);
                        // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                        // $ntoken = json_decode($access_token,true);
                        // $access_token = $ntoken['access_token'];
                        if($access_token){
                            $folderId = $_POST['MoveFolderID'];
                            $fileName = $fname;
                            $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fname);
                           if(isset($ar['entries']) != ''){
                                $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Move File');
                            }else{
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue Please Refresh Page. ');
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 's3'){
                        $this->load->helper('aws');
                        $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
                        if(isset($data['url'])){
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1,'smg'=>'Successfully Move File');
                             $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0,'smg'=>'Technical Issue Please Refresh Page. ');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Backblaze'){
                        $this->load->helper('back_blaz');
                        $ar = b2_uploadFile( $_POST['MoveFolderID'], $newfname, $fname, $CloudToken['key_id'], $CloudToken['application_key']);
                        $connection = 'Backblaze';
                        if(isset($ar['fileName'])){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                           $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Gofile'){
                        $this->load->library('Gofile_lib');
                        $ar = $this->gofile_lib->gofile_upload_file( $CloudToken['token'], $_POST['MoveFolderID'], $newfname );
                        $connection = 'Gofile';
                        if( $ar['status'] == 'ok'){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                          $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }
                }else{
                    $res = array('status'=>0,'data'=>'','smg'=> 'File Get Content Error');
                     unlink($newfname);
                }
            }
            else if($result[0]['connection_type'] == 'Ddownload'){
	            $oldPath = $_POST['oldPath'];
                if($result[0]['connection_type'] == $moveCLoudToken[0]['connection_type']){
                    $accessKey = $token['key'];
                    // $moveFileKey = $CloudToken['key'];
                    $ar = $this->ddownload_api->setFileInFolder($accessKey, $_POST['MoveFileID'], $_POST['MoveFolderID']);
                    $fileM = json_decode($ar,true);
                    if($fileM['status'] == '200'){
                        $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1);
                    }else{
                        $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0);
                    }
                }else{
                    $accessKey = $token['key'];
                    $moveCLoudToken = $CloudToken['access_token'];
                    if(!empty($_POST['NewFolderPath'])){
                        $path = $_POST['NewFolderPath'];
                    }else{
                        $path = '';
                    }
                    $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                    // $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $shareFileData = $this->ddownload_api->getFileInfo($accessKey, $_POST['MoveFileID']);
                    $shareFileLink = json_decode( $shareFileData, true );
                    // echo"<pre>";print_r($shareFileLink);die;
                    ///// kuldeep ////
                    // $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $oldPath);
                    if(empty($shareFileLink['links'])){
                        $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $oldPath);
                        $url = json_decode( $ru, true );
                    }else{
                       $url = $shareFileLink['links'][0]['url'];
                    }
                    $fileName = basename($_POST['oldPath']);
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/Dropbox/".$fileName;
                    $shareURL = str_replace( 'dl=0','raw=1', $url);
                    if(file_put_contents($newfname, file_get_contents($shareURL))){
                       if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
                            $folderid=$_POST['MoveFolderID'];
            	             $google = $this->uploadFileOnGoogleDrive( $newfname, $fileName, mime_content_type($newfname),$CloudToken,$folderid );
            	            if($google['fileID']){
            	                    if($result[0]['connection_type'] == 'DropBox'){
                                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                                        $ntoken = json_decode($access_token,true);
                                        $accessToken = $ntoken['access_token'];
                                        $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                    }
                               $res = array('status'=>1,'data'=>$google);
                               unlink($newfname);
                            }else{
                                 $res = array('status'=>0,'data'=>$google);
                                 unlink($newfname);
                            }
            	        }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                            $ar = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                            if(isset($ar['path_display']) && !empty($ar['path_display']) && !isset($dropBoxData['error_summary'])){
                                $res = array('status'=>1,'data'=>$ar);
                                    if($result[0]['connection_type'] == 'DropBox'){
                                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                                        $ntoken = json_decode($access_token,true);
                                        $accessToken = $ntoken['access_token'];
                                        $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                    }
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0,'data'=>$ar);
                                unlink($newfname);
                            }
            	        }else if($moveCLoudToken[0]['connection_type']=='OneDrive'){
            	            $this->load->library('onedrive');
                            $tk = json_decode($result[0]['access_token'],true);
                    	   // $ntk = json_decode($tk['access_token'],true);
                    	    $refresh_token=$tk['refresh_token'];
                    	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                            $tk = json_decode($res,true);
                            $accessToken = $tk['access_token'];
                            $ar = $this->onedrive->DownloadFile($accessToken,$_POST['MoveFileID']);
                            $ar1 = $this->onedrive->ShareURL($accessToken,$_POST['MoveFileID']);
                            $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/OneDrive/".$ar1['name'];
                            $tk = json_decode($CloudToken['access_token'],true);
                            $MoveAccessToken = $tk['access_token'];
                            $driveId = explode('!', $_POST['MoveFolderID']);
                            $data = $this->onedrive->UPloadFile($MoveAccessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$ar1['name'],$ar1['file']['mimeType']);
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data);
                                $driveId = explode('!', $_POST['MoveFileID']);
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data);
                                unlink($newfname);
                            }
            	        }else if($moveCLoudToken[0]['connection_type']=='Box'){
            	                $this->load->library('Box_api');
                                $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box', $result[0]['created']);
                                // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                                // $ntoken = json_decode($access_token,true);
                                // $access_token = $ntoken['access_token'];
                                if($access_token){
                                    $folderId = $_POST['MoveFolderID'];
                                    $fileName = $_POST['movefilename'];
                                    $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fileName);
                                   if(isset($ar['entries']) != ''){
                                        $this->load->library('onedrive');
                                        $tk = json_decode($result[0]['access_token'],true);
                                	   // $ntk = json_decode($tk['access_token'],true);
                                	    $refresh_token=$tk['refresh_token'];
                                	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                                        $tk = json_decode($res,true);
                                        $accessToken = $tk['access_token'];
                                        $driveId = explode('!', $_POST['MoveFileID']);
                                        $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                        unlink($newfname);
                                        $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1);
                                    }else{
                                        unlink($newfname);
                                        $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0);
                                    }
                                }
            	        }else if($moveCLoudToken[0]['connection_type']=='Ddownload'){
            	            $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                            $this->load->library('Ddownload_api');
                            $accessKey = $dDownloadKey['key'];
                            $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                            $uploadFileData = json_decode($ar, true);
                            // echo"<pre>";print_r($uploadFileData);die;
                            if($uploadFileData[0]['file_status'] == 'OK'){
                                $driveId = explode('!', $_POST['MoveFileID']);
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                $res = array('status'=>1,'data'=>$uploadFileData);
                            }else{
                                $res = array('status'=>0,'data'=>$uploadFileData);
                            } 
            	        }
                    }else{
                        echo 17;
                    }
                }
            }
            else if($result[0]['connection_type'] == 'OneDrive'){
                
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $TokenNew = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($TokenNew,true);
                $accessToken = $tk['access_token'];
	            $driveId = explode('!', $_POST['MoveFileID']);
                $ar = $this->onedrive->DownloadFile($accessToken,$_POST['MoveFileID']);
                $ar1 = $this->onedrive->ShareURL($accessToken,$_POST['MoveFileID']);
                
                $fname =$ar1['name'];
                $mime_type =$ar1['file']['mimeType'];
                $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/".$fname;
                if(file_put_contents($newfname, $ar)){
                    
                    if($moveCLoudToken[0]['connection_type']=='OneDrive'){
                       
                        if($result[0]['id'] == $moveCLoudToken[0]['id']){
                            $driveId = explode('!', $_POST['MoveFileID']);
                            $data = $this->onedrive->UPloadFile($accessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fname,$mime_type);
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data,'smg'=>'Successfully Move File');
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data,'smg'=>'Technical Issue Please Refresh Page ');
                                unlink($newfname);
                            }
                        }else{
                            $this->load->library('onedrive');
                            $tk = json_decode($moveCLoudToken[0]['access_token'],true);
                    	 
                    	    $refresh_token=$tk['refresh_token'];
                    	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                            $tk = json_decode($res,true);
                            $MoveAccessToken = $tk['access_token'];
                            $driveId = explode('!', $_POST['MoveFolderID']);
                            $data = $this->onedrive->UPloadFile($MoveAccessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fname,$mime_type);
                      
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data,'smg'=>'Successfully Move File');
                                $driveId = explode('!', $_POST['MoveFileID']);
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data,'smg'=>'Technical Issue Please Refresh Page ');
                                unlink($newfname);
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
                        $google = $this->uploadFileOnGoogleDrive( $newfname, $fname,$mime_type,$CloudToken,$_POST['MoveFolderID'] );
        	            if($google['fileID']){
                               $driveId = explode('!', $_POST['MoveFileID']);
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Move File ');
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$google,'smg'=>'Technical Issue Please Refresh Page. ');
                            unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($CloudToken['refresh_token']);
                        $ntoken = json_decode($access_token,true);
                        $accessToken = $ntoken['access_token'];
                        $moveFileToken = $CloudToken['access_token'];
                        if(!empty($_POST['NewFolderPath'])){
                            $path = $_POST['NewFolderPath'];
                        }else{
                            $path = '';
                        }
                        $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                        $fileName = $fname;
                        $dropbox = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                        if(isset($dropbox['path_display']) && !empty($dropbox['path_display']) && !isset($dropbox['error_summary'])){
                            $token = json_decode($result[0]['access_token'],true);
                             $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                            $res = array('status'=>1,'data'=>$dropbox,'smg'=>'Successfully Move File ');
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$dropbox,'smg'=>$dropbox['error_summary']);
                            unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Ddownload'){
                        $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                        $this->load->library('Ddownload_api');
                        $accessKey = $dDownloadKey['key'];
                        $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                        $uploadFileData = json_decode($ar, true);
                        if($uploadFileData[0]['file_status'] == 'OK'){
                            $file_code = $uploadFileData[0]['file_code'];
                            $Rname = $this->ddownload_api->fileRename($accessKey, $file_code, $fname);
                            $fileR = json_decode($Rname,true);
                            if($fileR['status'] == '200'){
                                $moveFile = $this->ddownload_api->setFileInFolder($accessKey, $file_code, $_POST['MoveFolderID']);
                                $fileM = json_decode($moveFile,true);
                                if($fileM['status'] == '200'){
                                    $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Upload File');
                                    unlink($target_file);
                                }else{
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                                }
                            }else{
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                            }
                        }else{
                            $res = array('status'=> 0,'smg'=>'SSL certificate problem: unable to get local issuer certificate{userId: "151');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Box'){
                        $this->load->library('Box_api');
                        $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box', $result[0]['created']);
                        if($access_token){
                            $folderId = $_POST['MoveFolderID'];
                            $fileName = $fname;
                            $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fname);
                           if(isset($ar['entries']) != ''){
                                 $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Move File');
                            }else{
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue Please Refresh Page ');
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 's3'){
                        $this->load->helper('aws');
                        $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
                        if(isset($data['url'])){
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1,'smg'=>'Successfully Move File ');
                              $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0,'smg'=>'Please refresh Page and try again ');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Backblaze'){
                        $this->load->helper('back_blaz');
                        $ar = b2_uploadFile( $_POST['MoveFolderID'], $newfname, $fname, $CloudToken['key_id'], $CloudToken['application_key']);
                        $connection = 'Backblaze';
                        if(isset($ar['fileName'])){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                            $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Gofile'){
                        $this->load->library('Gofile_lib');
                        $ar = $this->gofile_lib->gofile_upload_file( $CloudToken['token'], $_POST['MoveFolderID'], $newfname );
                        $connection = 'Gofile';
                        if( $ar['status'] == 'ok'){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                            $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh  Page. ');
                          unlink($newfname);
                        }
                    }
                
                    
                }else{
                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'File Get Content Error');
                }
            }
            else if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $Box_access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                if($Box_access_token){
                    if( ($result[0]['connection_type'] == $moveCLoudToken[0]['connection_type']) && ($result[0]['email'] == $moveCLoudToken[0]['email']) ){ 
                        $MoveFolderID = $_POST['MoveFolderID'];
                        $MoveFileID = $_POST['MoveFileID'];
                        $movefilename = $_POST['movefilename'];
                        $ar = $this->box_api->fileMoveBoxToBox($Box_access_token, $MoveFolderID, $MoveFileID, $movefilename);
                        $fileM = json_decode($ar,true);
                        // echo"<pre>";print_r($fileM);die;
                        if($fileM['item_status'] == 'active'){
                             $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Move File');
                        }else{
                             $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue Please Refresh Page ');
                        }
                    }else{
        	            $oldPath = $_POST['oldPath'];
                        $accessToken = $token['access_token'];
                        $moveFileToken = $CloudToken['access_token'];
                        if(!empty($_POST['NewFolderPath'])){
                            $path = $_POST['NewFolderPath'];
                        }else{
                            $path = '';
                        }
                        $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                        // $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                        $shareFileData = $this->box_api->getFileDetails($Box_access_token, $_POST['MoveFileID']);
                        $shareFileLink = json_decode( $shareFileData, true );
                        // print_r($shareFileLink);die;
                        if(!empty($shareFileLink['shared_link'])){
                            $shareURL = $shareFileLink['shared_link']['url'];
                            // $shareURL = $fileData['shared_link']['download_url'];
                        }else{
                            $shareURL = 'https://app.box.com/file/'.$_POST['MoveFileID'];
                        }
                        // getFileDetails
                        $fileName = basename($_POST['oldPath']);
                        $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/Dropbox/".$fileName;
                        $shareURL = str_replace( 'dl=0','raw=1', $url);
                        if(file_put_contents($newfname, file_get_contents($shareURL))){
                           if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
                                $folderid=$_POST['MoveFolderID'];
                	             $google = $this->uploadFileOnGoogleDrive( $newfname, $fileName, mime_content_type($newfname),$CloudToken,$folderid );
                	            if($google['fileID']){
                	                    if($result[0]['connection_type'] == 'DropBox'){
                                            $accessToken = $token['access_token'];
                                            $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                        }
                                   $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Move File');
                                   unlink($newfname);
                                }else{
                                     $res = array('status'=>0,'data'=>$google,'smg'=>'Technical Issue Please Refresh Page ');
                                     unlink($newfname);
                                }
                	        }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                                $ar = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                                if(isset($ar['path_display']) && !empty($ar['path_display']) && !isset($dropBoxData['error_summary'])){
                                    $res = array('status'=>1,'data'=>$ar);
                                        if($result[0]['connection_type'] == 'DropBox'){
                                            $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                                            $ntoken = json_decode($access_token,true);
                                            $accessToken = $ntoken['access_token'];
                                            $ar = $this->dropbox_libraries->deleteFile($accessToken,$oldPath);
                                        }
                                    unlink($newfname);
                                }else{
                                    $res = array('status'=>0,'data'=>$ar);
                                    unlink($newfname);
                                }
                	        }else if($moveCLoudToken[0]['connection_type']=='OneDrive'){
                	            $this->load->library('onedrive');
                                $tk = json_decode($result[0]['access_token'],true);
                        	   // $ntk = json_decode($tk['access_token'],true);
                        	    $refresh_token=$tk['refresh_token'];
                        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                                $tk = json_decode($res,true);
                                $accessToken = $tk['access_token'];
                                $ar = $this->onedrive->DownloadFile($accessToken,$_POST['MoveFileID']);
                                $ar1 = $this->onedrive->ShareURL($accessToken,$_POST['MoveFileID']);
                                $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/OneDrive/".$ar1['name'];
                                $tk = json_decode($CloudToken['access_token'],true);
                                $MoveAccessToken = $tk['access_token'];
                                $driveId = explode('!', $_POST['MoveFolderID']);
                                $data = $this->onedrive->UPloadFile($MoveAccessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$ar1['name'],$ar1['file']['mimeType']);
                                if(isset($data['id'])){
                                    $res = array('status'=>1, 'data'=>$data,'smg'=>'Successfully Move File');
                                    $driveId = explode('!', $_POST['MoveFileID']);
                                    $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                    unlink($newfname);
                                }else{
                                    $res = array('status'=>0, 'data'=>$data,'smg'=>'Technical Issue Please Refresh Page ');
                                    unlink($newfname);
                                }
                	        }else if($moveCLoudToken[0]['connection_type']=='Box'){
                	                $this->load->library('Box_api');
                                    $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box', $result[0]['created']);
                                    if($access_token){
                                        $folderId = $_POST['MoveFolderID'];
                                        $fileName = $_POST['movefilename'];
                                        $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fileName);
                                       if(isset($ar['entries']) != ''){
                                            $this->load->library('onedrive');
                                            $tk = json_decode($result[0]['access_token'],true);
                                    	   // $ntk = json_decode($tk['access_token'],true);
                                    	    $refresh_token=$tk['refresh_token'];
                                    	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                                            $tk = json_decode($res,true);
                                            $accessToken = $tk['access_token'];
                                            $driveId = explode('!', $_POST['MoveFileID']);
                                            $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                            unlink($newfname);
                                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Move File');
                                        }else{
                                            unlink($newfname);
                                            $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue Please Refresh Page ');
                                        }
                                    }
                	        }else if($moveCLoudToken[0]['connection_type']=='Ddownload'){
                	            $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                                $this->load->library('Ddownload_api');
                                $accessKey = $dDownloadKey['key'];
                                $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                                $uploadFileData = json_decode($ar, true);
                                // echo"<pre>";print_r($uploadFileData);die;
                                if($uploadFileData[0]['file_status'] == 'OK'){
                                    $driveId = explode('!', $_POST['MoveFileID']);
                                    $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                    $res = array('status'=>1,'data'=>$uploadFileData,'smg'=>'Successfully Move File');
                                }else{
                                    $res = array('status'=>0,'data'=>$uploadFileData,'smg'=>'Technical Issue Please Refresh Page ');
                                } 
                	        }
                        }else{
                            echo 17;
                        }
                    }
                }
            }else if($result[0]['connection_type'] == 's3'){
                $this->load->helper('aws');
                $folderid=$_POST['MoveFileID'];
                $res =  getUrl($token['BucketName'],$folderid,$token['key'],$token['secret'],$token['region']);
                $name=explode('/',$_POST['MoveFileID']);
                $fname = end($name);
                $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/".$fname;
               if(file_put_contents($newfname, file_get_contents($res))){
                    $mime_type = mime_content_type($newfname);  
                    if($moveCLoudToken[0]['connection_type']=='OneDrive'){
                        if($result[0]['id'] == $moveCLoudToken[0]['id']){
                            $driveId = explode('!', $_POST['MoveFileID']);
                            $data = $this->onedrive->UPloadFile($accessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fname,$mime_type);
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data);
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data);
                                unlink($newfname);
                            }
                        }else{
                            $this->load->library('onedrive');
                            $tk = json_decode($moveCLoudToken[0]['access_token'],true);
                    	   // $ntk = json_decode($tk['access_token'],true);
                    	    $refresh_token=$tk['refresh_token'];
                    	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                            $tk = json_decode($res,true);
                            $MoveAccessToken = $tk['access_token'];
                            $driveId = explode('!', $_POST['MoveFolderID']);
                            $data = $this->onedrive->UPloadFile($MoveAccessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fname,$mime_type);
                        //  print_r($data);
                        //  die();
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data);
                                $driveId = explode('!', $_POST['MoveFileID']);
                                $ar = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$_POST['MoveFileID']);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data);
                                unlink($newfname);
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
                        $google = $this->uploadFileOnGoogleDrive( $newfname, $fname,$mime_type,$CloudToken,$_POST['MoveFolderID'] );
        	            if($google['fileID']){
                               $driveId = explode('!', $_POST['MoveFileID']);
                               $ar = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                        $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Move File ');
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$google,'smg'=>'Technical Issue Please Refresh The Page. ');
                            unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($CloudToken['refresh_token']);
                        $ntoken = json_decode($access_token,true);
                        $accessToken = $ntoken['access_token'];
                        $moveFileToken = $CloudToken['access_token'];
                        if(!empty($_POST['NewFolderPath'])){
                            $path = $_POST['NewFolderPath'];
                        }else{
                            $path = '';
                        }
                        $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                        $fileName = $fname;
                        $dropbox = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                        if(isset($dropbox['path_display']) && !empty($dropbox['path_display']) && !isset($dropbox['error_summary'])){
                            $token = json_decode($result[0]['access_token'],true);
                            $ar = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                            $res = array('status'=>1,'data'=>$dropbox,'smg'=>'Successfully Move File ');
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$dropbox,'smg'=>$dropbox['error_summary']);
                            unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Ddownload'){
                        $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                        $this->load->library('Ddownload_api');
                        $accessKey = $dDownloadKey['key'];
                        $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                        $uploadFileData = json_decode($ar, true);
                        if($uploadFileData[0]['file_status'] == 'OK'){
                            $file_code = $uploadFileData[0]['file_code'];
                            $Rname = $this->ddownload_api->fileRename($accessKey, $file_code, $fname);
                            $fileR = json_decode($Rname,true);
                            if($fileR['status'] == '200'){
                                $moveFile = $this->ddownload_api->setFileInFolder($accessKey, $file_code, $_POST['MoveFolderID']);
                                $fileM = json_decode($moveFile,true);
                                if($fileM['status'] == '200'){
                                    $ar = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Upload File');
                                    unlink($target_file);
                                }else{
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                                }
                            }else{
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                            }
                        }else{
                            $res = array('status'=> 0,'smg'=>'SSL certificate problem: unable to get local issuer certificate{userId: "151');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Box'){
                        $this->load->library('Box_api');
                        $access_token = $this->checkBoxAccessToken($CloudToken, $result[0]['email'], 'Box', $result[0]['created']);
                        if($access_token){
                            $folderId = $_POST['MoveFolderID'];
                            $fileName = $fname;
                            $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fname);
                           if(isset($ar['entries']) != ''){
                                $ar = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'');
                            }else{
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'');
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 's3'){
                        $this->load->helper('aws');
                        $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
                        if(isset($data['url'])){
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1,'smg'=>'Successfully Move File ');
                             $data = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0,'smg'=>'Please refresh Page and try again ');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Backblaze'){
                        $this->load->helper('back_blaz');
                        $ar = b2_uploadFile( $_POST['MoveFolderID'], $newfname, $fname, $CloudToken['key_id'], $CloudToken['application_key']);
                        $connection = 'Backblaze';
                        if(isset($ar['fileName'])){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                           $dlt = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Gofile'){
                        $this->load->library('Gofile_lib');
                        $ar = $this->gofile_lib->gofile_upload_file( $CloudToken['token'], $_POST['MoveFolderID'], $newfname );
                        $connection = 'Gofile';
                        if( $ar['status'] == 'ok'){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Move File ');
                           $dlt = s3_delete_matching_object($_POST['MoveFileID'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }
                } 
            }else if($result[0]['connection_type'] == 'GooglePhoto'){
              $this->load->library('Googlephoto');
              $clientData = $this->googlephoto->Getbatch($token,$_POST['MoveFileID']);
              $GooglePhotos = json_decode($clientData,true);
              $data =  $GooglePhotos['mediaItemResults'][0]['mediaItem'];
              $mime_type = $data['mimeType'];
              $productUrl = $data['productUrl'];
              $baseUrl = $data['baseUrl'];
              $url = $baseUrl;
              //   print_r(file_get_contents($baseUrl));die();
        	  $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/googlePhotos/".$_POST['movefilename'];
        	  $fname = $_POST['movefilename'];
               if(file_put_contents($newfname, file_get_contents($url))){
                    if($moveCLoudToken[0]['connection_type']=='OneDrive'){
                        if($result[0]['id'] == $moveCLoudToken[0]['id']){
                            $driveId = explode('!', $_POST['MoveFileID']);
                            $data = $this->onedrive->UPloadFile($accessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fname,$mime_type);
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data);
                                unlink($newfname);
                            }
                        }else{
                            $this->load->library('onedrive');
                            $tk = json_decode($moveCLoudToken[0]['access_token'],true);
                    	    $refresh_token=$tk['refresh_token'];
                    	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                            $tk = json_decode($res,true);
                            $MoveAccessToken = $tk['access_token'];
                            $driveId = explode('!', $_POST['MoveFolderID']);
                            $data = $this->onedrive->UPloadFile($MoveAccessToken,$driveId[0],$_POST['MoveFolderID'],$newfname,$fname,$mime_type);
                            if(isset($data['id'])){
                                $res = array('status'=>1, 'data'=>$data,'smg'=>'Successfully Copy File ','type'=>3);
                                $driveId = explode('!', $_POST['MoveFileID']);
                                unlink($newfname);
                            }else{
                                $res = array('status'=>0, 'data'=>$data,'smg'=>'Technical Error Please Refresh Page ');
                                unlink($newfname);
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type']=='GoogleDrive'){
                        $google = $this->uploadFileOnGoogleDrive( $newfname, $fname,$mime_type,$CloudToken,$_POST['MoveFolderID'] );
        	            if($google['fileID']){
                               $driveId = explode('!', $_POST['MoveFileID']);
                        $res = array('status'=>1,'data'=>$google,'smg'=>'Successfully Copy File ','type'=>"3");
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$google,'smg'=>'Technical Issue Please Refresh The Page. ');
                            unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type']=='DropBox'){
                        $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($CloudToken['refresh_token']);
                        $ntoken = json_decode($access_token,true);
                        $moveFileToken = $ntoken['access_token'];
                        if(!empty($_POST['NewFolderPath'])){
                            $path = $_POST['NewFolderPath'];
                        }else{
                            $path = '';
                        }
                        $path = isset($_POST['NewFolderPath'])? '/'.$_POST['NewFolderPath'] : '/'; /////////////////////////////
                        $fileName = $fname;
                        $dropbox = $this->dropbox_libraries->uploadFile($moveFileToken, $path, $newfname , $fileName);
                        if(isset($dropbox['path_display']) && !empty($dropbox['path_display']) && !isset($dropbox['error_summary'])){
                            $res = array('status'=>1,'data'=>$dropbox,'smg'=>'Successfully Copy File ','type'=>"3");
                            unlink($newfname);
                        }else{
                            $res = array('status'=>0,'data'=>$dropbox,'smg'=>$dropbox['error_summary']);
                            unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Ddownload'){
                        $dDownloadKey = json_decode($moveCLoudToken[0]['access_token'],true);
                        $this->load->library('Ddownload_api');
                        $accessKey = $dDownloadKey['key'];
                        $ar = $this->ddownload_api->uploadFile($accessKey, $newfname);
                        $uploadFileData = json_decode($ar, true);
                        if($uploadFileData[0]['file_status'] == 'OK'){
                            $file_code = $uploadFileData[0]['file_code'];
                            $Rname = $this->ddownload_api->fileRename($accessKey, $file_code, $fname);
                            $fileR = json_decode($Rname,true);
                            if($fileR['status'] == '200'){
                                $moveFile = $this->ddownload_api->setFileInFolder($accessKey, $file_code, $_POST['MoveFolderID']);
                                $fileM = json_decode($moveFile,true);
                                if($fileM['status'] == '200'){
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Copy File','type'=>"3");
                                    unlink($target_file);
                                }else{
                                    $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                                }
                            }else{
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Issue....');
                            }
                        }else{
                            $res = array('status'=> 0,'smg'=>'SSL certificate problem: unable to get local issuer certificate{userId: "151');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Box'){
                        $this->load->library('Box_api');
                        $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                        $ntoken = json_decode($access_token,true);
                        $access_token = $ntoken['access_token'];
                        if($access_token){
                            $folderId = $_POST['MoveFolderID'];
                            $fileName = $fname;
                            $ar = $this->box_api->uploadFile($access_token, $folderId, $newfname, $fname);
                           if(isset($ar['entries']) != ''){
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>1,'smg'=>'Successfully Copy File ','type'=>"3");
                            }else{
                                unlink($newfname);
                                $res = array('userId'=>$result[0]['id'],'data'=>$ar,'status'=>0,'smg'=>'Technical Error');
                            }
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 's3'){
                        $this->load->helper('aws');
                        $data = upload_s3($newfname,$fname,$_POST['MoveFolderID'],null,$CloudToken['key'],$CloudToken['secret'],$CloudToken['region'],$CloudToken['BucketName']);
                        if(isset($data['url'])){
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>1,'smg'=>'Successfully Copy FIle ','type'=>"mediaitem");
                            unlink($newfname);
                        }else{
                            unlink($newfname);
                            $res = array('userId'=>$result[0]['id'],'data'=>$data,'status'=>0,'smg'=>'Technical Error ');
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Backblaze'){
                        $this->load->helper('back_blaz');
                        $ar = b2_uploadFile( $_POST['MoveFolderID'], $newfname, $fname, $CloudToken['key_id'], $CloudToken['application_key']);
                        $connection = 'Backblaze';
                        if(isset($ar['fileName'])){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Copy File ','type'=>"3");
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }else if($moveCLoudToken[0]['connection_type'] == 'Gofile'){
                        $this->load->library('Gofile_lib');
                        $ar = $this->gofile_lib->gofile_upload_file( $CloudToken['token'], $_POST['MoveFolderID'], $newfname );
                        $connection = 'Gofile';
                        if( $ar['status'] == 'ok'){
                          $res = array('status'=>1,'data'=>$ar, 'connection'=>$connection,'status'=>1,'smg'=>'Successfully Copy File ','type'=>"3");
                          unlink($newfname);
                        }else{
                          $res = array('status'=>0,'data'=>$ar, 'connection'=>$connection,'status'=>0,'smg'=>'Technical Issue Please Refresh The Page. ');
                          unlink($newfname);
                        }
                    }
                } 
            }
        }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
	private function uploadFileOnGoogleDrive($file, $filename, $type,$accessToken, $folderID = ''){
		$result = array();
		$folderName = 'New Folder ';
		  $this->load->library('Googledrive');
		if(empty($folderID)){
			$rf = $this->googledrive->createFolder($accessToken, $folderName);
			if(isset($rf['status']) && $rf['status']){
				$data = array(
					'folder_id' => $rf['folderid'],
					'datetime' => date('Y-m-d H:i:s')
				);
			}
		}
		 
		$gu = $this->googledrive->uploadFile( $filename, $folderID, $file, $type, $accessToken );
		if($gu['status']){
			$result = array( 
				'fileID' => $gu['filedata']['id'],
				'url' => "https://drive.google.com/uc?export=view&id={$gu['filedata']['id']}&ezex={$gu['ex']}"
			);
		}
		$result['folderID'] = $folderID;
		return $result;
	}
	function userDeleteFolder(){
	  if($_POST['folderID']=="undefined"){
	      $folderID =$_POST['drotyp']; 
	  }else{
	      $folderID =$_POST['folderID'];
	  }

	    $id =$_POST['user_id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $data = $this->googledrive->deleteFiles($folderID, $token);
                if ($data) {
                    $ar = array('status'=>1,'data'=>$data,'smg'=> 'Successfully Delete File');
                } else {
                    $ar = array('status'=>0,'data'=>$data,'smg'=> 'Please refresh the page and try again ');
                }
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $drotyp = $folderID;
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $data = $this->dropbox_libraries->deleteFile($accessToken,$drotyp);
                if ($data) {
                    $ar = array('status'=>1,'data'=>$data,'smg'=> 'Successfully Delete File');
                } else {
                    $ar = array('status'=>0,'data'=>$data,'smg'=> 'Please refresh the page and try again ');
                }
            }
            if($result[0]['connection_type'] == 'Ddownload'){
                // print_r($_POST);die;
                $this->load->library('Dropbox_libraries');
                $drotyp =$_POST['folderID'];
                $accessToken = $token['access_token'];
                $ar = $this->dropbox_libraries->deleteFile($accessToken,$drotyp);
            }
            if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                if($access_token){
                    $ar = $this->box_api->deleteFile($access_token, $_POST['folderID']);
                    // $tt = json_decode($data,true);
                    // if(isset($tt) && ($tt['type'] == 'error')){
                    //   $res = array('status'=>0,'data'=>$data);
                    // }else{
                    //   $res = array('status'=>1,'data'=>$data);
                    // }
                }
            }
            if($result[0]['connection_type'] == 'OneDrive'){
                $id =$_POST['user_id'];
                $folderid =$_POST['folderID'];
                $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	   // $ntk = json_decode($tk['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                $driveId = explode('!', $folderid);
                $data = $this->onedrive->DeleteFiles($accessToken,$driveId[0],$folderid);
               
               if (empty($data)) {
                    $ar = array('status'=>1,'data'=>$data,'smg'=> 'Successfully Delete File');
                } else {
                    $ar = array('status'=>0,'data'=>$data,'smg'=> 'Please refresh the page and try again ');
                }
            }
            if($result[0]['connection_type'] == 's3'){
                // print_r($_POST);
                // die();
               $this->load->helper('aws');
               $data = s3_delete_matching_object($_POST['fileid'],$token['BucketName'],$token['key'],$token['secret'],$token['region']);
                if ($data) {
                    $ar = array('status'=>1,'data'=>$data,'smg'=> 'Successfully Delete File');
                } else {
                    $ar = array('status'=>0,'data'=>$data,'smg'=> 'Please refresh the page and try again ');
                }
            }
            if($result[0]['connection_type'] == 'Backblaze'){
                $this->load->helper('back_blaz');
                $item_id =$_POST['folderID'];
                $item_name = isset($_POST['fileid'])?$_POST['fileid']:0;

                if(strstr($item_id, '/')){  // is folder
                    $type = 'folder';
                }else{                      // is file
                    $type = 'file';
                }
                // $type = isFileFolder($item_name);

                $data = [];
                if ($type == 'folder') {
                    $ret = b2_delete_folder_by_path( $item_id ,$token['key_id'], $token['application_key']  );
                    
                    foreach ($ret as $key => $value) {
                        $data['response'] = b2_delete_file_by_id( $value['id'], $value['name'] ,$token['key_id'], $token['application_key']  );
                    }

                }else if($type == 'file'){
                    $data['response'] = b2_delete_file_by_id( $item_id, $item_name ,$token['key_id'], $token['application_key']  );
                    // print_r($type);die;
                }

                $data['connType'] = 'Backblaze';
                if( isset($data['response']['fileId']) ){
                    $ar = array('status'=>1,'data'=>$data);
                }else{
                    $ar = array('status'=>0,'data'=>$data);
                }
            }
            if($result[0]['connection_type'] == 'Gofile'){
                $this->load->library('Gofile_lib');
                $folderID =$_POST['folderID'];
                $del_status = $this->gofile_lib->getfile_delete_id($token['token'], $folderID);
                $data['connType'] = 'Gofile';
                if( $del_status['status'] == 'ok' ){
                    $ar = array('status'=>1,'data'=>$data);
                }else{
                    $ar = array('status'=>0,'data'=>$data);
                }
            }
            if($result[0]['connection_type'] == 'Pcloud'){
                require_once(APPPATH.'libraries/pcloud/autoload.php');
                if ( empty($this->input->post('folderID')) ) { $type = 'file'; }else{ $type = 'folder'; }
                
                try {
                    if ($type == 'file') {
                        $pCloudFile = new pCloud\File();
                        $pCloudFile->delete( (int)$this->input->post('fileid'), $token['token'] );

                    }else if ($type == 'folder') {
                        $pCloudFolder = new pCloud\Folder();
                        $pCloudFolder->deleteRecursive( (int)$this->input->post('folderID'), $token['token']);

                    }
                    $del_status['status']   = 1;
                } catch (Exception $e) {
                    $del_status['status']   = 0;
                }
                
                $data['connType'] = 'Pcloud';
                if( $del_status['status'] == 1 ){
                    $ar = array('status'=>1,'data'=>$data);
                }else{
                    $ar = array('status'=>0,'data'=>$data);
                }
            }
            if($result[0]['connection_type'] == 'Ftp'){
                $this->load->helper('common');
                $type           = ($_POST['folderID'] != '')?'folderID':'fileid';
                $path           = $this->input->post($type);
                $ftp            = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp, $token['username'], $token['password']);
                $data['connType'] = 'Ftp';
                if ($type == 'folderID') {
                    if (ftp_rdel($ftp, $path)) {
                        $ar = array('status'=>1,'data'=>$data);
                    } else {
                        $ar = array('status'=>0,'data'=>$data);
                    }
                }
                if ($type == 'fileid') {
                    if (ftp_delete($ftp, $path)) {
                        $ar = array('status'=>1,'data'=>$data,'smg'=> 'Successfully Delete File');
                    } else {
                        $ar = array('status'=>0,'data'=>$data,'smg'=> 'Please refresh the page and try again ');
                    }
                }
                ftp_close($ftp);
            }
        }
        echo json_encode($ar,JSON_UNESCAPED_SLASHES); 
	    die();
	}
	function OneDriveAuth(){
	    // $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	    // print_r($actual_link);
	    // die();
	     $this->load->library('onedrive');
         $res = $this->onedrive->authResponse($_GET['code']);
         if($res){
             $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'email'=>$res['data']['email'],'connection_type'=>'OneDrive'));
            if(!empty($checkValue)){
               $data = array(
                    'email' => $res['userInfo']['email'],
                    'access_token' => json_encode( $res['token'] ),
                    'user_info' => json_encode( $res['userInfo'] ),
                    'status' => 1
                );
               $result =$this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$res['userInfo']['email'],'connection_type'=>'OneDrive'));
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                }else{
                     redirect( 'home/connection', 'refresh' );
                }
            }else{
               $result = $this->DBfile->put_data('connection',$res['data']);
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                }else{
                     redirect( 'home/connection', 'refresh' );
                }
            }
         }
	}
	  function get_info_google_photo(){
        if (isset($_GET['code'])) {
            $this->load->library('Googlephoto');
            $client = $this->googlephoto->getClientForAuth();
            $client->authenticate($_GET['code']);	
            $token = $client->getAccessToken();
            $userInfo['usedSpace'] = 0;
            $userInfo['totalSpace'] = 5; 
            $userInfo['name'] = $_COOKIE['Con_desplay_name']; 
            $userInfo['email'] = $_COOKIE['Con_desplay_email']; 
            $data = array(
                'user_id' => $_SESSION['id'],
                'access_token' => json_encode( $token ),
                'user_info' => json_encode( $userInfo ),
                'connection_type' => 'GooglePhoto',
                'email' => $_COOKIE['Con_desplay_name'],
                'status' => 1
            );
            $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'email'=>$_COOKIE['Con_desplay_name'],'connection_type'=>'GooglePhoto'));
           if(!empty($checkValue)){
               $data = array(
                    'email' =>$_COOKIE['Con_desplay_name'],
                    'access_token' => json_encode( $token ),
                    'user_info' => json_encode( $userInfo ),
                    'status' => 1
                );
               $result =$this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$_COOKIE['Con_desplay_name'],'connection_type'=>'GooglePhoto'));
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                      unset($_COOKIE['Con_desplay_name']);
                }else{
                     redirect( 'home/connection', 'refresh' );
                     unset($_COOKIE['Con_desplay_name']);
                }
           }else{
               $result = $this->DBfile->put_data('connection',$data);
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                     unset($_COOKIE['Con_desplay_name']);
                }else{
                     redirect( 'home/connection', 'refresh' );
                     unset($_COOKIE['Con_desplay_name']);
                }
           }
        }else if(isset($_GET['error'])=="access_denied"){
             redirect( 'home/connection', 'refresh' );
        }else{
             redirect( 'home/dashboard', 'refresh' );
        }
    }
    function get_info_google_drive(){
        $this->load->library('Googledrive');
        if (isset($_GET['code'])) {
            $client = $this->googledrive->getClientForAuth();
            $client->authenticate($_GET['code']);	
            $token = $client->getAccessToken();
            $userInfo = $this->googledrive->userInformation($token);
            $totalSpace = $userInfo['totalStorage'];
            $usedSpace = $userInfo['useStorage'];
            $userInfo['usedSpace'] = $usedSpace;
            $userInfo['totalSpace'] = $totalSpace;
            $data = array(
                'user_id' => $_SESSION['id'],
                'access_token' => json_encode( $token ),
                'user_info' => json_encode( $userInfo ),
                'connection_type' => 'GoogleDrive',
                'email' => $userInfo['email'],
                'status' => 1
            );
            $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'email'=>$userInfo['email'],'connection_type'=>'GoogleDrive'));
           if(!empty($checkValue)){
               $data = array(
                    'email' => $userInfo['email'],
                    'access_token' => json_encode( $token ),
                    'user_info' => json_encode( $userInfo ),
                    'status' => 1
                );
               $result =$this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$userInfo['email'],'connection_type'=>'GoogleDrive'));
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                }else{
                     redirect( 'home/connection', 'refresh' );
                }
           }else{
               $result = $this->DBfile->put_data('connection',$data);
                if($result==true){
                     redirect( 'home/dashboard', 'refresh' );
                }else{
                     redirect( 'home/connection', 'refresh' );
                }
           }
        }else if(isset($_GET['error'])=="access_denied"){
             redirect( 'home/connection', 'refresh' );
        }else{
             redirect( 'home/dashboard', 'refresh' );
        }
    }
    function GetAllConnection(){
    	$data = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'status'=>1));
    	$res = array('userId'=>$this->session->userdata('id'),'data'=>$data,'status'=>1);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    function moveFileDataget(){
        // echo"<pre>";print_r($_POST);die;
        $id=$_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $data['getAllFolder'] = $this->googledrive->getAllFolders($token,null);
                $data['connectionType']='Google Drive';
                $data['email']=$result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                $accessToken = $ntoken['access_token'];
                $data['getAllFolder'] = $this->dropbox_libraries->getAllFolders($accessToken);
                $data['connectionType']='DropBox';
                $data['email']=$result[0]['email'] ;
                // echo"<pre>";print_r($data['getAllFolder']);die;
            }else if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $data['getAllFolder'] = $this->ddownload_api->getAllFolders($accessKey, 0);
                $data['connectionType'] = 'Ddownload';
                $data['email'] = $result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'OneDrive'){
                $this->load->library('onedrive');
                $tk = json_decode($result[0]['access_token'],true);
        	   // $ntk = json_decode($tk['access_token'],true);
        	    $refresh_token=$tk['refresh_token'];
        	    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                $tk = json_decode($res,true);
                $accessToken = $tk['access_token'];
                $data['getAllFolder'] = $this->onedrive->getFiles($accessToken,null);
                $data['connectionType']='OneDrive'; 
                $data['email']=$result[0]['email'];
            }else if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                // $ntoken = json_decode($access_token,true);
                // $access_token = $ntoken['access_token'];
                if($access_token){
    			    $folderId = 0;
                    $data['getAllFolder'] = $this->box_api->getAllFolders($access_token, $folderId);
                    $data['connectionType'] = 'Box';
                    $data['email'] = $result[0]['email'] ;
                }
            }else if($result[0]['connection_type'] == 's3'){
                $this->load->helper('aws');
        	    $folderid ='';
                $data['getAllFolder']['data'] = s3_get_object($folderid,$token['BucketName'], $token['key'],$token['secret'],$token['region']);
                $data['connectionType']='s3';
                $data['email']=$result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'Backblaze'){
                $this->load->helper('back_blaz');
                $data_ = b2_get_folder('', $token['key_id'], $token['application_key'] );
                // echo"<pre>";print_r($data);die;
                $data['connectionType'] = 'Backblaze';
                $data['email'] = $result[0]['email'] ;
                foreach($data_ as $key => $value){
                    $name = basename( $value['fileName'] );
                    $data_[$key]['name'] = $name;
                    if( $data_[$key]['fileName'] == '.bzEmpty' ){ unset($data_[$key]); }
                }
                $data['getAllFolder']['data'] = $data_;
            }else if($result[0]['connection_type'] == 'Gofile'){
                $this->load->library('Gofile_lib');
                $connection = $this->gofile_lib->gofile_get_folder( $token['token'] , $token['root_id']);
                if ($connection['status'] == 'ok' ) {
                    $data['getAllFolder']['data'] = $connection['data']['contents'];
                    $data['email'] = $result[0]['email'] ;
                }else{
                    $data['getAllFolder'] = [];
                    $data['email'] = 'Hmm ! Seems you have no Premium Account or Your Auth Token is changed' ;
                }
                $data['connectionType'] = 'Gofile';
            }else if($result[0]['connection_type'] == 'GooglePhoto'){
                  $data['getAllFolder']['data'] = array(
                                            array('name'=>'Albums','id'=>1),
                                            array('name'=>'Sharing','id'=>2),
                                            array('name'=>'Photo','id'=>3),
                                        );
                $data['connectionType']='GooglePhoto';
                $data['email']=$result[0]['email'] ;
            }else if($result[0]['connection_type'] == 'Pcloud'){

                $this->load->library('Pcloud_lib');
                $res = $this->pcloud_lib->getFolderById(0, $token['token']);
                if ($res['status'] == 1) { 
                    $data['getAllFolder']['data'] = $res['folder_data'];
                    $data['email']                = $result[0]['email'] ;
                    $data['connectionType'] = 'Pcloud';
                }
                
            }else if($result[0]['connection_type'] == 'Ftp'){

                $ftp            = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp, $token['username'], $token['password']);
                
                if ($login_result == 1) { 
                    ftp_pasv($ftp, true);
                    $res = ftp_mlsd($ftp, ".");
                    // print_r($res);die();
                    foreach ($res as $key => $value) {
                        if ( $value['type'] == 'cdir' || $value['type'] == 'pdir') {
                            unset($res[$key]);
                        }
                    }
                    $data['getAllFolder']['data'] = $res;
                    $data['email']                = $result[0]['email'] ;
                }else{
                    $data['getAllFolder']['data'] = [];
                    $data['email']                = 'Invalid Details ! Your Connection details might be incorrect' ;
                }
                $data['connectionType'] = 'Ftp';
                ftp_close($ftp);

            }
        }
        $res = array('userId'=>$result[0]['id'],'data'=>$data);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    function RenameFile(){
        //  if($_SESSION['id']==1){
	        $this->checkUserLoginStatus();
	   // }
        $id = $_POST['id'];
        $result =$this->DBfile->get_data( 'connection', array('id'=>$id));
        $token = json_decode($result[0]['access_token'],true);
        if(isset($result)){
            if($result[0]['connection_type'] == 'GoogleDrive'){
                $this->load->library('Googledrive');
                $fileNAME= $_POST['foldername'];
                $ext = pathinfo($fileNAME, PATHINFO_EXTENSION);
                $data = $this->googledrive->renameFile($token,$_POST['fileID'],$_POST['fileName'],$ext);
               if(isset($data['id']) && !empty($data['id'])){
                   $res = array('status'=>1,'data'=>$data);
               }else{
                   $res = array('status'=>0,'data'=>$data);
               }
            }
            if($result[0]['connection_type'] == 'DropBox'){
                $this->load->library('Dropbox_libraries');
                
                $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken = json_decode($access_token,true);
                
                $accessToken = $ntoken['access_token'];
              
                $folderPath = $_POST['folderPath'];
                $path = $_POST['path'];
                $fileNewName = $_POST['fileName'];
                $fileName = basename($path);
                $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                $to_path = $folderPath.'/'.$fileNewName.'.'.$ext;
                $return = $this->dropbox_libraries->fileRename($accessToken, $path, $to_path);
                
               
                $data = $return;
                if(isset($data) && !empty($data)){
                   $res = array('status'=>1,'data'=>$data);
                }else{
                   $res = array('status'=>0,'data'=>$data);
                }
                // echo"<pre>";print_r($data['getAllFolder']);die;
            }
            if($result[0]['connection_type'] == 'Ddownload'){
                $this->load->library('Ddownload_api');
                $accessKey = $token['key'];
                $ext = pathinfo( $_POST['foldername'], PATHINFO_EXTENSION);
                $file_code = $_POST['fileCode'];
                $fileName = $_POST['fileName'].'.'.$ext;
                $data = $this->ddownload_api->fileRename($accessKey, $file_code, $fileName);
                $tt = json_decode($data,true);
                // echo"<pre>";print_r($tt);die;
                // $data = $ar;
                if(isset($data) && !empty($data)){
                   $res = array('status'=>1,'data'=>$data);
                }else{
                   $res = array('status'=>0,'data'=>$data);
                }
            }
            if($result[0]['connection_type'] == 'Box'){
                $this->load->library('Box_api');
                $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                // $access_token = $this->box_api->getAccesTokenFromRefreshToken($token['refresh_token']);
                // $ntoken = json_decode($access_token,true);
                // $access_token = $ntoken['access_token'];
                if($access_token){
                    if($_POST['typefile'] == 'file'){
                        $ext = pathinfo($_POST['foldername'], PATHINFO_EXTENSION);
                        $fileName = $_POST['fileName'] .'.'. $ext;
                        $data = $this->box_api->fileRename($access_token, $fileName, $_POST['fileID']);
                    }else{
                        $fileName = $_POST['fileName'];
                        $data = $this->box_api->folderRename($access_token, $fileName, $_POST['fileID']);
                    }  
                    $tt = json_decode($data,true);
                    // echo"<pre>";print_r($tt);die;
                    // $data = $ar;
                    if(isset($tt) && ($tt['type'] == 'error')){
                       $res = array('status'=>0,'data'=>$data);
                    }else{
                       $res = array('status'=>1,'data'=>$data);
                    }
                }
            }
            if($result[0]['connection_type'] == 'Ftp'){
                $ftp            = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp, $token['username'], $token['password']);
                $old_file   = $_POST['fileID'];
                $path_parts = pathinfo( $old_file );
                $new_name_validate = pathinfo($this->input->post('fileName'));
                $dir = $path_parts['dirname'];
                $ext = isset($path_parts['extension'])?'.'.$path_parts['extension']:'';
                $new_file = $dir.'/'.$new_name_validate['filename'].$ext;
                if (ftp_rename($ftp, $old_file, $new_file)) {
                    $res = array('status'=>1,'data'=>'Success');
                } else {
                    $res = array('status'=>0,'data'=>'Error');
                }
            }
            if($result[0]['connection_type'] == 'Pcloud'){
                require_once(APPPATH.'libraries/pcloud/autoload.php');
                $pCloudFile     = new pCloud\File();
                $pCloudFolder   = new pCloud\Folder();
                try {
                    if (isset($_POST['fileCode']) && !empty($_POST['fileCode']) ) {
                        $pCloudFolder->rename( (int)$_POST['fileCode'], $_POST['fileName'], $token['token']);
                    }else{
                        $a = $pCloudFile->getInfo( (int)$_POST['fileID'], $token['token']);
                        $ext_ar = explode('.', $a->metadata->name);
                        $pCloudFile->rename( (int)$_POST['fileID'], $_POST['fileName'].'.'.end($ext_ar), $token['token']);
                    }
                    $res    = array('status'=>1, 'data'=> ['status' => 'success'], 'message'=> '');
                } 
                catch (Exception $e) {
                    $msg    = $e->getMessage();
                    $res    = array('status'=>0, 'data'=> ['status' => 'error'], 'message'=> $msg);
                }
            }
            // if($_SESSION['id']==1){
                if($res['data']['status'] == 1){
        		    $this->DBfile->custom_query("UPDATE `usertbl` SET `task_count` = task_count + 1 WHERE `id` = ".$_SESSION['id']);
        		    $_SESSION['task_count'] = $_SESSION['task_count']+1;
                }
    	   // }
        }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    // for goFile auth api start
    function goFileAuth(){
        $this->load->library('Gofile_lib');
        $this->load->library('form_validation');
        if($this->input->is_ajax_request()){
                $this->form_validation->set_rules('gofile_tokenid', 'Gofile Token', 'required|trim');
				if ($this->form_validation->run() == FALSE){
					$error 	=	validation_errors();
					$res = array('status'=>0, 'smg'=> $error );
                    die();
				}else{
                    $gofile_token = $this->input->post('gofile_tokenid');
                    $connection = $this->gofile_lib->gofile_user_info( $gofile_token );
			        if ( $connection['status'] == 'ok' ) {
                        if ($connection['data']['tier'] != 'standard') {
                            $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'connection_type'=>'GoFile'));
                            $userInfo = array(
                                    'name'          => $connection['data']['email'],
                                    'id'            => $connection['data']['token'],
                                    'image'         => '',
                                    'totalStorage'  => 'NA',
                                    'useStorage'    => 'NA'
                            );
                            $tokencreate =array(
                                    'token'     => $connection['data']['token'],
                                    'tier'      => $connection['data']['tier'],
                                    'root_id'   => $connection['data']['rootFolder'],
                            );
                            if(!empty($checkValue)){
                                $matched = 0;
                                foreach ($checkValue as $key => $value) {
                                    $result = json_decode($checkValue[$key]['access_token'],true);
                                    if( $result['token'] == $connection['data']['token'] ){
                                        $matched += 1;
                                    }
                                }
                                if( $matched == 0 ){
                                    $data =array(
                                        'user_id'           => $_SESSION['id'],
                                        'email'             => $connection['data']['email'],
                                        'connection_type'   => 'Gofile',
                                        'access_token'      => json_encode($tokencreate),
                                        'refresh_token'     => '',
                                        'user_info'         => json_encode($userInfo),
                                    );
                                $result = $this->DBfile->put_data('connection',$data);
                                    if($result){
                                        $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Added  Successfully.');
                                    }else{
                                        $res = array('status'=>0);
                                    }
                                }else{
                                    $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Already Exist');
                                }
                            } else {
                                    $data =array(
                                        'user_id'           => $_SESSION['id'],
                                        'email'             => $connection['data']['email'],
                                        'connection_type'   => 'Gofile',
                                        'access_token'      => json_encode($tokencreate),
                                        'refresh_token'     => '',
                                        'user_info'         => json_encode($userInfo),
                                    );
                                $result = $this->DBfile->put_data('connection',$data);
                                if($result){
                                    $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Add  Successfully.');
                                }else{
                                    $res = array('status'=>0);
                                }
                            }
                        }else{
                            $res = array('status'=>0, 'smg'=> 'Gofile Needs Premium Account');
                        }
			        }else{
                        $res = array('status'=>0, 'smg'=> 'Please Provide Valid Configs');
                    }
				}
				echo json_encode($res, JSON_UNESCAPED_SLASHES); 
	            die();
        }
    }
    // for goFile auth api end
    // for b2 backblaze api start
    function backBlazeB2Auth(){
        $this->load->helper('back_blaz');
        $this->load->library('form_validation');
        if($this->input->is_ajax_request()){
                $this->form_validation->set_rules('b2_userName', 'User Name', 'required|trim');
				$this->form_validation->set_rules('b2_keyId', 'Key Id', 'required|trim');
				$this->form_validation->set_rules('b2_applicationKey', 'Application Key', 'required|trim');
				if ($this->form_validation->run() == FALSE){
					$error 	=	validation_errors();
					$res = array('status'=>0, 'smg'=> $error );
				}else{
                    $user_name = $this->input->post('b2_userName');
                    $keyId     = $this->input->post('b2_keyId');
                    $AppKey    = $this->input->post('b2_applicationKey');
                    $connection = checkB2Connection( $keyId , $AppKey );
			        if ( $connection['status'] == 1 ) {
			            $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'connection_type'=>'Backblaze'));
			            $userInfo =array(
                                'name'=>$user_name,
                                'id'=>$connection['data']['account_id'],
                                'image'=>'',
                                'totalStorage'=>'NA',
                                'useStorage'=>'NA'
                        );
                        $tokencreate =array(
                                'account_id'=>$connection['data']['account_id'],
                                'bucket_name'=>$connection['data']['bucket_name'],
                                'key_id'=> $keyId,
                                'application_key'=> $AppKey,
                        );
                        if(!empty($checkValue)){
                            $matched = 0;
                            foreach ($checkValue as $key => $value) {
                                $result = json_decode($checkValue[$key]['access_token'],true);
                                if( $result['key_id'] == $keyId && $result['application_key'] == $AppKey ){
                                    $matched += 1;
                                }
                            }
                            if( $matched == 0 ){
                                $data =array(
                                    'user_id'=>$_SESSION['id'],
                                    'email'=>$connection['data']['bucket_name'],
                                    'connection_type'=>'Backblaze',
                                    'access_token'=>json_encode($tokencreate),
                                    'refresh_token'=>'',
                                    'user_info'=>json_encode($userInfo),
                                );
                               $result = $this->DBfile->put_data('connection',$data);
                                if($result){
                                    $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Added  Successfully.');
                                }else{
                                    $res = array('status'=>0);
                                }
                            }else{
                                $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Already Exist');
                            }
                        }else{
                            $data =array(
                                'user_id'=>$_SESSION['id'],
                                'email'=>$connection['data']['bucket_name'],
                                'connection_type'=>'Backblaze',
                                'access_token'=>json_encode($tokencreate),
                                'refresh_token'=>'',
                                'user_info'=>json_encode($userInfo),
                            );
                            $result = $this->DBfile->put_data('connection',$data);
                            if($result){
                                $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Add  Successfully.');
                            }else{
                                $res = array('status'=>0);
                            }
                        }
			        }else{
                        $res = array('status'=>0, 'smg'=> 'Please Provide Valid Configs');
                    }
				}
				echo json_encode($res, JSON_UNESCAPED_SLASHES); 
	            die();
        }
    }
    // for b2 backblaze api end
    // for Ftp api start
    function ftpAuth(){
        $this->load->library('form_validation');
        if($this->input->is_ajax_request()){
                $this->form_validation->set_rules('ftp_host', 'Host Name', 'required|trim');
				$this->form_validation->set_rules('ftp_user', 'User Name', 'required|trim');
				$this->form_validation->set_rules('ftp_password', 'User Password', 'required|trim');
				$this->form_validation->set_rules('ftp_port', 'FTP Port', 'required|trim');
				if ($this->form_validation->run() == FALSE){
					$error 	=	validation_errors();
					$res    =   array('status'=>0, 'smg'=> $error );
				}else{
                    $ftp_host       = $this->input->post('ftp_host');
                    $ftp_user       = $this->input->post('ftp_user');
                    $ftp_pass       = $this->input->post('ftp_password');
                    $ftp_port       = $this->input->post('ftp_port');
                    $this->load->library('ftp');
                    $config['hostname'] = $ftp_host;
                    $config['username'] = $ftp_user;
                    $config['password'] = $ftp_pass;
                    $config['port']     = $ftp_port;
                    $config['debug']    = FALSE;
                    $res = $this->ftp->connect($config);
				    // print_r($res);die;
			        if ( $res == 1 ) {
			            $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'connection_type'=>'Ftp'));
			            $userInfo = array(
                                'name'          =>  $ftp_user,
                                'id'            =>  $ftp_host,
                                'image'         =>  '',
                                'totalStorage'  =>  'NA',
                                'useStorage'    =>  'NA'
                        );
                        $tokencreate = array(
                                'hostname'      => $ftp_host,
                                'username'      => $ftp_user,
                                'password'      => $ftp_pass,
                                'port'          => $ftp_port,
                        );
                        if(!empty($checkValue)){
                            $matched = 0;
                            foreach ($checkValue as $key => $value) {
                                $result = json_decode($checkValue[$key]['access_token'],true);
                                if( $result['hostname'] == $ftp_host && $result['username'] == $ftp_user && $result['password'] == $ftp_pass && $result['port'] == $ftp_port ){
                                    $matched += 1;
                                }
                            }
                            if( $matched == 0 ){
                                $data =array(
                                    'user_id'           => $_SESSION['id'],
                                    'email'             => $ftp_host,
                                    'connection_type'   => 'Ftp',
                                    'access_token'      => json_encode($tokencreate),
                                    'refresh_token'     => '',
                                    'user_info'         => json_encode($userInfo),
                                );
                               $result = $this->DBfile->put_data('connection',$data);
                                if($result){
                                    $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Added  Successfully.');
                                }else{
                                    $res = array('status'=>0);
                                }
                            }else{
                                $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Already Exist');
                            }
                        }else{
                            $data =array(
                                'user_id'           => $_SESSION['id'],
                                'email'             => $ftp_host,
                                'connection_type'   => 'Ftp',
                                'access_token'      => json_encode($tokencreate),
                                'refresh_token'     => '',
                                'user_info'         => json_encode($userInfo),
                            );
                            $result = $this->DBfile->put_data('connection',$data);
                            if($result){
                                $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Added  Successfully.');
                            }else{
                                $res = array('status'=>0);
                            }
                        }
			        }else{
                        $res = array('status'=>0, 'smg'=> 'Please Provide Valid Configs');
                    }
				}
				echo json_encode($res, JSON_UNESCAPED_SLASHES); 
	            die();
        }
    }
    // for ftp api end

        // for pcloud start
        function pcloudGenerateCode($appKey){

            require_once(APPPATH.'libraries/pcloud/autoload.php');
    
            try {
                $appInfo = ["appKey" => $appKey];
                $codeUrl = pCloud\App::getAuthorizeCodeUrl( json_decode(json_encode($appInfo), FALSE) );
    
                // $link = "Visit <a target=\"_blank\" href=\"{$codeUrl}\">{$codeUrl}</a>";
                $link   = $codeUrl;
                $res    = array('status'=>1, 'message'=> 'Success', 'link'=>$link);
            } 
            catch (Exception $e) {
                $msg    = $e->getMessage();
                $res    = array('status'=>0, 'message'=> $msg, 'link'=>'');
            }
            echo json_encode($res,JSON_UNESCAPED_SLASHES);
        }
    
        function pcloudAuth(){
            $this->load->library('form_validation');
            if($this->input->is_ajax_request()){
                    $this->form_validation->set_rules('pcloud_appkey', 'App Key', 'required|trim');
                    $this->form_validation->set_rules('pcloud_appsecret', 'App Secret', 'required|trim');
                    $this->form_validation->set_rules('pcloud_appcode', 'App Code', 'required|trim');
                    if ($this->form_validation->run() == FALSE){
                        $error 	=	validation_errors();
                        $res    =   array('status'=>0, 'smg'=> $error );
                    }else{
                        $appKey         = $this->input->post('pcloud_appkey');
                        $appSecret      = $this->input->post('pcloud_appsecret');
                        $appCode        = $this->input->post('pcloud_appcode');
    
                        $this->load->library('Pcloud_lib');
    
                        $token_status = $this->pcloud_lib->getToken( $appKey, $appSecret, $appCode );
                        
                        if ( $token_status['status'] == 1 ) {
                            $user_details = $this->pcloud_lib->pcloud_userdetails( $token_status['access_token'] );
                            
                            if ($user_details['status'] == 1) {
                                
                                $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'connection_type'=>'Pcloud'));
                                $userInfo = array(
                                        'name'          =>  $user_details['data']['email'],
                                        'id'            =>  $user_details['data']['userid'],
                                        'image'         =>  '',
                                        'totalStorage'  =>  'NA',
                                        'useStorage'    =>  'NA'
                                );
                                $tokencreate = array(
                                        'token'      => $token_status['access_token'],
                                        'appKey'     => $appKey,
                                        'appSecret'  => $appSecret,
                                        'appCode'    => $appCode,
                                );
                                if(!empty($checkValue)){
                                    $matched = 0;
                                    foreach ($checkValue as $key => $value) {
                                        $result = json_decode($checkValue[$key]['access_token'],true);
                                        if( $result['appKey'] == $appKey && $result['appSecret'] == $appSecret ){
                                            if ($result['appCode'] ==  $appCode) {
                                                $matched += 1;
                                            }else{
                                                $this->DBfile->set_data( 'connection', ['access_token' => json_encode($tokencreate)] , ['user_id'=>$this->session->userdata('id'),'connection_type' => 'Pcloud', 'id' => $value['id'] ]);
                                                $res = array('status'=>1, 'smg'=> 'Your App Code is updated');
                                                echo json_encode($res, JSON_UNESCAPED_SLASHES); 
                                                die();
                                            }
                                        }
                                    }
                                    if( $matched == 0 ){
                                        $data =array(
                                            'user_id'           => $_SESSION['id'],
                                            'email'             => $user_details['data']['email'],
                                            'connection_type'   => 'Pcloud',
                                            'access_token'      => json_encode($tokencreate),
                                            'refresh_token'     => '',
                                            'user_info'         => json_encode($userInfo),
                                        );
                                       $result = $this->DBfile->put_data('connection',$data);
                                        if($result){
                                            $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Added  Successfully.');
                                        }else{
                                            $res = array('status'=>0);
                                        }
                                    }else{
                                        $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Already Exist');
                                    }
                                }else{
                                    $data =array(
                                            'user_id'           => $_SESSION['id'],
                                            'email'             => $user_details['data']['email'],
                                            'connection_type'   => 'Pcloud',
                                            'access_token'      => json_encode($tokencreate),
                                            'refresh_token'     => '',
                                            'user_info'         => json_encode($userInfo),
                                    );
                                    $result = $this->DBfile->put_data('connection',$data);
                                    if($result){
                                        $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Added  Successfully.');
                                    }else{
                                        $res = array('status'=>0);
                                    }
                                }
                            }
    
                        }else{
                            $res = array('status'=>0, 'smg'=> 'Please Provide Valid Configs or Your AppCode is Expired');
                        }
                    }
                    
                    echo json_encode($res, JSON_UNESCAPED_SLASHES); 
                    die();
            }
        }
    
    // for pcloud end

    function AmazonS3Auth(){
        $this->load->helper('aws');
        $checkS3Connection = checkconnection($_POST['BucketName'],$_POST['accesskeyId'],$_POST['secretKey'],$_POST['region']);
        if($checkS3Connection['status']==0){
             $res = array('status'=>0,'smg'=> 'Please Correct Access Key & Secret Key & Bucket Name & Region');
        }else{
          $data =  $this->getS3usernameID($checkS3Connection['msg']);
            if(!empty($data)){
                $checkValue = $this->DBfile->get_data('connection',array('user_id'=>$this->session->userdata('id'),'connection_type'=>'s3'));
                $userInfo =array(
                        'name'=>$data['name'],
                        'id'=>$data['id'],
                        'image'=>'',
                        'email'=>$_POST['BucketName'],
                        'totalStorage'=>0,
                        'useStorage'=>0
                    );
                $tokencreate =array(
                        'region'=>$_POST['region'],
                        'key'=>$_POST['accesskeyId'],
                        'secret'=>$_POST['secretKey'],
                        'BucketName'=>$_POST['BucketName'],
                    );
                if(!empty($checkValue)){
                    $result = json_decode($checkValue[0]['access_token'],true);
                    if(isset($result['key'])==$_POST['accesskeyId'] && isset($result['secret'])==$_POST['secretKey']){
                        $data =array(
                            'email'=>$_POST['BucketName'],
                            'access_token'=>json_encode($tokencreate),
                            'user_info'=>json_encode($userInfo),
                        );
                       $result =$this->DBfile->set_data( 'connection', $data , array('user_id'=>$this->session->userdata('id'),'email'=>$_POST['BucketName'],'connection_type'=>'s3'));
                       if($result){
                            $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Updata Successfully.' );
                        }else{
                            $res = array('status'=>0);
                        }
                    }else{
                        $data =array(
                            'user_id'=>$_SESSION['id'],
                            'email'=>$_POST['BucketName'],
                            'connection_type'=>'s3',
                            'access_token'=>json_encode($tokencreate),
                            'refresh_token'=>'',
                            'user_info'=>json_encode($userInfo),
                        );
                       $result = $this->DBfile->put_data('connection',$data);
                        if($result){
                            $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Add  Successfully.');
                        }else{
                            $res = array('status'=>0);
                        }
                    }
                }else{
                    $data =array(
                        'user_id'=>$_SESSION['id'],
                        'email'=>$_POST['BucketName'],
                        'connection_type'=>'s3',
                        'access_token'=>json_encode($tokencreate),
                        'refresh_token'=>'',
                        'user_info'=>json_encode($userInfo),
                    );
                   $result = $this->DBfile->put_data('connection',$data);
                    if($result){
                        $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Add  Successfully.');
                    }else{
                        $res = array('status'=>0);
                    }
                }
            }else{
                $res = array('status'=>0,'smg'=> 'Please Correct Access Key & Secret Key & Bucket Name & Region'); 
            } 
        }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    function getS3usernameID($checkS3Connection){
        foreach($checkS3Connection as $value){
            for ($x = 1; $x <= 1; $x++) {
                $data = array(
                        'name'=>$value['DisplayName'],
                        'id'=>$value['ID']
                        );
                return $data;
            }
           die;
        }
    } 
    function anonfiles(){
         $userIfo =array(
                'name'=>$_POST['name'],
                'email'=>$_POST['email'],
                'Apikey'=>$_POST['apikey']
            );
         $data =array(
                'user_id'=>$_SESSION['id'],
                'email'=>$_POST['email'],
                'access_token'=>json_encode($userIfo),
                'refresh_token'=>'',
                'connection_type'=>'Anon',
                'user_info'=>json_encode($userIfo),
            );
        $result = $this->DBfile->put_data('connection',$data);
        if($result){
                $res = array('status'=>1,'data'=>$result,'smg'=>'Connection Add  Successfully.');
            }else{
                $res = array('status'=>0);
        }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    } 
    function GooglePhotoCon(){
        $cookie_name = "Con_desplay_name";
        $Con_desplay_email = "Con_desplay_email";
        $cookie_valuen =$_POST['displayName'];
        $cookie_valuee =$_POST['Email'];
        setcookie($cookie_name, $cookie_valuen, time() + (86400 * 30), "/");
        setcookie($Con_desplay_email, $cookie_valuee, time() + (86400 * 30), "/");
        $this->load->library('Googlephoto');
        $data = $this->googlephoto->googlePhotoAuth();
        $res = array('status'=>1,'AuthURL'=>$data);
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    // function cloudtransferDataTaskAdd(){
    //     // print_r($_POST);die;
    //     $type = isset($_POST['connectionTyep']) ? $_POST['connectionTyep'] : $_POST['type'];
    //     if($type =="from"){
    //         $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['UserId']));
    //         // print_r($checkValue);
    //         // die();
    //         $fileName = [];
    //         $FileId = [];
    //         if(isset($_POST['FileName']) && !empty($_POST['FileName'])){
    //             foreach($_POST['FileName']  as $value){
    //                 array_push($fileName,$value);
    //             }
    //         }
    //         if(isset($checkValue[0]['connection_type'])=="DropBox"){
    //             if(isset($_POST['droType']) && !empty($_POST['droType'])){
    //                 foreach($_POST['droType'] as $value){
    //                     array_push($FileId,$value);
    //                 }
    //             }
    //         }else{
    //             if(isset($_POST['FileId']) && !empty($_POST['FileId'])){
    //                 foreach($_POST['FileId'] as $value){
    //                     array_push($FileId,$value);
    //                 }
    //             }
    //         }
    //             $data =array(
    //                 'connection_type'=>$checkValue[0]['connection_type'],
    //                 'fileName'=>json_encode($fileName),
    //                 'fileID'=>json_encode($FileId),
    //                 'ConnectionID'=>$_POST['UserId'],
    //                 'type'=>$_POST['type'],
    //                 'droType'=>$_POST['droType'],
    //         );
    //     }else{
    //         $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['CloudtransferConToId']));
    //         if($checkValue[0]['connection_type']=="DropBox"){
    //              $folderId = $_POST['droType'];
    //         }else{
    //              $folderId = $_POST['FileId'];
    //         }
    //         $data =array(
    //                 'connection_type'=>$checkValue[0]['connection_type'],
    //                 'fileName'=>$_POST['FileName'],
    //                 'fileID'=>$folderId,
    //                 'ConnectionID'=>$_POST['CloudtransferConToId'],
    //                 'type'=>$_POST['connectionTyep'],
    //                 'droType'=>$_POST['droType'],
    //         );
    //     }
    //   echo json_encode($data,JSON_UNESCAPED_SLASHES); 
	   // die();
    // }   
    // function transferNowCloudDataOffline(){
    //     print_r($_POST);
    //     die();
    //   if(!empty($_POST)){
    //       $this->load->library('Googledrive');
    //         $this->load->library('Dropbox_libraries');
    //         $result =$this->DBfile->get_data( 'connection', array('id'=>$_POST['CloudtransferConFromId']));
    //         $token = json_decode($result[0]['access_token'],true);
    // 		$moveCLoudToken = $this->DBfile->get_data( 'connection', array('id'=>$_POST['CloudtransferConToId']));
    // 		$CloudToken =  json_decode($moveCLoudToken[0]['access_token'],true);
    //         if(isset($result)){
    //             $FromFilesName  = [];
    //             $FromFilesId    = [];
    //             $Fromfiles_url  = [];
    //             $ToFilesName    = [];
    //             $ToFilesId      = [];
    //             if($result[0]['connection_type'] == 'GoogleDrive'){
    //                 foreach($_POST['FileName'] as $value){
    //                     array_push($FromFilesName,$value);
    //                 }
    //                 foreach($_POST['FileId'] as $value){
    //                     array_push($FromFilesId,$value);
    //                     $file = $this->googledrive->getFileInfo($token,$value);
    //                     $url = $file['data']['url'];
    //             	    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$file['data']['name'];
    //             	    file_put_contents($newfname, file_get_contents($url));
    //             	    array_push($Fromfiles_url,$newfname);
    //                 }
    //             }else if($result[0]['connection_type'] == 'DropBox'){
    //                 foreach($_POST['FileName'] as $value){
    //                     array_push($FromFilesName,$value);
    //                 }
    //                 foreach($_POST['FileId'] as $key=>$value){
    //                     array_push($FromFilesId,$value); 
    //                     $this->load->library('Dropbox_libraries');
    //                     $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$_POST['FileName'][$key];
    //                     $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
    //                     $ntoken = json_decode($access_token,true);
    //                     $accessToken = $ntoken['access_token'];
    //                     $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $value);
    //                     $shareFileLink = json_decode( $shareFileData, true );
    //                     if(empty($shareFileLink['links'])){
    //                         $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $value);
    //                         $ru = json_decode( $ru, true );
    //                         if(isset($ru['url'])){
    //                             $shareUrl = $ru['url'];
    //                         }
    //                     }else{
    //                       $shareUrl =$shareFileLink['links'][0]['url'];
    //                     }
    //             	    file_put_contents($newfname, file_get_contents($shareUrl));
    //             	    array_push($Fromfiles_url,$newfname);
    //                 }
    //             }else if($result[0]['connection_type'] == 'Backblaze'){
    //                 foreach($_POST['FileName'] as $value){
    //                     array_push($FromFilesName,$value);
    //                 }
    //                 foreach($_POST['FileId'] as $key=>$value){
    //                     array_push($FromFilesId,$value); 
    //                     $this->load->library('Dropbox_libraries');
    //                     $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$_POST['FileName'][$key];
    //                     $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
    //                     $ntoken = json_decode($access_token,true);
    //                     $accessToken = $ntoken['access_token'];
    //                     $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $value);
    //                     $shareFileLink = json_decode( $shareFileData, true );
    //                     if(empty($shareFileLink['links'])){
    //                         $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $value);
    //                         $ru = json_decode( $ru, true );
    //                         if(isset($ru['url'])){
    //                             $shareUrl = $ru['url'];
    //                         }
    //                     }else{
    //                       $shareUrl =$shareFileLink['links'][0]['url'];
    //                     }
    //             	    file_put_contents($newfname, file_get_contents($shareUrl));
    //             	    array_push($Fromfiles_url,$newfname);
    //                 }    
    //             }else if($result[0]['connection_type'] == 'Gofile'){
    //                 foreach($_POST['FileName'] as $value){
    //                     array_push($FromFilesName,$value);
    //                 }
    //                 foreach($_POST['FileId'] as $key=>$value){
    //                     array_push($FromFilesId,$value); 
    //                     $this->load->library('Dropbox_libraries');
    //                     $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$_POST['FileName'][$key];
    //                     $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
    //                     $ntoken = json_decode($access_token,true);
    //                     $accessToken = $ntoken['access_token'];
    //                     $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $value);
    //                     $shareFileLink = json_decode( $shareFileData, true );
    //                     if(empty($shareFileLink['links'])){
    //                         $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $value);
    //                         $ru = json_decode( $ru, true );
    //                         if(isset($ru['url'])){
    //                             $shareUrl = $ru['url'];
    //                         }
    //                     }else{
    //                       $shareUrl =$shareFileLink['links'][0]['url'];
    //                     }
    //             	    file_put_contents($newfname, file_get_contents($shareUrl));
    //             	    array_push($Fromfiles_url,$newfname);
    //                 }   
    //             }else if($result[0]['connection_type'] == 's3'){
    //                 foreach($_POST['FileName'] as $value){
    //                     array_push($FromFilesName,$value);
    //                 }
    //                 foreach($_POST['FileId'] as $key=>$value){
    //                     array_push($FromFilesId,$value); 
    //                     $this->load->library('Dropbox_libraries');
    //                     $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$_POST['FileName'][$key];
    //                     $access_token = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
    //                     $ntoken = json_decode($access_token,true);
    //                     $accessToken = $ntoken['access_token'];
    //                     $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $value);
    //                     $shareFileLink = json_decode( $shareFileData, true );
    //                     if(empty($shareFileLink['links'])){
    //                         $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $value);
    //                         $ru = json_decode( $ru, true );
    //                         if(isset($ru['url'])){
    //                             $shareUrl = $ru['url'];
    //                         }
    //                     }else{
    //                       $shareUrl =$shareFileLink['links'][0]['url'];
    //                     }
    //             	    file_put_contents($newfname, file_get_contents($shareUrl));
    //             	    array_push($Fromfiles_url,$newfname);
    //                 }   
    //             }
    //             if($moveCLoudToken[0]['connection_type'] == 'DropBox'){
    //                 $CloudToken = json_decode($moveCLoudToken[0]['access_token'],true);
    //                 array_push($ToFilesName,$_POST['foldername']);
    //                 array_push($ToFilesId,$_POST['TransferFolderFileID']);
    //             }else if($moveCLoudToken[0]['connection_type'] == 'GoogleDrive'){
    //                 array_push($ToFilesName,$_POST['foldername']);
    //                 array_push($ToFilesId,$_POST['TransferFolderFileID']);
    //             }else if($moveCLoudToken[0]['connection_type'] == 'Backblaze'){
    //                 array_push($ToFilesName,$_POST['foldername']);
    //                 array_push($ToFilesId,$_POST['TransferFolderFileID']);
    //             }else if($moveCLoudToken[0]['connection_type'] == 'Gofile'){
    //                 array_push($ToFilesName,$_POST['foldername']);
    //                 array_push($ToFilesId,$_POST['TransferFolderFileID']);
    //             }else if($moveCLoudToken[0]['connection_type'] == 's3'){
    //                 array_push($ToFilesName,$_POST['foldername']);
    //                 array_push($ToFilesId,$_POST['TransferFolderFileID']);
    //             }
    //         }
    //         $data =array(
    //             'user_id'=>$_SESSION['id'],
    //             'from_name'=>$result[0]['connection_type'],
    //             'to_name'=>$moveCLoudToken[0]['connection_type'],
    //             'from_file_name'=>json_encode($FromFilesName),
    //             'from_file_id'=>json_encode($FromFilesId),
    //             'to_file_name'=>json_encode($ToFilesName),
    //             'to_file_id'=>json_encode($ToFilesId),
    //             'from_files_url'=>json_encode($Fromfiles_url),
    //             'transfer_from_id'=>$_POST['CloudtransferConFromId'],
    //             'transfer_to_id'=>$_POST['CloudtransferConToId'],
    //             'status'=>0,
    //             'transfer_status'=>2,
    //         );
    //         $result = $this->DBfile->put_data('task_list',$data);
    //         if($result){
    //                 $res = array('status'=>1,'data'=>$result,'smg'=>'Successfully Add Task ');
    //             }else{
    //                 $res = array('status'=>0,'smg'=>'Technicle Issue ');
    //         }
    //   }else{
    //         $res = array('status'=>0,'smg'=>'Please Select  File &  Folder ');
    //   }
    //     echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	   // die();
    // }
    
    function cloudtransferDataTaskAdd(){
        // print_r($_POST);die;
        $type = isset($_POST['connectionTyep']) ? $_POST['connectionTyep'] : $_POST['type'];
        if($type =="from"){
            if(!empty($_POST['data_cloud_type'])){
                $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['CloudtransferConToId']));
                if($checkValue[0]['connection_type']=="DropBox"){
                     $folderId = $_POST['droType'];
                }else{
                     $folderId = $_POST['FileId'];
                }
                $data =array(
                        'connection_type'=>$checkValue[0]['connection_type'],
                        'fileName'=>$_POST['FileName'],
                        'fileID'=>$folderId,
                        'ConnectionID'=>$_POST['CloudtransferConToId'],
                        'type'=>$_POST['connectionTyep'],
                        'droType'=>$_POST['droType'],
                        'data_cloud_type'=>$_POST['data_cloud_type'],
                );
            }else{
                
                $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['UserId']));
                // print_r($checkValue);
                // die();
                $fileName = [];
                $FileId = [];
                if(isset($_POST['FileName']) && !empty($_POST['FileName'])){
                    foreach($_POST['FileName']  as $value){
                        array_push($fileName,$value);
                    }
                }
                if(isset($checkValue[0]['connection_type']) && $checkValue[0]['connection_type']=="DropBox"){
                    if(isset($_POST['droType']) && !empty($_POST['droType'])){
                        foreach($_POST['droType'] as $value){
                            array_push($FileId,$value);
                        }
                    }
                }else{
                    if(isset($_POST['FileId']) && !empty($_POST['FileId'])){
                        foreach($_POST['FileId'] as $value){
                            array_push($FileId,$value);
                        }
                    }
                }
                    $data =array(
                        'connection_type'=>$checkValue[0]['connection_type'],
                        'fileName'=>json_encode($fileName),
                        'fileID'=>json_encode($FileId),
                        'ConnectionID'=>$_POST['UserId'],
                        'type'=>$_POST['type'],
                        'droType'=>$_POST['droType'],
                        'data_cloud_type'=>$_POST['data_cloud_type'],
                );
                
            }
        }else{
            $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['CloudtransferConToId']));
            if($checkValue[0]['connection_type']=="DropBox"){
                 $folderId = $_POST['droType'];
            }else{
                 $folderId = $_POST['FileId'];
            }
            $data =array(
                    'connection_type'=>$checkValue[0]['connection_type'],
                    'fileName'=>$_POST['FileName'],
                    'fileID'=>$folderId,
                    'ConnectionID'=>$_POST['CloudtransferConToId'],
                    'type'=>$_POST['connectionTyep'],
                    'droType'=>$_POST['droType'],
                    'data_cloud_type'=>$_POST['data_cloud_type'],
            );
        }
       echo json_encode($data,JSON_UNESCAPED_SLASHES); 
	    die();
    } 

        function cloudBackupDataTaskAdd(){
        // print_r($_POST);die;
        $type = isset($_POST['connectionTyep']) ? $_POST['connectionTyep'] : $_POST['type'];
        if($type =="from"){
            if(!empty($_POST['data_cloud_type'])){
                $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['CloudtransferConToId']));
                if($checkValue[0]['connection_type']=="DropBox"){
                     $folderId = $_POST['droType'];
                }else{
                     $folderId = $_POST['FileId'];
                }
                $data =array(
                        'connection_type'=>$checkValue[0]['connection_type'],
                        'fileName'=>$_POST['FileName'],
                        'fileID'=>$folderId,
                        'ConnectionID'=>$_POST['CloudtransferConToId'],
                        'type'=>$_POST['connectionTyep'],
                        'droType'=>$_POST['droType'],
                        'data_cloud_type'=>$_POST['data_cloud_type'],
                );
            }else{
                
                $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['UserId']));
                // print_r($checkValue);
                // die();
                $fileName = [];
                $FileId = [];
                if(isset($_POST['FileName']) && !empty($_POST['FileName'])){
                    foreach($_POST['FileName']  as $value){
                        array_push($fileName,$value);
                    }
                }
                if(isset($checkValue[0]['connection_type'])=="DropBox"){
                    if(isset($_POST['droType']) && !empty($_POST['droType'])){
                        foreach($_POST['droType'] as $value){
                            array_push($FileId,$value);
                        }
                    }
                }else{
                    if(isset($_POST['FileId']) && !empty($_POST['FileId'])){
                        foreach($_POST['FileId'] as $value){
                            array_push($FileId,$value);
                        }
                    }
                }
                    $data =array(
                        'connection_type'=>$checkValue[0]['connection_type'],
                        'fileName'=>json_encode($fileName),
                        'fileID'=>json_encode($FileId),
                        'ConnectionID'=>$_POST['UserId'],
                        'type'=>$_POST['type'],
                        'droType'=>$_POST['droType'],
                        'data_cloud_type'=>$_POST['data_cloud_type'],
                );
                
            }
        }else{
            $checkValue = $this->DBfile->get_data('connection',array('id'=>$_POST['CloudtransferConToId']));
            if($checkValue[0]['connection_type']=="DropBox"){
                 $folderId = $_POST['droType'];
            }else{
                 $folderId = $_POST['FileId'];
            }
            $data =array(
                    'connection_type'=>$checkValue[0]['connection_type'],
                    'fileName'=>$_POST['FileName'],
                    'fileID'=>$folderId,
                    'ConnectionID'=>$_POST['CloudtransferConToId'],
                    'type'=>$_POST['connectionTyep'],
                    'droType'=>$_POST['droType'],
                    'data_cloud_type'=>$_POST['data_cloud_type'],
            );
        }
       echo json_encode($data,JSON_UNESCAPED_SLASHES); 
	    die();
    }   
    
   

    function CanceledTransfer(){
        $data = $this->DBfile->set_data('task_list',array('transfer_status'=>0),array('id'=>$_POST['id']));
		if($data==true){
		    $res = array('status'=>1);
		}else{
	       $res = array('status'=>0);
		}
		echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    function retransfer(){
        $data = $this->DBfile->set_data('task_list',array('transfer_status'=>2),array('id'=>$_POST['id']));
		if($data==true){
		    $res = array('status'=>1);
		}else{
	       $res = array('status'=>0);
		}
		echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
    function DeleteTask(){
        $data = $this->DBfile->get_data('task_list',array('id'=>1,'status'=>0,'to_name'=>'GoogleDrive'));
        // foreach($data as $value){
        // }
	    $reusult=$this->DBfile->delete_data( 'task_list', array('id'=>$_POST['id']));
		if($reusult==true){
		    $res = array('status'=>1);
		}else{
	       $res = array('status'=>0);
		}
		echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    }
     function userTastlimitEmpty(){
        $data = $this->DBfile->set_data('usertbl',array('task_count'=>0),array('task_count !='=>0));
    }
    function test(){
        $this->load->library('Googledrive');
        $file = $this->googledrive->ptest($token);
    }

    function getFolderRecursively( $token, $id, $drive, $meta_data = [] ){
        $folder_data = [];
        if ($drive == 'GoogleDrive' ) {
            $this->load->library('Googledrive');

            $ar = $this->googledrive->getAllFolders($token, $id);
            foreach($ar['data'] as $key => $value){
                $check_file_Folder_type = explode(".",$value['name']);

                if(!empty($check_file_Folder_type[1])){
                    
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$value['name'];
                    $data = $this->googledrive->getFileInfo($token,$value['id']);
                    $url = $data['data']['url'];
                    // file_put_contents($newfname, file_get_contents($url));
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $value['name'], "id"=>$value['id'], "drive_location"=>$url, "location"=>$newfname );

                }else{
                    $ret_ar = $this->getFolderRecursively($token, $value['id'], $drive);
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $value['name'], "id"=>$value['id'], 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'DropBox' ) {
            $this->load->library('Dropbox_libraries');
            
            $access_token    = $this->dropbox_libraries->getAccesTokenFromRefreshToken( $token['refresh_token'] );
            $ntoken          = json_decode($access_token, true);
            $accessToken     = $ntoken['access_token'];

            $data = $this->dropbox_libraries->getAllFolders($accessToken,$id);
            foreach($data['data'] as $key => $value){
                if( $value['.tag'] == 'file' ){
                    // print_r($value['.tag']);die;
                    
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$value['name'];

                    $shareFileData = $this->dropbox_libraries->getSharedFiles($accessToken, $value['path_lower']);
                            
                    $shareFileLink = json_decode( $shareFileData, true );
                    
                    if(empty($shareFileLink['links'])){
                        $ru = $this->dropbox_libraries->fileSharedLink($accessToken, $value['path_lower']);
                        $ru = json_decode( $ru, true );
                        if(isset($ru['url'])){ $shareUrl = $ru['url']; }
                    }else{
                        $shareUrl =$shareFileLink['links'][0]['url'];
                    }
                    $url = str_replace( 'dl=0','raw=1', $shareUrl);
                    // print_r($url);die;
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $value['name'], "id"=>$value['path_lower'], "drive_location"=>$url ,"location"=>$newfname );
                                        
                }else{
                    $ret_ar = $this->getFolderRecursively($token, $value['path_lower'], $drive );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $value['name'], "id"=>$value['path_lower'], 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'Backblaze' ) {
            $this->load->helper('back_blaz');
        
            
            $data = b2_get_folder($id, $token['key_id'], $token['application_key'] );
            
            foreach($data as $key => $value){
                if( $value['action'] == 'upload' ){
                    $path_parts = pathinfo($value['fileName']);
                    $file_name = $path_parts['basename'];
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                    $drive_location = b2_create_friendly_url($id, $file_name, $token['bucket_name'], $token['key_id'], $token['application_key']);
                    
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$value['fileName'], "drive_location"=>$drive_location ,"location"=>$newfname );
                                        
                }else{
                    $path_parts = pathinfo($value['fileName']);
                    $file_name = $path_parts['basename'];
                    $ret_ar = $this->getFolderRecursively($token, $value['fileName'], $drive );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $file_name, "id"=>$value['fileName'], 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'Pcloud' ) {
            $this->load->library('Pcloud_lib');
            $data = $this->pcloud_lib->getFolderById( (int)$id, $token['token']);
            
            foreach($data['folder_data'] as $key => $value){
                if( $value['icon'] != 'folder' ){
                    $file_name = $value['name'];
                    // $newfname = base_url()."/uploads/backup/".$file_name;
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                    // $drive_location = b2_create_friendly_url($id, $file_name, $token['bucket_name'], $token['key_id'], $token['application_key']);
                    $res = $this->pcloud_lib->getFileDownloadLink($value['fileid'], $token['token']);
                    $drive_location = $res['file_id'];
                    
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$value['fileid'], "drive_location"=>$drive_location ,"location"=>$newfname );
                                        
                }else{
                    $file_name = $value['name'];
                    $ret_ar = $this->getFolderRecursively($token, $value['folderid'], $drive );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $file_name, "id"=>$value['folderid'], 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'OneDrive' ) {
            $this->load->library('onedrive');
            $refresh_token=$token['refresh_token'];
            $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
            $tk = json_decode($res,true);
            $accessToken = $tk['access_token'];
            $driveId = explode('!', $id);
            $data = $this->onedrive->getFiles($accessToken,$id,$driveId[0]);
            
            foreach($data['data'] as $key => $value){
                if( isset( $value['file'] ) ){
                    $file_name = $value['name'];
                    // $newfname = base_url()."/uploads/backup/".$file_name;
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                    $drive_location = $value['@microsoft.graph.downloadUrl'];
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$value['id'], "drive_location"=>$drive_location ,"location"=>$newfname );
                                        
                }else{
                    $file_name = $value['name'];
                    $ret_ar = $this->getFolderRecursively($token, $value['id'], $drive );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $file_name, "id"=>$value['id'], 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'Gofile' ) {
            $this->load->library('Gofile_lib');

            $data = $this->gofile_lib->gofile_get_folder( $token['token'] ,$id);
            foreach($data['data']['contents'] as $key => $value){
                if( $value['type'] == 'file'  ){
                    $file_name = $value['name'];
                    $newfname = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                    $drive_location = $value['directLink'];
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$value['id'], "drive_location"=>$drive_location ,"location"=>$newfname );
                                        
                }else{
                    $file_name = $value['name'];
                    $ret_ar = $this->getFolderRecursively($token, $value['id'], $drive );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $file_name, "id"=>$value['id'], 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'Ftp' ) {
            
            $ftp            = ftp_connect( $token['hostname'] );
            $login_result   = ftp_login($ftp, $token['username'], $token['password']);
            $data_          = ftp_mlsd($ftp, $id);
            foreach ($data_ as $key => $value) {
                    if ( $value['type'] == 'cdir' || $value['type'] == 'pdir') {
                            unset($data_[$key]);
                    }
            }
            ftp_close($ftp);

            foreach($data_ as $key => $value){
                if( $value['type'] == 'file'  ){
                    $file_name      = $value['name'];
                    $newfname       = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                    // $drive_location = base_url('/uploads/backup/'.$file_name);
                    $drive_location = 'ftp://'.$token['username'].':'.$token['password'].'@'.$token['hostname'].$id.'/'.$file_name; //creating ftp url of file
                    // $ftp            = ftp_connect( $token['hostname'] );
                    // $login_result   = ftp_login($ftp, $token['username'], $token['password']);
                    // if (ftp_get($ftp, $newfname, $id.'/'.$file_name, FTP_BINARY )) {
                    //     ftp_close($ftp);
                    // }
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$id.'/'.$file_name, "drive_location"=>$drive_location ,"location"=>$newfname );
                                        
                }else{
                    $file_name = $value['name'];
                    $ret_ar = $this->getFolderRecursively($token, $id.'/'.$file_name, $drive );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $file_name, "id"=>$id.'/'.$file_name, 'content'=> $ret_ar );
                }
            }
        }
        else if( $drive == 'GooglePhoto' ){
            $this->load->library('Googlephoto');
            if($id!=1 && $id!=2 && $id!=3){
                $file = $this->googlephoto->getAlbumItem($token,$id);
                $ar = isset($file['mediaItems']) && !empty($file['mediaItems']) ? $file['mediaItems'] : '' ;
                $type ="mediaItemSearch";
            }else{
                if($id==1){
                    $clientData = $this->googlephoto->ListAlbum($token);
                    $data = json_decode($clientData,true);
                    $ar = isset($data['albums']) && !empty($data['albums']) ? $data['albums'] : '' ;
                }else if($id==2){
                    $clientData = $this->googlephoto->listShare($token);
                    $data = json_decode($clientData,true);
                    $ar = isset($data['sharedAlbums']) && !empty($data['sharedAlbums']) ? $data['sharedAlbums'] : '' ;
                }else if($id==3){
                    $clientData = $this->googlephoto->MediaItem($token);   
                    $data = json_decode($clientData,true);
                    $ar = isset($data['mediaItems']) && !empty($data['mediaItems']) ? $data['mediaItems'] : '' ;
                }
            }  
            if ( isset($ar[0]) ) {
                foreach($ar as $key => $value){
                    if( isset($value['mimeType'])  ){
                        $file_name      = $value['filename'];
                        $newfname       = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                        $drive_location = $value['baseUrl'];
                        $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$value['id'], "drive_location"=>$drive_location ,"location"=>$newfname );
                                            
                    }else{
                        $folder_name = $value['title'];
                        $ret_ar = $this->getFolderRecursively($token, $value['id'], $drive );
                        $folder_data[$key] = array("type"=> 'folder' , "name"=> $folder_name, "id"=>$value['id'], 'content'=> $ret_ar );
                    }
                }  
            }
        }
        else if( $drive == 'Box' ) {

            $this->load->library('Box_api');
            $access_token = $this->checkBoxAccessToken($token, $meta_data['email'], 'Box', $meta_data['created']);
            // $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
            if($access_token){
                $data_ = $this->box_api->getAllFolders($access_token, $id);
            }

            foreach($data_['data'] as $key => $value){
                if( $value['type'] == 'file'  ){
                    $file_name      = $value['name'];
                    $newfname       = $_SERVER['DOCUMENT_ROOT']."/uploads/backup/".$file_name;
                    $drive_location = '';
                    // $access_token = $this->checkBoxAccessToken($token, $meta_data['email'], 'Box', $meta_data['created']);
                    if($access_token){
                        $file = $this->box_api->getFileDetails($access_token, $value['id']);
                        $fileData = json_decode($file, true);
                        if(empty($fileData['shared_link'])){
                            $file = $this->box_api->getSharedLinkOfFile($access_token, $value['id']);
                            $fileData = json_decode($file, true);
                        }
                        $url = $fileData['shared_link']['url'];
                        $url_ar = explode("/",$url);
                        $sharedName = end($url_ar);

                        $box_url = "https://app.box.com/index.php?rm=box_download_shared_file&shared_name=".$sharedName."&file_id=f_".$value['id'];
                        file_put_contents( $newfname, $this->box_api->file_get_contents_curl($box_url) );
                    }
                    $folder_data[$key] = array("type"=> 'file' , "name"=> $file_name, "id"=>$value['id'], "drive_location"=>$newfname ,"location"=>$newfname );
                                        
                }else{
                    $folder_name = $value['name'];
                    $ret_ar = $this->getFolderRecursively($token, $value['id'], $drive, $meta_data );
                    $folder_data[$key] = array("type"=> 'folder' , "name"=> $folder_name, "id"=>$value['id'], 'content'=> $ret_ar );
                }
            }

        }
        return $folder_data;
    }

    function createBackup($parentId, $folder_data, $token, $drive, $meta_data = []){
        if ($drive == 'GoogleDrive') {
            $this->load->library('Googledrive');
            foreach ($folder_data as $key => $value) {
                    
                if ($value['type'] == 'folder') {
                    $ar = $this->googledrive->createSubFolder($value['name'], $parentId,$token);
                    $ar = json_decode($ar, true);
                    $status = isset($ar['status']) ? $ar['status']: 0;
                    if ( $status == 1) {
                        $ret = $this->createBackup($ar['folderid'], $value['content'], $token, $drive);
                    }else{
                        print_r($ar);
                        die;
                    }
                    
                }else{

                    $mime_type = get_mime_type($value['name']);
                    $this->googledrive->uploadFile( $value['name'], $parentId, $value['drive_location'], $mime_type, $token, true );
                }
            }
        }else if ( $drive == 'DropBox' ){
            $this->load->library('Dropbox_libraries');
            foreach ($folder_data as $key => $value) {

                $access_token   = $this->dropbox_libraries->getAccesTokenFromRefreshToken($token['refresh_token']);
                $ntoken         = json_decode($access_token,true);
                $accessToken    = $ntoken['access_token'];

                if ($value['type'] == 'folder') {
                    
                    $drotype        = $parentId;  // $TransferFolderFileIDto
                    $subFolderName  = $drotype."/".$value['name']; 
                    $ar = $this->dropbox_libraries->createFolder($accessToken,'',$subFolderName );
                    $ret = $this->createBackup($subFolderName, $value['content'], $token, $drive);

                }else{
    
                    $ar = $this->dropbox_libraries->uploadFile($accessToken, $parentId, $value['drive_location'], $value['name']);
                }
            }
        }else if ( $drive == 'Backblaze' ){
            $this->load->helper('back_blaz');
            foreach ($folder_data as $key => $value) {

                if ($value['type'] == 'folder') {
                    $folderName = clean($value['name']);
                    // print_r($token['key_id']);die;
                    $data_ = b2_create_folder( $parentId, $folderName.'/', $token['key_id'], $token['application_key'] );

                    $ret = $this->createBackup($parentId.$folderName.'/', $value['content'], $token, $drive);
                    // print_r($data_);die;
                }else{
                    $filename = preg_replace("/[^a-zA-Z0-9.]/", "", $value['name'] );
                    $ar = b2_uploadFile( $parentId, $value['drive_location'], $filename, $token['key_id'], $token['application_key']);
                }
            }
        }else if ( $drive == 'Pcloud' ){
            $this->load->library('Pcloud_lib');
            foreach ($folder_data as $key => $value) {

                if ($value['type'] == 'folder') {
                    $res = $this->pcloud_lib->createFolder($value['name'], $token['token'], (int)$parentId);
                    $ret = $this->createBackup((int)$res['folderid'], $value['content'], $token, $drive);
                }else{
                    // file_put_contents( $value['location'], file_get_contents( $value['drive_location'] ) );
                    $res = $this->pcloud_lib->uploadFile($value['drive_location'], $value['name'], $token['token'], (int)$parentId);
                }
            }
        }else if ( $drive == 'OneDrive' ){
            $this->load->library('onedrive');
            $refresh_token=$token['refresh_token'];
            $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
            $tk = json_decode($res,true);
            $accessToken = $tk['access_token'];
            $parent_id = explode('!', $parentId);
            foreach ($folder_data as $key => $value) {

                if ($value['type'] == 'folder') {
                    $a = $this->onedrive->CreateFolder($accessToken, $value['name'], $parent_id[0], $parentId);
                    $res = json_decode($a,true);
                    $ret = $this->createBackup($res['id'], $value['content'], $token, $drive);
                }else{
                    $ar = $this->onedrive->UPloadFile($accessToken, $parent_id[0], $parentId ,$value['drive_location'], $value['name'], get_mime_type($value['name']));
                }
            }
        }else if ( $drive == 'Gofile' ){
            $this->load->library('Gofile_lib');
            foreach ($folder_data as $key => $value) {

                if ($value['type'] == 'folder') {
                    $res = $this->gofile_lib->gofile_create_folder( $token['token'], $parentId, $value['name'] );
                    $ret = $this->createBackup($res['data']['id'], $value['content'], $token, $drive);
                }else{
                    file_put_contents( $value['location'], file_get_contents($value['drive_location']) );
                    $ar = $this->gofile_lib->gofile_upload_file( $token['token'], $parentId, $value['location'] );
                    unlink($value['location']);
                    // print_r($ar);die;
                }
            }
        }else if ( $drive == 'Ftp' ){
            foreach ($folder_data as $key => $value) {

                $ftp_conn       = ftp_connect( $token['hostname'] );
                $login_result   = ftp_login($ftp_conn, $token['username'], $token['password']);
                if ($value['type'] == 'folder') {
                    if (ftp_mkdir($ftp_conn, $parentId.'/'.$value['name']))
                    $ret = $this->createBackup($parentId.'/'.$value['name'], $value['content'], $token, $drive);
                }else{
                    $value['name'] = preg_replace("/[^a-zA-Z0-9.]/", "", $value['name'] );
                    // upload file
                    if (ftp_put($ftp_conn, $parentId.'/'.$value['name'], $value['drive_location'], FTP_BINARY  )){}
                }
                ftp_close($ftp_conn);
            }
        }else if ( $drive == 'Box' ){
            $this->load->library('Box_api');
            $access_token = $this->checkBoxAccessToken($token, $meta_data['email'], 'Box', $meta_data['created']);
            foreach ($folder_data as $key => $value) {

                if ($value['type'] == 'folder') {
                    if($access_token){ $ar = $this->box_api->createFolder($access_token, $value['name'], $parentId); }
                    $ar_d = json_decode($ar, true);
                    $ret = $this->createBackup($ar_d['id'], $value['content'], $token, $drive, $meta_data);
                }else{
                    file_put_contents( $value['location'], file_get_contents( $value['drive_location'] ) );
                    $ar = $this->box_api->uploadFile($access_token, $parentId, $value['location'], $value['name']);   
                    // print_r($ar);  
                    unlink( $value['location'] );
                }
                
            }
        }
        
    }

    function cronCloudTransfer(){
         $join_array  = array(
            'multiple',
                array(
                    array('connection as to_token', 'to_token.id = task_list.transfer_to_id','left'),
                    array('connection as from_token', 'from_token.id = task_list.transfer_from_id', 'left'),
                )
        );

        $query_res = $this->DBfile->select_data('task_list.*, from_token.access_token as from_token,to_token.access_token as to_token,from_token.email,from_token.created' , 'task_list' , array( 'task_list.cron_status' => 0,'task_list.transfer_status' => 2, 'task_list.transfer_type' => 'transfer' ) , $limit = '' , $order = '' , $like = '' , $join_array , $group = '', $or_like = '',$where_in ='');
        
        if (isset($query_res[0])) {
            $this->DBfile->set_data( 'task_list', ['cron_status' => 1] , [ 'id'=>$query_res[0]['id'] ]);
            $from_token       = json_decode( $query_res[0]['from_token'],true);
            $to_token         = json_decode( $query_res[0]['to_token'],true);
            $collections      = json_decode( $query_res[0]['from_files_url'],true);
            $from_folder_id   = json_decode( $query_res[0]['from_file_id'] ,true);
            $from_folder_name = json_decode( $query_res[0]['from_file_name'] ,true);
            $path             = json_decode( $query_res[0]['to_file_id'], true );

            $folder_all_data = [];
            // print_r( $collections );die;
            $meta_data = ['email' => $query_res[0]['email'], 'created' => $query_res[0]['created']];
            
            foreach ($collections as $key => $value) {
                if ($value['type'] == 'folder') {
                    $data_ = $this->getFolderRecursively( $from_token, $value['id'], $query_res[0]['from_name'], $meta_data);
                    $folder_all_data[$key] = array("type"=> 'folder', "name"=> $value['name'], "id"=> $value['id'], 'content'=> $data_ );
                }else{
                    $folder_all_data[$key] = array("type"=> 'file', "name"=> $value['name'], "id"=> $value['id'], 'location'=> $value['location'], 'drive_location' => $value['drive_location'] );
                }
            }
            // print_r($folder_all_data);die;
            $locations        = $this->createBackup( $path[0], $folder_all_data, $to_token, $query_res[0]['to_name'], $meta_data ); 
            $this->DBfile->set_data( 'task_list', ['transfer_status' => 1] , [ 'id'=>$query_res[0]['id'] ]);

            
        }
    }

    function transferNowCloudDataOffline(){
        //   print_r($_POST);die();
            $this->load->library('Googledrive');
            $this->load->library('Dropbox_libraries');
            $this->load->helper('common');
            $result =$this->DBfile->get_data( 'connection', array('id'=>$_POST['CloudtransferConFromId']));
            $token = json_decode($result[0]['access_token'],true);
    
            $moveCLoudToken = $this->DBfile->get_data( 'connection', array('id'=>$_POST['CloudtransferConToId']));
            $CloudToken =  json_decode($moveCLoudToken[0]['access_token'],true);
          
            if(isset($result)){
                $FromFilesName  = [];
                $FromFilesId    = [];
                $Fromfiles_url  = [];
                $ToFilesName    = [];
                $ToFilesId      = [];
                
                if($result[0]['connection_type'] == 'GoogleDrive'){
                    $this->current_token = $token;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { $file = $ci->googledrive->getFileInfo($ci->current_token,$ar2); $drive_location = $file['data']['url']; $location = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$file['data']['name']; }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }
                   
                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);

                } else if($result[0]['connection_type'] == 'DropBox'){
                    
                    $access_token    = $this->dropbox_libraries->getAccesTokenFromRefreshToken( $token['refresh_token'] );
                    $ntoken          = json_decode($access_token, true);
                    $accessToken     = $ntoken['access_token'];

                    $this->current_token = $accessToken;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { $shareFileData = $ci->dropbox_libraries->getSharedFiles($ci->current_token,$ar2); $shareFileLink = json_decode( $shareFileData, true ); if(empty($shareFileLink['links'])){ $ru = $ci->dropbox_libraries->fileSharedLink($ci->current_token,$ar2); $ru = json_decode( $ru, true ); if(isset($ru['url'])){ $drive_location = $ru['url']; } }else{ $drive_location =$shareFileLink['links'][0]['url']; } $location = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                   
                } else if($result[0]['connection_type'] == 'Backblaze'){
                    $this->load->helper('back_blaz');
                    $this->current_token = $token;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 
                            $drive_location = b2_create_friendly_url($ar2, '', $ci->current_token['bucket_name'], $ci->current_token['key_id'], $ci->current_token['application_key']);
                            $location = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                   
                } else if($result[0]['connection_type'] == 'Pcloud'){
                    $this->load->library('Pcloud_lib');
                    $this->current_token = $token;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 
                            $res = $ci->pcloud_lib->getFileDownloadLink((int)$ar2, $ci->current_token['token']);
                            $drive_location = ($res['status'] == 1)?$res['file_id']:0; // getting file location
                            $location = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                    
                } else if($result[0]['connection_type'] == 'OneDrive'){
                    $this->load->library('onedrive');
                    $refresh_token=$token['refresh_token'];
                    $res = $this->onedrive->getAccesTokenFromRefreshToken($refresh_token);
                    $tk = json_decode($res,true);
                    $accessToken = $tk['access_token'];
                    $this->current_token = $accessToken;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 
                            $res = $ci->onedrive->ShareURL($ci->current_token, $ar2);
                            $drive_location = ( isset($res['@microsoft.graph.downloadUrl']) )?$res['@microsoft.graph.downloadUrl']:null; // getting file location
                            $location = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                } else if($result[0]['connection_type'] == 'Gofile'){
                    $this->load->library('Gofile_lib');
                    $this->current_token = $token['token'];
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 
                            $drive_location = $ci->gofile_lib->gofile_getFileLink($ar2, $ar1); // getting file location
                            $location       = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);

                } else if($result[0]['connection_type'] == 'Ftp'){
                    $this->current_token = $token;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 
                            $drive_location = 'ftp://'.$ci->current_token['username'].':'.$ci->current_token['password'].'@'.$ci->current_token['hostname'].$ar2.'/'.$ar1; //creating ftp url of file
                            $location       = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                } else if($result[0]['connection_type'] == 'GooglePhoto'){
                    $this->load->library('Googlephoto');
                    $this->current_token = $token;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 
                            $ret = $ci->googlephoto->getShareUrl($ci->current_token,$ar2);                        //creating ftp url of file
                            $drive_location = $ret['baseUrl'];
                            $location       = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $drive_location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                } else if($result[0]['connection_type'] == 'Box'){
                    $this->load->library('Box_api');
                    $access_token = $this->checkBoxAccessToken($token, $result[0]['email'], 'Box', $result[0]['created']);
                    $this->current_token = $access_token;
                    $mapped_array = array();

                    function myfunction($ar1, $ar2) { 
                        $type = isFileFolder($ar1);
                        $drive_location = 'NA';
                        $location       = 'NA';
                        $ci =& get_instance();
                        if ($type == 'file') { 

                            $file = $ci->box_api->getFileDetails($ci->current_token, $ar2); $fileData = json_decode($file, true);
                            if(empty($fileData['shared_link'])){ $file = $ci->box_api->getSharedLinkOfFile($ci->current_token, $ar2); $fileData = json_decode($file, true); }
                            $url = $fileData['shared_link']['url']; $url_ar = explode("/",$url); $sharedName = end($url_ar);
                            $box_url = "https://app.box.com/index.php?rm=box_download_shared_file&shared_name=".$sharedName."&file_id=f_".$ar2;
                            $location       = $_SERVER['DOCUMENT_ROOT']."/uploads/cloudTransfer/".$ar1; 
                            file_put_contents( $location, $ci->box_api->file_get_contents_curl($box_url) );
                        }
                        $mapped_array = ['name' => $ar1, 'id' => $ar2, 'type' => isFileFolder($ar1), 'location' => $location, 'drive_location' => $location ];
                        return $mapped_array ; 
                    }

                    $Fromfiles_url = array_map("myfunction",$_POST['FileName'],$_POST['FileId']);
                }
                
                    
            array_push($ToFilesName,$_POST['foldername']);
            array_push($ToFilesId,$_POST['TransferFolderFileID']);
            }
            $data =array(
                'user_id'          => $_SESSION['id'],
                'from_name'        => $result[0]['connection_type'],
                'to_name'          => $moveCLoudToken[0]['connection_type'],
                'from_file_name'   => json_encode($FromFilesName),
                'from_file_id'     => json_encode($FromFilesId),
                'transfer_type'    => 'transfer',
                'to_file_name'     => json_encode($ToFilesName),
                'to_file_id'       => json_encode($ToFilesId),
                'from_files_url'   => json_encode($Fromfiles_url),
                'transfer_from_id' => $_POST['CloudtransferConFromId'],
                'transfer_to_id'   => $_POST['CloudtransferConToId'],
                'status'           => 0,
                'transfer_status'  => 2,
                
            );
            
            $result = $this->DBfile->put_data('task_list',$data);
            
            if($result){
                $res = array('status'=>1,'data'=>$result,'smg'=>'Task Added Successfully');
            }else{
                $res = array('status'=>0);
            }
            echo json_encode($res,JSON_UNESCAPED_SLASHES); 
            die();
    }
    
    function cronBackupFunction(){

        $join_array  = array(
            'multiple',
                array(
                    array('connection as to_token', 'to_token.id = task_list.transfer_to_id','left'),
                    array('connection as from_token', 'from_token.id = task_list.transfer_from_id', 'left'),
                )
        );

        $query_res = $this->DBfile->select_data('task_list.*, from_token.access_token as from_token,to_token.access_token as to_token,from_token.email,from_token.created' , 'task_list' , array( 'task_list.cron_status' => 0,'task_list.transfer_status' => 2, 'task_list.transfer_type' => 'backup' ) , $limit = '' , $order = '' , $like = '' , $join_array , $group = '', $or_like = '',$where_in ='');
        
        if (isset($query_res[0])) {
            $this->DBfile->set_data( 'task_list', ['cron_status' => 1] , [ 'id'=>$query_res[0]['id'] ]);
            $from_token       = json_decode( $query_res[0]['from_token'],true);
            $to_token         = json_decode( $query_res[0]['to_token'],true);
            $from_folder_id   = json_decode( $query_res[0]['from_file_id'] ,true);
            $from_folder_name = json_decode( $query_res[0]['from_file_name'] ,true);
            $path             = json_decode( $query_res[0]['to_file_id'], true );
            // print_r( $query_res );die;
            $meta_data = ['email' => $query_res[0]['email'], 'created' => $query_res[0]['created']];

            $data_           = $this->getFolderRecursively($from_token, $from_folder_id[0], $query_res[0]['from_name'], $meta_data);
            $folder_all_data = [ array("type"=> 'folder', "name"=> $from_folder_name[0], "id"=>$from_folder_id[0], 'content'=> $data_ ) ];
            // print_r('<pre>');
            // print_r($folder_all_data);die;
            $locations       = $this->createBackup( $path[0], $folder_all_data, $to_token, $query_res[0]['to_name'], $meta_data); 
            $this->DBfile->set_data( 'task_list', ['transfer_status' => 1] , [ 'id'=>$query_res[0]['id'] ]);
        }

    }
    

    function BackupNowCloudDataOffline(){
 
       if(!empty($_POST)){
           
           $this->load->library('Googledrive');
            $this->load->library('Dropbox_libraries');
            
            $result =$this->DBfile->get_data( 'connection', array('id'=>$_POST['CloudtransferConFromId']));
            $token = json_decode($result[0]['access_token'],true);
            
    		$moveCLoudToken = $this->DBfile->get_data( 'connection', array('id'=>$_POST['CloudtransferConToId']));
    		$CloudToken =  json_decode($moveCLoudToken[0]['access_token'],true);

            if(isset($result)){
                
                $TransferFolderFileIDfrom   = $_POST['TransferFolderFileIDfrom'];
                $connectionIdFrom           = $_POST['CloudtransferConFromId'];
                $folderNameFrom             = $_POST['foldernamefrom'];
                
                $TransferFolderFileIDto     = $_POST['TransferFolderFileIDto'];
                $connectionIdTo             = $_POST['CloudtransferConToId'];
                $folderNameTo               = $_POST['foldernameto'];
                
                $FromFilesName  = [];
                $FromFilesId    = [];
                $Fromfiles_url  = [];
                
                $ToFilesName    = [];
                $ToFilesId      = [];

                
               
                
                if(!empty($_POST['data_cloud_type'])){
                    
                    $subFolderName  = [];
                    $subFromFilesName  = [];
                    $subFromFilesId    = [];
                    $subFromfiles_url  = [];
                    
                    $subtoFromFilesName  = [];
                    $subtoFromFilesId    = [];
                    $subtoFromfiles_url  = [];
                    
                    $subtosubFromFilesName  = [];
                    $subtosubFromFilesId    = [];
                    $subtosubFromfiles_url  = [];
                    $folder_data = [];
                    
                    array_push($FromFilesName,$_POST['foldernamefrom']);
                    $Fromfiles_url = $folder_all_data = '';
                    array_push($FromFilesId, $TransferFolderFileIDfrom);
                    array_push($ToFilesName, $folderNameTo);
                    array_push($ToFilesId  , $TransferFolderFileIDto);
                    $transfer_type = 'backup';
                          
                        
                }
            }
                
                $data =array(
                    'user_id'           => $_SESSION['id'],
                    'from_name'         => $result[0]['connection_type'],
                    'to_name'           => $moveCLoudToken[0]['connection_type'],
                    'from_file_name'    => json_encode($FromFilesName),
                    'from_file_id'      => json_encode($FromFilesId),
                    'transfer_type'     => $transfer_type,
                    'to_file_name'      => json_encode($ToFilesName),
                    'to_file_id'        => json_encode($ToFilesId),
                    'from_files_url'    => json_encode($Fromfiles_url),
                    'transfer_from_id'  => $_POST['CloudtransferConFromId'],
                    'transfer_to_id'    => $_POST['CloudtransferConToId'],
                    'status'            => 0,
                    'transfer_status'   => 2,
                );
              
                $result = $this->DBfile->put_data('task_list',$data);
                if($result){
                        $res = array('status'=>1,'data'=>$result,'smg'=>'Successfully Add Task ');
                    }else{
                        $res = array('status'=>0,'smg'=>'Technicle Issue ');
                }
                // $this->cronBackupFunction();
       }else{
            $res = array('status'=>0,'smg'=>'Please Select  File &  Folder ');
       }
        echo json_encode($res,JSON_UNESCAPED_SLASHES); 
	    die();
    } 

}
?>