<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller {
		
	public function index(){
		die('Unauthorize Access!!');
	}
		
	private function checkValidAJAX(){
		$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		if( strpos($ref,base_url()) === false )
			die('Unauthorize Access!!');
		if( !isset($_POST) )
			die('Unauthorize Access!!');
		
		$postData = array();
		foreach( $_POST as $key=>$value ){
			$temp = $this->input->post($key,TRUE);
			$temp = $this->db->escape($temp);
			$postData[$key] = $temp;
		}
		return json_encode($postData);
	}
	
	public function login(){
	   // print_r($_COOKIE);
	   // die;
	    $type = '';
	    /*if(!empty($_COOKIE['uem']) || !empty($_COOKIE['em'])){
	        $email = !empty($_COOKIE['uem']) ? $_COOKIE['uem'] : $_COOKIE['em'];
	        $pass = !empty($_COOKIE['upwd']) ? $_COOKIE['upwd'] : $_COOKIE['pwd'];
	        if(in_array($_SERVER['HTTP_HOST'] , MAIN_DOMAIN_LIST)){
    		    $where = ['email' => isset($_POST['em'])?$_POST['em']:'', 'user_type !=' => 2 ];
        		$res = $this->DBfile->get_data( 'usertbl', $where,'*');
	        }else{
	            $cnd = [
	                'usertbl.email' => $_POST['em'],
	                'usertbl.user_type' => 2,  
	                'wl_setting.domain_name' => $_SERVER['HTTP_HOST'],  
                ];
	            
	            $res = $this->my_model->select_data(array(
                    'field' => 'usertbl.*',
                    'table' => 'usertbl',
                    'where' => $cnd,
                    'limit' => 1,
                    'join' => [
                        'wl_setting',
                        'wl_setting.user_id = usertbl.parent_id'
                    ]
                ));
                // print_r($res);die;
	        }
    		if(!empty($res)){
    			if($res[0]['password'] != md5($pass)){
    				echo '1';die();
    			}
    			if($res[0]['status'] == 2 ){
    			    
    				echo $res[0]['parent_id'] == 0 ? '2' : '4';die(); // InActive User
    			}
    			if($res[0]['status'] == 3 ){
    				echo '3';die(); // Blocked User
    			}
    			$nameInitials = substr($res[0]['name'],0,2);
    			$user_detail = array(
    				'loginstatus' => 1,
    				'id' => $res[0]['id'],
    				'email' => $res[0]['email'],
    				'firstname' => explode(' ',$res[0]['name'])[0],
    				'initials' => $nameInitials,
    				'type' => $res[0]['role'],
    				'current_plan' => $res[0]['plan'] != ''?json_decode($res[0]['plan'] , true):[],
    				'task_count' => $res[0]['task_count'],
    				'user_type' => $res[0]['user_type'],
    			);
    			$this->session->set_userdata($user_detail);
    
    			$t = $res[0]['role'] == 2 ? 'u' : '' ; 
    
    			setcookie('check_seo_login', 1 , time() + (86400 * 30 * 24), "/");
    
    			if( $email !="" ) {
    				setcookie($t.'em', $_POST['em'], time() + (86400 * 30 * 24), "/");
    				setcookie($t.'pwd', $_POST['pwd'], time() + (86400 * 30 * 24), "/");
    			}
    			else{
    				setcookie($t.'em', '', time() - (86400 * 30), "/");
    				setcookie($t.'pwd', '', time() - (86400 * 30), "/");
    			}
    			echo '5';
    		}
    		else
    			echo '0';
    		die();
	    }else{*/
	        $jsonPost = $this->checkValidAJAX();
    		$postData = json_decode($jsonPost,TRUE);
    		
    		if(in_array($_SERVER['HTTP_HOST'] , MAIN_DOMAIN_LIST)){
    		    $where = ['email' => isset($_POST['em'])?$_POST['em'] :'', 'role' => 2 ];
			
    		    $where = array(
		                'email'=>$_POST['em']
		        );
        		$res = $this->DBfile->get_data( 'usertbl', $where,'*');
	        }else{
	            $cnd = [
	                'usertbl.email' => $_POST['em'],
	                'usertbl.user_type' => 2,  
	                'wl_setting.domain_name' => $_SERVER['HTTP_HOST'],  
                ];
	            
	            $res = $this->my_model->select_data(array(
                    'field' => 'usertbl.*',
                    'table' => 'usertbl',
                    'where' => $cnd,
                    'limit' => 1,
                    'join' => [
                        'wl_setting',
                        'wl_setting.user_id = usertbl.parent_id'
                    ]
                ));
               
	        }
			
	        $t = isset($res[0]['plan'])? $res[0]['plan'] : '';
	        $plan = $t == '' ? array() : json_decode($t);
	       
	       
            if (in_array(1, $plan))
            {
            	if(!empty($res)){
        			if($res[0]['password'] != md5($_POST['pwd'])){
        				echo '1';die();
        			}
        			if($res[0]['status'] == 2 ){
        			    
        				echo $res[0]['parent_id'] == 0 ? '2' : '4';die(); // InActive User Your account is Blocked. Please, contact your agency owner
        			}
        			if($res[0]['status'] == 3 ){
        				echo '3';die(); // Blocked User
        			}
        			$nameInitials = substr($res[0]['name'],0,2);
        			$user_detail = array(
        				'loginstatus' => 1,
        				'id' => $res[0]['id'],
        				'email' => $res[0]['email'],
        				'firstname' => explode(' ',$res[0]['name'])[0],
        				'initials' => $nameInitials,
        				'type' => $res[0]['role'],
        				'current_plan' => $res[0]['plan'] != ''?json_decode($res[0]['plan'] , true):[],
        				'task_count' => $res[0]['task_count'],
        				'user_type' => $res[0]['user_type'],
        			);
        			$this->session->set_userdata($user_detail);
        
        			$t = $res[0]['role'] == 2 ? 'u' : '' ; 
        
        			setcookie('check_seo_login', 1 , time() + (86400 * 30 * 24), "/");
        
        			if( $_POST['rem'] == 1 ) {
        				setcookie($t.'em', $_POST['em'], time() + (86400 * 30 * 24), "/");
        				setcookie($t.'pwd', $_POST['pwd'], time() + (86400 * 30 * 24), "/");
        			}
        			else{
        				setcookie($t.'em', '', time() - (86400 * 30), "/");
        				setcookie($t.'pwd', '', time() - (86400 * 30), "/");
        			}
        			echo '5'; //Welcome, you logged in successfully
        		}
        		else
        			echo '0';
        		die();
            }
            else
            {
                echo 6; // Without FE Purchase not Access login 
            }
    	
	    //}
		
	}

    public function signup(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$res = $this->DBfile->get_data( 'usertbl', array('email'=>$_POST['em']),'*');
		
		if(!empty($res)){
			$this->DBfile->set_data( 'usertbl', array('password'=>md5($_POST['pwd'])) , array('id'=>$res[0]['id']) );
			echo '2';
		}
		else{
			$dArray = array(
                'name'    =>  $_POST['nm'],
                'email'    =>  $_POST['em'],
                'password'    =>  md5($_POST['pwd']),
                'status'    =>  1,
                'role'    =>  2,
            );
			$this->DBfile->put_data( 'usertbl', $dArray);
			echo '5';
		}
		die();
	}

	public function forgotSection(){
		$jsonPost = $this->checkValidAJAX();
		$postData = json_decode($jsonPost,TRUE);
		$res = $this->DBfile->get_data( 'usertbl', array('email'=>$_POST['em']),'id,name,password,status');
		
		if(!empty($res)){
			if($res[0]['status'] == 2 ){
				echo '2';die(); // InActive User
			}
			if($res[0]['status'] == 3 ){
				echo '3';die(); // Blocked User
			}
			$pwd = substr(md5($res[0]['password']),0,8);
			$this->DBfile->set_data( 'usertbl', array('password'=>md5($pwd)) , array('id'=>$res[0]['id']) );
			
			$body = '<p>Hello '.$res[0]['name'].'</p>';
			$body .= '<p>Here is the new password for your '.SITENAME.' account.</p>';
			$body .= '<p>Password is : '.$pwd.'</p><br/>';
			$body .= '<p>To your success.<br/>Team, '.SITENAME.'</p>';
			sendUserEmailMandrill($_POST['em'],'New Password ['.SITENAME.']',$body);
			echo '5';
		}
		else
			echo '0';
		die();
	}

	public function getCustomerLogin(){
		 $status = 0;
		 $msg = 'Something went wrong !';
		
		 if(isset($_POST['auth_email'])) {
			 $email = $_POST['auth_email'];
			 $password =  md5($_POST['auth_password']);
			 $w_id =  $_POST['w_id'];
			 $remember =  isset($_POST['auth_remember']) ? 1 : 0;
			 $check_user = $this->DBfile->get_data( 'customers', array('c_email'=>$email , 'c_password' =>$password , 'c_w_id' =>$w_id),'*');
			 
			 if ($check_user){
				 if($check_user[0]['c_status']==1){ 
					 $status = 1;
					 $msg = 'Congratulations! You logged in successfully';
					 $c_id = $check_user[0]['c_id'];
					 
					 $user_detail = array(
						'c_id' =>$c_id, 
						'c_name' =>explode(' ',$check_user[0]['c_name'])[0],
						'c_email' =>$check_user[0]['c_email'],
						'c_w_id' =>$check_user[0]['c_w_id'],
						'profile_image' =>$check_user[0]['profile_image'],
						'c_initials' => strtoupper(substr($check_user[0]['c_name'],0,2)),
						'c_loggedin' =>1,
					  );
				    $this->session->set_userdata($user_detail);
				    if(isset($_POST['auth_remember'])){
                        setcookie("c_emanu", $_POST['auth_email'] , time()+3600 * 24 * 14,'/');
                        setcookie("c_dwp", $_POST['auth_password'] , time()+3600 * 24 * 14,'/');
                    }
                    else
                    {
                        setcookie("c_emanu", '' , time()-3600 * 24 * 365,'/');
                        setcookie("c_dwp", '' , time()-3600 * 24 * 365,'/');
                    }
				 }else{
					$msg = 'Your account is inactive, Please verify your account'; 
				 }
				  
			 }else{
				 $msg = 'Please, check your login details.';
			 }
		 }
		 echo json_encode(array('status' => $status , 'msg' => $msg ,'data_reload' =>1) , JSON_UNESCAPED_SLASHES);
		 die(); 
	}
	
	public function getCustomerSignUp(){
		$resData = array();
		if(isset($_POST['auth_name'])){
			$auth_name = trim($_POST['auth_name']);
			$auth_email   = trim($_POST['auth_email']);
			$auth_pass   = trim($_POST['auth_pass']);
			$w_id =  $_POST['w_id'];
			$webRes = $this->DBfile->get_data('website',array('w_id'=>$w_id),'w_title,w_userid');
					
			$checkEmail = $this->DBfile->get_data( 'customers', array('c_email'=>$auth_email , 'c_w_id' =>$w_id),'c_id');
			
			if($webRes && empty($checkEmail)){
				$SITENAME = $webRes[0]['w_title'];
				$key = md5(date('his').$auth_email);
		  
				$upData  = array(
				'c_name' => $auth_name,
				'c_w_id' => $w_id,
				'c_parent_uid' => $webRes[0]['w_userid'],
				'c_email' => $auth_email,
				'c_password' => md5($auth_pass),
				'c_key' => $key,
				'c_status' => 0,
				);
				
				$this->DBfile->put_data( 'customers', $upData);
				$activationlink = base_url().'authenticate/customer_verify/'.$w_id.'/'.$key;
				
				$body = '<p>Hello '.$auth_name.'</p>';
				$body .= '<p>Thanks you for Signup  '.$SITENAME.'</p>';
				$body .= '<p>Your account Email is : '.$auth_email.'</p><br/>';
				$body .= '<p>Click this link to activate your account <a href="'.$activationlink.'">Activate</a></p>';
				sendUserEmailMandrill($auth_email,'Activation Link',$body); 

				sendToAutoResponder($auth_email,$auth_name,$w_id,'w_signuplist');
				
				$resData = array('status' =>1  , 'msg' => 'Thank you for registering with us  , Please, check your email for the activation link' , 'data_reload' =>1);
			}else{
				$resData = array('status' =>0  , 'msg' => 'This email is already taken ,  please try other email.');
			}	
		}else{
			$resData = array('status' =>0  , 'msg' => 'Please select at least one user role.');
		}
		echo json_encode($resData , JSON_UNESCAPED_SLASHES);
		die(); 
		
	}
	
	public function getCustomerForgetPassword(){
		 $status = 0;
		 $msg = 'Something went wrong !';
		
		 if(isset($_POST['auth_email'])) {
			 $email = $_POST['auth_email'];
			 $w_id =  $_POST['w_id'];
			 $webRes = $this->DBfile->get_data('website',array('w_id'=>$w_id));
			 $check_user = $this->DBfile->get_data( 'customers', array('c_email'=>$email , 'c_w_id' =>$w_id),'*');
			 if ($webRes && $check_user){
				 if($check_user[0]['c_status']==1){ 
					 $status = 1;
					 $SITENAME = $webRes[0]['w_title'];
					 
					    $pwd = substr(md5($check_user[0]['c_password']),0,8);
						$this->DBfile->set_data( 'customers', array('c_password'=>md5($pwd)) , array('c_id'=>$check_user[0]['c_id']) );
						
						$body = '<p>Hello '.$check_user[0]['c_name'].'</p>';
						$body .= '<p>Here is the new password for your '.$SITENAME.' account.</p>';
						$body .= '<p>Password is : '.$pwd.'</p><br/>';
						$body .= '<p>To your success.<br/>Team, '.$SITENAME.'</p>';
						sendUserEmailMandrill($email,'New Password ['.$SITENAME.']',$body);
					    $msg = 'We have sent your password to your account email.';
				 }else{
					$msg = 'Your account is inactive, Please verify your account'; 
				 }
			 }else{
				 $msg = 'Please, check your login details.';
			 }
		 }
		 echo json_encode(array('status' => $status , 'msg' => $msg ,'data_reload' =>1) , JSON_UNESCAPED_SLASHES);
		 die(); 
	}
	
	function customer_verify($w_id = '' , $key = ''){
	    if($w_id && $key){
			$check_user = $this->DBfile->get_data( 'customers', array('c_w_id'=>$w_id , 'c_key' =>$key),'*');
			$webRes = $this->DBfile->get_data('website',array('w_id'=>$w_id));
			if ($webRes  && $check_user){
				$this->DBfile->set_data( 'customers', array('c_status'=>1 , 'c_key' =>'') , array('c_id'=>$check_user[0]['c_id']) );
				redirect(base_url().'plr/'.$webRes[0]['w_siteurl']);				
			}else{
				redirect(base_url());				
			}
		}else{
			redirect(base_url());		
		}
	    
	}
	
}
?>