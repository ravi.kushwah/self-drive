<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GoogleDrive extends CI_Controller {

    public function google(){
        if (isset($_GET['code'])) {
            $userID = $this->session->userdata( 'user_id' );
            $client = $this->googledrive->getClientForAuth();
            $client->authenticate($_GET['code']);	
            $token = $client->getAccessToken();
            $userInfo = $this->googledrive->userInformation($token);
         
            $data = array(
                'user_id' => $userID,
                'access_token' => json_encode( $token ),
                'user_info' => json_encode( $userInfo ),
                'connection_type'=>'GoogleDrive',
                'status' => 1
            );

            $result = $this->DBfile->put_data('drive_connection',$data);

            redirect( 'home/connection', 'refresh' );

        }else{
           redirect( 'home/connection', 'refresh' );
        }		
    }
    function connection(){
        $this->load->library('Googledrive');
        $userID = $this->session->userdata( 'user_id' );
        $googleAuthURL = $this->googledrive->userInformation();
        print_r($googleAuthURL);
    }

	 function AcccessToken1(){
        $end_point = 'https://accounts.google.com/o/oauth2/v2/auth';
        $client_id = '807654682686-nnsj19rdjd0blklm5an1clq7nbo73rda.apps.googleusercontent.com';
        $client_secret = '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';
        $redirect_uri = 'https://selfcloud.co.in/selfcloud/Upload/AcccessToken1';
        $scope = 'https://www.googleapis.com/auth/drive.metadata.readonly';
        
        
        $authUrl = $end_point.'?'.http_build_query([
            'client_id'              => $client_id,
            'redirect_uri'           => $redirect_uri,              
            'scope'                  => $scope,
            'access_type'            => 'offline',
            'include_granted_scopes' => 'true',
            'state'                  => 'state_parameter_passthrough_value',
            'response_type'          => 'code',
        ]);
        
        echo '<a href = "'.$authUrl.'">Authorize</a></br>';
        
        // Generate new Access Token and Refresh Token if token.json doesn't exist
        if ( !file_exists('token.json') ){
            
            if ( isset($_GET['code'])){
                $code = $_GET['code'];         // Visit $authUrl and get the authentication code
            }else{
                return;
            } 
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://accounts.google.com/o/oauth2/token");
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/x-www-form-urlencoded']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
                'code'          => $code,
                'client_id'     => $client_id,
                'client_secret' => $client_secret,
                'redirect_uri'  => $redirect_uri,
                'grant_type'    => 'authorization_code',
            ]));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close ($ch);
            
            file_put_contents('token.json', $response);
        }
        else{
            $response = file_get_contents('token.json');
            $array = json_decode($response);
            $access_token = $array->access_token;
            $refresh_token = $array->refresh_token;
        
            // Check if the access token already expired
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='.$access_token); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $error_response = curl_exec($ch);
            $array = json_decode($error_response);
            
            if( isset($array->error)){
                
                // Generate new Access Token using old Refresh Token
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://accounts.google.com/o/oauth2/token");
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
                    'client_id'     => $client_id,
                    'client_secret' => $client_secret,
                    'refresh_token'  => $refresh_token,
                    'grant_type'    => 'refresh_token',
                ]));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
          
                curl_close ($ch);
            }  
        }
         echo "<pre>"; print_r($response);
        // var_dump($response);
        
    }
	
	

}
?>