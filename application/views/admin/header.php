<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= SITENAME ?><?= isset($pagename) ? ' | '.$pagename : "" ?></title>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/js/select2/select2.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/js/toastr/toastr.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/style.css">
        <link rel="shortcut icon" href="<?= base_url() ?>assets/backend/images/favicon.png" type="image/x-icon">
    </head>
    <body>
        <div class="admin_main_wrapper ad_mainMenuOpen">
            <div class="admin_header">
                <div class="ad_headerLeft"><span class="ad_toggle"><svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px"><path d="M 0 2 L 0 4 L 24 4 L 24 2 Z M 0 11 L 0 13 L 24 13 L 24 11 Z M 0 20 L 0 22 L 24 22 L 24 20 Z"/></svg></span><h1><?= $pagename ?></h1></div>
                <div class="ad_headerRight">
                    <div class="ad_profile_wrapper">
                       <span><img class="priViewImg" src="<?= base_url() ?>assets/admin/images/<?php echo !empty($userList[0]['profile_image']) ? $userList[0]['profile_image'] :'profile.jpg';?>" alt=""/><span class="ad_userName ad_subheading1">Hello <?= $_SESSION['firstname'] ?></span></span> 

                       <div class="ad_profileDropdown">
                            <ul>
                                <li><a href="<?= base_url('admin/profile') ?>">Profile</a></li>
                                <li><a href="<?= base_url('admin/logout') ?>">Logout</a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div>
            <div class="admin_sidebar">
                <div class="ad_main_menu">
                    <ul>
                        <li>
                            <a href="<?= base_url('admin/dashboard') ?>">
                                <span><svg version="1.1" width="24px" height="24px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><g><path d="M506.188,236.413L297.798,26.65c-0.267-0.27-0.544-0.532-0.826-0.786c-22.755-20.431-57.14-20.504-79.982-0.169c-0.284,0.253-0.56,0.514-0.829,0.782L5.872,236.352c-7.818,7.804-7.831,20.467-0.028,28.285c7.804,7.818,20.467,7.83,28.284,0.028L50,248.824v172.684c0,44.112,35.888,80,80,80h72c11.046,0,20-8.954,20-20v-163h70v163c0,11.046,8.954,20,20,20h70c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20c0,22.056-17.944,40-40,40h-50v-163c0-11.046-8.954-20-20-20H202c-11.046,0-20,8.954-20,20v163h-52c-22.056,0-40-17.944-40-40v-212c0-0.2-0.003-0.399-0.009-0.597L243.946,55.26c7.493-6.363,18.483-6.339,25.947,0.055L422,208.425v113.083c0,11.046,8.954,20,20,20c11.046,0,20-8.954,20-20v-72.82l15.812,15.917c3.909,3.935,9.047,5.904,14.188,5.904c5.097,0,10.195-1.937,14.096-5.812C513.932,256.912,513.974,244.249,506.188,236.413z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></span>
                                <h5>Dashboard</h5>
                            </a>
                        </li>
                       
                        <li>
                            <a href="<?= base_url('admin/users') ?>">
                                <span><svg version="1.1" width="21px" height="21px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><g><path d="M437.02,330.98c-27.883-27.882-61.071-48.523-97.281-61.018C378.521,243.251,404,198.548,404,148C404,66.393,337.607,0,256,0S108,66.393,108,148c0,50.548,25.479,95.251,64.262,121.962c-36.21,12.495-69.398,33.136-97.281,61.018C26.629,379.333,0,443.62,0,512h40c0-119.103,96.897-216,216-216s216,96.897,216,216h40C512,443.62,485.371,379.333,437.02,330.98z M256,256c-59.551,0-108-48.448-108-108S196.449,40,256,40c59.551,0,108,48.448,108,108S315.551,256,256,256z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></span>
                                <h5>Users</h5>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>