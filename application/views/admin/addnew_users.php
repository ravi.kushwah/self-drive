<div class="admin_main">
    <?php 
    if(!empty($userData))    
    $plan = json_decode($userData[0]['plan']);
    // print_r(in_array("1", $plan));die;
    ?>
    <form class="form "><div class="row">
        <div class="col-xl-12 col-md-12">
            <div class="ad_box ad_mBottom30">
                <div class="form-group">
                    <label class="ad_label">Name</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="name"name="name" value="<?= !empty($userData) ? $userData[0]['name'] : '' ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Email</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control" id="email"name="email" value="<?= !empty($userData) ? $userData[0]['email'] : '' ?>" />
                        </div>
                    </div> 
                </div>
                <div class="form-group">
                    <label class="ad_label">Password</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <input type="text" class="form-control"name="password" id="password" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">Status</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="status" class="form-control" name="status" >
                                <option value="1" <?= !empty($userData) ? ( $userData[0]['status'] == 1 ? 'selected' : '' ) : ''?>>Active</option>
                                <option value="2" <?= !empty($userData) ? ( $userData[0]['status'] == 2 ? 'selected' : '' ) : ''?>>InActive</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="ad_label">JV User</label>
                    <div class="row">
                        <div class="col-xl-12">
                            <select id="status" class="form-control" name="user_type" >
                                <option value="0" <?= !empty($userData) ? ( $userData[0]['user_type'] == 0 ? 'selected' : '' ) : ''?>>No</option>
                                <option value="3" <?= !empty($userData) ? ( $userData[0]['user_type'] == 3 ? 'selected' : '' ) : ''?>>Yes</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="sc_checkbox_wrapper">
                    <div class="checkbox">
                        <input type="checkbox"  id="1" class="fe"name="OTO[]" value="1" <?= !empty($userData) ? ( in_array("1", $plan) == 1) ? 'checked' : '' : ''?>>
                        <label for="1">FE </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="2" class="fe"name="OTO[]" value="2" <?= !empty($userData) ? ( in_array("2", $plan) == 2 ? 'checked' : '' ) : ''?>>
                        <label for="2">OTO1 Unlimited </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="3" class="fe"name="OTO[]" value="3" <?= !empty($userData) ? ( in_array("3", $plan) == 3 ? 'checked' : '' ) : ''?>>
                        <label for="3">OTO2 Max </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="4" class="fe"name="OTO[]" value="4" <?= !empty($userData) ? ( in_array("4", $plan) == 4 ? 'checked' : '' ) : ''?>>
                        <label for="4">OTO3 Turbo </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="5" class="fe"name="OTO[]" value="5" <?= !empty($userData) ? ( in_array('5', $plan) == 5 ? 'checked' : '' ) : ''?>>
                        <label for="5">OTO4 Prime </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="6" class="fe"name="OTO[]" value="6" <?= !empty($userData) ? ( in_array('6', $plan) == 6 ? 'checked' : '' ) : ''?>>
                        <label for="6">OTO5 Agency </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="7" class="fe"name="OTO[]" value="7" <?= !empty($userData) ? ( in_array('7', $plan) == 7 ? 'checked' : '' ) : ''?>>
                        <label for="7">OTO6 Reseller </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="8" class="fe"name="OTO[]" value="8" <?= !empty($userData) ? ( in_array('8', $plan) == 8 ? 'checked' : '' ) : ''?>>
                        <label for="8">OTO7 Whitelabel </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="9" class="fe"name="OTO[]" value="9" <?= !empty($userData) ? ( in_array('9', $plan) == 9 ? 'checked' : '' ) : ''?>>
                        <label for="9">Pro VIP Pack </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="10" class="fe"name="OTO[]" value="10" <?= !empty($userData) ? ( in_array('10', $plan) == 10 ? 'checked' : '' ) : ''?>>
                        <label for="10">Unlimited Ninja </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="11" class="fe"name="OTO[]" value="11" <?= !empty($userData) ? ( in_array('11', $plan) == 11 ? 'checked' : '' ) : ''?>>
                        <label for="11">Max 20x </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="12" class="fe"name="OTO[]" value="12" <?= !empty($userData) ? ( in_array('12', $plan) == 12 ? 'checked' : '' ) : ''?>>
                        <label for="12">Turbo VIP </label>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox"  id="13" class="fe"name="OTO[]" value="13" <?= !empty($userData) ? ( in_array('13', $plan) == 13 ? 'checked' : '' ) : ''?>>
                        <label for="13">Prime Golden Member </label>
                    </div>
                </div>
                <input type="hidden" value="<?= !empty($userData) ? $userData[0]['id'] : 0 ?>"name="id" id="id">
                <input type="button" class="ad_btn ad_mTop15 bg-blue submitUserRecords" value="<?= !empty($userData) ? 'Update' : 'Save' ?>">
            </div>
        </div>
    </div>
</div></form>