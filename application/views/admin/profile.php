<div class="admin_main page-wrapper">
    <div class="main-content">
        <div class="row"><form class="form profile_form " method="post" enctype="multipart/form-data">
            <div class="col-xl-12">
                <div class="card sc_profile_section">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Profile Settings</h4>
                    </div>
                    <div class="card-body">
                        <div class="plr_profile_title">   
                            <div class="plr_profileimg">
                                <img class="priViewImg" src="<?= base_url() ?>assets/admin/images/<?php echo !empty($userList[0]['profile_image']) ? $userList[0]['profile_image'] :'profile.jpg';?>" alt=""/>
                                <input type="file" id="user_profile" class="plr_fileupload" name="profile_image">
                                <label  class="plr_uploade_label">
        							<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
        								<path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>
        							</svg></span>
        						</label>
                            </div>
                            <div class="media-body">
                                <h5 class="mb-1"><?= $_SESSION['firstname'] ?></h5>
                                <p><?= $_SESSION['email'] ?></p>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label class="form-label">First Name</label>
                                        <input type="text" class="form-control" placeholder="Enter first name"  name="name"value="<?= $userList[0]['name'] ?>" id="name">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <input type="text" class="form-control"readonly placeholder="Enter last email"name="email" value="<?= $userList[0]['email'] ?>" id="email">
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Password</label>
                                        <input type="password" class="form-control" placeholder="Enter password"name="password" id="pwd">
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="form-footer mt-2">
                                        <?php if($_SESSION['id'] == 4) { ?>
                                        <button class="btn btn-primary squer-btn">Disabled for JV Access</button>
                                        <?php } else { ?>
                                        <input type="button" class="btn btn-primary squer-btn saveProfile" value="Save Details">
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </form>