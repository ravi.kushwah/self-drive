<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<body class="wab_body" style="margin:0px; padding:0px;" bgcolor="#ffffff">
<p><b><?=$salutation;?></b></p>
    
<p><?=$title;?></p>
    
<p><?=$body;?></p>

<p>Note: For any query, please email us at <a href="mailto:tickets@self-drive-pro.p.tawk.email">tickets@self-drive-pro.p.tawk.email</a><br/><br/> To your success.<br/>Team, Self Cloud</p>
<!--<p>Please feel free to reach out to our suppot team if you have any troubles logging in or with the application.<br/> <a href="https://vineasx.helpscoutdocs.com/">Vinea SX Help Desk</a> or email us at <a href="mailto:support@vineasx.com">support@vineasx.com</a> <br/><br/></p>-->
    
<p>Copyright <?=date('Y').' <b style="color:#0180ff;"> '.$appName.'</b>.';?></p>
    </body>
</html>