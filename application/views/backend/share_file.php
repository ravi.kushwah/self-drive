<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<!-- Begin Head -->

<head>
    
    <title><?= $this->common_lib->siteTitle.((isset($pageName))?' | '.$pageName:''); ?> </title> 
    <?//= SITENAME ?><?//= $pageName != '' ? ' | '.$pageName : '' ?>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="MobileOptimized" content="320">
    <?php
        $time = time();
    ?>
    <!--Start Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/spectrum.min.css?<?=$time;?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/bootstrap.min.css?<?=$time;?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/datatables.css?<?=$time;?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/font-awesome.min.css?<?=$time;?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/icofont.min.css?<?=$time;?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/style.css?<?=$time;?>"/>
    <!--<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/dropzone.css?<?=$time;?>"/>-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/admin/js/toastr/toastr.min.css" />
    <!-- Favicon Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?<?=$time;?>"/>
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet"/>
    <link rel="shortcut icon" type="image/png" href="<?= $this->common_lib->siteFavicon;?>"/>
    
    <style> :root {--primary : <?=$this->common_lib->sitePrimaryColor;?>;--secondary : <?=$this->common_lib->siteSecondaryColor;?>;}</style>
</head>
<?php
    $userDetail =  $this->DBfile->get_data('usertbl',array('id'=>$this->session->userdata('id')));
    // echo"<pre>";print_R($userDetail);die;
?>
<body>
    <div class="loadingio-spinner-double-ring-baufz6bmrz loader">
        <div class="ldio-120u37vr0w5 spinner">
            <div></div>
            <div></div>
            <div><div></div></div>
            <div><div></div></div>
        </div>
    </div>
    <header class="header-wrapper main-header sp_share_url_header">
            <div class="header-inner-wrapper">
                <div class="header-right">
                    <!--<div class="header-left">-->
                    <!--    <div class="header-links">-->
                    <!--        <a href="javascript:void(0);" class="toggle-btn">-->
                    <!--            <span></span>-->
                    <!--        </a>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="sc_pages_title">
                    	<img src="<?=$this->common_lib->siteLogo;?>" class="">
                    </div>
                    <!--<div class="header-controls">-->
                    <!--    <div class="sc_share_private_url">-->
                    <!--        <a href="<?= base_url() ?>" class="ad-btn">Sign In </a>-->
                    <!--        <a href="<?= base_url() ?>home/signup" class="ad-btn">Sign Up </a>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
            </div>
        </header>
	<div class="sc_share_file_wrapper">
    	<div class="no_dash_box">
    		<img src="<?=base_url();?>assets/backend/images/bshare_file.png" alt="">
    		<h4>SelfCloud Private Share File</h4>
    		<form class="formPrivateFileUrl">
        		<div class="form-group">
    		        <label class="col-form-label">Plaase enter the password</label>
        		    <input type="text" placeholder="Enter password" name="file_password" class="form-control" id="file_password">
        		</div>
        		<div class="add-group">
        		    <input type="hidden" value="<?= $file_id?>" id="file_id" >
        		    <input type="hidden" value="<?= $conn_id?>" id="conn_id" >
        			<a href="javascript:;" class="ad-btn privateFileUrl">Submit</a>
        		</div>
    		</form>
    	</div>
    </div>
        <!--===Notification model===-->
    <div class="plr_notification">
        	<div class="plr_close_icon">
        		<span class='close'>×</span>
        	</div>
        	<div class="plr_success_flex">
        		<div class="plr_happy_img">
        			<img src=""/>
        		</div>
        		<div class="plr_yeah">
        			<h5></h5>
        			<p></p>
        		</div>
        	</div>
        </div> 
        <!--===Notification model===-->
    <!-- Script Start -->
    <script>window.baseurl = "<?= base_url() ?>"</script>
	<script src="<?= base_url() ?>assets/common/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/datatable/datatables.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/datatable/responsive/dataTables.responsive.min.js"></script>
  
    <script src="<?= base_url() ?>assets/backend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/spectrum.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/plupload.full.min.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/backend/js/chunk.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/admin/js/toastr/toastr.min.js"></script>
	<!-- Page Specific -->
<?php if($this->uri->segment(2) == 'autoresponder_integration'){?>
    <script src="<?= base_url() ?>assets/backend/js/ar_settings.js?<?= date('his') ?>"></script>
<?php }?>
    <!-- Custom Script -->
    <script src="<?= base_url() ?>assets/backend/js/cloudTransfer.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/backend/js/custom.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/backend/js/google_drive/custom.js?<?= date('his') ?>"></script>
    <script async defer src="<?= base_url() ?>assets/backend/js/google_drive/api.js"></script>
    <script async defer src="<?= base_url() ?>assets/backend/js/google_drive/client.js"></script>
    <script async defer src="<?= base_url() ?>assets/backend/js/google_drive/drive_login.js?<?= date('his') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/plupload/3.1.5/plupload.full.min.js" integrity="sha512-yLlgKhLJjLhTYMuClLJ8GGEzwSCn/uwigfXug5Wf2uU5UdOtA8WRSMJHJcZ+mHgHmNY+lDc/Sfp86IT9hve0Rg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>