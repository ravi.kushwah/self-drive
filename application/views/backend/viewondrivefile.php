<?php 
   // echo"<pre>";print_r($getAllFolder);die;
   ?>
<div class="page-wrapper">
   <div class="main-content">
      <div class="row">
         <div class="col xl-12 col-lg-12">
            <div class="page-connection-wrapper">
               <div class="sc_title_bar">
                  <h4><?=isset($connectionType)?$connectionType:''?></h4>
                  <p>You can manage all your shared files here.</p>
                  <p><?=isset($email)?$email:''?></p>
               </div>
               <div class="sc_share_btn">
                  <a href="javascript:;" id="<?php echo $this->uri->segment(3);?>" class="ad-btn folderBack">
                     <svg xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
                        <path d="M13.825,18.000 C10.772,18.000 7.928,16.517 6.217,14.033 C5.870,13.530 6.006,12.845 6.519,12.505 C7.032,12.165 7.729,12.297 8.076,12.801 C9.369,14.678 11.518,15.799 13.825,15.799 C17.647,15.799 20.757,12.749 20.757,9.000 C20.757,5.251 17.647,2.200 13.825,2.200 C11.512,2.200 9.359,3.326 8.067,5.212 C7.721,5.717 7.024,5.851 6.510,5.512 C5.996,5.173 5.859,4.489 6.205,3.985 C7.915,1.490 10.763,-0.000 13.825,-0.000 C18.884,-0.000 23.000,4.037 23.000,9.000 C23.000,13.963 18.884,18.000 13.825,18.000 ZM11.293,6.704 C10.902,6.313 10.902,5.679 11.293,5.287 C11.683,4.896 12.316,4.896 12.707,5.287 L15.706,8.291 C15.730,8.315 15.752,8.339 15.772,8.365 C15.777,8.371 15.782,8.377 15.787,8.384 C15.802,8.403 15.817,8.423 15.831,8.443 C15.835,8.449 15.838,8.455 15.842,8.462 C15.856,8.483 15.869,8.505 15.881,8.527 C15.883,8.532 15.886,8.537 15.888,8.541 C15.901,8.566 15.913,8.590 15.923,8.616 C15.925,8.619 15.925,8.622 15.927,8.626 C15.938,8.653 15.948,8.680 15.956,8.708 C15.957,8.712 15.958,8.715 15.959,8.719 C15.967,8.747 15.974,8.774 15.980,8.803 C15.982,8.812 15.982,8.820 15.984,8.829 C15.988,8.852 15.992,8.876 15.995,8.900 C15.998,8.933 16.000,8.966 16.000,9.000 C16.000,9.033 15.998,9.067 15.995,9.100 C15.992,9.124 15.988,9.148 15.984,9.172 C15.982,9.180 15.982,9.189 15.980,9.197 C15.974,9.226 15.967,9.254 15.959,9.282 C15.958,9.285 15.957,9.288 15.956,9.292 C15.948,9.320 15.938,9.348 15.927,9.375 C15.925,9.378 15.925,9.381 15.923,9.384 C15.913,9.410 15.901,9.435 15.888,9.459 C15.886,9.464 15.883,9.468 15.881,9.473 C15.869,9.495 15.856,9.517 15.842,9.539 C15.838,9.545 15.835,9.551 15.831,9.557 C15.817,9.577 15.802,9.597 15.787,9.616 C15.782,9.623 15.777,9.629 15.772,9.636 C15.752,9.661 15.730,9.685 15.707,9.708 L12.707,12.712 C12.511,12.908 12.256,13.006 12.000,13.006 C11.744,13.006 11.488,12.908 11.293,12.712 C10.902,12.321 10.902,11.688 11.293,11.296 L12.586,10.001 L1.000,10.001 C0.448,10.001 -0.000,9.553 -0.000,9.000 C-0.000,8.447 0.448,7.998 1.000,7.998 L12.586,7.998 L11.293,6.704 Z" class="cls-1"></path>
                     </svg>
                     Back
                  </a>
                  <a href="javascript:;"  class="ad-btn UploadFile">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M4 8c0 .6.4 1 1 1h2c.6 0 1-.4 1-1V5h2L6 0 2 5h2v3zm5.4 1c-.3.5-.8 1-1.4 1H4c-.7 0-1.4-.4-1.7-1H0v3h12V9H9.4zm1.6 2h-1v-1h1v1z"/>
                     </svg>
                     Upload File
                  </a>
                  <a href="javascript:;" class="ad-btn CreateFolderonedrive">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M11 3H7V2c0-.6-.4-1-1-1H1v9c0 .6.4 1 1 1h9c.6 0 1-.4 1-1V4c0-.6-.4-1-1-1zM9 7H7v2H6V7H4V6h2V4h1v2h2v1z"/>
                     </svg>
                     New Folder
                  </a>
               </div>
               <div class="sc_share_btn checkBoxSelect d-none">
                  <a hidden href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">
                        <path fill-rule="evenodd"
                           d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>
                     </svg>
                     Copy To
                  </a>
                  <a  href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                        <path fill-rule="evenodd"
                           d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                     </svg>
                     Download
                  </a>
                  <a href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                     </svg>
                     Share
                  </a>
                  <a hidden href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                     </svg>
                     Move
                  </a>
                  <a href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                     </svg>
                     Delete
                  </a>
                  <a hidden href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                        <path fill-rule="evenodd"
                           d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                     </svg>
                     Preview
                  </a>
                  <a href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
                        <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>
                     </svg>
                     Rename
                  </a>
                  <a hidden href="javascript:;" class="ad-btn">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>
                     </svg>
                     Copy
                  </a>
               </div>
               <div class="sc_share_main_section">
                  <?php $i=1;
                //   print_r($getAllFolder);
                //   die();
                        foreach($getAllFolder['data'] as $GetFolder){
                         
                         if($connectionType=="OneDrive"){
                        //   echo"<pre>";print_r($GetFolder['id']);
                            $viewId = '';
                            if(isset($GetFolder['file'])){
                                $fileTag = 'file'; // folder
                            }else{
                                $fileTag = 'folder'; // folder
                            }
                         }
                         
                         $c = $i++;
                             ?> 
                    <div class="sc_share_wrapper" option="option<?php echo  $c;?>">
                        <div class="checkbox" hidden>
                            <input type="checkbox" id="takenBefore" placeholder="Yes">
                        <!--<input type="checkbox" id="rememberme<?php echo  $c; ?>" name=""class="optionCheckBox" value="" >-->
                            <label for="rememberme<?php echo  $c;?>" checkBoxValue="rememberme<?php echo  $c;?>" class="option"></label>
                        </div>
                        <div class="sc_share_img sc_folder_img onegetSubFolderByFolderId" type="<?= $connectionType?>" id="<?php echo $this->uri->segment(3);?>" data-folder-name="<?= $GetFolder['name'];?>"  droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>" data-folder-Id="<?php echo isset($GetFolder['id']) ? $GetFolder['id'] : '';?>">
                         <img src="<?php echo base_url('assets/backend/images/'.($fileTag).'.svg');?>" alt="icon">
                            <!--<img src="<?php echo base_url();?>assets/backend/images/folder.png" alt="icon">-->
                        </div>
                        <div class="sc_share_detail onegetSubFolderByFolderId"  type="<?= $connectionType?>" id="<?php echo $this->uri->segment(3);?>" data-folder-name="<?= $GetFolder['name'];?>" droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>"  data-folder-Id="<?php echo isset($GetFolder['id']) ? $GetFolder['id'] : '';?>">
                        <!--<span>July 29, 2022</span>-->
                            <h5 ><?= $GetFolder['name'];?></h5>
                        <!--<p>Size -2 mb</p>-->
                        </div>
                        <div class="checkOptionMenu tbf_IconHolter option<?php echo  $c;?>" clickValue="option<?php echo  $c;?>" style="display:none;">
                            <div class="checkBoxSelect">
                               <a hidden href="javascript:;">
                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">
                                     <path fill-rule="evenodd"
                                        d="M12.112,4.707 L8.331,0.902 C7.962,0.532 7.364,0.532 6.995,0.902 C6.816,1.082 6.718,1.321 6.718,1.575 C6.718,1.829 6.816,2.068 6.995,2.247 L9.162,4.428 L1.363,4.428 C0.841,4.428 0.417,4.855 0.417,5.379 L0.417,11.720 C0.417,12.244 0.841,12.671 1.363,12.671 C1.884,12.671 2.308,12.244 2.308,11.720 L2.308,6.330 L9.162,6.330 L6.995,8.511 C6.816,8.691 6.718,8.930 6.718,9.184 C6.718,9.438 6.816,9.677 6.995,9.856 C7.364,10.228 7.962,10.228 8.331,9.856 L12.112,6.052 C12.479,5.681 12.479,5.078 12.112,4.707 Z"/>
                                  </svg>
                                  Copy To
                               </a>
                               <a hidden href="javascript:;">
                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                     <path fill-rule="evenodd"
                                        d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                  </svg>
                                  Download
                               </a>
                               <a hidden href="javascript:;">
                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                     <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                  </svg>
                                  Share
                               </a>
                               <a hidden href="javascript:;" >
                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                     <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                  </svg>
                                  Cut
                               </a>
                               <a  href="javascript:;" class="oneDeletFileAndFolder" user_id="<?php echo $this->uri->segment(3);?>" folderID="<?php echo isset($GetFolder['path_lower'])?$GetFolder['path_lower'] :$GetFolder['id'];?>" >
                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                     <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                  </svg>
                                  Delete
                               </a>
                               <a hidden href="javascript:;" >
                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                     <path fill-rule="evenodd"
                                        d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                  </svg>
                                  Preview
                               </a>
                               <a hidden href="javascript:;" >
                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
                                     <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>
                                  </svg>
                                  Rename
                               </a>
                               <a hidden href="javascript:;" >
                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                     <path d="M8.5 0h-8v10h2V1h6V0zm3 2v2l-2-2h-1v3h3V2zm-4 0h-4v10h8V6h-4V2z"/>
                                  </svg>
                                  Copy
                               </a>
                            </div>
                        </div>
                    </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--<div class="modal fade " id="CreateFolder" tabindex="-1" role="dialog" aria-hidden="true">-->
<!--   <div class="modal-dialog modal-dialog-centered" role="document">-->
<!--      <div class="modal-content">-->
<!--         <div class="modal-header">-->
<!--            <h5 class="modal-title" id="add_customer_title">Create Folder</h5>-->
<!--            <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--            <span aria-hidden="true">&times;</span>-->
<!--            </button>-->
<!--         </div>-->
<!--         <form class="form formCreateFolder">-->
<!--            <div class="modal-body">-->
<!--               <div class="form-group">-->
<!--                  <label class="col-form-label">Create Folder</label>-->
<!--                  <input type="text" placeholder="Enter Folder Name" value="" class="form-control custAddCls" id="folder_name">-->
<!--               </div>-->
<!--            </div>-->
<!--            <div class="modal-footer">-->
<!--               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="user_id" >-->
<!--               <input type="button" class="btn ad-btn UserCreateFolder" value="Create Folder">-->
<!--            </div>-->
<!--         </form>-->
<!--      </div>-->
<!--   </div>-->
<!--</div>-->
<div class="modal fade " id="Renamefiles" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">File Rename</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form FormfileName">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">File Rename </label>
                  <input type="text" placeholder="Enter Your File Name" value="" class="form-control custAddCls" id="fileName">
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="user_id" >
               <input type="button" class="btn ad-btn RenameFile" value="Rename File ">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade " id="ShareURL" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Share URL</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form FormfileName">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">Share File URL  </label>
                  <input type="text" class="form-control ShareUrlLink" id="copyTarget" value="Text to Copy" readonly>
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="user_id" >
               <input type="button" class="btn ad-btn CopyMe" id="copyButton" value="Copy URL">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade " id="OneDriveCreateFolder" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Create Folder</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form formCreateFolder">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">Create Folder</label>
                  <input type="text" placeholder="Enter Folder Name" value="" class="form-control custAddCls" id="folder_name">
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="user_id" >
               <input type="button" class="btn ad-btn OneDriveCreateFolder" value="Create Folder">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="moveFile" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Move File</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <!--<a href="javascript:;" id="<?php echo $this->uri->segment(3);?>" class="ad-btn MoveBackFolder">          Back            </a>-->
         </div>
         <div class="MovingFile sc_dash_move_list">
             
         </div>
         <form class="form moveFileForm">
            <div class="modal-body">
                <input type="text" name="" value="" id="fileId" hidden>
                <input type="text" name="" value="" id="UserID" hidden>
            </div>
          <div class="form-group MoveProgressBar">
              <div class="progress hide">
                 <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"></div>
              </div>
           </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="user_id"  id="user_id">
               <input type="button" class="btn ad-btn MoveUploadFile" value="Move File">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="UploadFile" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Drive Upload File</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">Upload File</label>
                  <div class="plr_upload_box">
                     <input type="file" placeholder="Enter Folder Name" name="file" class="form-control UploadFile" id="UploadFile_Name">
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                        <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class="cls-1"></path>
                     </svg>
                  </div>
               </div>
               <div class="form-group">
                  <div class="progress hide">
                     <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"></div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="user_id"  id="user_id">
               <input type="button" class="btn ad-btn CloudUploadFile" value="Upload">
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="fileView" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog sc_modal_large modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Priview File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="sc_iframe_box" id="AllFilePreview">
                </div>
             </div>
        </div>
    </div>
</div>