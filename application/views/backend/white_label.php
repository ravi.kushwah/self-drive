<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="col-lg-12">
				<div class="card">
                    <div class="card-content">
                        <div class="card-body">
                           <div class="mfh-machine-profile">
								<ul class="nav nav-tabs" id="myTab1" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="home-tab1" data-toggle="tab" href="#home0" role="tab" aria-controls="home" aria-selected="true">White Label</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="profile-tab20" data-toggle="tab" href="#profile0" role="tab" aria-controls="profile" aria-selected="false">White Label Users</a>
									</li>
								</ul>
								<div class="tab-content ad-content2" id="myTabContent2">
									<div class="tab-pane fade show active" id="home0" role="tabpanel" >
                                        <form action="" method="post" id="whiteLableData">
    									    <div class="sc_BasicDetails">
                                                <h4>Basic Details</h4>
                                            </div>
    										<div class="row">
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">Title</label>
                                                        <input type="text" class="form-control require" placeholder="Enter Here Title" name="title" value="<?=(!empty($whiteLabelData[0]['title'])? $whiteLabelData[0]['title']:'');?>">
                                                    </div>
                                                </div>
                                               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="ao_label ao_label_list">Keywords
                                                            <span><img src="<?=base_url()?>assets/backend/images/info_icon.svg" alt="">
                                                                <div class="ao_tooltip_component">
                                                                    <p> You can add Keywords .</p>
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <input type="text" class="form-control require" placeholder="Enter Here Keyword" name="keyword" value="<?=(!empty($whiteLabelData[0]['keyword'])? $whiteLabelData[0]['keyword']:'');?>">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">Author Name</label>
                                                        <input type="text" class="form-control require" placeholder="Enter Here Author Name" name="author_name" value="<?=(!empty($whiteLabelData[0]['author_name'])? $whiteLabelData[0]['author_name']:'');?>">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">Domain Name</label>
                                                        <a href="javascript:;" class="ao_skipfor_now" data-toggle="modal" data-target="#DomainModal">How To Add Domain?</a>
                                                        <input type="text" id="myAddonDomain" class="form-control require" placeholder="Please add and validate domain" name="domain_name" value="<?=(!empty($whiteLabelData[0]['domain_name'])? $whiteLabelData[0]['domain_name']:'');?>">
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Description</label>
                                                        <!--<textarea class="form-control" placeholder="Enter Description" name=""></textarea>-->
                                                        <textarea rows="6" class="form-control require ao_textarea" placeholder="Enter Here Description" name="description"><?=(!empty($whiteLabelData[0]['description'])? $whiteLabelData[0]['description']:'');?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-12">
                                                    <div class="plr_upload_wrapper">
                                                        <label class="col-form-label">Upload Logo</label>
                                                        <div class="plr_upload_box">
                                                            <input class="checkProductImages" type="file" name="wl_logo" onchange="readURL(this)" >
                                                            <div class="plr_upload_icon">
                                                                <img src="<?= (!empty($whiteLabelData[0]['wl_logo'])? base_url('/assets/backend/images/white_label/'.$whiteLabelData[0]['wl_logo']):base_url('assets/auth/images/logo.png'));?>" class="">
                                                            </div>
                                                            <div class="plr_upload_detail">
                                                                <h6>Supports: JPG, JPEG, PNG</h6>
                                                                <p>Recommended Size - 180x47 px</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3 col-lg-12">
                                                    <div class="plr_upload_wrapper">
                                                        <label class="col-form-label">Upload Favicon</label>
                                                        <div class="plr_upload_box">
                                                            <input class="checkProductImages" type="file" name="wl_fav_icon" onchange="readURL(this)" >
                                                            <div class="plr_upload_icon plr_upload_favicon">
                                                                <img src="<?= (!empty($whiteLabelData[0]['wl_fav_icon'])? base_url('/assets/backend/images/white_label/'.$whiteLabelData[0]['wl_fav_icon']):base_url('assets/auth/images/favicon.png'));?>" class="">
                                                            </div>
                                                            <div class="plr_upload_detail">
                                                                <h6>Supports: JPG, JPEG, PNG</h6>
                                                                <p>Recommended Size - 51x51 px</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12">
                                                    <div class="form-group ao_colorpicker_dv" data-id="WhiteColor1">
                                                        <label class="col-form-label">Primary Color</label>
                                                        <input type="text" class="ao_colorPicker form-control require"  value="<?=(!empty($whiteLabelData[0]['wl_primary_color'])? $whiteLabelData[0]['wl_primary_color']:'#5886ff');?>" name="wl_primary_color" id='WhiteColor1'/>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-12">
                                                    <div class="form-group ao_colorpicker_dv" data-id="WhiteColor2">
                                                        <label class="col-form-label">Secondary Color</label>
                                                        <input type="text" class="ao_colorPicker form-control require"  value="<?=(!empty($whiteLabelData[0]['wl_secondary_color'])? $whiteLabelData[0]['wl_secondary_color']:'#29294c');?>" name="wl_secondary_color" id='WhiteColor2'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                             $smtp_details = !empty($whiteLabelData) && $whiteLabelData[0]['smtp_details'] != '' ? json_decode($whiteLabelData[0]['smtp_details'] , true) : [];
                                            ?>
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-12">
                                                    <div class="sc_BasicDetails d-block">
                                                        <h4>SMTP Details</h4>
                                                        <p>Please note: To send default notification emails on your behalf to your Whitelabel users you must add SMTP details received from your email service provider.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">From Name</label>
                                                        <input type="text" class="form-control require" name="from_name" placeholder="Enter From Name"  value="<?= isset($smtp_details['from_name']) ? $smtp_details['from_name']:''; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">From Email</label>
                                                         <input type="text" class="form-control require email" data-valid="email" data-error="Email should be valid." name="from_email" placeholder="Enter From Email" value="<?= isset($smtp_details['from_email']) ? $smtp_details['from_email']:''; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">SMTP Host</label>
                                                        <input type="text" class="form-control require" name="smtp_host" placeholder="Enter SMTP Host Name"  value="<?= isset($smtp_details['smtp_host']) ? $smtp_details['smtp_host']:''; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">Port</label>
                                                        <input type="number" class="form-control require" name="port" placeholder="Enter Port" value="<?= isset($smtp_details['port']) ? $smtp_details['port']:''; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">User Name</label>
                                                        <input type="text" class="form-control require" name="smtp_user" placeholder="Enter User Name" value="<?= isset($smtp_details['smtp_user']) ? $smtp_details['smtp_user']:''; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div class="form-group plr_require">
                                                        <label class="form-label">Password</label>
                                                        <input class="form-control require" type="password" name="smtp_pass" placeholder="Enter Password"  value="<?= isset($smtp_details['smtp_pass']) ? $smtp_details['smtp_pass']:''; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group ao_require">
                                                        <input type="hidden" value="<?=(!empty($whiteLabelData[0]['id']) ? $whiteLabelData[0]['id']:'');?>" name="id" >
                                                        <button type="button" class="ad-btn" id="submitWhiteLabelData">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
									</div>
									<div class="tab-pane fade" id="profile0" role="tabpanel">
										<div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="chart-card">
                                                    <div class="sc_BasicDetails">
                                                        <h4>Users Details</h4>
                                                        <a href="javascript:;" class="ad-btn whitelabelUserAdd" id="0">Add New</a> 
                                                    </div>
                                                    <div class="plr_data_table">
                                                        <table id="example" class="table table-striped table-bordered dt-responsive nowrap server_datatable " data-source="<?=base_url()?>home/user_data/white_label" style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Name</th>
                                                                    <th>Email</th>
                                                                    <th>Status</th>
                                                                    <th>Edit</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
        <!-- Dashboard Start -->
        
        
        
<div class="modal fade" id="whitelabelUserAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_whitelabel_title">Add White Label User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form">
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Name </label>
                    <input type="text" placeholder="Name" name="name" class="form-control whitelabelUserAddCls require" id="name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Email</label>
                    <input type="text" placeholder="Email" data-valid="email" data-error="Email should be valid." name="email" class="form-control whitelabelUserAddCls require" name="email" id="email">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <input type="password" placeholder="Enter Password" name="password" class="form-control whitelabelUserAddCls require" id="password">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Confirm Password</label>
                    <input type="password" placeholder="Enter Confirm Password" name="cnf_password" class="form-control whitelabelUserAddCls require" id="cnf_password">
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <input type="checkbox" id="sendMail" class="" name="send_on_mail">
                        <label for="sendMail">Send login details on email  </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer sc_google_btn">
                <input type="hidden" value="wl_user" name="type" id="type">
                <input type="hidden" value="" name="user_id" id="user_id">
                <button type="button" class="btn ad-btn" id="submitWhiteLabelUserData" >Submit</button>
            </div>
         </form>
        </div>
    </div>
</div>