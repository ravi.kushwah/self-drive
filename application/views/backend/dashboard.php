<?php
    $userDetail =  $this->DBfile->get_data('usertbl',array('id'=>$_SESSION['id']));
?>
<div class="page-wrapper">
    <div class="main-content">
        <?php if(empty($my_cloud)){?>
                <div class="sc_no_dashboard">
                    <div class="no_dash_box">
                        <img src="<?= base_url() ?>assets/backend/images/no_dash.png" alt="image">
                        <h4>No Connected Drives</h4>
                        <p>You haven’t connected any accounts yet.</p>
                        <div class="add-group">
            				<a href="<?= base_url() ?>home/connection" class="ad-btn">add cloud</a>
            			</div>
            		</div>	
                </div>
                <?php
            }else{
                ?>
                <div class="row">
                    <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-title-wrapper">
                            <div class="page-title-box ad-title-box-use">
                                <h4 class="page-title">My Cloud Drives</h4>
                            </div>
                            <?php 
                                if(isset($_SESSION['task_count']) && $_SESSION['task_count'] >= 100 ){
                    		      
                                   ?><p class="sc_daily_limit">Your daily limit of Uploading &amp; downloading files has been exceeded.</p><?php
                    		    }
                            ?>
                            <div class="ad-breadcrumb">
                                <ul>
                                    <li>
        								<div class="add-group">
        									<a href="<?= base_url() ?>home/connection" class="ad-btn">add cloud</a>
        								</div>
        							</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dashboard Start -->
                <div class="sc_dash_main_section">
                    <?php 
                        if(isset($my_cloud)){
                            foreach($my_cloud as $connectData){
                        //   print_r($connectData['user_info']);
                                if($connectData['connection_type'] == 'GoogleDrive'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    if($userData['image'] != ''){
                                        // $img = $userData['image'];
                                        $img = base_url('assets/backend/images/icon1.png');
                                    }else{
                                        $img = base_url('assets/backend/images/icon1.png');
                                    }
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = $userData['usedSpace']/1073741824;
                                    $totalSpace = $userData['totalSpace']/1073741824 ;
                                }else
                                if($connectData['connection_type'] == 'DropBox'){
                                    $userData = json_decode( $connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/dropbox.png');
                                // echo"<pre>";print_r($userData);die;
                                    // $nameType = json_decode( $userData['name'], true );
                                    $name = $userData['name']['display_name']; 
                                    $email = $connectData['email'];
                                    $usedSpace = $userData['usedSpace']/1073741824;
                                    $totalSpace = $userData['totalSpace']/1073741824 ;
                                }else
                                if($connectData['connection_type'] == 'Ddownload'){
                                    $userData = json_decode( $connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/ddownload.png');
                                    $nam = explode('@',$connectData['email']);
                                    $name = $nam[0]; 
                                    $email = $connectData['email'];
                                    $usedSpace = $userData['storage_used']/1073741824;
                                    // $totalSpace = $userData['']/1073741824 ;
                                    $totalSpace = 10 ;
                                }else
                                if($connectData['connection_type'] == 'Box'){
                                    $userData = json_decode( $connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon8.png');
                                    $name = $userData['name']; 
                                    $email = $connectData['email'];
                                    $usedSpace = isset($userData['space_used']) ? $userData['space_used']:'0'/1073741824;
                                    $totalSpace = isset($userData['space_amount']) ? $userData['space_amount']:'0'/1073741824 ;
                                }else
                                if($connectData['connection_type'] == 'OneDrive'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/onedrive.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0/1073741824;
                                    $totalSpace = 5;
                                }else
                                if($connectData['connection_type'] == 's3'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon18.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0/1073741824;
                                    $totalSpace = 5;
                                }else if($connectData['connection_type'] == 'Anon'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon_1.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0;
                                    $totalSpace = 5;
                                }else if($connectData['connection_type'] == 'GooglePhoto'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon6.png');
                                    $name = $userData['name'];
                                    $email = $userData['email'];
                                    $usedSpace = 0;
                                    $totalSpace = 15;
                                }else
                                if($connectData['connection_type'] == 'Backblaze'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon26.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0/1073741824;
                                    $totalSpace = 5;
                                }else
                                if($connectData['connection_type'] == 'Gofile'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon31.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0/1073741824;
                                    $totalSpace = 5;
                                }else
                                if($connectData['connection_type'] == 'Ftp'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon7.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0/1073741824;
                                    $totalSpace = 5;
                                }else
                                if($connectData['connection_type'] == 'Pcloud'){
                                    $userData = json_decode($connectData['user_info'], true );
                                    $img = base_url('assets/backend/images/icon3.png');
                                    $name = $userData['name'];
                                    $email = $connectData['email'];
                                    $usedSpace = 0/1073741824;
                                    $totalSpace = 5;
                                }
                    ?>
                                <div class="sc_dash_box_list">
                                    <!-- Start Card-->
                                   <a href="<?php echo base_url();?>home/cloud_details/<?php echo $connectData['id'];?>">
                                        <div class="ad-info-card">
                                            <div class="mt_close_btn removeConnection" connectionID="<?php echo $connectData['id'];?>">
            									<span></span>
            								</div>
                                            <div class="card-body dd-flex align-items-center">
                                                <div class="icon-box-wrapper">
                                                    <img src="<?= $img;?>" alt="image">
                                                    <!--<img src="<?= base_url() ?>assets/backend/images/icon1.png" alt="icon">-->
                                                </div>
                                                <div class="icon-info-text">
                                                    <h5><?= $name;?></h5>
                                                    <h6><?= $email;?></h6>
                                                    <h6 hidden><?= (isset($usedSpace)?round($usedSpace,2).'GB':0)?>/<?= (isset($totalSpace)?round($totalSpace).'GB':0)?></h6>
                                                     <div class="progress"hidden>
                                                        <div class="progress-bar" style="width:70%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>  
                    <?php          
                            }
                        }
                    ?>
                </div> 
                <?php
            }
        ?>