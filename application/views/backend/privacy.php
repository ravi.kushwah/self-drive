
<html>
<head>
	<title>SelfDrive - Privacy Policy</title>
  <meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML, CSS, JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Red+Hat+Display:400,500,700,900&amp;display=swap" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="https://selfdrive.work/assets/auth/images/favicon.png">
  <style>
	  h3 {
		font-size: 23.5pt;
		font-family: Arial;
		color: #333333;
		background-color: transparent;
		font-weight: bold;
		font-style: normal;
		font-variant: normal;
		text-decoration: none;
		vertical-align: baseline;
		white-space: pre-wrap;
		margin-bottom: 0px;
	}
	.te_page_box {
		font-family: Arial;
		color: #444444;
		font-weight: 500;
		width: 1150px;
		margin: 50px auto;
		line-height: 1.38;
	}
	body {
    margin: 0;
    padding: 0;
    font-family: var(--font-family);
    color: var(--light_color);
    background-color: #f0f7fd;
    font-size: 14px;
    letter-spacing: 0.4px;
    font-weight: 500;
    overflow-x: hidden;
}
:root {
    --main_color: #0180ff;
    --light_main_color: #e3f7fe;
    --light_color: #a1a3ce;
    --dark_color: #3f3f59;
    --black_color: #11122e;
    --font-family: "Be Vietnam", sans-serif;
}

.te_page_header {
    padding: 25px 40px;
    display: flex;
    margin-left: 285px;
    justify-content: space-between;
    align-items: center;
    background-color: #ffffff;
    box-shadow: 0px 0px 30px 0px rgb(221 227 236 / 5%);
    margin: 40px auto 40px;
    border-radius: 12px;
}

.te_page_header .mh_btn {
    display: inline-block;
    text-align: center;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    border: none;
    font-size: 14px;
    color: #ffffff;
    font-weight: 600;
    text-transform: capitalize;
    background: var(--main_color);
    border-radius: 5px;
    cursor: cursor;
    text-decoration: none;
}

.te_page_inner {
        padding: 25px 40px;
    justify-content: space-between;
    align-items: center;
    background-color: #ffffff;
    box-shadow: 0px 0px 30px 0px rgb(221 227 236 / 5%);
    margin: 40px auto 40px;
    border-radius: 12px;
    position: relative;
    padding-top: 100px;	
}
.te_page_inner h1 {
    font-weight: 700;
    text-transform: capitalize;
    color: var(--dark_color);
    margin: 0 0 30px;
    background: #fafcff;
    border-bottom: 1px solid #e5e7f7;
    padding: 30px 40px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    font-size: 25px;
    border-radius: 10px 10px 0 0;
}
.te_page_inner h3 {
    font-size: 20px;
}
ul.page_links {
    display: flex;
    align-items: center;
    justify-content: center;
    list-style: none;
    padding: 0;
    margin: 0;
}
ul.page_links li {
    margin: 0 10px;
}
ul.page_links li a {
    color: #0180ff;
    font-size: 16px; 
    font-weight: 700;
}
  </style>
</head>
<body>
	<div class="te_page_box">
	    <div class="te_page_header">
			<div class="te_page_logo">
				<a href="https://selfdrive.work/" class="mh_logo" target="_blank"><img src="https://selfdrive.work/assets/auth/images/logo.png" class=""></a>
			</div>
			<div class="te_page_button">
				<a href="https://selfdrive.work/" target="_blank" class="mh_btn">login now</a> 
			</div>
		</div>
		<div class="te_page_inner">
			<h1>Privacy Policy</h1>
            <p>We at SelfDrive understand your concern about how we share, secure and use the information you have shared with us when you visit the website <a href="https://selfdrive.work/"></a> (our “Website”) and use our services. Our Privacy Policy makes it clear how that information is handled at our end, and our practices for collecting, using, maintaining, protecting and disclosing that information. We adhere to what we say in this Privacy Policy.</p>
            <p>Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, you can choose not to use our Website. By accessing or using our Website, you agree to this Privacy Policy. This policy may change from time to time. Your continued use of the Website after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.</p>
            <p><b>Notes:</b> After authorization, SelfDrive accesses the data in the user's cloud drive (Google Drive, Google Photos, OneDrive, Dropbox, etc.) without saving the contents. All operations on the user data are based-on the API interface provided by the cloud disk. Therefore, SelfDrive plays just as a bridge to connect one cloud storage to another.</p>
            
            <h3>01. INFORMATION WE COLLECT FROM YOU</h3>
            <p><b>Customer-Provided Information:</b>  To use SelfDrive services, you may first download the SelfDrive application (the “App”). To download the App, you will be requested to provide certain information. We will retain your email identification provided at the time of sign-up or App download, email messages we receive from you and the responses given to you by SelfDrive. If you recommend someone to SelfDrive, we will use the provided email address exclusively to send an invitation to that person.</p>
           
            <p><b>Cookies Information:</b> When you visit SelfDrive.com, we send one or more cookies (a text file including a few alphanumeric characters) to your system to identify your browser. Cookies we send do not pick-up any of your personal information. SelfDrive sends two types of cookies – persistent cookies (those that continue to exist in your hard drive even after closing your browser) and the session cookies (temporary cookies that get deleted after closing your browser). Persistent cookies are used by your browser for subsequent visits to SelfDrive. Your web browser can be reset to reject SelfDrive cookies or to alert you when a cookie is sent to the browser. Rejecting the cookies may not allow all features of our Website to function properly.</p>
            
            <h3>4. Other disclosures</h3>
            <p><b>Log File Information:</b> Your browser sends certain information to our servers every time you access the Website. This information is automatically recorded by our servers. The server log file includes information such as IP address, browser type, web request from your browser, referring or exit pages, number of clicks, landing pages, URLs, pages viewed and other related information.</p>
            <p><b>Clear Gifs Information:</b> When you use the SelfDrive Website, we use clear gifs (also known as Web Beacons) to keep track of your online usage pattern and to check which of the emails sent by us are opened by you. Our Web Beacons do not collect any of your personal information from your SelfDrive account. Information collected by clear gifs enables us to assess and improve marketing strategies and make SelfDrive serve you in a better way.</p>
            
        
            <h3>02. HOW SELFDRIVE USES THE INFORMATION THAT WE COLLECT</h3>
            <p><b>Automatically Recorded Information:</b> SelfDrive uses automatically recorded information only for the internal purpose of managing our Website and providing you a better user experience. SelfDrive uses clear gifs, cookies and log file content to:<br>. Retain information about you so as to prevent you from re-entering the information every time you view the web page<br>. Provide personalized information<br>. Keep track of the efficiency of our service<br>. Analyze the total number of visitors to the Website and the traffic<br>Identify and resolve technical problems reported by customers or engineers related to IP addresses regulated by a specific web company or an internet service provider</p>
            <p><b>User-Provided Information:</b> We may convert or combine some personal information of users into de-identified or aggregated data that does not disclose any of the personal information of any individual user. We may use any such de-identified or aggregated data without restriction.</p>
            <p>If you purchase products or services, we may transmit your personal information to payment processors. We may also use your personal information to carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection.</p>
            <p>Providing your email address to SelfDrive means that you agree to receive notices at your email address. You can choose not to receive any type of updates to the service, special offers, newsletters and other message from SelfDrive or alter the frequency of service-related communications by selecting the unsubscribe link included at the end of every email that we send to you. Users who prefer not to receive messages from SelfDrive can unsubscribe or alter mailing priorities by posting a request to us. Unsubscribing prevents you from obtaining email messages related to special offers and updates to the service.</p>
            <p>We will retain your information for as long as your account is active and for a short period thereafter in case you choose to reactivate your account. If you wish to cancel your account or request that we no longer use your information to provide you services, you may delete your account. We may retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. Consistent with these requirements, we will try to delete your information quickly upon request. Please note, however, that there might be latency in deleting information from our servers and backed-up versions might exist after deletion.</p>
            
            
            <h3>03. HOW SELFDRIVE SHARES YOUR INFORMATION</h3>
            <p><b>Personally Identifiable Information:</b> We may convert or combine some personal information of users into de-identified or aggregated data that does not disclose any of the personal Information of any individual user. We may disclose any such de-identified or aggregated data without restriction.</p>
            <p>We may disclose personal information to our subsidiaries and affiliates and to any contractors, service providers and other third parties who need to know such information to support our permitted uses of personal information. We may disclose such personal information to a buyer or other successor to our business in the event of a sale of equity or assets, reorganization, merger or a similar corporate transaction.</p>
            <p>We may also disclose your personal information to comply with any court order, law or legal process, including to respond to any government or regulatory request and to enforce or apply our Terms of Use, including for billing and collection purposes, or if we believe disclosure is necessary or appropriate to protect the rights, property, or safety of our customers or others. This includes exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction.</p>
            <p>Except as provided above, SelfDrive does not sell, rent or disclose your personally identifiable information to third parties.</p>
            <p><b>Non-Personally Identifiable Information:</b> We share non-personally identifiable information such as exit pages, URL’s, number of clicks, platform types and similar information with third parties to enable them understand how to use our services.</p>
            
            
            <h3>04. SOCIAL MEDIA AND WIDGETS</h3>
            <p>If you log onto the Website through Facebook or a similar social media site, certain information about you will be transferred to the Website, and the fact that you have logged onto the Website and some of that transferred information may be available to other users of the social media site. We will restrict access to any such transferred information about you to the extent required by the policies of any such social media site, but all transferred information will otherwise be subject to the terms of this policy.</p>
            <p>You may be able to use social media widgets such as the Facebook Like button on the Website. These widgets will collect your IP address and identify which page you are visiting on the Website, and make your preference information available to third parties. The information collected by such widgets is controlled by third party privacy policies.</p>
            
            <h3>05. OTHER WEB SITES LINKED TO SELFDRIVE</h3>
            <p>Our Privacy Policy is no longer effective with respect to information that you disclose on any website linked to the SelfDrive Website. Your actions at other websites that are linked to our Website are subject to that website’s policies and terms, and you should review those terms and policies before disclosing personal information.</p>
            
            <h3>06. HOW SELFDRIVE PROTECTS USER INFORMATION</h3>
            <p>We use technical, physical and managerial safeguards to maintain the security and reliability of your information. Unfortunately, the transmission of information over the Internet is not completely secure. Although we have implemented security measures that we think are adequate, we cannot guarantee the security of your personal information transmitted to our Website. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on the Website, and we cannot ensure the information transferred to us is not accessed, changed, disclosed or deleted in case of a security breach of any of our technical, managerial or physical safeguards. We will notify you in the manner required by applicable law if your personal information is compromised by a security breach</p>
            
            <h3>07. USERS OUTSIDE OF THE UNITED STATES</h3>
            <p>Our Website is hosted in the United States and our services are provided from the United States. Certain information will be stored on servers in Selfiple other countries on the “cloud” or other similar distributed hosting platforms. If you are a user accessing our Website or services from the European Union, Asia or any other region with laws governing personal data collection, use, and disclosure that differ from United States laws, please note that you are transferring your personal information outside your home jurisdiction, and that by providing your personal information you are consenting to the transfer of your personal information to the United States and other jurisdictions as indicated above, and to our use of your personal information in accordance with this policy.</p>
            
            <h3>08. ACCESSING AND CORRECTING YOUR INFORMATION</h3>
            <p>Users can check and update the information stored with SelfDrive by contacting us at<br><a href="support@selfdrive.work" target="_blank">support@selfdrive.work</a></p>
            
            <h3>09. CHILDREN’S PRIVACY</h3>
            <p>With the aim of protecting the privacy of children, SelfDrive does not allow children below the age group of 13 years to register and use our services. We do not collect any information from children under 13. We request children under the age of 13 to not send us any of their personally identifiable information such as email address, name, contact number or address. If we have obtained personal information from a child under 13 years of age without parental consent, then we will delete the acquired information as early as possible. Users who believe that we hold information about a child under the age of 13 are requested to contact us<br><a href="support@selfdrive.work" target="_blank">support@selfdrive.work</a></p>
        
            <h3>10. MODIFICATIONS TO OUR PRIVACY POLICY</h3>
            <p>We post any changes we make to our Privacy Policy on this page. If we make material changes to how we treat our users’ personal information, we will notify you by e-mail to the primary e-mail address specified in your account. The date the Privacy Policy was last revised is identified at the top of the page. You are responsible for ensuring we have an up-to-date, active and deliverable e-mail address for you, and for periodically visiting our Website and this Privacy Policy to check for any changes. For any questions regarding the Privacy Policy, please contact us at<br><a href="support@selfdrive.work" target="_blank">support@selfdrive.work</a></p>
        
        </div>
        <ul class="page_links">
            <li><a href="https://selfdrive.work/home">Home</a></li> 
            <li><span>|</span></li>
            <li><a href="https://selfdrive.work/terms">Terms &amp; Conditions</a></li> 
        </ul>
	</div>
</body> 
</html>