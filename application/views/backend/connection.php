<div class="page-wrapper">
    <div class="main-content">
        <div class="row">
            <div class="col xl-12 col-lg-12">
                <div class="page-connection-wrapper">
                    <div class="sc_title_bar">
                        <h4>Click on the cloud drive to connect it</h4>
                    </div>
                    <div class="sc_icon_gallery choose-product">
                        <label for="" class="rdo">
                            <div class="sc_icon_box custEdit GoogleDrive">
                                <img src="<?= base_url() ?>assets/backend/images/icon1.png" alt="icon">
                                <p>Google Drive</p>
                            </div>
                            <input id="" type="checkbox" name="" value=""<?php echo (isset($checkUserConnectCloud) && !empty($checkUserConnectCloud)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>  
                        <label for="" class="rdo">  
                            <div class="sc_icon_box custEdit dropBox">
                                <img src="<?= base_url() ?>assets/backend/images/dropbox.png" alt="icon">
                                <p>Drop Box</p>
                            </div>
                            <input id="" type="checkbox" name="" value=""<?php echo (isset($DropBox) && !empty($DropBox)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                         <label for="" class="rdo ">
                            <div class="sc_icon_box custEdit Box">
                                <img src="<?= base_url() ?>assets/backend/images/icon8.png" alt="icon">
                                <p>Box</p>
                            </div>
                            <input id="" type="checkbox" name="" value=""<?php echo (isset($BoxCon) &&!empty($BoxCon)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label for="" class="rdo ">
                            <div class="sc_icon_box custEdit Ddownload">
                                <img src="<?= base_url() ?>assets/backend/images/ddownload.png" alt="icon">
                                <p>Ddownload</p>
                            </div>
                            <input id="" type="checkbox" name="" value=""<?php echo (isset($Ddownload) && !empty($Ddownload)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                           <label for="" class="rdo ">  
                                <div class="sc_icon_box custEdit amazonS3">
                                        <img src="<?= base_url() ?>assets/backend/images/icon18.png" alt="icon">
                                        <p>Amazon s3</p>
                                    </div>
                                    <input id="" type="checkbox" name="" value="" <?php echo (isset($amazomS3) &&!empty($amazomS3)) ? 'checked' : '';?>>
                                    <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                                    <span class="checkmark"></span>
                            </label>
                             <label  for="" class="rdo ">  
                                <div class="sc_icon_box custEdit googlePhoto">
                                    <img src="<?= base_url() ?>assets/backend/images/icon6.png" alt="icon">
                                    <p>Google Photos</p>
                                </div>
                                <input id="" type="checkbox" name="" value="" <?php echo (isset($googlePhptos) && !empty($googlePhptos)) ? 'checked' : '';?>>
                                <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                                <span class="checkmark"></span>
                            </label> 
                            <label for="" class="rdo ">
                                <div class="sc_icon_box custEdit OneDrive">
                                    <img src="<?= base_url() ?>assets/backend/images/onedrive.png" alt="icon">
                                    <p>One Drive</p>
                                </div>
                                <input id="" type="checkbox" name="" value=""<?php echo (isset($OneDrive) &&!empty($OneDrive)) ? 'checked' : '';?>>
                                <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                                <span class="checkmark"></span>
                            </label>
                         <?php 
                            if($_SESSION['email']=='vivek.tiwari@pixelnx.com' || $_SESSION['email']=='just@mailinator.com'){
                            ?>
                         
                                <label for="" class="rdo ">  
                        <div class="sc_icon_box custEdit anonfilesCon">
                                <img src="<?= base_url() ?>assets/backend/images/icon_1.png" alt="icon">
                                <p>Anon Files</p>
                            </div>
                            <input id="" type="checkbox" name="" value=""<?php echo (isset($Anon) && !empty($Anon)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                          <?php 
                            }
                        ?>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon4.png" alt="icon">
                                <p>Share Point</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon5.png" alt="icon">
                                <p>Mega</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label for="" class="rdo " data-toggle="modal" data-target="#ftp">  
                        <div class="sc_icon_box custEdit">
                                <img src="<?= base_url() ?>assets/backend/images/icon7.png" alt="icon">
                                <p>FTP</p>
                            </div>
                            <input id="" type="checkbox" name="" value="" <?php echo (isset($ftp) &&!empty($ftp)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon9.png" alt="icon">
                                <p>BaiDu</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon10.png" alt="icon">
                                <p>Flickr</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon11.png" alt="icon">
                                <p>HiDrive</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon12.png" alt="icon">
                                <p>Yandex</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon13.png" alt="icon">
                                <p>NAS</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon14.png" alt="icon">
                                <p>MediaFire</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon15.png" alt="icon">
                                <p>WebDAV</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon16.png" alt="icon">
                                <p>Wasabi</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon17.png" alt="icon">
                                <p>Evernote</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon18.png" alt="icon">
                                <p>Amazon S3</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon19.png" alt="icon">
                                <p>hubiC</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon20.png" alt="icon">
                                <p>ownCloud</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon21.png" alt="icon">
                                <p>MySQL</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon22.png" alt="icon">
                                <p>Egnyte</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon23.png" alt="icon">
                                <p>Putio</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon24.png" alt="icon">
                                <p>ADrive</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon25.png" alt="icon">
                                <p>SugarSync</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label for="" class="rdo ">  
                        <div class="sc_icon_box custEdit backBlaze">
                                <img src="<?= base_url() ?>assets/backend/images/icon26.png" alt="icon">
                                <p>Backblaze</p>
                            </div>
                            <input id="" type="checkbox" name="" value="" <?php echo (isset($Backblaze) &&!empty($Backblaze)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label for="" class="rdo">  
                        <div class="sc_icon_box custEdit gofile">
                                <img src="<?= base_url() ?>assets/backend/images/icon31.png" alt="icon">
                                <p>Go File</p>
                            </div>
                            <input id="" type="checkbox" name="" value="" <?php echo (isset($Gofile) &&!empty($Gofile)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label for="" class="rdo" data-toggle="modal" data-target="#pcloud" >  
                        <div class="sc_icon_box custEdit">
                                <img src="<?= base_url() ?>assets/backend/images/icon3.png" alt="icon">
                                <p>PCloud</p>
                            </div>
                            <input id="" type="checkbox" name="" value="" <?php echo (isset($Pcloud) &&!empty($Pcloud)) ? 'checked' : '';?>>
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon27.png" alt="icon">
                                <p>WEB.DE</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon28.png" alt="icon">
                                <p>CloudMe</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon29.png" alt="icon">
                                <p>MyDrive</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                        <label hidden for="" class="rdo ">  
                        <div class="sc_icon_box custEdit openModal">
                                <img src="<?= base_url() ?>assets/backend/images/icon30.png" alt="icon">
                                <p>Cubby</p>
                            </div>
                            <input id="" type="checkbox" name="" value="">
                            <img src="<?= base_url() ?>assets/backend/images/check_icon.png" class="checkbox_img">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>