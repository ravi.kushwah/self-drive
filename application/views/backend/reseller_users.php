<div class="page-wrapper">
    <div class="main-content sc_get_affiliate">
        <div class="row">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="page-title-box">
                        <h2>Welcome</h2>
                        <h3><span>Your Reseller Code:</span> LGIRUDJ100</h3>
                    </div>
                    <div class="add-group">
						<a href="https://selfdrivepro.com/regfdsgyjhgdsarghgaffssg" target="_blank" class="ad-btn">Get Affiliate Link</a>
					</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Step 1:  Go to Affiliate Sign Up Page and Request for You Link</h4>
                    </div>
                    <div class="card-body sc-box-hieght">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <img src="<?php echo base_url();?>assets/backend/images/reseller_1_sc.jpeg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Step 2:  Enter CODE ONLY into Request Notes area and Click on Request Offer</h4>
                    </div>
                    <div class="card-body sc-box-hieght">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <img src="<?php echo base_url();?>assets/backend/images/reseller_2_sc.jpeg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Step 3:  Please Fill this Form <a href="https://docs.google.com/forms/d/e/1FAIpQLSdESbfjWLGHYaM3X39f818qhBDypdIzWKdCW5jDFAUnfiRcnA/viewform" target="_blank">Click Here</a></h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <p>If You do not get Approved in 3 Working Days,  you can   <a href="mailto:tickets@self-drive-pro.p.tawk.email">Contact us here</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Disclaimer</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <p>Please do engagement to Promote the Offer . Only for Post Launch Purpose. Use on own discretion, We do not take responsibility for the outcomes by using above material. You Should Use Reseller After the Launch Ends. If we find any Affiliate promoting the Offer with Reseller during the Launch & Contest, we reserve the rights to cancel the same from Contest and the Offer.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        <!-- Page Title Start -->
        <div class="row sc_hide">
            <div class="colxl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Dashboard Start -->
        
        <div class="row sc_hide">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card chart-card">
                    <div class="card-header plr_heading_box">
                        <h4><?= $pageName ?></h4>
                        <a href="javascript:;" class="ad-btn resellerUserAdd" id="0">Add New</a> 
                    </div>
                    <div class="card-body">
                        <div class="plr_data_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap server_datatable " data-source="<?=base_url()?>home/user_data/reseller" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
<div class="modal fade" id="resellerUserAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_reseller_title">Add New Reseller User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form">
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Name </label>
                    <input type="text" placeholder="Name" name="name" class="form-control resellerUserAddCls require" id="name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Email</label>
                    <input type="text" placeholder="Email" data-valid="email" data-error="Email should be valid." name="email" class="form-control resellerUserAddCls require" name="email" id="email">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <input type="password" placeholder="Enter Password" name="password" class="form-control resellerUserAddCls require" id="password">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Confirm Password</label>
                    <input type="password" placeholder="Enter Confirm Password" name="cnf_password" class="form-control resellerUserAddCls require" id="cnf_password">
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <input type="checkbox" id="sendMail" class="" name="send_on_mail">
                        <label for="sendMail">Send login details on email  </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer sc_google_btn">
                <input type="hidden" value="reseller" name="type" id="type">
                <input type="hidden" value="" name="user_id" id="user_id">
                <button type="button" class="btn ad-btn" id="submitResellerUserData">Submit</button>
            </div>
         </form>
        </div>
    </div>
</div>