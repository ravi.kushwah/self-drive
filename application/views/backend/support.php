<div class="page-wrapper">
    <div class="main-content">
        <!-- Page Title Start -->
        <div class="row">
            <div class="col xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-title-wrapper">
                    <div class="breadcrumb-list">
                        <ul>
                            <li class="breadcrumb-link">
                                <a href="<?= base_url() ?>home/dashboard"><i class="fas fa-home mr-2"></i>Dashboard</a>
                            </li>
                            <li class="breadcrumb-link active"><?= $pageName ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Have any Query?</h4>
                    </div>
                    <div class="card-body">
                        <h5>Get in touch With Support Team Over Email - <a href="mailto:tickets@self-drive-pro.p.tawk.email" class="card-header-mail" style="color:#019ee9; text-transform: lowercase;">tickets@self-drive-pro.p.tawk.email</a>  </h5>
                    </div>
                </div>
            </div>
        </div>