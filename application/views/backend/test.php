<div class="page-wrapper">
    <div class="main-content">
<div id="container">
	<h1>Chunk Uploading</h1>

	<div id="body">
		<p>Upload Big file chunk by chunk using Plupload.</p>

		<div id="filelist">Your browser doesn't have Flashs, Silverlight or HTML5 support.</div>
		<div id="container">
			<div class="form-group">
				<a id="uploadFile"class="form-control" name="uploadFile" href="javascript:;">Select file</a>
			</div>

			<div class="form-group">
				<a id="upload" href="javascript:;" class="btn btn-danger">Upload files</a>
			</div>
		</div>
		<input type="hidden" id="file_ext" name="file_ext" value="<?=substr( md5( rand(10,100) ) , 0 ,10 )?>">
		<div id="console"></div>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>
<div class="progress"></div>



</div>
</div>