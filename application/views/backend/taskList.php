<style> 
.pagination {
  display: flex;
  justify-content: center;
  list-style: none;
  margin: 20px 0;
  padding: 0;
}

.pagination li {
  margin: 0 5px;
}

.pagination a {
    background-color: var(--white);
    border: 1px solid var(--border-color);
    color: #333;
    text-decoration: none;
    min-width: 40px;
    height: 40px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    padding: 0 15px;
    border-radius: 5px;
}
.pagination a i {
    margin-left: 6px;
}
.pagination a:hover {
  background-color: var(--primary);
  color: var(--white);
}

.pagination .active a {
  background-color: var(--primary);
  border-color: var(--primary);
  color: var(--white);
}
</style>
<div class="page-wrapper">
    <div class="main-content">
        <div class="row">
        <div class="col xl-12 col-lg-12">
            <div class="TaskList_Section">
                <div class="sc_title_bar">
                    <h4>Task List</h4>
                    <p>You can manage cloud transfer, cloud sync and shared file saving tasks here</p>
                </div>
                <div class="sc_task_list"></div>
                <div id="pagination"></div>
            </div>
        </div>
    </div>
              
