<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<!--<![endif]-->
<!-- Begin Head -->

<head>
    <title><?= SITENAME ?><?= $pageName != '' ? ' | '.$pageName : '' ?></title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="MobileOptimized" content="320">
    <!--Start Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/empty/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/empty/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/empty/css/style.css">
    <!-- Favicon Link -->
    <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?= base_url() ?>assets/backend/images/favicon.png">
</head>


<body>
<div class="loader">
	  <div class="spinner">
		<img src="<?= base_url() ?>assets/backend/images/loader.gif" alt=""/>
	  </div> 
	</div>
    <!-- Main Body -->
    <div class="page-wrapper">
        <!-- Header Start -->
        <header class="header-wrapper main-header plr_welcome_screen">
            <div class="header-inner-wrapper">
                <div class="header-right">
                    <div class="header-controls">
                        <div class="logo-wrapper">
                            <a href="javascript:;">
                                <img src="<?= base_url() ?>assets/backend/images/logo2.png" alt="logo">
                            </a>
                        </div>
                        <div class="user-info-wrapper header-links">
                            <div class="prl_user_box_profile">
                                <div class="prl_user_name">
                                    <h4>Hi, <?= $this->session->userdata['firstname'] ?></h4>
                                    <p><?= $this->session->userdata['email'] ?></p>
                                </div>
                                <div class="user-info">
                                    <a href="javascript:void(0);" class="">
                                        <img src="<?= base_url() ?>assets/backend/images/user.jpg" alt="" class="user-img">
                                        <div class="blink-animation">
                                            <span class="blink-circle"></span>
                                            <span class="main-circle"></span>
                                        </div>
                                    </a>
                                    <div class="user-info-box">
                                        <ul>
                                            <li>
                                                <a href="<?= base_url() ?>home/profile">
                                                    <i class="far fa-edit"></i> Profile
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= base_url() ?>home/logout">
                                                    <i class="fas fa-sign-out-alt"></i> logout
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Sidebar Start -->
        
        <!-- Container Start -->
        <div class="page-wrapper">
            <div class="main-content plr_welcome_section">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="plr_welmain_wrapper">
                                <div class="plr_wel_flexbox">
                                    <div class="plr_hand_img">
                                        <img src="<?= base_url() ?>assets/backend/images/hand_img.png" alt="image">
                                    </div>
                                    <div class="plr_wel_content">
                                        <h5>Hey <?= $this->session->userdata['firstname'] ?>!<span>Welcome to <?= SITENAME ?>.</span></h5>
                                    </div>
                                </div>
                                <div class="plr_video_box">
                                    <h3>Check this demo to know how it work</h3>
                                   <div style="    padding: 0;    position: relative;    width: 100%;    max-width: 580px;    height: 310px;    margin: 0 auto;"><iframe src="https://player.vimeo.com/video/752991225?h=c73ab31e8f&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                                    <div class="plr_wel_btn">
                                        <a href="<?= base_url() ?>home/add_new_website" class="ad-btn">add new site</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				

			    <div class="ad-footer-btm">
                    <p>Copyright <?= date('Y') ?> &copy; <?= SITENAME ?> All Rights Reserved.</p>
				</div>
            </div>
        </div>
    </div>

	
    <!-- Script Start -->
	<script src="<?= base_url() ?>assets/backend/empty/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/empty/js/bootstrap.min.js"></script>
    <!-- Custom Script -->
    <script src="<?= base_url() ?>assets/backend/empty/js/custom.js"></script>
</body>

</html>