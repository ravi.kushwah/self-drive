
<html>
<head>
	<title>SelfDrive - About us</title>
  <meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML, CSS, JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Red+Hat+Display:400,500,700,900&amp;display=swap" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="https://selfdrive.work/assets/auth/images/favicon.png">
  <style>
body {
	font-family: 'Red Hat Display';
	font-weight:400;
	font-size: 15px;
	line-height:1.78;
	color: #666565;
	-webkit-font-smoothing: antialiased;
	-webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}

*,*:before,*:after{
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
}
a, a:hover, a:focus{
	color: #222222;
	text-transform: capitalize;
	text-decoration: none;
	-webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
h1, h2, h3, h4, h5, h6{
	-webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
	margin:0;
	padding:0;
	text-transform:capitalize;
    font-family: 'Red Hat Display';
}
img {
	max-width: 100%;
}
input, textarea, select, button, label, svg, svg path, svg rect, svg polygon, img, a, :after, :before, :hover, :focus, .form-control:focus{
	outline: none !important;
	box-shadow:none;
}
ul{
	padding:0;
	margin:0;
	list-style-type: none;
}
p {
  margin: 0px;
}
html {
  scroll-behavior: smooth;
}
.container {
    max-width: 1170px;
}
/* button css */
.mfh_btn, .mfh_btn:focus {
    min-width: 150px;
    padding: 0 30px;
    height: 50px;
    line-height: 50px;
    display: inline-block;
    text-align: center;
    color: #ffffff;
    border-radius: 5px;
    text-transform: capitalize;
	background-color: #0180ff;
    font-size: 14px;
    font-weight: 500;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
.mfh_btn:hover {
    color: #ffffff;
    background-color: #29294c;
}
/* button css */


/* checkbox start */
.mfh_check_section>label {
    margin-bottom: 20px;
}
.mfh_check_section ul li {
    display: inline-block;
    justify-content: space-between;
    text-transform: capitalize;
    margin-right: 20px;
}
.mfh_check_section ul li span>a {
    color: #0180ff;
    font-weight: 600;
}
.mfh_check_section ul li span>a:hover {
    color: #2e3039;
}
.mfh_check_section p {
    margin-bottom: 20px;
}
.mfh_checkbox {
	text-transform: capitalize;
}
.mfh_checkbox > input{
	position:absolute;
	left:-999999px;
}
.mfh_checkbox > label {
    font-size: 14px;
    position: relative;
    cursor: pointer;
    color: #9ca2ab;
}
.mfh_checkbox > label:last-child {
	margin-bottom: 0;
}
.multi-select-menuitems label input{
	opacity: 0;
}
.mfh_checkbox > label:before {
	content: "";
    width: 16px;
    height: 16px;
    border: solid 1px #d5d5d5;
    border-radius: 3px;
    display: inline-block;
    margin-right: 10px;
	vertical-align: text-top;
    -webkit-transform: translateY(3px);
    -moz-transform: translateY(3px);
    -ms-transform: translateY(3px);
    transform: translateY(2px);
}
.mfh_checkbox > label:after {
	content: "";
    position: absolute; 
    top: 9px;
    left: 3px;
    width: 10px;
    height: 5px;
    border-left: 2px solid #ffffff;
    border-bottom: 2px solid #ffffff;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    transform: rotate(-45deg);
	opacity:0;
	visibility:hidden;
}
.mfh_checkbox > input:checked ~ label:after {
	 opacity:1;
	 visibility:visible;
}
.mfh_checkbox > input:checked ~ label:before {
	background-color:#0180ff;
	border: solid 1px #0180ff;
}
.mfh_checkbox > input:checked {
	color:#0180ff;
}
/* checkbox end */
.mfh_spanred {
    color: #ff0000 !important;
}
/* radio botton css */

.mfh_shop_radio>label {
    margin-bottom: 20px;
}
.mfh_shop_radio ul li {
    display: inline-block;
    margin-right: 20px;
}
.mfh_shop_radio [type="radio"]:checked,
[type="radio"]:not(:checked) {
    position: absolute;
    left: -9999px;
}
.mfh_shop_radio [type="radio"]:checked + label,
[type="radio"]:not(:checked) + label
{
    position: relative;
    padding-left: 28px;
    cursor: pointer;
    line-height: 20px;
    display: inline-block;
    color: #666;
	margin-bottom: 16px;
}
.mfh_shop_radio [type="radio"]:checked + label:before,
[type="radio"]:not(:checked) + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 18px;
    height: 18px;
    border: 1px solid #ddd;
    border-radius: 100%;
    background: #fff;
}
.mfh_shop_radio [type="radio"]:checked + label:after,
[type="radio"]:not(:checked) + label:after {
    content: '';
    width: 12px;
    height: 12px;
    background: #0180ff;
    position: absolute;
    top: 3px;
    left: 3px;
    border-radius: 100%;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
}
.mfh_shop_radio [type="radio"]:not(:checked) + label:after {
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
}
.mfh_shop_radio [type="radio"]:checked + label:after {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
}
/* radio botton css */

 /********************************************************
1. body end
 *******************************************************/
 /********************************************************
 /********************************************************
2. header start
 *******************************************************/
.mfh_top_header {
    background: #0180ff;
    padding: 10px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
	-webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
}
.mfh_top_header p {
    color: #ffffff;
    text-align: center;
	font-size: 16px;
	line-height: normal;
	font-weight: 500;
	letter-spacing: 1px;
}
.mfh_top_header p span {
    margin: 0 0 0 5px;
}
.mfh_top_header a {
    color: #ffffff;
    margin-left: 5px;
}
.mfh_top_header a:hover {
    color: #01e675;
}
.fixed .mfh_top_header {
    position: absolute;
    top: -45px;
	-webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
}
.mfh_header_wrapper {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    padding: 20px 0;
    z-index: 99;
    background-color: #ffffff;
	-webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
}

.mfh_logo {
    padding-top: 4px;
}
.mfh_main_menu {
    display: flex;
    justify-content: end;
}
.mfh_menus {
    flex: 1;
    margin-top: 10px;
}
.mfh_menus>ul>li {
    display: inline-block;
}
.mfh_menus>ul>li>a {
    color: #222222;
    font-weight: 600;
	margin-left: 35px;
    text-transform: capitalize;
	position: relative;
}
.mfh_menus>ul>li:first-child a {
    margin-left: 0;
}
.mfh_menus>ul>li>a:after {
    position: absolute;
    content: "";
    background-color: #0180ff;
    left: 0;
    right: 0;
    bottom: -10px;
    margin: 0 auto;
    width: 100%;
    height: 3px;
    width: 0%;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
}
.mfh_menus > ul > li > a:hover:after{
	width: 100%;
}
.mfh_menus_btn {
    margin-left: 30px;
}

.mfh_menus_login {
    margin-left: 30px;
	margin-top: -5px;
	position: relative;
}
.mfh_menus_profile img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 15px;
    border: solid 1px #d0d0d0;
}
.mfh_menus_profile span {
    margin-left: 7px;
    color: #959595;
}
ul.mfh_hover_dropdown {
    position: absolute;
    top: 72px;
    right: 0;
    text-align: left;
    background-color: white;
    width: 160px;
    border: solid 1px #eeeeee;
    border-top: solid 2px #0180ff;
    border-radius: 0;
    transition: 0.5s all;
    transform: translateY(100px);
    opacity: 0;
    visibility: hidden;
    z-index: 1;
}
ul.mfh_hover_dropdown:after {
    position: absolute;
    content: "";
    border-left: 15px solid #ff000000;
    border-right: 0px solid #0000ff00;
    border-bottom: 15px solid #0180ff;
    top: -15px;
    right: 40px;
}
ul.mfh_hover_dropdown.mfh_menu_open {
    opacity: 1;
    visibility: visible;
    transform: translateY(0px);
}
ul.mfh_hover_dropdown li {
    border-bottom: solid 1px #ffebe5;
}
ul.mfh_hover_dropdown li:last-child {
    border-bottom: none;
}
ul.mfh_hover_dropdown li a {
    padding: 10px 15px;
    display: inline-block;
}
ul.mfh_hover_dropdown li a:hover {
    color: #fc6e3d;
}
/* Banner css */
.mfh_header_banner {
    background-color: #29294c;
    padding: 100px 80px;
    background-repeat: no-repeat;
}
.mfh_banner_text {
    max-width: 600px;
}
.mfh_banner_text h1 {
    font-size: 30px;
    font-weight: 600;
    color: #ffffff;
}
.mfh_banner_text h3 {
    font-size: 22px;
    font-weight: 500;
    color: #0180ff;
    margin: 15px 0 20px;
}
.mfh_banner_text h2 {
    font-size: 52px;
    font-weight: 800;
    color: #ffffff;
}
.mfh_banner_text p {
    color: #ffffff;
    font-size: 18px;
	margin-top: 15px;
}
.mfh_video_btn {
    margin-top: 35px;
}
.mfh_video_btn>ul>li {
    display: inline-block;
	vertical-align: middle;
	margin-right: 26px;
}
.mfh_video_btn a.active {
    border: solid 2px #ffffff;
    background-color: transparent;
}
.mfh_banner_video iframe {
    border-radius: 6px;
    width: 100%;
}
/* Banner css */

/* About section css  */
.mfh_about_section {
	padding: 80px 0;
    background-color: #f8f8f8;
}
.mfh_services.mfh_services_ht {
    min-height: 230px;
}
.mfh_about_img {
    position: relative;
    text-align: center;
}
.sc_spacer {
    padding: 50px 0;
}
.mfh_ab_detail h1 {
    font-size: 28px;
    font-weight: 600;
    color: #222222;
}
.mfh_ab_detail h2 {
    font-size: 36px;
    color: #2e3039;
    font-weight: 700;
}
.mfh_ab_detail h4 {
    font-size: 16px;
    font-weight: 500;
    margin: 10px 0 0;
    line-height: 1.6;
}
.mfh_ab_detail p {
    margin: 10px 0 0;
	color: #909195;
}
.mfh_ab_detail a {
    margin-top: 20px;
}
/* About section css  */

/* Vector section css  */
.mfh_vector_section {
    padding: 80px 0;
}
/* Vector section css  */

/* Video section css  */
.mfh_video_section {
    padding: 80px 0;
}
.mfh_video_img {
    position: relative;
    text-align: center;
}
.mfh_video_img img {
    max-width: 350px;
    width: 100%;
}
.mfh_ab_main {
    position: relative;
    z-index: 2;
}
.mfh_ab_main:before {
    position: absolute;
    content: "";
    top: -7px;
    left: 0;
    right: 0;
    bottom: -7px;
    background-color: #0180ff;
    z-index: -1;
    border-radius: 10px;
    width: 94%;
    margin: auto;
}
.mfh_ab_detail {
    background-color: #ffffff;
    padding: 30px 20px;
    border-radius: 20px;
    box-shadow: 0px 0px 20px 0px rgb(204 204 204 / 20%);
}
/* Video section css  */

/* Service section css  */
.mfh_services_section {
    background-color: #f0f7fd;
    padding: 73px 0 80px;
	text-align: center;
}
.mfh_heading {
    margin: 0 auto 45px;
	text-align: center;
}
.mfh_heading.mfh_heading_space {
    margin: 80px 0 30px;
}
.mfh_heading h2 {
    font-size: 36px;
    font-weight: 600;
    color: #2e3039;
    text-align: center;
}
.mfh_dash_border {
    position: relative;
}
.dash_border_png {
    position: absolute;
    right: -180px;
    top: 40px;
	z-index: 0;
}
.mfh_services {
    margin: 20px 0 0;
    text-align: center;
    background-color: #ffffff;
    padding: 30px 15px;
    border-radius: 15px;
    box-shadow: 0px 0px 20px 0px rgba(204, 204, 204, 0.2);
}
.mfh_icon img {
    max-width: 190px;
}
.mfh_services h4 {
    color: #222222;
    font-size: 16px;
    margin: 25px 0 5px;
    font-weight: 600;
    line-height: 1.6;
}
.mfh_services.mfh_servimain {
    min-height: 400px;
}
/* Service section css  */

/* Testimonial section css  */
.mfh_testimonial_section {
    background-color: #ffffff;
    padding: 72px 0 20px;
    text-align: center;
}
.mfh_testimonial_section.mfh_testimonial_hide {
    display: none;
}
.mfh_testimonial_section .mfh_heading {
    margin-bottom: 25px;
}
.mfh_swiper_slider {
    position: relative;
}
.mfh_swiper_slider .swiper-container {
    padding: 20px 15px 0px;
}
.mfh_web_name {
    background-color: #ffffff;
    padding: 50px 40px;
    height: 395px;
	border-radius: 20px;
    box-shadow: 0px 0px 20px 0px #f2f2f2;
	position: relative;
}
.mfh_web_name:after {
    position: absolute;
    content: "";
    top: 50px;
    left: 50px;
    background-image: url(../images/quote_right.png);
    width: 60px;
    height: 50px;
    background-repeat: no-repeat;
}
.mfh_web_name:before {
    position: absolute;
    content: "";
    top: 50px;
    right: 50px;
    background-image: url(../images/quote_left.png);
    width: 60px;
    height: 50px;
    background-repeat: no-repeat;
}
.mfh_web_text img {
    border-radius: 100%;
    width: 100px;
    height: 100px;
}
.mfh_web_text h4 {
    font-size: 18px;
    color: #0180ff;
    margin: 15px 0 5px;
}
.mfh_web_text h5 {
    font-size: 16px;
    color: #6f707a;
    font-weight: 600;
	margin-bottom: 15px;
}
.mfh_web_text p {
    color: #909195;
}
.mfh_swiper_slider .swiper-wrapper, .mfh_swiper_slider .swiper-slide {
    height: auto;
	padding: 0px 0px 30px;
}
.mfh_swiper_slider .swiper-button-next.swiper-button-white, .mfh_swiper_slider .swiper-button-prev.swiper-button-white {
    --swiper-navigation-color: #dbdbdb;
}
.mfh_swiper_slider .swiper-button-next.swiper-button-white, .mfh_swiper_slider .swiper-button-prev.swiper-button-white {
    border: solid 1px;
    border-radius: 100px;
    width: 40px;
    height: 40px;
	top: 46%;
	border-color: #0180ff;
	outline: none;
    transition: 0.3s;
    -webkit-transition: 0.3s;
    -ms-transition: 0.3s;
    -o-transition: 0.3s;
    -moz-transition: 0.3s;
}
.mfh_swiper_slider .swiper-button-next.swiper-button-white:hover, .mfh_swiper_slider .swiper-button-prev.swiper-button-white:hover {
    background: #0180ff;
	border-color: #0180ff;
}
.mfh_swiper_slider .swiper-button-next:after, .mfh_swiper_slider .swiper-button-prev:after {
    font-size: 14px;
	color: #0180ff;
}
.mfh_swiper_slider .swiper-button-next:hover:after, .mfh_swiper_slider .swiper-button-prev:hover:after {
	color: #ffffff;
}
.mfh_swiper_slider .swiper-button-prev, .mfh_swiper_slider .swiper-container-rtl .swiper-button-next {
    left: 315px;
}
.mfh_swiper_slider .swiper-button-next, .mfh_swiper_slider .swiper-container-rtl .swiper-button-prev {
    right: 315px;
}
/* Testimonial section css  */









.mfh_cotegory_slider {
    position: relative;
}
.mfh_cotegory_slider .swiper-container {
    padding: 20px 15px 0px;
}

.mfh_cotegory_slider .swiper-wrapper, .mfh_cotegory_slider .swiper-slide {
    height: auto;
	padding: 0px 0px 30px;
}
.mfh_cotegory_slider .swiper-button-next.swiper-button-white, .mfh_cotegory_slider .swiper-button-prev.swiper-button-white {
    --swiper-navigation-color: #dbdbdb;
}
.mfh_cotegory_slider .swiper-button-next.swiper-button-white, .mfh_cotegory_slider .swiper-button-prev.swiper-button-white {
    border: solid 1px;
    border-radius: 100px;
    width: 40px;
    height: 40px;
	top: 41%;
	border-color: #0180ff;
	outline: none;
    transition: 0.3s;
    -webkit-transition: 0.3s;
    -ms-transition: 0.3s;
    -o-transition: 0.3s;
    -moz-transition: 0.3s;
}
.mfh_cotegory_slider .swiper-button-next.swiper-button-white:hover, .mfh_cotegory_slider .swiper-button-prev.swiper-button-white:hover {
    background: #0180ff;
	border-color: #0180ff;
}
.mfh_cotegory_slider .swiper-button-next:after, .mfh_cotegory_slider .swiper-button-prev:after {
    font-size: 14px;
	color: #0180ff;
}
.mfh_cotegory_slider .swiper-button-next:hover:after, .mfh_cotegory_slider .swiper-button-prev:hover:after {
	color: #ffffff;
}
.mfh_cotegory_slider .swiper-button-prev, .mfh_cotegory_slider .swiper-container-rtl .swiper-button-next {
    left: 315px;
}
.mfh_cotegory_slider .swiper-button-next, .mfh_cotegory_slider .swiper-container-rtl .swiper-button-prev {
    right: 315px;
}















/* Footer section css  */
.mfh_footer_section {
	background-color: #27282c;
	padding: 80px 0 66px;
}
.mfh_social_icon {
    padding-top: 30px;
}
.mfh_social_icon ul li { 
    display: inline-block;
}
.mfh_social_icon ul li a {
    background-color: #ffffff;
    color: #0180ff;
    font-weight: 500;
    width: 35px;
    height: 35px;
    line-height: 35px;
    display: inline-block;
    text-align: center;
    border-radius: 100%;
    margin-right: 10px;
}
.mfh_social_icon ul li a svg {
    fill: #0180ff;
    width: 16px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
.mfh_social_icon ul li a:hover svg {
    fill: #ffffff;
}
.mfh_social_icon ul li:last-child a {
    margin-right: 0px;
}
.mfh_social_icon ul li a:hover {
    color: #ffffff;
    box-shadow: 0 0 0 25px #0180ff inset;
}
.mfh_footer_box p {
    color: #cfcfd3;
    margin: 23px 0 10px;
	width: 250px;
    display: none;
}
.mfh_footer_box a {
    color: #fb6d3a;
}
.mfh_footer_box a:hover {
    color: #ffffff;
}
.col-lg-3.col-md-6.newslatter_hide {
    display: none;
}
.mfh_footer_box h3 {
    color: #ffffff;
    font-size: 24px;
    font-weight: 600;
    margin: 0px 0 30px;
}
ul.mfh_footer_list li a {
    display: inline-block;
    margin-bottom: 10px;
	color: #cfcfd3;
}
ul.mfh_footer_location li {
    color: #cfcfd3;
    position: relative;
    padding-left: 30px;
    margin-bottom: 15px;
}
ul.mfh_footer_location li:last-child {
    margin-bottom: 0;
}
ul.mfh_footer_location li svg {
    position: absolute;
    top: 3px;
    left: 0;
    fill: #ffffff;
}
.mfh_footer_box .input-group.mb-3 {
    margin-top: 25px;
}
.mfh_footer_box input.form-control {
    background-color: transparent;
    border-color: rgba(207, 207, 211, 0.3);
	font-size: 14px;
    color: #cfcfd3;
	height: 40px;
    line-height: 40px;
	border-radius: 5px;
}
.mfh_footer_box input::placeholder {
    color: #9a9ba0;
}
.mfh_footer_box button {
    background-color: #0180ff;
    border: none;
    cursor: pointer;
    padding: 0px 18px;
    border-radius: 0 5px 5px 0;
    transition: 0.3s all;
    position: absolute;
    top: 0;
    right: 14px;
    height: 40px;
    line-height: 40px;
}
.mfh_footer_box button:hover {
    background-color: #ffffff;
}
.mfh_footer_box i.fa.fa-location-arrow {
    color: #ffffff;
    transform: rotate(43deg);
    font-size: 16px;
}
.mfh_footer_box button:hover i.fa.fa-location-arrow {
    color: #0180ff;
}
.mfh_mini_footer {
    background-color: #1c1d20;
    padding: 20px 0;
}
.mfh_copyright p {
    color: #ffffff;
}
.mfh_menu_copy {
    text-align: center;
}
.mfh_menu_copy ul li {
    display: inline-block;
}
.mfh_menu_copy ul li a {
    color: #ffffff;
    margin: 0 30px;
    font-size: 16px;
}
.mfh_menu_copy ul li a:hover {
    color: #0180ff;
}
/* Footer section css  */


/* Privacy Policies css */
.mfh_privacy h3 {
    margin-bottom: 30px;
    color: #222222;
    font-weight: 600;
}
.mfh_privacy h5 {
    margin-top: 30px;
    font-size: 18px;
    font-weight: 500;
    color: #222222;
}
.mfh_privacy p {
    margin-top: 10px;
}
/* Privacy Policies css */






/********************************************************
22. responsive start
********************************************************/
@media(max-width: 1199px){

.mfh_menus>ul>li>a {
    margin-left: 12px;
}
.mfh_header_banner {
    padding: 100px 15px;
}
.mfh_about_section {
    padding: 72px 0 80px;
}
.mfh_ab_img3 {
    display: none;
}
.mfh_ab_main {
    margin-top: 0;
}
.mfh_plan_btn a {
    width: auto;
}
.mfh_input_box.mfh_search_box {
    flex: 35%;
    margin-right: 10px;
}
.mfh_recent_detail img {
    width: 100%;
}
.mfh_search_details {
    margin-left: 0;
}
.mfh_recent_detail a {
    width: auto;
}
.mfh_search_img img {
    width: auto;
    height: auto;
}
ul.mfh_hover_dropdown {
	right: 0px;
}
}

@media(max-width: 991px){
.mfh_header_banner {
    padding: 120px 0;
    text-align: center;
}
.mfh_banner_text {
    max-width: 600px;
    margin: 40px auto 0;
}

ul.mfh_hover_dropdown {
    right: 30px;
}
.mfh_menus_btn {
    margin-left: 0;
	margin-right: 0;
}
.mfh_ab_img {
    margin: 0;
}
.mfh_ab_img2 {
    display: none;
}
.dash_border_png {
    display: none;
}
.mfh_web_name {
    padding: 30px 15px;
}
.mfh_footer_section {
    padding: 40px 0 64px;
}
.mfh_footer_box {
    margin-top: 40px;
}
ul.action_icon li {
    display: block;
}
ul.action_icon li a {
    margin: 0px 0 3px;
}
.mfh_dumb_truck {
    margin-bottom: 30px;
}
.ml_video_lock_wrapper {
    width: 100% !important;
}
}


@media(max-width: 767px){
.mfh_header_banner {
    padding: 80px 0;
}
.mfh_banner_text {
    text-align: center;
}
.mfh_banner_text h1 {
    font-size: 36px;
}
.mfh_banner_text h3 {
    font-size: 30px;
}
.mfh_banner_text h2 {
    font-size: 36px;
}
.mfh_about_section {
    padding: 50px 0 0px;
}
.mfh_ab_main {
    margin-top: 30px;
	margin-bottom: 50px;
}
.mfh_vector_section {
    padding: 0 0 50px;
}
.mfh_video_section {
    padding: 60px 0 ;
}
.mfh_video_section.mfh_video_tr {
    padding: 50px 0 0;
}
.mfh_services_section {
    padding: 60px 0;
}
.mfh_testimonial_section {
    padding: 42px 0 0px;
}
.mfh_heading {
    margin: 0 auto 21px;
}
.mfh_testimonial_section .mfh_heading {
    margin-bottom: 1px;
}
.mfh_heading h2, .mfh_heading {
	font-size: 26px;
}
.mfh_ab_img {
    text-align: left;
}
.mfh_swiper_slider .swiper-wrapper, .mfh_swiper_slider .swiper-slide {
    padding: 0px 0px 5px;
}
.mfh_footer_section {
    padding: 10px 0 34px;
}
.mfh_footer_box h3 {
    margin: 0px 0 20px;
}
.mfh_copyright {
    text-align: center;
	margin-bottom: 10px;
}
.mfh_menu_copy {
    text-align: center;
}
.mfh_menu_copy ul li a {
    margin: 0 15px;
}
.mfh_plan_section {
    padding: 43px 0 20px;
    margin-top: 55px;
}
.mfh_machine_img {
    margin-bottom: 30px;
}
.mfh_add_machine {
    padding: 50px 0;
    margin-top: 55px;
}
.mfh_recent_section {
    padding: 46px 0 50px;
}
.mfh_search_machine {
    padding: 46px 0 34px;
    margin-top: 85px;
}
.mfh_no_machine {
    padding: 18px 0 0;
}
.mfh_input_box.mfh_search_box {
    margin-right: 0;
}
.mfh_search_flex {
    display: block;
}
.mfh_search_details {
    margin-top: 28px;
}
.mfh_related_machine {
    padding: 20px 0 0px;
}
.mfh_shop_radio {
    margin-top: 30px;
}
.mfh_recent_section.mfh_category_section {
    padding: 46px 0 20px;
}
.mfh_machine_profile .nav-tabs .nav-item.show .nav-link, .mfh_machine_profile .nav-tabs .nav-link.active:after {
    display: none;
}
.modal:before {
  display: none;
}
.sc_spacer {
    padding: 20px 0;
}
}


@media(max-width: 575px){
.mfh_banner_text h1 {
    font-size: 30px;
}
.mfh_banner_text h3 {
    font-size: 26px;
}
.mfh_banner_text h2 {
    font-size: 30px;
}
.mfh_video_btn>ul>li {
    display: block;
    margin: 20px 0 0;
}
.mfh_services h4 {
    margin: 15px 0 0px;
}
.mfh_web_name:after {
    left: 20px;
}
.mfh_web_name:before {
    right: 20px;
}
.mfh_menu_copy ul li a {
    margin: 0 10px;
}
.mfh_heading_flex {
    display: block;
    text-align: center;
}
.mfh_heading_btn {
    text-align: center;
}
.mfh_heading_one h2 {
    margin-bottom: 20px;
}
.mfh_machine_profile {
    padding: 30px;
}
.mfh_addnewbtn {
    text-align: left;
}
.mfh_pagination {
    text-align: center;
}
.mfh_contact_popup ul li:first-child {
    margin-right: 0;
    margin-bottom: 10px;
}
.mfh_popup_header .mfh_search_details {
    padding: 24px 20px 40px;
}
.mfh_seller p {
    font-size: 18px;
}
.pb_border_box {
    width: auto !important;
}
.pb_border_box h2 {
    width: 100% !important;
    text-align: center !important;
}
.mfh_contact_popup {
    padding: 0px 10px 20px;
}
}


@media(max-width: 480px){
.mfh_top_header p {
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 0;
}
.mfh_menus_btn a {
    padding: 0;
}
.mfh_search_details ul li {
    display: block;
    justify-content: normal;
}
.mfh_search_details.mfh_search_code ul li {
    display: flex;
    justify-content: space-between;
}
}

@media(max-width: 384px){

}
/********************************************************
22. responsive end
********************************************************/
  </style>
</head>
<body>
	<div class="te_page_box">
		<div class="te_page_header">
			<div class="te_page_logo">
				<a href="https://selfdrive.work/" class="mh_logo" target="_blank"><img src="https://selfdrive.work/assets/auth/images/logo.png" class=""></a>
			</div>
			<div class="te_page_button">
				<a href="https://selfdrive.work/" target="_blank" class="mh_btn">login now</a> 
			</div>
		</div>
	</div>
	<div class="mfh_main_wrapper">
		<!--===Header Section Start===-->
		<div class="mfh_header_wrapper_main"> 
			<div class="mfh_header_wrapper"> 
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-4">
							<div class="mfh_logo">
								<a href="https://selfdrive.work/" class="mh_logo" target="_blank"><img src="https://selfdrive.work/assets/auth/images/logo.png" class=""></a>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-8">
							<div class="mfh_main_menu">
								<div class="mfh_menus_btn">
									<a href="https://selfdrive.work/" target="_blank" class="mfh_btn">login now</a> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Header Section End===-->
		
		<!--===Header Banner Start===-->
		<div class="mfh_header_banner">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-12 align-self-center">
						<div class="mfh_banner_video">
							<img src="<?php echo base_url();?>assets/backend/images/picture1.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 align-self-center">
						<div class="mfh_banner_main">
							<div class="mfh_banner_text">
								<h1>SelfDrive</h1>
								<h3>Host & Transfer your files across Different Clouds from One Platform</h3>
								<h1>Cloud Transfer</h1>
								<p>Combine All Clouds into One & Transfer & Sync files between Different Clouds.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Header Banner End===-->
		
		<!--===About us Section Start===-->
		<div class="mfh_about_section">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_about_img">
							<div class="mfh_ab_img">
								<img src="<?php echo base_url();?>assets/backend/images/picture2.png"  alt=""/>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>About SelfDrive</h1>
								<p>SelfDrive allows you to connect all your cloud drives and transfer or sync files from one cloud service directly to another. You can also manage online files from Selfiple clouds all under one virtual roof with upload, download, cut, copy, paste, move, delete. Rename, etc.</p>
								<h4>Directly transfer files from one cloud drive to another.</h4>
								<h4>Sync clouds and scheduled cloud-to-cloud backup.</h4>
								<h4>Completely easy to manage your online storage.</h4>
								<h4>File transfer in background with browser closed.</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===About us Section End===-->
		
		<!--===Service Section Start===-->
		<div class="mfh_vector_section">
			<div class="container">
				<div class="mfh_machine_profile">
					<div class="row">
						<div class="col-lg-12">
							<div class="mfh_privacy">
								<h3>Cloud File Transfer</h3>
								<h5>Transfer Data across Cloud Drives</h5>
								<p>Copy, move or migrate files from one cloud directly to another with Copy & Paste or Drag & Drop, instead of downloading and re-uploading files between different cloud accounts anymore. For instance, you can copy and paste a certain file from Dropbox to Google Drive through a windows type of service or set up a transfer task to migrate one cloud to another.</p>

								<h5>Transfer files between cloud services</h5>
								<p>SelfDrive can connect all your clouds and enable you to securely access all the online files like documents, images, music and videos from Selfiple cloud storage services all under one virtual roof. With the help of SelfDrive, you can transfer files between cloud services using drag & drop. The process of transfer runs on the server of SelfDrive, so there's no need to keep the device working all the time to download from one cloud and upload the files to another cloud service.</p>

								<h5>What can SelfDrive help me with?</h5>
								<p>Online storage is an integral part of life now, but with so many cloud storage services with different features, you might have many different cloud storage services to fulfill all your needs. For instance, many people have an account with both Google Drive and Dropbox. Before, when we need to do cloud file transfer, we had to download the files from one service and upload these files to another. If you are transferring large files, it may be more troublesome.</p>
								<p>With the help of SelfDrive, you’ll be able to:</p>
								<p>Access all your Selfiple clouds with one account.</p>
								<p>Transfer data across clouds using drag & drop.</p><br>
								<p>Automatically transfer files from cloud to cloud by setting up a scheduled transfer task.</p>

								<h5>Offline Data Transfer</h5>
								<p>The process of transfer runs on the server of SelfDrive, so the data is still transferring even when your device is power off. As it is offline cloud sync or transfer, once you launch data transmission, you can simply shut down your browser or computer and leave the rest to SelfDrive.</p><br>
								<p>SelfDrive can connect all your cloud storage services and access all your files from one single web interface. You can transfer, sync or backup files between clouds by setting up transfer tasks, or simply copy the file(s) from one cloud to another. The process of cloud file transfer runs on the server of SelfDrive, so it will be still transferring even when your device is powered off.</p>
								<p class="not">NOTE: SelfDrive will NOT store users’ files permanently. When the transfer of data is complete, the data will be deleted from SelfDrive database.</p>

								<h5>Why is offline file transfer more convenient?</h5>
								<p>Recently, it is more and more popular to save files or backup data on cloud storage services like Dropbox, Google Drive, OneDrive, Box, etc. And, many people use more than one cloud storage services to expand their free online storage or to have combination of more features provided by those different cloud services. However, if users want to transfer files across different clouds, they have to download files from one cloud to their devices and re-upload them to another cloud. With SelfDrive, for instance, we can transfer files from OneDrive to Dropbox by setting up a transfer task between these two clouds or simply use "Copy" or "Copy to" and it is offline cloud transfer, so no need to download and re-upload files from one cloud to another.</p>

								<h5>Online Storage Services Supported by SelfDrive</h5>
								<p>SelfDrive supports more than 10 cloud services like</p>
								<p>Google Drive, Dropbox, MEGA, OneDrive, etc.</p>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
		<!--===Service Section End===-->

		<!--===Icons Start===-->
		<div class="mfh_services_section">
			<div class="container">
				<div class="mfh_heading">
					<h2>Security Guarantee</h2>
				</div>
				<div class="mfh_how_work">
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="mfh_services mfh_services_ht">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon1.png" alt=""/>
								</div>
								<h4>Website connect and cloud transfer using 256-bit AES encryption for SSL.</h4>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="mfh_services mfh_services_ht">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon2.png" alt=""/>
								</div>
								<h4>Does not save or cache your data and files on our servers.</h4>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="mfh_services mfh_services_ht">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon3.png" alt=""/>
								</div>
								<h4>Access cloud drives with OAuth authorization and does not save your password.</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Icons End===-->
		
		<!--===Video Section Start===-->
		<div class="mfh_video_section">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>About SelfDrive</h1>
								<h4>The Smart Cloud Manager for ALL Your Cloud Transfer, Sync, Backup and Management Needs.<br>
									Data security is at the heart of us, we take security seriously.</h4>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture2.png"  alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="mfh_video_section mfh_video_tr">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture3.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>Who We Are</h1>
								<h4>Get Work For Your Idle Machines In Just Few Clicks</h4>
								<p>Welcome to SelfDrive. We are an international software company established in 2022. Through years of continuous research and development, we have created this award-winning and highly-rated brand software SelfDrive. It is a leading web service that can help users easily achieve cloud data management, migration, synchronization and backup between cloud services quickly, safely and smoothly.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Video us Section End===-->
		
		<!--===Icons Start===-->
		<div class="mfh_services_section">
			<div class="container">
				<div class="mfh_heading">
					<h2>Our Values</h2>
				</div>
				<div class="mfh_how_work">
					<div class="row">
						<div class="col-lg-3 col-md-6">
							<div class="mfh_services">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon4.png" alt=""/>
								</div>
								<h4>Users First</h4>
								<p>Users are the ones on whom our livelihood depends and the presenter of SelfDrive values.</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="mfh_services">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon5.png" alt=""/>
								</div>
								<h4>Honesty</h4>
								<p>Those who are not trust-worthy will achieve nothing; those businesses without honest will not succeed.</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="mfh_services">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon6.png" alt=""/>
								</div>
								<h4>Respect</h4>
								<p>The first important thing of managing human relations is respecting others.</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="mfh_services">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon7.png" alt=""/>
								</div>
								<h4>Responsibility</h4>
								<p>How much of the responsibility would you lay is the degree to how great is the success.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="mfh_heading mfh_heading_space">
					<h2>What SelfDrive Offer</h2>
				</div>
				<div class="mfh_how_work">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="mfh_services mfh_servimain">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon8.png" alt=""/>
								</div>
								<h4>Highlights Features</h4>
								<ul>
									<li>Industry-leading seamless cloud data migration engine.</li>
									<li>Customize a synchronization strategy for cloud data.</li>
									<li>Automatic, smart and convenient online cloud backup.</li>
									<li>Manage all cloud content with just one single login.</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="mfh_services mfh_servimain">
								<div class="mfh_icon">
									<img src="<?php echo base_url();?>assets/backend/images/icon/icon9.png" alt=""/>
								</div>
								<h4>How We're Different</h4>
								<ul>
									<li>Power: Support 10+ leading cloud storage providers.</li>
									<li>Safe: 100% data security with the strongest 256-bit AES encryption.</li>
									<li>Easy: Get done smoothly without tech knowledges or skills.</li>
									<li>Fast: All operations with the highest success rate at a superfast speed.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Icons End===-->

		<div class="mfh_video_section">
			<div class="container">
				<div class="mfh_heading">
					<h2>SelfDrive Security</h2>
					<p>Get a peace of mind of knowing you are safe</p>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture7.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>Never Save Your Data</h1>
								<p>SelfDrive doesn’t store user’s data in any form. When you try to visit one of your cloud drive accounts via SelfDrive, a connection will be built up automatically between the two servers: SelfDrive and the cloud drive you request. SelfDrive only works as a channel. And all the files and data that you visited via SelfDrive will not be saved on our server and they are still kept in their original cloud drives. Thus, there is no need for you to worry about the privacy and security of your data. SelfDrive can only be allowed to access into your cloud drive accounts with your own permission.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="sc_spacer"></div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>Oath Authorization System</h1>
								<p>The authorization system of SelfDrive is based on OAuth , which means the username and password for the cloud drive account you add to SelfDrive will not be recorded. OAuth is a standard authorization framework supported by Google Drive, SkyDrive, and Dropbox, etc. It enables third-party applications to build up connections with cloud services without a username and password.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture8.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="sc_spacer"></div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture9.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>256-bit AES Encryption</h1>
								<p>We choose to apply 256-bit AES Encryption for SSL during data transfer. It’s an advanced encryption technology that can effectively protect your data from tampering, interception and cracking. Please rest assured that it’s 100% safe for you to use the service of SelfDrive.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="sc_spacer"></div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>Compliance With GDPR & Privacy</h1>
								<p>We respect your privacy. We apply appropriate security and technical measures to protect the privacy of personal data. Besides, only personal data necessary for a specific purpose can be processed by SelfDrive. You can view our privacy policy to understand what information we collect and how we use, share, and protect user information.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture4.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="sc_spacer"></div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture5.png"  alt=""/>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>Remove Added Clouds Anytime</h1>
								<p>When you add a cloud account to SelfDrive, you need to authorize SelfDrive connection permissions. Absolutely, you can revoke authorization to SelfDrive anytime. If you revoke the authorization, SelfDrive will no longer be connected to this cloud account. Learn more about it in your cloud account treaty.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="sc_spacer"></div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_ab_main">
							<div class="mfh_ab_detail">
								<h1>Delete Account Permanently</h1>
								<p>We respect all your decisions and your privacy. You can permanently delete SelfDrive account at any time. When you decide to close it, all your account information will be cleared from our database.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 align-self-center">
						<div class="mfh_video_img">
							<img src="<?php echo base_url();?>assets/backend/images/picture6.png"  alt=""/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Video us Section End===-->
		
		<!--===Testimonial Section Start===-->
		<div class="mfh_footer_main">
			<div class="mfh_mini_footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="mfh_menu_copy">  
								<ul class="page_links">
                                    <li><a href="https://selfdrive.work/home/terms">Terms &amp; Conditions</a></li> 
                                    <li><span>|</span></li>
                                    <li><a href="https://selfdrive.work/home/privacy">Privacy Policy</a></li>
                                </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--===Testimonial Section End===-->
	</div>
</body> 
</html>