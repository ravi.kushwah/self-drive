
<html>
<head>
	<title>SelfDrive - About us</title>
  <meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML, CSS, JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Red+Hat+Display:400,500,700,900&amp;display=swap" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="https://selfdrive.work/assets/auth/images/favicon.png">
  <style>
	  h3 {
		font-size: 23.5pt;
		font-family: Arial;
		color: #333333;
		background-color: transparent;
		font-weight: bold;
		font-style: normal;
		font-variant: normal;
		text-decoration: none;
		vertical-align: baseline;
		white-space: pre-wrap;
		margin-bottom: 0px;
	}
	.te_page_box {
		font-family: Arial;
		color: #444444;
		font-weight: 500;
		width: 1150px;
		margin: 50px auto;
		line-height: 1.38;
	}
	body {
    margin: 0;
    padding: 0;
    font-family: var(--font-family);
    color: var(--light_color);
    background-color: #f0f7fd;
    font-size: 14px;
    letter-spacing: 0.4px;
    font-weight: 500;
    overflow-x: hidden;
}
:root {
    --main_color: #0180ff;
    --light_main_color: #e3f7fe;
    --light_color: #a1a3ce;
    --dark_color: #3f3f59;
    --black_color: #11122e;
    --font-family: "Be Vietnam", sans-serif;
}

.te_page_header {
    padding: 25px 40px;
    display: flex;
    margin-left: 285px;
    justify-content: space-between;
    align-items: center;
    background-color: #ffffff;
    box-shadow: 0px 0px 30px 0px rgb(221 227 236 / 5%);
    margin: 40px auto 40px;
    border-radius: 12px;
}

.te_page_header .mh_btn {
    display: inline-block;
    text-align: center;
    height: 50px;
    line-height: 50px;
    padding: 0 30px;
    border: none;
    font-size: 14px;
    color: #ffffff;
    font-weight: 600;
    text-transform: capitalize;
    background: var(--main_color);
    border-radius: 5px;
    cursor: cursor;
    text-decoration: none;
}

.te_page_inner {
        padding: 25px 40px;
    justify-content: space-between;
    align-items: center;
    background-color: #ffffff;
    box-shadow: 0px 0px 30px 0px rgb(221 227 236 / 5%);
    margin: 40px auto 40px;
    border-radius: 12px;
    position: relative;
    padding-top: 100px;	
}
.te_page_inner h1 {
    font-weight: 700;
    text-transform: capitalize;
    color: var(--dark_color);
    margin: 0 0 30px;
    background: #fafcff;
    border-bottom: 1px solid #e5e7f7;
    padding: 30px 40px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    font-size: 25px;
    border-radius: 10px 10px 0 0;
}
.te_page_inner h3 {
    font-size: 20px;
}
ul.page_links {
    display: flex;
    align-items: center;
    justify-content: center;
    list-style: none;
    padding: 0;
    margin: 0;
}
ul.page_links li {
    margin: 0 10px;
}
ul.page_links li a {
    color: #0180ff;
    font-size: 16px; 
    font-weight: 700;
}
  </style>
</head>
<body>
	<div class="te_page_box">
	    <div class="te_page_header">
			<div class="te_page_logo">
				<a href="https://selfdrive.work/" class="mh_logo" target="_blank"><img src="https://selfdrive.work/assets/auth/images/logo.png" class=""></a>
			</div>
			<div class="te_page_button">
				<a href="https://selfdrive.work/" target="_blank" class="mh_btn">login now</a> 
			</div>
		</div>
		<div class="te_page_inner">
			<h1>Terms Of Service</h1>
			<h3>1. Terms</h3>
            <p>By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law. </p>
            
            <h3>Use of Service</h3>
            <h3>Your Stuff & Your Permissions:</h3>
             <p>When you use our Services, you provide us with things like your files, content, messages, contacts, and so on (“Your Stuff”).</p>
             <p>Your Stuff is yours. These Terms don’t give us any rights to Your Stuff except for the limited rights that enable us to offer the Services.</p>
             <p>We need your permission to do things like hosting Your Stuff, backing it up, and sharing it when you ask us to. Our Services also provide you with features like commenting, sharing, searching, image thumbnails, document previews, optical character recognition (OCR), easy sorting and organization, and personalization to help reduce busywork. To provide these and other features, We accesses, stores, and scans Your Stuff. You give us permission to do those things, and this permission extends to our affiliates and trusted third parties we work with.</p>
             
            
            <h3>Your Responsibilities:</h3>
            <p>Your use of our Services must comply with our Acceptable Use Policy. Content in the Services may be protected by others’ intellectual property rights. Please don’t copy, upload, download, or share content unless you have the right to do so.</p>
            <p>We may review your conduct and content for compliance with these Terms and our Acceptable Use Policy. We aren’t responsible for the content people post and share via the Services.</p>
            <p>Help us keep Your Stuff protected. Safeguard your password to the Services, and keep your account information current. Don’t share your account credentials or give others access to your account.</p>
            <p>You may use our Services only as permitted by applicable law, including export control laws and regulations. Finally, to use our Services, you must be at least 13 (or older, depending on where you live).</p>
            
            <h3>Copyright:</h3>
            <p>We respect the intellectual property of others and ask that you do too. We respond to notices of alleged copyright infringement if they comply with the law, and such notices should be reported using our Copyright Policy. We reserve the right to delete or disable content alleged to be infringing and terminate accounts of repeat infringers. Our designated agent for notice of alleged copyright infringement on the Services is:</p>
            
            <h3>Acceptable Use Policy:</h3>
            <p>This Software is used by millions of people, and we're proud of the trust placed in us. In exchange, we trust you to use our services responsibly.</p>
            <p>You agree not to misuse the Our services ("Services") or help anyone else to do so. For example, you must not even try to do any of the following in connection with the Services:</p>
            <p>probe, scan, or test the vulnerability of any system or network, unless done in compliance with our Bug Bounty Program;</p>
            <p>breach or otherwise circumvent any security or authentication measures;</p>
            <p>access, tamper with, or use non-public areas or parts of the Services, or shared areas of the Services you haven't been invited to;</p>
            <p>interfere with or disrupt any user, host, or network, for example by sending a virus, overloading, flooding, spamming, or mail-bombing any part of the Services;</p>
            <p>access, search, or create accounts for the Services by any means other than our publicly supported interfaces (for example, "scraping" or creating accounts in bulk);</p>
            <p>send unsolicited communications, promotions or advertisements, or spam;</p>
            <p>send altered, deceptive or false source-identifying information, including "spoofing" or "phishing";</p>
            <p>promote or advertise products or services other than your own without appropriate authorization;</p>
            <p>abuse referrals or promotions to get more storage space than deserved or to sell storage space received from referrals or promotions;</p>
            <p>circumvent storage space limits;</p>
            <p>sell the Services unless specifically authorized to do so;</p>
            <p>publish or share materials that are unlawfully pornographic or indecent, or that contain extreme acts of violence or terrorist activity, including terror propaganda;</p>
            <p>advocate bigotry or hatred against any person or group of people based on their race, religion, ethnicity, sex, gender identity, sexual orientation, disability, or impairment;</p>
            <p>harass or abuse Our personnel or representatives or agents performing services on behalf of Us; violate the law in any way, including storing, publishing or sharing material that's fraudulent, defamatory, or misleading; or violate the privacy or infringe the rights of others. All Services may only be used for lawful purposes. The laws of the United States of America apply to all Clients of Our Services</p>
            <p>You agree to indemnify and hold harmless Our Service from any claims resulting from your use of Our Services. You represent and warrant to Us that you are eighteen (18) years of age or older. Any use of or access to the Services by anyone under eighteen (18) years of age is prohibited.</p>
            <p>If you use the Services on behalf of another party, you agree that you are authorized to bind such other party to this Agreement and to act on such other party's behalf with respect to any actions you take in connection with the Services.</p>
            <p>Use of the Our Services to infringe upon any copyright or trademark is prohibited. This includes but is not limited to unauthorized copying of music, books, photographs, or any other copyrighted work. The offer of sale of any counterfeit merchandise of a trademark holder will result in the immediate termination of your account. Any account found to be in violation of another's copyright will be expeditiously removed, or access to the material disabled. Any account found to be in repeated violation of copyright laws will be suspended and/or terminated from our hosting. If you believe that your copyright or trademark is being infringed upon, please contact us with the information pertaining to your claim.</p>
            <p>Using a shared account as a backup/storage device is not permitted, with the exception of one cPanel backup of the same account. Please do not take backups of your backups.</p>
            <p>Examples of unacceptable material on all Shared and Reseller servers include:</p>
            
            <h3>X IRC Scripts/Bots</h3>
            <h3>Proxy Scripts/Anonymizers</h3>
            <h3>Pirated Software/Warez</h3>
            <h3>Image Hosting Scripts (similar to Photobucket or Tinypic)</h3>
            <h3>IP Scanners</h3>
            <h3>Bruteforce Programs/Scripts/Applications</h3>
            <h3>Mail Bombers/Spam Scripts</h3>
            <h3>File Dump/Mirror Scripts (similar to Rapidshare)</h3>
            <h3>Sale of any controlled substance without prior proof of appropriate permit(s)</h3>
            <h3>Lottery/Gambling Sites</h3>
            <h3>Sites promoting illegal activities</h3>
            <h3>Forums and/or websites that distribute or link to Warez/pirated/illegal content.</h3>
            <h3>Fraudulent Sites (Including, but not limited to sites listed at aa419.org & escrow-fraud.com).</h3>
            
            <p>Zero-Tolerance SPAM Policy (Only applicable for email service if we offer)</p>
            <p>We has zero-tolerance for Clients sending unsolicited e-mail, unsolicited e-mailing, and SPAM. "Safe lists," purchased lists, and selling of lists will be treated as SPAM. Any Client who sends out SPAM will have their account terminated, and may be terminated without notice. Additionally, Clients who engage in unsolicited mailing will, at our ’ own discretion, be charged a consulting fee of two hundred fifty dollars ($250) per hour for any remedial actions that we elects to take in the event that, as a result of a Client's activities, Our servers or IP space are placed in any third-party mail filtering software or blackhole lists.</p>
            <p>To protect the IP reputation of our network, and as a proactive measure to prevent our IP space from being used to distribute unsolicited e-mail, We reserves the right to apply an SMTP filter as a precaution to accounts flagged by our Abuse Team. As long as this filter is in place, all active or newly-ordered services across the Client’s account will be placed under this filter.</p>
            <p>To have this filter lifted, We  would require the Client to pay a deposit of up to two hundred fifty dollars ($250) per server in order to be allowed to send emails. Should any SPAM complaints be received after the filter had been lifted, it would immediately be re-applied to the client’s account. So long as no SPAM complaints have been received, the deposit will be refunded upon request at the time of cancellation.</p>
            
            
            <h3>Backups and Data Loss</h3>
            <p>Your use of Our Services is at your sole risk. Our servers are setup with RAID to constantly mirror all data to a second disk. We is not responsible for files and/or data stored on our servers. You agree to take full responsibility for files and data transferred and to maintain all appropriate backups of files and data stored on our servers. We do not take backups of any kind of customer data, customer's are responsible for managing and backing up their own data.</p>
            
            <h3>2. Use License</h3>
            <p>Permission is granted to temporarily download one copy of the materials (information or software) on Our web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not: modify or copy the materials; use the materials for any commercial purpose, or for any public display (commercial or non-commercial); attempt to decompile or reverse engineer any software contained on Our web site; remove any copyright or other proprietary notations from the materials; or transfer the materials to another person or “mirror” the materials on any other server. This license shall automatically terminate if you violate any of these restrictions and may be terminated by us at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format. </p>
            
            <h3>3. Disclaimer</h3>
            <p>The materials on Our web site are provided “as is”. We makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, We do not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site. </p>
            
            <h3>4. Limitations</h3>
            <p>In no event shall Our Software or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on our Internet site, even if our or an authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you. </p>
            
            <h3>5. Revisions and Errata</h3>
            <p>The materials appearing on our web site could include technical, typographical, or photographic errors. We do not warrant that any of the materials on its web site are accurate, complete, or current. We may make changes to the materials contained on its web site at any time without notice. We do not, however, make any commitment to update the materials. </p>
            
            <h3>6. Links</h3>
            <p>We had not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by our site. Use of any such linked web site is at the user’s own risk. </p>
        
            <h3>7. Site Terms of Use Modifications</h3>
            <p>We may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use. </p>
            
            <h3>8. Governing Law </h3>
            <p>Any claim relating to our web site shall be governed by the laws of  India without regard to its conflict of law provisions. General Terms and Conditions applicable to Use of a Web Site. We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. </p>
           
        </div>
        <ul class="page_links">
            <li><a href="https://selfdrive.work/home">Home</a></li> 
            <li><span>|</span></li>
            <li><a href="https://selfdrive.work/privacy">Privacy Policy</a></li>
        </ul>
	</div>
</body> 
</html>