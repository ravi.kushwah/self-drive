<?php 
    // echo"<pre>";print_r($getAllFolder);die; 
   
   ?>
<div class="page-wrapper">
    <div class="main-content">
      <div class="row">
         <div class="col xl-12 col-lg-12">
            <div class="page-connection-wrapper">
               <div class="sc_title_bar">
                  <h4 class="cd_header_title"><?=isset($connectionType)?$connectionType:''?></h4>
                  <p>You can manage all your shared files here.</p>
                  <p><?=isset($email)?$email:''?></p>
                  <?php if($connectionType=="GooglePhoto"){ ?>
                    <h5 class="not_box_wrapper">1. You have full access to the albums that are created within SelfDrive. <br> 2. If you have created an album using Google photos then you can view that album in SelfDrive however won't be able to upload any new media file.</h5>
                  <?php } if($connectionType=="Gofile"){ ?>
                    <h5 class="not_box_wrapper">1. Response from Gofile is slower. So if you see any success response than your changes maybe reflected after 1-2 refresh.</h5>
                  <?php } if($connectionType=="Box"){ ?>
                    <h5 class="not_box_wrapper">1. The Box token has lifespan of 1 hour. After 1 hour you'll need to re-authorize your drive.</h5>
                  <?php } if($connectionType=="DropBox"){ ?>
                    <h5 class="not_box_wrapper">1. Please make sure you have turned On the sharing option for your files from Dropbox.</h5>
                  <?php } ?>
               </div>
               <div class="sc_share_btn">
                  <a href="javascript:;" id="<?php echo $this->uri->segment(3);?>" tofolderid="" class="ad-btn folderBack">
                     <svg xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
                        <path d="M13.825,18.000 C10.772,18.000 7.928,16.517 6.217,14.033 C5.870,13.530 6.006,12.845 6.519,12.505 C7.032,12.165 7.729,12.297 8.076,12.801 C9.369,14.678 11.518,15.799 13.825,15.799 C17.647,15.799 20.757,12.749 20.757,9.000 C20.757,5.251 17.647,2.200 13.825,2.200 C11.512,2.200 9.359,3.326 8.067,5.212 C7.721,5.717 7.024,5.851 6.510,5.512 C5.996,5.173 5.859,4.489 6.205,3.985 C7.915,1.490 10.763,-0.000 13.825,-0.000 C18.884,-0.000 23.000,4.037 23.000,9.000 C23.000,13.963 18.884,18.000 13.825,18.000 ZM11.293,6.704 C10.902,6.313 10.902,5.679 11.293,5.287 C11.683,4.896 12.316,4.896 12.707,5.287 L15.706,8.291 C15.730,8.315 15.752,8.339 15.772,8.365 C15.777,8.371 15.782,8.377 15.787,8.384 C15.802,8.403 15.817,8.423 15.831,8.443 C15.835,8.449 15.838,8.455 15.842,8.462 C15.856,8.483 15.869,8.505 15.881,8.527 C15.883,8.532 15.886,8.537 15.888,8.541 C15.901,8.566 15.913,8.590 15.923,8.616 C15.925,8.619 15.925,8.622 15.927,8.626 C15.938,8.653 15.948,8.680 15.956,8.708 C15.957,8.712 15.958,8.715 15.959,8.719 C15.967,8.747 15.974,8.774 15.980,8.803 C15.982,8.812 15.982,8.820 15.984,8.829 C15.988,8.852 15.992,8.876 15.995,8.900 C15.998,8.933 16.000,8.966 16.000,9.000 C16.000,9.033 15.998,9.067 15.995,9.100 C15.992,9.124 15.988,9.148 15.984,9.172 C15.982,9.180 15.982,9.189 15.980,9.197 C15.974,9.226 15.967,9.254 15.959,9.282 C15.958,9.285 15.957,9.288 15.956,9.292 C15.948,9.320 15.938,9.348 15.927,9.375 C15.925,9.378 15.925,9.381 15.923,9.384 C15.913,9.410 15.901,9.435 15.888,9.459 C15.886,9.464 15.883,9.468 15.881,9.473 C15.869,9.495 15.856,9.517 15.842,9.539 C15.838,9.545 15.835,9.551 15.831,9.557 C15.817,9.577 15.802,9.597 15.787,9.616 C15.782,9.623 15.777,9.629 15.772,9.636 C15.752,9.661 15.730,9.685 15.707,9.708 L12.707,12.712 C12.511,12.908 12.256,13.006 12.000,13.006 C11.744,13.006 11.488,12.908 11.293,12.712 C10.902,12.321 10.902,11.688 11.293,11.296 L12.586,10.001 L1.000,10.001 C0.448,10.001 -0.000,9.553 -0.000,9.000 C-0.000,8.447 0.448,7.998 1.000,7.998 L12.586,7.998 L11.293,6.704 Z" class=""></path>
                     </svg>
                     Back
                  </a>
                
                    <a  href="javascript:;"  class="ad-btn GooglePgotos ">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M4 8c0 .6.4 1 1 1h2c.6 0 1-.4 1-1V5h2L6 0 2 5h2v3zm5.4 1c-.3.5-.8 1-1.4 1H4c-.7 0-1.4-.4-1.7-1H0v3h12V9H9.4zm1.6 2h-1v-1h1v1z"/>
                     </svg>
                     Upload File
                  </a>
                 <?php 
                    if($connectionType=="s3" || $connectionType=="Anon" ){
                        ?>
                         <a  href="javascript:;"  class="ad-btn amazonS3UploadFile" <?= $connectionType == 's3'? 'style="display:none"': ''; ?>>
                             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                <path d="M4 8c0 .6.4 1 1 1h2c.6 0 1-.4 1-1V5h2L6 0 2 5h2v3zm5.4 1c-.3.5-.8 1-1.4 1H4c-.7 0-1.4-.4-1.7-1H0v3h12V9H9.4zm1.6 2h-1v-1h1v1z"/>
                             </svg>
                             Upload File
                          </a>
                        <?php
                        
                    }else{
                        ?> <a href="javascript:;"  class="ad-btn UploadFile">
                             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                <path d="M4 8c0 .6.4 1 1 1h2c.6 0 1-.4 1-1V5h2L6 0 2 5h2v3zm5.4 1c-.3.5-.8 1-1.4 1H4c-.7 0-1.4-.4-1.7-1H0v3h12V9H9.4zm1.6 2h-1v-1h1v1z"/>
                             </svg>
                             Upload File
                          </a>
                        <?php
                    }
                
                if(count($_SESSION['current_plan']) > 1){
                
                    if($connectionType=="GooglePhoto"){
                        ?>
                            <a href="javascript:;" class="ad-btn CreateAlbum CreateFolder" popuptitle="Create Album">
                                 <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                    <path d="M11 3H7V2c0-.6-.4-1-1-1H1v9c0 .6.4 1 1 1h9c.6 0 1-.4 1-1V4c0-.6-.4-1-1-1zM9 7H7v2H6V7H4V6h2V4h1v2h2v1z"/>
                                 </svg>
                                Create Album
                            </a>
                        <?php
                    }else if($connectionType != "Anon"){
                        ?>
                            <a href="javascript:;" class="ad-btn CreateFolder" popuptitle ="Create Folder">
                                 <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                    <path d="M11 3H7V2c0-.6-.4-1-1-1H1v9c0 .6.4 1 1 1h9c.6 0 1-.4 1-1V4c0-.6-.4-1-1-1zM9 7H7v2H6V7H4V6h2V4h1v2h2v1z"/>
                                 </svg>
                                 New Folder
                            </a>
                        <?php
                    }
                  
                  }?>
               </div>
               <div class="sc_share_main_section <?=(empty($getAllFolder['data']) ? 'sc_nofile_avai': '')?> ">
                  <?php $i=1;
                        if(!empty($getAllFolder['data'])){
                            $class = '';
                            $hideOption = '';
                            $data=  isset($getAllFolder['data']['CommonPrefixes']) ? $getAllFolder['data']['CommonPrefixes'] : $getAllFolder['data'];
                            //   echo "<pre>"; print_r($getAllFolder['data']);die();
                            $is_folder = 'getSubFolderByFolderId';
    
                            $my_specs = array('delete'=> '','download' => '','copy_to' => '','share' => '','cut' => '','rename' => 'hidden','copy' => '','move' => '','preview' => '');

                            foreach($data as $GetFolder){
                                if($connectionType=="Google Drive"){
                                    $viewId = $GetFolder['id'];
                                    $fileTag = 'folder';
                                    $class = "getSubFolderByFolderId";
                                    $hideOption = 'hidden';
                                }
                                if($connectionType=="DropBox"){
                                    $viewId = $GetFolder['path_lower'];
                                    $fileTag = $GetFolder['.tag']; // folder
                                    
                                    if($fileTag=="folder"){
                                        $class = "getSubFolderByFolderId";
                                         $hideOption = 'hidden';
                                    }else{
                                        $class = "";
                                        $hideOption = '';
                                    }
                                }
                                if($connectionType=="Ddownload"){
                                    if(isset($GetFolder['file_code'])){
                                        $viewId = $GetFolder['fld_id'];
                                        $file_code = $GetFolder['file_code'];
                                    }else{
                                        $viewId = $GetFolder['fld_id'];
                                    }
                                    $fileTag = $GetFolder['tag']; // folder
                                    $class = "getSubFolderByFolderId";
                                    $hideOption = '';
                                }
                                if($connectionType=="OneDrive"){
                                    $viewId = $GetFolder['id'];
                                    if(isset($GetFolder['file'])){
                                        $fileTag = 'file'; // folder
                                        $class = "";
                                        $hideOption = '';
                                    }else{
                                        $fileTag = 'folder'; // folder
                                        $hideOption = 'hidden';
                                        $class = "getSubFolderByFolderId";
                                    }
                                }
                                if($connectionType=="Box"){
                                    $viewId = $GetFolder['id'];
                                    $fileTag = $GetFolder['type'];
                                    $hideOption = '';
                                    $class = "getSubFolderByFolderId";
                                }
                                if($connectionType=="Gofile"){
                                  $viewId     = $GetFolder['id'];
                                  $fileTag    = $GetFolder['type'];
                                  $downloadUrl = isset($GetFolder['directLink'])?$GetFolder['directLink']:'';
                                  if ( $fileTag == 'folder') {
                                      $class = "getSubFolderByFolderId";
                                       $hideOption = 'hidden';
                                     $my_specs = array('delete'=> '','download' => 'hidden','copy_to' => 'hidden','share' => 'hidden','cut' => 'hidden','rename' => 'hidden','copy' => 'hidden','move' => 'hidden','preview' => 'hidden');
                                  }else{
                                      $class = "";
                                      $hideOption = '';
                                      $my_specs = array('delete'=> '','download' => '','copy_to' => 'hidden','share' => '','cut' => 'hidden','rename' => 'hidden','copy' => 'hidden','move' => '','preview' => 'hidden');
                                  }
                               }
                                if($connectionType=="Ftp"){
                                  $viewId     = $GetFolder['name'];
                                  $fileTag    = $GetFolder['type'];
                                  $downloadUrl = base_url('home/downloadFtpFile/?id='.$this->uri->segment(3).'&path='.urlencode($viewId));
    
                                  if ( $fileTag == 'file') {
                                     $my_specs = array('delete'=> 'fileid = "'.$viewId.'" folderID = ""','download' => 'href="'.$downloadUrl.'" class="" target="_blank"'      ,'copy_to' => 'hidden','share' => ''      ,'cut' => 'hidden','rename' => 'class="Renamefiles" fileID="'.$viewId.'" fileCode="'.$viewId.'"'      ,'copy' => 'hidden','move' => '','preview' => 'hidden');
                                     $is_folder = '';
                                     $class = "";
                                     $hideOption = '';
                                  }else{
                                     $fileTag = 'folder';
                                     $hideOption = 'hidden';
                                      $class = "getSubFolderByFolderId";
                                     $my_specs = array('delete'=> 'folderID = "'.$viewId.'"'            ,'download' => 'hidden','copy_to' => 'hidden','share' => 'hidden','cut' => 'hidden','rename' => 'class="Renamefiles" fileID="'.$viewId.'" fileCode="'.$viewId.'"','copy' => 'hidden','move' => '','preview' => 'hidden');
                                  }
                               }
                                if($connectionType=="Pcloud"){
                                    $type = $GetFolder['isfolder'];
                                        
                                    if ( $type == 1) {
                                        $fileTag = 'folder';
                                        $viewId     = $GetFolder['folderid'];
                                        $my_specs = array('delete'=> 'folderID = "'.$GetFolder['folderid'].'"'            ,'download' => 'hidden','copy_to' => 'hidden','share' => 'hidden','cut' => 'hidden','rename' => 'class="Renamefiles" fileCode="'.$GetFolder['folderid'].'"','copy' => 'hidden','move' => '','preview' => 'hidden');
                                        $class = "getSubFolderByFolderId";
                                        $hideOption = 'hidden';
                                    }else{
                                        $fileTag = 'file';
                                        $viewId     = $GetFolder['fileid'];
                                        $downloadUrl = base_url('home/downloadPcloudFile/'.$this->uri->segment(3).'/'.$GetFolder['fileid']);
                                        $my_specs = array('delete'=> 'fileid = "'.$GetFolder['fileid'].'" folderID = ""','download' => 'href="'.$downloadUrl.'" class="" target="_blank"'      ,'copy_to' => 'hidden','share' => ''      ,'cut' => 'hidden','rename' => 'class="Renamefiles" fileID="'.$GetFolder['fileid'].'" '      ,'copy' => 'hidden','move' => '','preview' => 'hidden');
                                        $class = "";
                                        $hideOption = '';
                                    }
                                }
                                if($connectionType=="Backblaze"){
                                  $type = $GetFolder['action'];
                                  if($type == 'folder'){
                                      $viewId = $GetFolder['fileName'];
                                      $fileTag = $type;
                                      $file_code = '';
                                      $class = "getSubFolderByFolderId";
                                      $hideOption = 'hidden';
                                      $my_specs = array('delete'=> '','download' => 'hidden' ,'copy_to' => 'hidden','share' => 'hidden' ,'cut' => 'hidden','rename' => 'hidden' ,'copy' => 'hidden','move' => '','preview' => 'hidden');
                                  }else{
                                      $viewId = $GetFolder['fileId'];
                                      $fileTag = 'file';
                                       $hideOption = '';
                                      $file_code = $GetFolder['fileId'];
                                      $downloadUrl = base_url('home/downloadB2File/'.$file_code.'/'.$this->uri->segment(3).'/'.basename($GetFolder['fileName']) );
                                      $my_specs = array('delete'=> 'folderID = "'.$GetFolder['fileName'].'"' ,'download' => 'href="'.$downloadUrl.'" class="" target="_blank"','copy_to' => 'hidden','share' => '','cut' => 'hidden','rename' => 'hidden','copy' => 'hidden','move' => '','preview' => 'hidden');
                                  }
                                 $GetFolder['name'] = str_replace('/', '', $GetFolder['fileName']);
                                }
                                if($connectionType=="GooglePhoto"){
                                    $viewId = $GetFolder['id'];
                                    $fileTag = 'folder';
                                    $hideOption = 'hidden';
                                    $class = "getSubFolderByFolderId";
                                }
                                if($connectionType=="Anon"){
                                    // echo "<pre>"; print_r($GetFolder);die();
                                    $id = $GetFolder['conn_id'];
                                    $viewId = $GetFolder['file_id'];
                                    $fileTag = 'file';
                                    // https://anonfiles.com/account/file/'.$fileId.'/remove?p=1
                                    $downloadUrl = $GetFolder['file_url'];
                                    $deleteUrl =  'https://anonfiles.com/account/file/'.$viewId.'/remove?p=1';
                                    $GetFolder['name'] =  $GetFolder['file_name'];
                                    $class = "getSubFolderByFolderId";
                                    // echo"<pre>";print_r($GetFolder['name']);die;
                                    // echo"<pre>";print_R($viewId);die;
                                }
                                if($connectionType=="s3"){
                                    $viewId = $GetFolder['Prefix'];
                                    $str_arr = explode ("/", $viewId); 
                                    $GetFolder = $str_arr[0]; 
                                    $path_lower= "";
                                    $fileTag = 'folder';
                                    $fileext= '.png';
                                    $class = "getSubFolderByFolderId";
                                    $hideOption = 'hidden';
                                    ?>
                                    <div class="sc_share_wrapper <?=$class?>"  option="option<?php echo $i;?>"type="<?= $connectionType?>" id="<?php echo $id;?>" data-folder-name="<?= $GetFolder?>"   droType="<?php echo isset($viewId) ? $viewId : '';?>" data-file-Id="<?php echo isset($viewId) ? $viewId : '';?>" data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>">
                                        <div class="sc_dots_img">
                                              <img src="<?php echo base_url('assets/backend/images/dots.svg') ?>" alt="">
                                        </div>
                                        <div class="checkbox" hidden>
                                            <input type="checkbox" id="takenBefore" placeholder="Yes">
                                            <label for="rememberme<?php echo  $i;?>" checkBoxValue="rememberme<?php echo  $i;?>" class="option"></label>
                                        </div>
                                        <div class="sc_share_img sc_folder_img ">
                                            <img src="<?php echo base_url('assets/backend/images/'.($fileTag).'.svg');?>" alt="icon">
                                        </div>
                                        <div class="sc_share_detail "  type="<?= $connectionType?>" id="<?php echo $this->uri->segment(3);?>" data-folder-name="<?= $GetFolder?>" droType="<?php echo isset($GetFolder) ? $GetFolder : '';?>" data-file-Id="<?php echo isset($viewId) ? $viewId : '';?>"  data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>">
                                            <h5 class="" type="<?= $connectionType?>" id="<?php echo $this->uri->segment(3);?>" data-folder-name="<?= $GetFolder?>"   droType="<?php echo isset($viewId) ? $viewId : '';?>" data-file-Id="<?php echo isset($viewId) ? $viewId : '';?>" data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>"><?=(isset($GetFolder) && $GetFolder !="")? $GetFolder :"";?></h5>
                                        </div>
                                        <div  class="checkOptionMenu tbf_IconHolter option<?php echo  $i;?>" clickValue="option<?php echo  $i;?>" >
                                            <div class="checkBoxSelect">
                                               <a hidden href="javascript:;" class="DownloadFile" drotype="" folderidshareurl="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userid="<?=(isset($id) && $id !="")? $id :"";?>">
                                                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                     <path fill-rule="evenodd"
                                                        d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                  </svg>
                                                  Download
                                               </a>
                                               <?php if(count($_SESSION['current_plan']) > 1 && !empty(array_intersect(["3"], $_SESSION['current_plan']))){?>
                                                <a hidden href="javascript:;" class="ShareURL" filename="<?=(isset($GetFolder) && $GetFolder !="")? $GetFolder :"";?>" drotype="" folderidshareurl="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" fileidshareurl="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userid="<?=(isset($id) && $id !="")? $id :"";?>">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"></path>
                                                    </svg>
                                                    Share
                                                </a>
                                                <?php }?>
                                               <a href="javascript:;" class="DeletFileAndFolder" user_id="<?php echo $this->uri->segment(3);?>" folderID =<?=(isset($viewId) && $viewId !="")? $viewId :"";?> fileID="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" >
                                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                     <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                  </svg>
                                                  Delete
                                               </a>
                                               <a hidden href="javascript:;" class="previewFile" drotype="" folderidshareurl="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userid="<?=(isset($id) && $id !="")? $id :"";?>">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                        <path fill-rule="evenodd" d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"></path>
                                                    </svg>
                                                    Preview
                                                </a>
                                                <?php if(count($_SESSION['current_plan']) > 1 && !empty(array_intersect(["3"], $_SESSION['current_plan']))){?>
                                                <a hidden href="javascript:;" class="moveFile " filename="<?=(isset($GetFolder) && $GetFolder !="")? $GetFolder :"";?>" userid="<?=(isset($id) && $id !="")? $id :"";?>" fileId="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" drotype="undefined">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                        <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"></path>
                                                    </svg>
                                                    Move
                                                </a>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                   <?php
                                }else if($connectionType=="GooglePhoto"){
                                     ?>
                                        <div class="sc_share_wrapper <?=$class?>" option="option<?php echo $i;?>"  type="<?= $connectionType?>" id="<?php echo $id;?>" data-folder-name="<?= $GetFolder['name'];?>"   droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>" data-file-Id="<?php echo isset($file_code) ? $file_code : '';?>" data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>">
                                           <div class="sc_dots_img" hidden>
                                                 <img src="<?php echo base_url('assets/backend/images/dots.svg') ?>" alt="">
                                           </div>
                                            <div class="checkbox" hidden>
                                                <input type="checkbox" id="takenBefore" placeholder="Yes">
                                                <label for="rememberme<?php echo  $i;?>" checkBoxValue="rememberme<?php echo  $i;?>" class="option"></label>
                                            </div>
                                            <div class="sc_share_img sc_folder_img ">
                                                <img src="<?php echo base_url('assets/backend/images/'.($fileTag).'.svg');?>" alt="icon">
                                            </div>
                                            <div class="sc_share_detail "  type="<?= $connectionType?>" id="<?php echo $this->uri->segment(3);?>" data-folder-name="<?= $GetFolder['name'];?>" droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>" data-file-Id="<?php echo isset($file_code) ? $file_code : '';?>"  data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>">
                                             <h5 ><?= $GetFolder['name'];?></h5>
                                            </div>
                                        </div>
                                       <?php
                                }else{
                                    $fileTag = ($fileTag == 'file')?'file.png':'folder.svg';
                                       ?>
                                        <div class="sc_share_wrapper">
                                           <div class="sc_dots_img">
                                                 <img src="<?php echo base_url('assets/backend/images/dots.svg') ?>" alt="">
                                           </div>
                                            <div class="checkbox" hidden>
                                                <input type="checkbox" id="takenBefore" placeholder="Yes">
                                                <label for="rememberme<?php echo  $i;?>" checkBoxValue="rememberme<?php echo  $i;?>" class="option"></label>
                                            </div>
                                            <div class="sc_share_img sc_folder_img <?=$class?>" option="option<?php echo $i;?>"type="<?= $connectionType?>" id="<?php echo $id;?>" data-folder-name="<?php echo isset($GetFolder['name']) ? $GetFolder['name'] :$GetFolder['fileName']; ?>"   droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>" data-file-Id="<?php echo isset($file_code) ? $file_code : '';?>" data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>">
                                                <img src="<?php echo base_url('assets/backend/images/'.($fileTag));?>" alt="icon">
                                            </div>
                                            <div class="sc_share_detail "  type="<?= $connectionType?>" id="<?php echo $this->uri->segment(3);?>" data-folder-name="<?php echo isset($GetFolder['name']) ? $GetFolder['name'] :$GetFolder['fileName'];?>" droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>" data-file-Id="<?php echo isset($file_code) ? $file_code : '';?>"  data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>">
                                                <h5 class="<?=$class?>" option="option<?php echo $i;?>"type="<?= $connectionType?>" id="<?php echo $id;?>" data-folder-name="<?php echo isset($GetFolder['name']) ? $GetFolder['name'] :$GetFolder['fileName']; ?>"   droType="<?php echo isset($GetFolder['path_lower']) ? $GetFolder['path_lower'] : '';?>" data-file-Id="<?php echo isset($file_code) ? $file_code : '';?>" data-folder-Id="<?php echo isset($viewId) ? $viewId : '';?>"><?php echo isset($GetFolder['name']) ? $GetFolder['name'] :$GetFolder['fileName'];?></h5>
                                            </div>
                                            <div class="checkOptionMenu tbf_IconHolter option<?php echo  $i;?>" clickValue="option<?php echo  $i;?>" style="display:none;">
                                				<div class="checkBoxSelect">
                                				    <a <?= $my_specs['download']; ?> href="<?=(isset($downloadUrl) ? $downloadUrl : 'javascript:;');?>" target="<?=(isset($downloadUrl) ? '_blank' : '');?>" <?=$hideOption?> class="DownloadFile"  folderIDShareURL="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userID="<?=$id?>" folderID="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" drotype="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.000,11.250 C12.000,11.664 11.664,12.000 11.250,12.000 L0.750,12.000 C0.336,12.000 -0.000,11.664 -0.000,11.250 C-0.000,10.836 0.336,10.500 0.750,10.500 L11.250,10.500 C11.664,10.500 12.000,10.836 12.000,11.250 ZM5.470,8.874 C5.616,9.021 5.808,9.094 6.000,9.094 C6.192,9.094 6.384,9.021 6.530,8.874 L9.188,6.217 C9.480,5.924 9.480,5.449 9.188,5.156 C8.895,4.863 8.420,4.863 8.127,5.156 L6.750,6.533 L6.750,0.750 C6.750,0.336 6.414,-0.000 6.000,-0.000 C5.586,-0.000 5.250,0.336 5.250,0.750 L5.250,6.533 L3.873,5.156 C3.580,4.863 3.105,4.863 2.812,5.156 C2.519,5.449 2.519,5.924 2.812,6.217 L5.470,8.874 Z"/>
                                                        </svg>
                                                        Download
                                                    </a>
                                                    <?php if(!empty(array_intersect(["3"], $_SESSION['current_plan']))){?>
                                                    <a <?= $my_specs['share']; ?> href="javascript:;" <?=$hideOption?> class="ShareURL" filename="<?php echo isset($GetFolder['name']) ? $GetFolder['name'] :$GetFolder['fileName'];?>" fileIDShareURL="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" folderIDShareURL="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userID="<?=$id?>" folderID="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" drotype="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9 4c-.5 0-1-.2-1.4-.5L4.9 5.1c.1.2.2.4.2.7 0 .4-.1.8-.4 1.1l2.5 1.4c.3-.1.6-.3 1-.3 1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2c0-.4.1-.7.3-1L4 7.5c-.3.1-.6.2-1 .2-1.1 0-2-.9-2-2s.9-2 2-2c.6 0 1.1.2 1.4.6L7 2.6c0-.1-.1-.4-.1-.6 0-1.1.9-2 2-2s2 .9 2 2c.1 1.1-.8 2-1.9 2z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Share
                                                    </a>
                                                    <a  kuldeep  <?= $my_specs['move']; ?> href="javascript:;" <?= (($connectionType=="Anon" || $connectionType=="Box" || $connectionType=="Ddownload")?'hidden':'');?> <?=$hideOption?> class="moveFile " filename="<?php echo isset($GetFolder['name']) ? $GetFolder['name'] :$GetFolder['fileName'];?>" folderID="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" drotype="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userId="<?=$id?>" fileId="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M10.6 8.1c-.6-.6-1.3-.9-1.9-.9h-.3l-.8-.8 3.1-3.2c.8-.8.8-2.4 0-3.2L6 4.8 1.3 0c-.8.8-.8 2.4 0 3.2l3.1 3.2-.8.8h-.3c-.7 0-1.4.3-1.9.9-1 1-1.1 2.5-.3 3.4.3.3.8.5 1.3.5.7 0 1.4-.3 1.9-.9.7-.6 1-1.5.9-2.3L6 8l.8.8c-.1.8.2 1.7.8 2.3.6.6 1.3.9 2 .9.5 0 1-.2 1.4-.6.8-.8.7-2.3-.4-3.3zM4 9.8c0 .2-.2.4-.4.6-.2.2-.4.3-.6.4-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.2-.2-.2-.2-.5-.2-.7 0-.2 0-.4.1-.6.1-.2.2-.4.4-.6s.4-.3.6-.4c.2-.1.4-.1.6-.1.2 0 .4 0 .6.2.2.2.3.5.3.7 0 .2-.1.4-.2.6zm2-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1zm4.2 3.2c-.2.2-.5.2-.6.2-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.6-.4-.2-.2-.4-.4-.4-.6-.1-.2-.2-.4-.2-.6 0-.2 0-.5.2-.7.2-.2.5-.2.6-.2.2 0 .4 0 .6.1.2.1.4.2.6.4.2.2.3.4.4.6.1.2.1.4.1.6.2.2.1.5-.1.7zm0 0"/>
                                                        </svg>
                                                        Move
                                                    </a>
                                                    <?php }?>
                                                    <a <?= $my_specs['delete']; ?> href="<?=(isset($deleteUrl) ? $deleteUrl : 'javascript:;');?>" target="<?=(isset($deleteUrl) ? '_blank' : '');?>" class="DeletFileAndFolder"  user_id="<?=$id?>" fileid="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" folderID="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" drotype="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                            <path d="M9.3 1H2.7c-.6 0-1.2.6-1.2 1.3v.6h9v-.6c0-.7-.6-1.3-1.2-1.3zM6.7 0H5.3c-.5 0-.8 1-.8 1h3s-.4-1-.8-1zm3 4H2.3c-.4 0-.8.4-.8.8v6.4c0 .4.4.8.8.8h7.4c.4 0 .8-.4.8-.8V4.8c0-.4-.4-.8-.8-.8zm-5.2 6h-1V6h1v4zm2 0h-1V6h1v4zm2 0h-1V6h1v4z" fill-rule="evenodd" clip-rule="evenodd"/>
                                                        </svg>
                                                        Delete
                                                    </a>
                                                    <a <?= $my_specs['preview']; ?> href="javascript:;" <?= (($connectionType=="Anon" || $connectionType=="Box" || $connectionType=="Ddownload")?'hidden':'');?> <?=$hideOption?> class="previewFile"  fileIDShareURL="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" folderIDShareURL="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" userID="<?=$id?>" folderID="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>" drotype="<?=(isset($viewId) && $viewId !="")? $viewId :"";?>">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="12px">
                                                            <path fill-rule="evenodd"d="M12.250,12.000 L11.375,12.000 C11.053,12.000 10.792,11.732 10.792,11.400 C10.792,11.068 11.053,10.800 11.375,10.800 L12.250,10.800 C12.571,10.800 12.833,10.531 12.833,10.200 L12.833,9.300 C12.833,8.968 13.095,8.700 13.417,8.700 C13.739,8.700 14.000,8.968 14.000,9.300 L14.000,10.200 C14.000,11.192 13.215,12.000 12.250,12.000 ZM13.417,3.300 C13.095,3.300 12.833,3.032 12.833,2.700 L12.833,1.800 C12.833,1.469 12.571,1.200 12.250,1.200 L11.375,1.200 C11.053,1.200 10.792,0.932 10.792,0.600 C10.792,0.268 11.053,-0.000 11.375,-0.000 L12.250,-0.000 C13.215,-0.000 14.000,0.808 14.000,1.800 L14.000,2.700 C14.000,3.032 13.739,3.300 13.417,3.300 ZM11.654,6.108 C11.159,8.190 9.245,9.642 6.999,9.642 C4.754,9.642 2.840,8.190 2.344,6.108 C2.327,6.036 2.327,5.964 2.344,5.892 C2.840,3.810 4.754,2.358 6.999,2.358 C9.245,2.358 11.159,3.810 11.654,5.892 C11.672,5.964 11.672,6.036 11.654,6.108 ZM6.999,4.200 C6.037,4.200 5.249,5.010 5.249,6.000 C5.249,6.990 6.037,7.800 6.999,7.800 C7.962,7.800 8.749,6.990 8.749,6.000 C8.749,5.010 7.962,4.200 6.999,4.200 ZM2.625,1.200 L1.750,1.200 C1.429,1.200 1.167,1.469 1.167,1.800 L1.167,2.700 C1.167,3.032 0.905,3.300 0.583,3.300 C0.261,3.300 -0.000,3.032 -0.000,2.700 L-0.000,1.800 C-0.000,0.808 0.785,-0.000 1.750,-0.000 L2.625,-0.000 C2.947,-0.000 3.208,0.268 3.208,0.600 C3.208,0.932 2.947,1.200 2.625,1.200 ZM0.583,8.700 C0.905,8.700 1.167,8.968 1.167,9.300 L1.167,10.200 C1.167,10.531 1.429,10.800 1.750,10.800 L2.625,10.800 C2.947,10.800 3.208,11.068 3.208,11.400 C3.208,11.732 2.947,12.000 2.625,12.000 L1.750,12.000 C0.785,12.000 -0.000,11.192 -0.000,10.200 L-0.000,9.300 C-0.000,8.968 0.261,8.700 0.583,8.700 Z"/>
                                                        </svg>
                                                        Preview
                                                    </a>
                                                    <a <?= $my_specs['rename'] ?>  href="javascript:;" >
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17" width="18" height="17">
                                                            <path id="Shape 11" class="shp0" d="M10.68 4.14L13.52 6.81L6.33 13.55L3.49 10.89L10.68 4.14ZM15.68 3.5L14.41 2.31C13.92 1.85 13.13 1.85 12.64 2.31L11.42 3.45L14.26 6.11L15.68 4.79C16.06 4.43 16.06 3.85 15.68 3.5L15.68 3.5ZM2.01 14.63C1.96 14.85 2.17 15.04 2.4 14.99L5.56 14.27L2.73 11.61L2.01 14.63ZM2.01 14.63"></path>
                                                        </svg>
                                                        Rename
                                                    </a>
                                				</div>
                            				</div>
                                        </div>
                                       <?php
                                }
                             ?> 
                  <?php $i++; }}else{?>
                                <div class="no_dash_box ">
                                    <img src="<?php echo base_url('assets/backend/images/no_file.png') ?>" alt="">
                                    <h4>No Files Are Available</h4>
                                    <p>You haven't uploaded any files yet</p>
                                    <div class="add-group">
                                        <!--<a href="javascript:;" class="ad-btn">Upload File</a>-->
                                    </div>
                                </div>
                        <?php }?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

