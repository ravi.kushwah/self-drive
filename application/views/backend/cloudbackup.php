<div class="page-wrapper">
    <div class="main-content">
        <div class="row">
            <div class="col xl-12 col-lg-12">
                <div class="page-connection-wrapper">
                    <div class="sc_title_bar">
                        <h4>Cloud Backup</h4>
                        <p>Set up a cloud backup task to backup online files from one cloud to another</p>
                    </div>
                    <form class="form">
                        <div class="sc_transfer_wrapper">
                             <a href="javascript:;" class="cloudTransfer" type="from" userid="<?=$id?>" data-cloud-type="backup">
                                 <div class="sc_transfer_box">
                                    <div class="sc_transfer_detail">
                                        <img src="<?= base_url() ?>assets/backend/images/file_backup.png" alt="Image" class="fileConnectionIMGfrom">
                                        <h5>File To Backup<div class="setFileNameFrom"></div></h5>
                                        <p class="fileNameFrom">Click here to select the cloud drive or folders you'd like to sync from</p>
                                     </div>
                                </div>
                            </a>
                            <!--<a href="javascript:;">-->
                                <div class="sc_file_box">
                                    <img src="<?= base_url() ?>assets/backend/images/backup_center.png" alt="Image">
                                </div>
                            <!--</a>-->
                             <a href="javascript:;" class="cloudTransfer" type="to" userid="<?=$id?>" data-cloud-type="backup">
                                <div class="sc_transfer_box">
                                    <div class="sc_transfer_detail">
                                        <img src="<?= base_url() ?>assets/backend/images/file_backup2.png" alt="Image" class="fileConnectionIMGto">
                                        <h5>Where To Save Backup<div class="setFileNameto"></div></h5>
                                        <p class="fileNameto">Click here to select the target cloud to save the backup</p>
                                     </div>
                                </div>
                            </a>
                        </div>
                        <div class="sc_file_btn">
                            <a href="javascript:;" data-url="home/BackupNowCloudDataOffline" class="ad-btn BackUpClouddata">Backup Now</a>
                         </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>