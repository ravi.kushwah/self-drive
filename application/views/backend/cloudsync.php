<div class="page-wrapper">
    <div class="main-content">
        <div class="row">
            <div class="col xl-12 col-lg-12">
                <div class="page-connection-wrapper">
                    <div class="sc_title_bar">
                        <h4>Cloud Sync</h4>
                        <p>Create a sync task to sync files between cloud drives</p>
                    </div>
                    <div class="sc_transfer_wrapper">
                        <a href="javascript:;">
                             <div class="sc_transfer_box">
                                <div class="sc_transfer_detail">
                                    <img src="<?= base_url() ?>assets/backend/images/file_mini.png" alt="Image">
                                    <p>Click here to select the cloud drive or folders you'd like to sync from</p>
                                 </div>
                            </div>
                        </a>
                        <a href="javascript:;">
                            <div class="sc_file_box">
                                <img src="<?= base_url() ?>assets/backend/images/folder_transfer.png" alt="Image">
                            </div>
                        </a>
                        <a href="javascript:;">
                            <div class="sc_transfer_box">
                                <div class="sc_transfer_detail">
                                    <img src="<?= base_url() ?>assets/backend/images/file_mini2.png" alt="Image">
                                    <p>Click here to select the cloud drives or folders you'd like to sync to</p>
                                 </div>
                            </div>
                        </a>
                    </div>
                    <div class="sc_file_btn">
                        <a href="javascript:;" class="ad-btn btn-black">Switch to one-way sync</a>
                        <a href="javascript:;" class="ad-btn">Sync Now</a>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>