<div class="ad-footer-btm">
            <p>Copyright <?= date('Y') ?> &copy; <?= SITENAME ?> All Rights Reserved.</p>
        </div>
    </div>
</div>
</div>

<!-- cloud Transfer modal Start  -->
<div class="modal fade" id="cloudtransferfrom" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered sc_cloud_transfer_modal" role="document">
      <div class="modal-content"><form class="form cloudTrnsfer">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">
            <button type="button" class="sc_back_iconbtn cloudTransferBackBTN" >
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="18" viewBox="0 0 23 18">
                    <path d="M13.825,18.000 C10.772,18.000 7.928,16.517 6.217,14.033 C5.870,13.530 6.006,12.845 6.519,12.505 C7.032,12.165 7.729,12.297 8.076,12.801 C9.369,14.678 11.518,15.799 13.825,15.799 C17.647,15.799 20.757,12.749 20.757,9.000 C20.757,5.251 17.647,2.200 13.825,2.200 C11.512,2.200 9.359,3.326 8.067,5.212 C7.721,5.717 7.024,5.851 6.510,5.512 C5.996,5.173 5.859,4.489 6.205,3.985 C7.915,1.490 10.763,-0.000 13.825,-0.000 C18.884,-0.000 23.000,4.037 23.000,9.000 C23.000,13.963 18.884,18.000 13.825,18.000 ZM11.293,6.704 C10.902,6.313 10.902,5.679 11.293,5.287 C11.683,4.896 12.316,4.896 12.707,5.287 L15.706,8.291 C15.730,8.315 15.752,8.339 15.772,8.365 C15.777,8.371 15.782,8.377 15.787,8.384 C15.802,8.403 15.817,8.423 15.831,8.443 C15.835,8.449 15.838,8.455 15.842,8.462 C15.856,8.483 15.869,8.505 15.881,8.527 C15.883,8.532 15.886,8.537 15.888,8.541 C15.901,8.566 15.913,8.590 15.923,8.616 C15.925,8.619 15.925,8.622 15.927,8.626 C15.938,8.653 15.948,8.680 15.956,8.708 C15.957,8.712 15.958,8.715 15.959,8.719 C15.967,8.747 15.974,8.774 15.980,8.803 C15.982,8.812 15.982,8.820 15.984,8.829 C15.988,8.852 15.992,8.876 15.995,8.900 C15.998,8.933 16.000,8.966 16.000,9.000 C16.000,9.033 15.998,9.067 15.995,9.100 C15.992,9.124 15.988,9.148 15.984,9.172 C15.982,9.180 15.982,9.189 15.980,9.197 C15.974,9.226 15.967,9.254 15.959,9.282 C15.958,9.285 15.957,9.288 15.956,9.292 C15.948,9.320 15.938,9.348 15.927,9.375 C15.925,9.378 15.925,9.381 15.923,9.384 C15.913,9.410 15.901,9.435 15.888,9.459 C15.886,9.464 15.883,9.468 15.881,9.473 C15.869,9.495 15.856,9.517 15.842,9.539 C15.838,9.545 15.835,9.551 15.831,9.557 C15.817,9.577 15.802,9.597 15.787,9.616 C15.782,9.623 15.777,9.629 15.772,9.636 C15.752,9.661 15.730,9.685 15.707,9.708 L12.707,12.712 C12.511,12.908 12.256,13.006 12.000,13.006 C11.744,13.006 11.488,12.908 11.293,12.712 C10.902,12.321 10.902,11.688 11.293,11.296 L12.586,10.001 L1.000,10.001 C0.448,10.001 -0.000,9.553 -0.000,9.000 C-0.000,8.447 0.448,7.998 1.000,7.998 L12.586,7.998 L11.293,6.704 Z" class=""></path>
                </svg>
            </button>
            Cloud Transfer <span id="moveFolderName"></span> </h5>
            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
            </button>
         </div>
       
         <div class="cloudConnation sc_dash_move_list ">
             
         </div>
           
            <div class="modal-body">
                <input type="text" name="" value="" id="fileId" hidden>
                <input type="text" name="" value="" id="UserID" hidden>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?=$id?>" name="user_id"  id="user_id">
               <input type="button" class="btn ad-btn cloudTransferSelectFile" value="OK">
            </div>
         </form>
      </div>
   </div>
</div>

<!-- cloud Transfer modal Start  -->

<!-- cloud Details modal Start  -->

<div class="modal fade" id="CreateFolder" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Create Folder</h5>
            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form formCreateFolder">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label Folderame">Create Folder</label>
                  <input type="text" placeholder="Enter Name" value="New Folder" class="form-control custAddCls" id="folder_name">
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="user_id" >
               <input type="button" class="btn ad-btn UserCreateFolder" value="Create Folder">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="Renamefiles" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">File Rename</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form FormfileName">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">File Rename </label>
                  <input type="text" placeholder="Enter Your File Name" value="" class="form-control custAddCls" id="fileName">
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" id="user_id" >
               <input type="button" class="btn ad-btn RenameFile" value="Rename File ">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="ShareURL" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Share URL</h5>
            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form FormfileName">
            <div class="modal-body">
                <div class="sc_share_link ">
                    <div class="checkbox">
                        <input type="radio" id="public_share" class="url_type" name="url_type" value="1">
                        <label for="public_share">Public Share </label>
                        <p>Tips:Anyone who gets the link can view the file</p>
                    </div>
                    <div class="checkbox">
                        <input type="radio" id="private_share" class="url_type" name="url_type" value="2">
                        <label for="private_share">Private Share </label>
                        <p>Tips:Others need to enter password to view the file</p>
                    </div>
                </div>
                <div class="form-group d-none ">
                    <label class="col-form-label">Share File URL  </label>
                    <input type="text" class="form-control ShareUrlLink" id="copyTargetUrl" value="Text to Copy" readonly="">
                </div>
                <div class="form-group d-none">
                    <label class="col-form-label">Password</label>
                    <input type="text" class="form-control " id="copyTargetPassword" value="Password" readonly="">
                </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="" id="user_id">
               <!--<input type="text" class="btn ad-btn d-none" id="privareUrl" > -->
               <button  type="button" class="btn ad-btn" id="copyButton"  >Create Url</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="moveFile" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sc_cloud_transfer_modal" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Move File <span id="moveFolderName"></span></h5>
            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <!--<a href="javascript:;" id="<?php echo $this->uri->segment(3);?>" class="ad-btn MoveBackFolder">          Back            </a>-->
         </div>
         <div class="MovingFile sc_dash_move_list">
         </div>
         <form class="form moveFileForm">
            <div class="modal-body">
                <input type="text" name="" value="" id="fileId" hidden>
                <input type="text" name="" value="" id="UserID" hidden>
                <div class="form-group MoveProgressBar">
                  <div class="progress hide">
                     <div class="progress-bar progress-bar-striped progress-bar-animated" id="progressbar" role="progressbar"></div>
                  </div>
                  </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="user_id"  id="user_id">
               <input type="button" class="btn ad-btn MoveUploadFile" value="Move File">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="UploadFile" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title">Drive Upload File</h5>
            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         
         <form class="form" enctype="multipart/form-data" >
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">Upload File</label>
                  <div class="plr_upload_wrapper">
                    <div class="plr_upload_box uploadImage">
                        <input class="checkProductImages" type="file"  name="file" class="form-control UploadFile" id="UploadFile_Name" onchange="readURL(this)">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                            <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class=""></path>
                         </svg>
                       <img src="" class="blobImage">
                    </div>
                </div>
               </div>
               <div class="form-group">
                  <div class="progress">
                     <div class="progress-bar progress-bar-striped progress-bar-animated" id="progressbar" role="progressbar"></div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="user_id"  id="user_id">
               <input type="button" class="btn ad-btn CloudUploadFile" value="Upload">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="fileView" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog sc_modal_large modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Preview File</h5>
                <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="sc_iframe_box" id="AllFilePreview">
                </div>
             </div>
        </div>
    </div>
</div>
<div class="modal fade" id="amazons3fileupload" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="add_customer_title"><?php echo($connectionType=="s3")?'Amazon s3':'AnonFile'?> Upload File</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form class="form">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-form-label">Upload File</label>
                  <div class="plr_upload_box">
                     <input type="file" placeholder="Enter Folder Name" name="file" class="form-control " id="UploadFile_Name" folder_id="1dWUlJrxLaqEec6RzKhKwuSq2bhENO2QU" >
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="59" height="50" viewBox="0 0 59 50">
                        <path d="M47.177,41.803 L45.403,41.802 C44.557,41.802 43.868,41.120 43.868,40.281 C43.868,39.875 44.028,39.493 44.318,39.206 C44.608,38.919 44.993,38.761 45.403,38.761 L47.177,38.761 C49.537,38.761 51.750,37.842 53.410,36.172 C55.070,34.504 55.964,32.297 55.928,29.960 C55.855,25.254 51.876,21.427 47.057,21.427 L44.577,21.427 L44.577,18.293 C44.577,9.921 37.766,3.079 29.393,3.041 L29.322,3.041 C21.472,3.041 14.948,8.865 14.148,16.587 L14.018,17.847 L12.743,17.948 C7.320,18.377 3.071,22.941 3.071,28.337 C3.071,34.077 7.786,38.753 13.582,38.761 L13.713,38.761 C14.561,38.761 15.250,39.443 15.250,40.281 C15.250,41.120 14.561,41.802 13.715,41.802 L13.598,41.803 C6.099,41.803 -0.000,35.762 -0.000,28.337 C-0.000,24.931 1.288,21.679 3.625,19.183 C5.537,17.142 7.991,15.756 10.722,15.174 L11.253,15.061 L11.361,14.534 C12.138,10.729 14.091,7.332 17.007,4.709 C20.384,1.672 24.758,-0.000 29.323,-0.000 C39.157,-0.000 47.375,7.918 47.642,17.651 L47.662,18.373 L48.387,18.446 C54.437,19.057 59.000,24.064 59.000,30.094 C59.000,36.550 53.696,41.803 47.177,41.803 ZM41.151,31.226 L40.696,31.440 C39.207,32.141 37.425,31.837 36.261,30.684 L31.095,25.568 L31.095,48.479 C31.095,49.318 30.406,50.000 29.559,50.000 C28.712,50.000 28.024,49.318 28.024,48.479 L28.024,25.568 L22.858,30.684 C22.121,31.413 21.142,31.815 20.100,31.815 C19.510,31.815 18.945,31.689 18.420,31.441 L17.967,31.226 L29.559,19.746 L41.151,31.226 Z" class=""></path>
                     </svg>
                  </div>
               </div>
               <div class="form-group">
                  <div class="progress hide">
                     <div class="progress-bar progress-bar-striped progress-bar-animated" id="progressbar" role="progressbar"></div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="user_id" id="user_id">
               <input type="button" class="btn ad-btn s3UploadFile" id="s3UploadFile" value="Upload"  >
            </div>
         </form>
      </div>
   </div>
</div>

<!-- cloud Details modal End -->

<div class="modal fade" id="add_cloud" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Add Cloud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <img src="<?=base_url('assets/backend/images/icon12.png');?>" alt="Icon">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Display Name</label>
                    <input type="text" placeholder="Enter Display Name" class="form-control custAddCls" id="c_name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">User Name</label>
                    <input type="text" placeholder="Enter User Name" class="form-control custAddCls" id="c_email">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <input type="text" placeholder="Enter Password" class="form-control custAddCls" id="c_password">
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="c_id">
                <button type="button" class="btn ad-btn" id="submitCustomerData">Add Cloud</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="amazonS3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Amazon S3 Connection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form amazonS3">
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Access key ID</label>
                    <input type="text" placeholder="Access key ID" name="accesskeyId" class="form-control custAddCls" id="accesskeyId">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Secret Access key</label>
                    <input type="text" placeholder="Secret access key" name="secretKey" class="form-control custAddCls" id="secretKey">
                </div>
                 <div class="form-group">
                    <label class="col-form-label">Bucket Name</label>
                    <input type="text" placeholder="Bucket Name" name="BucketName" class="form-control custAddCls" id="BucketName">
                </div>
                 <div class="form-group">
                    <label class="col-form-label">Region</label>
                    <input type="text" placeholder="Region" name="region" class="form-control custAddCls" id="region">
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" value="" id="c_id">
                <button type="button" class="btn ad-btn" id="amazonS3Connectoin">Add Amazon S3</button>
            </div>
         </form>
        </div>
    </div>
</div>
<div class="modal fade" id="anonfiles" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Anon Files</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form">
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Name </label>
                    <input type="text" placeholder="Name" name="name" class="form-control custAddCls" id="name">
                </div>
                <div class="form-group">
                    <label class="col-form-label">Email</label>
                    <input type="text" placeholder="Email" name="email" class="form-control custAddCls" id="email">
                </div>
                 <div class="form-group">
                    <label class="col-form-label">API Key</label>
                    <input type="text" placeholder="API Key" name="apikey" class="form-control custAddCls" id="apikey">
                </div>
            </div>
              <div class="modal-footer sc_google_btn">
                <input type="hidden" value="" id="c_id">
               <a href="javascript:;" class="btn ad-btn" id="anonfilesConnection"><span><img src="<?= base_url() ?>assets/backend/images/icon_1.png" alt="icon"></span>Anonfiles Connetion</a>
            </div>
         </form>
        </div>
    </div>
</div>
<div class="modal fade" id="GoogleDrive" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Add Cloud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <h4>Please login to your Google Account </h4>
                </div>
            </div>
            <div class="modal-footer sc_google_btn">
                <input type="hidden" value="" id="c_id">
               <a  class="btn ad-btn" href = "<?php echo $googleAuthUrl?>"><span><img src="<?= base_url() ?>assets/backend/images/google.png" alt="icon"></span>Connect with Google</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="googlePhoto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Please login to your Google Photo Account </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form ">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-form-label">Display Name</label>
                        <input type="text" placeholder="Display Name" name="displayName" class="form-control custAddCls require" id="displayName">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Display Email</label>
                        <input type="text" placeholder="Display Email" name="displayEmail" class="form-control custAddCls require" id="displayEmail" data-valid="email" data-error="Email should be valid.">
                    </div>
                </div>
               <div class="modal-footer sc_google_btn">
                   <!--<a  class="btn ad-btn" href = "<?php echo $googlePhotoAuth;?>"><span><img src="<?= base_url() ?>assets/backend/images/icon6.png" alt="icon"></span>Connect with Google Photo</a>-->
                   <a href="javascript:;" class="btn ad-btn" id="GooglePhotosCon"><span><img src="<?= base_url() ?>assets/backend/images/icon6.png" alt="icon"></span>Connect with Google Photo</a>
                </div>
             </form>
        </div>
    </div>
</div>
<div class="modal fade" id="googlePhoto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Please login to your Google Photo Account </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form ">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-form-label">Display Name</label>
                        <input type="text" placeholder="Display Name" name="displayName" class="form-control custAddCls" id="displayName">
                    </div>
                </div>
               <div class="modal-footer sc_google_btn">
                   <!--<a  class="btn ad-btn" href = "<?php echo $googlePhotoAuth;?>"><span><img src="<?= base_url() ?>assets/backend/images/icon6.png" alt="icon"></span>Connect with Google Photo</a>-->
                   <a href="javascript:;" class="btn ad-btn" id="GooglePhotosCon"><span><img src="<?= base_url() ?>assets/backend/images/icon6.png" alt="icon"></span>Connect with Google Photo</a>
                </div>
             </form>
        </div>
    </div>
</div>
<div class="modal fade" id="OneDrive" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Add Cloud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <h4>Please login to your One Drive Account </h4>
                </div>
            </div>
            <div class="modal-footer sc_google_btn">
                <input type="hidden" value="" id="c_id">
               <a  class="btn ad-btn" href = "<?php echo $Url?>"><span><img src="<?= base_url() ?>assets/backend/images/onedrive.png" alt="icon"></span>Connect with OneDrive</a>
            </div>
            
        </div>
    </div>
</div>
<div class="modal fade" id="Ddownload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Add Ddownload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <h4>Please Enter your Ddownload Account Key </h4>
                </div>
                <div class="form-group">
                    <label class="form-label">Key</label>
                    <input type="text" class="form-control" placeholder="Enter key" name="ddownload_key" value="" id="ddownload_key">
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn ad-btn" id="ddownload_submit">Submit</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="Box" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Please login to your Box Account </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form ">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-form-label"> Name</label>
                        <input type="text" placeholder="Display Name" name="name" class="form-control custAddCls" id="name">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Email</label>
                        <input type="text" placeholder="Display Name" name="email" data-valid="email" data-error="Email should be valid." class="form-control custAddCls" id="email">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Account ID</label>
                        <input type="text" placeholder="Display Name" name="accountId" class="form-control custAddCls" id="accountId">
                    </div>
                </div>
               <div class="modal-footer sc_google_btn">
                   <a href="javascript:;" class="btn ad-btn" id="ConnetionBOX"><span><img src="<?= base_url() ?>assets/backend/images/icon8.png" alt="icon"></span>Connect with Box</a>
                   <!--<a  class="btn ad-btn" id="ConnetionBOXtest"><span><img src="<?= base_url() ?>assets/backend/images/icon8.png" alt="icon"></span>Connect with Box</a>-->
                </div>
             </form>
        </div>
    </div>
</div>
<div class="modal fade" id="dropBox" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_customer_title">Add Cloud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                   <h4>Please login to your DropBox Account </h4>
                </div>
            </div> 
            <div class="modal-footer sc_google_btn">
                <input type="hidden" value="" id="c_id">
                <a class="btn ad-btn" href = "<?php echo $dropboxAuthUrl;?>"><span><img src="<?= base_url() ?>assets/backend/images/dropbox.png" alt="icon"></span>Connect with DropBox</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="agencyuserClsEdit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="add_agencyuser_title">Add New Categories</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <form class="form ">
                <div class="modal-body"> 
                    <div class="form-group">
                        <label class="col-form-label">Name</label>
                        <input type="text" placeholder="Enter Name" value="" class="form-control agencyuserAddCls" id="userName">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Email</label>
                        <input type="text" placeholder="Enter Email"value=""  class="form-control agencyuserAddCls" id="UserEmail" >
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Password</label>
                        <input type="password" placeholder="Enter Password" value="" class="form-control agencyuserAddCls" id="password">
                        <p>Don't want to change, leave it empty.</p>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Access Level  ( <span style="color:red;">Get 250 Accounts Only</span> ) </label>
                        <div class="modal_checkbox">
                            <div class="checkbox">
                                <input type="checkbox" id="is_fe" class="fe" name="is_fe" disabled checked required>
                                <label for="is_fe">FE <span style="color:red;">*</span></label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="is_oto1" class="oto1" name="is_oto1" checked>
                                <label for="is_oto1">OTO1 </label>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" id="is_oto2" class="oto2" name="is_oto2" checked>
                                <label for="is_oto2">OTO2 </label>
                            </div>
                        </div>    
                    </div>
                    
                </div>
             </form>
            <div class="modal-footer">
            <input type="hidden" value="" id="id">
              <button type="button" class="btn ad-btn" id="submitAgencyUserData">Submit</button>
            </div>
          </div>
        </div>
    </div>

<!-- back blaze modal start -->

<div class="modal fade" id="backBlaze" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="">BackBlaze B2 Connection</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

     <form class="form backBlaze" action="home/backBlazeB2Auth" >
        <div class="modal-body">
            <div class="form-group">
                <label class="col-form-label">Your Name</label>
                <input type="text" placeholder="Your Name" name="b2_userName" class="form-control custAddCls require" id="b2_userName">
            </div>
             <div class="form-group">
                <label class="col-form-label">Key ID</label>
                <input type="text" placeholder="Key Id" name="b2_keyId" class="form-control custAddCls require" id="b2_keyId">
            </div>
             <div class="form-group">
                <label class="col-form-label">Application Key</label>
                <input type="text" placeholder="Application Key" name="b2_applicationKey" class="form-control custAddCls require" id="b2_applicationKey">
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn ad-btn makeConnection" id="backBlazeB2Connectoin">Add BackBlaze B2</button>
        </div>

     </form>
    </div>
</div>
</div>

<!-- back blaze modal end   -->
    
<!-- goFile modal start -->
<div class="modal fade" id="gofile" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">GoFile Connection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form gofile"  action="home/goFileAuth" >
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Token Id</label>
                    <input type="text" placeholder="Token Id" name="gofile_tokenid" class="form-control custAddCls require" id="gofile_tokenid">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn ad-btn makeConnection" id="goFileConnection">Add GoFile</button>
            </div>
         </form>
        </div>
    </div>
</div>
<!-- goFile modal end   -->

<!-- ftp modal start -->
<div class="modal fade" id="ftp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">FTP Connection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form ftp" action="home/ftpAuth">
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">Host</label>
                    <input type="text" placeholder="Host" name="ftp_host" class="form-control custAddCls require">
                </div>
                <div class="form-group">
                    <label class="col-form-label">User</label>
                    <input type="text" placeholder="User" name="ftp_user" class="form-control custAddCls require" >
                </div>
                <div class="form-group">
                    <label class="col-form-label">Password</label>
                    <input type="text" placeholder="Password" name="ftp_password" class="form-control custAddCls require" >
                </div>
                <div class="form-group">
                    <label class="col-form-label">Port</label>
                    <input type="text" placeholder="Port" name="ftp_port" class="form-control custAddCls require" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn ad-btn makeConnection" id="ftp">Add FTP</button>
            </div>
         </form>
        </div>
    </div>
</div>
<!-- ftp modal end   -->

<!-- pcloud modal start -->
<div class="modal fade" id="pcloud" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">PCloud Connection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
         <form class="form pcloud" action="home/pcloudAuth">
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-form-label">App Key</label>
                    <input type="text" placeholder="App Key" name="pcloud_appkey" class="form-control custAddCls require">
                </div>
                <div class="form-group">
                    <label class="col-form-label">App Secret</label>
                    <input type="text" placeholder="App Secret" name="pcloud_appsecret" class="form-control custAddCls require" >
                </div>
                <div class="form-group">
                    <label class="col-form-label">Code Link</label>
                    <input type="button" value="Generate" name="pcloud_generatecode" class="form-control custAddCls require" >
                    <a style="display:none;" href="" target="_blank" id="pcloud_codelink" rel="noopener noreferrer"></a>
                </div>
                <div class="form-group">
                    <label class="col-form-label">App Code</label>
                    <input type="text" placeholder="App Code" name="pcloud_appcode" class="form-control custAddCls require" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn ad-btn makeConnection" id="pcloud">Add PCloud</button>
            </div>
         </form>
        </div>
    </div>
</div>
<!-- pcloud modal end   -->

<!-- Domain modal start -->
<div class="modal fade" id="DomainModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered sc_cloud_transfer_modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Domain Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body sc_domain_content">
                <p>1. Please use top-level domains only (sub-domains won’t work).</p>
                <p>2. Please enter your domain above without any www, http or https (domain.com for example).</p>
                <p>3. Enter your domain name for example "mydomain.com" (without the quotation marks) and click the "Add Domain" button.</p>
                <p>4. Copy this IP address for the A Record: 104.255.220.50</p>
                <p>5. Log into your domain registrar account and find the option to add Host Records, which can usually be found in the Advanced DNS section. If you're struggling to find this, perform a Google search for "[Domain Registrar Here] how to change host records".</p>
                <p>6. Select your type as "A Record".</p>
                <p>7. Set Host as "@".</p>
                <p>8. Paste the copied IP Address from step 4 into the value field.</p>
                <p>9. Leave TTL as Automatic or 14400.</p>
                <p>10. Click Save and you're done.</p>
                <p class="ca_red ca_info"><b>Note:</b> <i>It can take up to 48-72 hours to reflect.</i></p> 
            </div> 
        </div>
    </div>
</div>
<!-- Domain modal end   -->
    
<!-- Confirm Popup Model -->
<div class="modal fade" id="confirmPopup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog sc_are_sure modal-dialog-centered" role="document">
          <div class="modal-content plr_delete_modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <img src="<?= base_url() ?>assets/backend/images/question-mark.svg" alt="" class="img">
                <h4>Are you sure?</h4>
                <p class="delete_popup_txt">You Want to Remove Accout</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn squer-btn btn-secondary mt-2 mr-2" data-dismiss="modal">Cancel</button>
              <button type="button" id="confirmPopupBtn" class="btn btn-danger squer-btn mt-2 mr-2">Yes, Delete</button>
            </div>
          </div>
        </div>
    </div>

<!--===Notification model===-->
<div class="plr_notification">
	<div class="plr_close_icon">
		<span class='close'>×</span>
	</div>
	<div class="plr_success_flex">
		<div class="plr_happy_img">
			<img src=""/>
		</div>
		<div class="plr_yeah">
			<h5></h5>
			<p></p>
		</div>
	</div>
</div> 
<!--===Notification model===-->
    <!-- Script Start -->
    <script>
        window.baseurl = "<?= base_url() ?>";
        let current_plan = <?= json_encode($_SESSION['current_plan']);?>;
        var user_id = "<?= $_SESSION['id']?>";
    </script>
	<script src="<?= base_url() ?>assets/common/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/datatable/datatables.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/datatable/responsive/dataTables.responsive.min.js"></script>
  
    <script src="<?= base_url() ?>assets/backend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/spectrum.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/js/plupload.full.min.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/backend/js/chunk.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/admin/js/toastr/toastr.min.js"></script>
	<!-- Page Specific -->
<?php if($this->uri->segment(2) == 'autoresponder_integration'){?>
    <script src="<?= base_url() ?>assets/backend/js/ar_settings.js?<?= date('his') ?>"></script>
<?php }?>
<script src="<?= base_url() ?>assets/backend/js/cloud_backup.js?<?= date('his') ?>"></script>
<!-- <?php if($this->uri->segment(2) == 'CloudBackup'){?>
    <script src="<?= base_url() ?>assets/backend/js/cloud_backup.js?<?= date('his') ?>"></script>
<?php }else{
    ?><script src="<?= base_url() ?>assets/backend/js/cloudTransfer.js?<?= date('his') ?>"></script><?php
}?> -->
    <!-- Custom Script -->
    
    <script src="<?= base_url() ?>assets/backend/js/custom.js?<?= date('his') ?>"></script>
    <script src="<?= base_url() ?>assets/backend/js/google_drive/custom.js?<?= date('his') ?>"></script>
    <script async defer src="<?= base_url() ?>assets/backend/js/google_drive/api.js"></script>
    <script async defer src="<?= base_url() ?>assets/backend/js/google_drive/client.js"></script>
    <script async defer src="<?= base_url() ?>assets/backend/js/google_drive/drive_login.js?<?= date('his') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/plupload/3.1.5/plupload.full.min.js" integrity="sha512-yLlgKhLJjLhTYMuClLJ8GGEzwSCn/uwigfXug5Wf2uU5UdOtA8WRSMJHJcZ+mHgHmNY+lDc/Sfp86IT9hve0Rg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>