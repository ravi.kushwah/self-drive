<div class="plr_login_form">
    <h1>Hello,<img src="<?= base_url() ?>assets/auth/images/hand.png" alt=""/><br>Welcome Back To <span><?= SITENAME ?></span></h1>
    <h5>Please Register To Continue your account.</h5>
    <div class="login-content">
        <div class="plr_input_main">
            <div class="plr_input">
                <div class="plr_label_box">
					<label>Name</label>
				</div>
                <input type="text" value="" placeholder="Enter Name Here..." id="nm" >
            </div>
            <div class="plr_input">
                <div class="plr_label_box">
					<label>Email Address</label>
				</div>
                <input type="text" value="" placeholder="Enter Email Here..." id="em" >
            </div>
            <div class="plr_input">
                <div class="plr_label_box">
					<label>Password</label>
				</div>
                <input type="password" value="" placeholder="Enter Password" id="pwd">
            </div>
        </div>
    </div>
    <div class="plr_login_btn">
        <a href="javascript:;" class="plr_btn" onclick="signupSection()">Sign Up</a>
        <p>Already have an account ? <a href="<?php echo base_url();?>">Login Now</a></p>
    </div>
</div>

