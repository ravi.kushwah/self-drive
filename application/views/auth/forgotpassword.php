<div class="plr_login_form">
    <h1>Hello,<img src="<?= base_url() ?>assets/auth/images/hand.png" alt=""/><br>Welcome Back To <span><?= SITENAME ?></span></h1>
    <h5>Enter your email and we will send you a new password.</h5>
    <div class="login-content">
        <div class="plr_input_main">
            <div class="plr_input">
                <div class="plr_label_box">
					<label>Email Address</label>
				</div>
                <input type="text" placeholder="Enter Your Email" id="em" >
                <!--<img src="<?= base_url() ?>assets/auth/images/msg.svg" alt=""/>-->
            </div>
        </div>
    </div>
    <div class="plr_login_btn">
        <a href="javascript:;" class="plr_btn" onclick="forgotSection()">Send Password</a>
        <p>Already have an account? <a href="<?= base_url() ?>">Login Now</a></p>
    </div>
</div>