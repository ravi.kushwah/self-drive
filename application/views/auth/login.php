
<div class="plr_login_form ">
    <h1>Hello,<img src="<?= base_url() ?>assets/auth/images/hand.png" alt=""/><br>Welcome Back To <span><?=$this->common_lib->siteTitle;?></span></h1>
    <h5>Please Enter Your Login Credentials Below To<br>Start Using The <?=$this->common_lib->siteTitle;?>.</h5>
    <form action="" method="post">
        <div class="login-content">
            <div class="plr_input_main">
                <div class="plr_input">
                    <div class="plr_label_box">
    					<label>Email Address</label>
    				</div>
                    <input type="text" value="<?= isset($_COOKIE['uem']) ? $_COOKIE['uem'] : '' ?>" placeholder="Enter Email Here..." id="em" >
                    <!--<img src="<?= base_url() ?>assets/auth/images/msg.svg" alt=""/>-->
                </div>
                <div class="plr_input">
                    <div class="plr_label_box">
    					<label>Password</label>
    				</div>
                    <input type="password" value="<?= isset($_COOKIE['uem']) ? $_COOKIE['upwd'] : '' ?>" placeholder="Enter Password" id="pwd">
                    <!--<img src="<?= base_url() ?>assets/auth/images/password.svg" alt=""/>-->
                </div>
            </div>
        </div>
        <div class="plr_check_section">
            <ul>
                <li>
                    <div class="plr_checkbox">
                        <input type="checkbox" <?= isset($_COOKIE['uem']) ? 'checked' : '' ?> id="rememberme" value="1">
                        <label for="rememberme">remember me</label>
                    </div>
                    <span><a href="<?= base_url('home/forgotpassword') ?>">forgot password?</a></span>
                </li>
            </ul>	
        </div>
        <div class="plr_login_btn">
            <!--<a href="javascript:;" class="plr_btn" onclick="loginSection()">Login Now</a>-->
            <input class="plr_btn loginSection" type="submit" value="Login Now">
            <p hidden>Don’t have an account? <a href="javascript:;">Sign Up</a></p>
        </div>
    </form>
</div>

<div class="plr_login_form sc_hide WithoutFeUser" hidden>
    <div class="sc_support_alert">
        <img src="<?= base_url() ?>assets/auth/images/alert.png" alt=""/>
        <span>Alert</span>
    </div>
    <h5>In order to access this OTO, you need to purchase the FE base product of <?=$this->common_lib->siteTitle;?>.</h5>
    <div class="plr_login_btn">
        <a href="https://selfdrivepro.com/vip" class="plr_btn" target="_blank">Get <?=$this->common_lib->siteTitle;?> Here</a>
    </div>
</div>

<div class="support_link plr_login_btn">
    <p>Need help? Contact us at<a href="mailto:tickets@self-drive-pro.p.tawk.email">tickets@self-drive-pro.p.tawk.email</a></p>
</div>
<div class="df_terms_link">
    <a href="<?= base_url('home') ?>">Home</a>
    <a href="<?= base_url('terms') ?>">Terms & Conditions</a>
    <a href="<?= base_url('privacy') ?>">Privacy Policy</a>
</div>



