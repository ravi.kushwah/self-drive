<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

////// White Label //////


$domain = $_SERVER['HTTP_HOST']; //

// print_r($_SERVER['HTTP_HOST']);die;
if(!in_array($domain , MAIN_DOMAIN_LIST)){
  
    // Create connection
    // echo DB_USER_NAME;
    // echo DB_PASSWORD;
    // echo DB_NAME;
    $conn = new mysqli("localhost", DB_USER_NAME , DB_PASSWORD , DB_NAME); //connect DB to check WL domain
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error); 
    }else{
        $result = $conn->query("SELECT usertbl.status,wl_setting.* FROM wl_setting use INDEX(domain_name) INNER JOIN usertbl ON usertbl.id=wl_setting.user_id WHERE domain_name = '$domain' LIMIT 1"); //check domain is added as WL Domain
        if ($result && $result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { 
                @session_start();
                $_SESSION['wlDetails'] = $row;
                if($row['status'] != 1){ // if usertbl status is in-active then will block all the functionality
                    echo 'Access denied.'; // add a message which you want to show for in-active user's WL domain 
                    exit;
                }
            }
        } else { //in case of domain is not added as WL domain will check the campaign
            $route['default_controller'] = 'verify';
            // $route['install_app/(:any)/(:any)'] = 'verify/incrementInstallCount/$1/$2';
            // $route['(:any)'] = 'verify/live_site/$1';
        }
        $conn->close(); 
    }
            // echo"<pre>";print_r($_SESSION);die;
}


$route['default_controller'] = 'home';
$route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;

$route['page-not-found'] = 'Home/Page_not_found';
$route['support-desk'] = 'Home/supportDesk';
$route['home'] = 'Home/about_us';
$route['privacy'] = 'Home/privacy';
$route['terms'] = 'Home/terms';
$route['user-task-limit-check'] = 'Home/userTastlimitEmpty';
$route['share/(:any)/(:any)'] = '/common/share_file/$1/$2';
