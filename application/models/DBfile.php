<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DBfile extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    public function get_data( $table_name, $where = array(), $field = '*', $order = array() , $limit = ""){
        $this->db->select( $field );
		$this->db->from( $table_name );
		if( !empty($where) ){
			$this->db->where( $where );
		}
		if(!empty($order)){
			foreach($order as $k=>$v){
				$this->db->order_by($k, $v);
			}
		}
		if($limit==1)
			$this->db->limit($limit);
		elseif(isset($limit[1]) && $limit[1] !== '' && $limit[0] !== ''){
			$this->db->limit($limit[1], $limit[0]);
		}

		$query = $this->db->get();
        return $query->result_array();   
    }
    
    public function put_data( $table_name, $what = array() ){
        $this->db->insert($table_name,$what);
		return $this->db->insert_id();
    }
    
    public function set_data( $table_name, $what = array() , $where = array()){
        $this->db->where($where);
	    return	$this->db->update($table_name,$what);
    }
	
	public function delete_data( $table_name, $what = array() ){
       return $this->db->delete( $table_name, $what);
    }
	
	public function get_data_limit( $table_name, $where = array(), $field = '*', $limit = array(), $orderby = '', $order = '', $where_in = array()){
		$this->db->select( $field );
		$this->db->from( $table_name );
		if( !empty($where) ){
			$this->db->where( $where );
		}
		if( !empty($where_in) ){
			foreach($where_in as $k=>$v){
				$this->db->where_in( $k, $v );
			}
		}
		
		if($limit==1)
			$this->db->limit($limit);
		elseif(isset($limit[1]) && $limit[1] !== '' && $limit[0] !== ''){
			$this->db->limit($limit[1], $limit[0]);
		}
		
		if($orderby != '' && $order != ''){
			$this->db->order_by($orderby, $order);
		}
		
		$query = $this->db->get();
        return $query->result_array();
	}
	
	public function search_Mediadata( $table_name, $where = array(), $like = '', $value = '', $pos = '' ){
		$this->db->select( '*' );
		$this->db->from( $table_name );
		if( !empty($where) ){
			$this->db->where( $where );
		}
		
		if(!empty($like)){
			if($pos == '')
				$this->db->like($like, $value);
			else
				$this->db->like($like, $value, $pos);
		}
		//$this->db->limit(24, 0);
		$this->db->order_by('id', 'desc');
		
		$query = $this->db->get();		
		return $query->result_array();
		
	}
	function select_data($field , $table , $where = '' , $limit = '' , $order = '' , $like = '' , $join_array = '' , $group = '', $or_like = '',$where_in =''){ 
		$this->db->select($field);
		$this->db->from($table);
		if($where != ""){ 
			$this->db->where($where);
		}
	    if($where_in !='' ){
           
           $this->db->where_in($where_in[0],explode(',',$where_in[1]));
        }
		if($join_array != ''){
			if(in_array('multiple',$join_array)){
				foreach($join_array['1'] as $joinArray){
					$this->db->join($joinArray[0], $joinArray[1]);
				}
			}else{
				$this->db->join($join_array[0], $join_array[1]);
			}
		}
		
		if($order != ""){
			$this->db->order_by($order['0'] , $order['1']);
		}
		
		if($group != ""){
			$this->db->group_by($group);
		}
		
		if($limit != ""){
			if(is_array($limit) && count($limit)>1){
				$this->db->limit($limit['0'] , $limit['1']);
			}else{
				$this->db->limit($limit);
			}
			
		}
		
		if($like != ""){
			$like_key = explode(',',$like['0']);
			$like_data = explode(',',$like['1']);
			for($i='0'; $i<count($like_key); $i++){
				if($like_data[$i] != ''){
					$this->db->like($like_key[$i] , $like_data[$i]);
				}
			} 
		}

		if($or_like != ""){
			if(is_array($or_like)){
				foreach($or_like as $like){
					$this->db->or_like($like[0] , $like[1]);
				}
			}
		}

		return $this->db->get()->result_array();
		die();
	}
	
	public function get_join_data( $table_name, $join_table, $on, $where = array(), $field = '*', $order = array(), $join = 'inner', $group = '' , $limit = ''){
		$this->db->select( $field );
		$this->db->from( $table_name );
		
		if(!empty($join_table) && !empty($on)){
			$this->db->join( $join_table, $on, $join );
		}
		
		if( !empty($where) ){
			$this->db->where( $where );
		}
		
		if(!empty($order)){
			foreach($order as $k=>$v){
				$this->db->order_by($k, $v);
			}
		}
		
		if( !empty($group) ){
			$this->db->group_by( $group ); 
		}

		if( !empty($limit) ){
			$this->db->limit($limit);
		}
		
		$query = $this->db->get();
        return $query->result_array();
	}
	
	public function get_inornot_data( $table_name, $where = array(), $field = '*', $target='', $where_in='', $in='' , $limit = '',$orderby=''){
		$this->db->select( $field );
		$this->db->from( $table_name );
				
		if( !empty($where_in) ){
			if($in == 'yes'){
				$this->db->where_in( $target, $where_in );
			}else{
				$this->db->where_not_in( $target, $where_in );
			}
		}
		
		if( !empty($where) ){
			$this->db->where( $where );
		}

		if( !empty($limit) ){
			$this->db->limit($limit);
		}

		if( !empty($orderby) ){
			$this->db->order_by($orderby[0], $orderby[1]);
		}
		
		$query = $this->db->get();
        return $query->result_array();
	}
	
	public function get_multijoin_data( $table_name, $join_table = array(), $where = array(), $field = '*', $join = 'left', $group = '' , $orderby = '' , $limit = '' ){
		$this->db->select( $field );
		$this->db->from( $table_name );
		
		if(!empty($join_table)){
			foreach($join_table as $table=>$on){
				$this->db->join( $table, $on, $join );
			}
		}
		
		if( !empty($where) ){
			$this->db->where( $where );
		}
		
		if( !empty($group) ){
			$this->db->group_by( $group ); 
		}
		
		if( !empty($limit) ){
			$this->db->limit($limit[0], $limit[1]);
		}
		
		if( !empty($orderby) ){
			$this->db->order_by($orderby[0], $orderby[1]);
		}
		
		$query = $this->db->get();
        return $query->result_array();
	}
	
	public function get_like_data( $table_name, $where = array(), $like = array() , $field = '*', $group = '', $orderby = '' ) {
		$this->db->select( $field );
		$this->db->from( $table_name );
		if( !empty($where) ){
			$this->db->where( $where );
		}
		if( !empty($group) ){
			$this->db->group_by( $group ); 
		}
		if( !empty($like) ){
			$this->db->like($like[0], $like[1] );
		}
		if( !empty($orderby) ){
			$this->db->order_by($orderby[0], $orderby[1]);
		}
		$query = $this->db->get();
        return $query->result_array();
	}
	
	public function query( $query, $return = true ){
		$query = $this->db->query( $query );
		if($return){
			return $query->result_array();
		}
	}

	public function getCount($table_name,$where=array()){
		$this->db->select( 'count(*) as totCount' );
		$this->db->from( $table_name );
		if( !empty($where) ){
			$this->db->where( $where );
		}
		$query = $this->db->get();
        return $query->result_array();
	}
		# function for run custom query  
	function custom_query($query){
		return $this->db->query($query);
		$this->db->insert_id();
		die();
	}
	# function for call the aggregate function like as 'SUM' , 'COUNT' etc 
	function aggregate_data($table , $field_nm , $function , $where = NULL , $join_array = NULL){
		$this->db->select("$function($field_nm) AS MyFun");
        $this->db->from($table);
		if($where != ''){
			 $this->db->where($where);
		}
		
		if($join_array != ''){
			if(in_array('multiple',$join_array)){
				foreach($join_array['1'] as $joinArray){
					$this->db->join($joinArray[0], $joinArray[1]);
				}
			}else{
				$this->db->join($join_array[0], $join_array[1]);
			}
		}
		
        $query1 = $this->db->get();
		
        if($query1->num_rows() > 0){ 
			$res = $query1->row_array();
			return $res['MyFun'];													
        }else{
			return array();
		}  
		die();  
	}
		
}
?>