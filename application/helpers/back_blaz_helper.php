<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	global $bucket_id;

    function send_response($status, $data, $msg)
    {
        return ['status' => $status, 'data' => $data, 'msg' => $msg];
    }
    
    
    function checkB2Connection($application_key_id, $application_key){
            
        $credentials    = base64_encode($application_key_id . ":" . $application_key);
        $url            = "https://api.backblazeb2.com/b2api/v2/b2_authorize_account";

        $session = curl_init($url);

        // Add headers
        $headers    = array();
        $headers[]  = "Accept: application/json";
        $headers[]  = "Authorization: Basic " . $credentials;
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);                // Add headers
        curl_setopt($session, CURLOPT_HTTPGET, true);                       // HTTP GET
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);                // Receive server response
        $server_output = curl_exec($session);
        
        curl_close ($session);

        $json_response = json_decode( $server_output, TRUE );
        
        if (isset( $json_response['authorizationToken'] )) {
            $ret = send_response(1, ['auth_token' => $json_response['authorizationToken'], 'api_url' => $json_response['apiUrl'], 'bucket_id' => $json_response['allowed']['bucketId'], 'bucket_name' => $json_response['allowed']['bucketName'], 'account_id' => $json_response['accountId'] ], '' );
        }
        else{
            $ret = send_response(0, '', '' );
        }
        return $ret;
    }
    
	function create_backBlaze_client($application_key_id, $application_key){
		
        // $application_key_id = "f028f2c92cb9";                                  // Obtained from your B2 account page
        // $application_key    = "004bc64f53f0b75da563932b09bc98b7df38e1a686";    // Obtained from your B2 account page  
        // $application_key_id = "004f028f2c92cb90000000002";          // Obtained from your B2 account page
        // $application_key    = "K004rhSttF/iynQ76JXujErJSh2QfZY";    // Obtained from your B2 account page  
            
        $credentials    = base64_encode($application_key_id . ":" . $application_key);
        $url            = "https://api.backblazeb2.com/b2api/v2/b2_authorize_account";

        $session = curl_init($url);

        // Add headers
        $headers    = array();
        $headers[]  = "Accept: application/json";
        $headers[]  = "Authorization: Basic " . $credentials;
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);                // Add headers
        curl_setopt($session, CURLOPT_HTTPGET, true);                       // HTTP GET
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);                // Receive server response  
        $server_output = curl_exec($session);
        
        curl_close ($session);

        $json_response = json_decode( $server_output, TRUE );
        
        if (isset( $json_response['authorizationToken'] )) {
            $ret = send_response(1, 
                                    [
                                     'auth_token' => $json_response['authorizationToken'], 
                                     'api_url' => $json_response['apiUrl'], 
                                     'download_url' => $json_response['downloadUrl'], 
                                     'bucket_id' => $json_response['allowed']['bucketId'] 
                                    ], 
                                '' );
        }
        else{
            $ret = send_response(0, '', '' );
        }
        return $ret;

	}

    function get_b2_uploadUrl($key_id, $application_key){
        $b2_client = create_backBlaze_client($key_id, $application_key);

        if ($b2_client['status'] == 1) {
            
            $api_url        = $b2_client['data']['api_url']; // From b2_authorize_account call
            $auth_token     = $b2_client['data']['auth_token']; // From b2_authorize_account call
            $bucket_id      = $b2_client['data']['bucket_id']; // From b2_authorize_account call

            $session = curl_init($api_url .  "/b2api/v2/b2_get_upload_url");
    
            // Add post fields
            $data = array("bucketId" => $bucket_id);
            $post_fields = json_encode($data);
            curl_setopt($session, CURLOPT_POSTFIELDS, $post_fields); 
    
            // Add headers
            $headers = array();
            $headers[] = "Authorization: " . $auth_token;
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers); 
    
            curl_setopt($session, CURLOPT_POST, true); // HTTP POST
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
            $server_output = curl_exec($session); // Let's do this!
            curl_close ($session); // Clean up
            $json_response = json_decode( $server_output, TRUE );

            return $json_response;

        }
        
        else{
            return 'empty';
        }
    }
	
    function b2_get_folder( $folder, $key_id, $application_key){
        
        $b2_client = create_backBlaze_client($key_id, $application_key);
        
        if ($b2_client['status'] == 1) {

            $api_url        = $b2_client['data']['api_url']; // From b2_authorize_account call
            $auth_token     = $b2_client['data']['auth_token']; // From b2_authorize_account call
            $bucket_id      = $b2_client['data']['bucket_id']; // From b2_authorize_account call

            $session = curl_init($api_url .  "/b2api/v2/b2_list_file_names");

            // Add post fields
            $data = array("bucketId" => $bucket_id, "prefix" => $folder, "delimiter" => "/");
            $post_fields = json_encode($data);
            curl_setopt($session, CURLOPT_POSTFIELDS, $post_fields); 

            // Add headers
            $headers = array();
            $headers[] = "Authorization: " . $auth_token;
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers); 

            curl_setopt($session, CURLOPT_POST, true); // HTTP POST
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
            $server_output = curl_exec($session); // Let's do this!
            curl_close ($session); // Clean up

            $json_response = json_decode( $server_output, TRUE );
            // echo"<pre>";print_r($json_response);
            // die;
            return $json_response['files'];

        }

        else{
            return 'error';
        }

	}

    function b2_uploadFile( $current_folder, $target, $file_name, $key_id, $application_key){
        
        $b2_upload_data = get_b2_uploadUrl( $key_id, $application_key );
        
        if ( isset($b2_upload_data['uploadUrl']) ) {
            
            // $handle = fopen($target, 'r');
            // $read_file = fread($handle,filesize($target));

            $read_file = file_get_contents($target);

            $upload_url = $b2_upload_data['uploadUrl']; // Provided by b2_get_upload_url
            $upload_auth_token = $b2_upload_data['authorizationToken']; // Provided by b2_get_upload_url
            $bucket_id = $b2_upload_data['bucketId'];  // The ID of the bucket
            $content_type = "text/plain";
            $sha1_of_file_data = sha1_file($target);

            $session = curl_init($upload_url);

            // Add read file as post field
            curl_setopt($session, CURLOPT_POSTFIELDS, $read_file); 

            // Add headers
            $headers = array();
            $headers[] = "Authorization: " . $upload_auth_token;
            $headers[] = "X-Bz-File-Name: " . $current_folder . $file_name;
            $headers[] = "Content-Type: " . $content_type;
            $headers[] = "X-Bz-Content-Sha1: " . $sha1_of_file_data;
            $headers[] = "X-Bz-Info-Author: " . "unknown";
            $headers[] = "X-Bz-Server-Side-Encryption: " . "AES256";
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers); 

            curl_setopt($session, CURLOPT_POST, true); // HTTP POST
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
            $server_output = curl_exec($session); // Let's do this!
            curl_close ($session); // Clean up

            $json_response = json_decode( $server_output, TRUE );
            
            return $json_response;
        }

        else{
            return 'error';
        }

	}

    function b2_create_folder( $current_folder, $folder_name, $key_id, $application_key){
        
        $b2_upload_data = get_b2_uploadUrl($key_id, $application_key);
        
        if ( isset($b2_upload_data['uploadUrl']) ) {
            $file_name = ".bzEmpty";
            $my_file = $_SERVER['DOCUMENT_ROOT'] .'/assets/backend/empty/txt/bzEmpty';
            // print_r($_SERVER['DOCUMENT_ROOT'] );die();
            $handle = fopen($my_file, 'r');
            $read_file = fread($handle,filesize($my_file));

            $upload_url = $b2_upload_data['uploadUrl']; // Provided by b2_get_upload_url
            $upload_auth_token = $b2_upload_data['authorizationToken']; // Provided by b2_get_upload_url
            $bucket_id = $b2_upload_data['bucketId'];  // The ID of the bucket
            $content_type = "text/plain";
            $sha1_of_file_data = sha1_file($my_file);

            $session = curl_init($upload_url);

            // Add read file as post field
            curl_setopt($session, CURLOPT_POSTFIELDS, $read_file); 

            // Add headers
            $headers = array();
            $headers[] = "Authorization: " . $upload_auth_token;
            $headers[] = "X-Bz-File-Name: " . $current_folder . $folder_name . $file_name;
            $headers[] = "Content-Type: " . $content_type;
            $headers[] = "X-Bz-Content-Sha1: " . $sha1_of_file_data;
            $headers[] = "X-Bz-Info-Author: " . "unknown";
            $headers[] = "X-Bz-Server-Side-Encryption: " . "AES256";
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers); 

            curl_setopt($session, CURLOPT_POST, true); // HTTP POST
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
            $server_output = curl_exec($session); // Let's do this!
            curl_close ($session); // Clean up

            $json_response = json_decode( $server_output, TRUE );
            // echo"<pre>";print_r($json_response);
            // die;
            return $json_response;
        }

        else{
            return 'error';
        }

	}

    function b2_delete_file_by_id( $file_id, $file_name, $key_id, $application_key ){
        
        $b2_client = create_backBlaze_client( $key_id, $application_key );
        
        if ( $b2_client['status'] == 1 ) {
            $api_url        = $b2_client['data']['api_url']; // From b2_authorize_account call
            $auth_token     = $b2_client['data']['auth_token']; // From b2_authorize_account call

            $session = curl_init($api_url .  "/b2api/v2/b2_delete_file_version");

            // Add post fields
            $data = array("fileId" => $file_id, "fileName" => $file_name);
            $post_fields = json_encode($data);
            curl_setopt($session, CURLOPT_POSTFIELDS, $post_fields); 

            // Add headers
            $headers = array();
            $headers[] = "Authorization: " . $auth_token;
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers); 

            curl_setopt($session, CURLOPT_POST, true); // HTTP POST
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
            $server_output = curl_exec($session); // Let's do this!
            curl_close ($session); // Clean up

            $json_response = json_decode( $server_output, TRUE );
            // echo"<pre>";print_r($json_response);
            // die;
            return $json_response;
        }

        else{
            return 'error';
        }

	}
    function b2_delete_folder_by_path( $folder, $key_id, $application_key ){
        $id_arr = [];
        $get_files = b2_get_folder( $folder, $key_id, $application_key);
        // print_r($get_files);die;
        foreach ($get_files as $key => $value) {
            if ($value['action'] == 'upload') {
                // array_push($id_arr, $value['fileId']);
                $id_arr[] = array("id"=> $value['fileId'] , "name"=> $value['fileName'] );
            }else if($value['action'] == 'folder'){
                $ar = b2_delete_folder_by_path( $value['fileName'], $key_id, $application_key );
                $id_arr = array_merge($id_arr,$ar);
            }
        }
        return $id_arr;
        // print_r($id_arr);die();
	}

    function b2_download_file_by_id( $file_id, $key_id, $application_key ){
        
        $b2_client = create_backBlaze_client( $key_id, $application_key );
        
        if ( $b2_client['status'] == 1 ) {
            $download_url        = $b2_client['data']['download_url']; // From b2_authorize_account call\

            $uri = $download_url . "/b2api/v2/b2_download_file_by_id?fileId=" . $file_id;

            $session = curl_init($uri);

            curl_setopt($session, CURLOPT_HTTPGET, true); // HTTP GET
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);  // Receive server response
            $server_output = curl_exec($session); // Let's do this!
            curl_close ($session); // Clean up
            // echo ($server_output); // Tell me about the rabbits, George!
            // die;
            // $json_response = json_decode( $server_output, TRUE );
            // echo"<pre>";print_r($json_response);
            // die;
            return $server_output;
        }

        else{
            return 'error';
        }

	}

    function b2_create_friendly_url($file_id, $filename, $bucket_name, $key_id, $application_key)
    {
        $b2_client = create_backBlaze_client($key_id, $application_key);
        
        if ($b2_client['status'] == 1) {

            $api_url        = $b2_client['data']['api_url']; // From b2_authorize_account call
            $auth_token     = $b2_client['data']['auth_token']; // From b2_authorize_account call
            $bucket_id      = $b2_client['data']['bucket_id']; // From b2_authorize_account call
        }

        $url = $api_url. "/file/".$bucket_name."/".$file_id.$filename;
        return $url;
    }
    
    function b2_copy_file( $file_id, $file_name ){
        
        $b2_client = create_backBlaze_client();
        
        if ( $b2_client['status'] == 1 ) {
            $file_name = ""; // File to be uploaded
            $bucket_id = ""; // Provided by b2_create_bucket, b2_list_buckets
            $content_type = ""; // The content type of the file. See b2_start_large_file documentation for more information.
            // Construct the JSON to post
            $data = array("fileName" => $file_name, "bucketId" => $bucket_id, "contentType" => $content_type);
            $post_fields = json_encode($data);

            // Setup headers
            $headers = array();
            $headers[] = "Accept: application/json";
            $headers[] = "Authorization: " . $account_auth_token;

            // Setup curl to do the post
            $session = curl_init($api_url . "/b2api/v2/b2_start_large_file");
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers);  // Add headers
            curl_setopt($session, CURLOPT_POSTFIELDS, $post_fields); 
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true); // Receive server response
            // Post the data
            $server_output = curl_exec($session);
            curl_close ($session);

            $json_response = json_decode( $server_output, TRUE );
            echo"<pre>";print_r($json_response);
            die;
        }

        else{
            echo '404';
        }

	}
	
	function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }



?>