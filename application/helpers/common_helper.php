<?php
    function sendUserEmailMandrill($to='', $subject = '', $body = ''){
        $tos[] = array(
            'email' => $to,
            'name' => '',
            'type' => 'to'
        );
         $message = array(
            'html' => $body,	
            'subject' => $subject,
            'from_email' => 'support@selfdrive.work',
            'from_name' => 'Self Drive',
            'to' =>$tos,
        ); 	   
        $POSTFIELDS = array(
            'key' => 'NtSxVXvKNB_5JQOjv5bFTw',
            'message' => $message
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://mandrillapp.com/api/1.0/messages/send.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($POSTFIELDS));
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);
    }
    function generateString($len=8){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
        for ($i = 0; $i < $len; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
        return $randomString;
    }
    function getImageExtension($imgName='a.jpg'){
        $temp = explode('.',$imgName);
        $temp = array_reverse($temp);
        return '.'.$temp[0];
    }
    function slugify($text){
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
          return 'n-a';
        }
        return $text;
      }
    function sendToAutoResponder($user_email,$user_name,$w_id,$type){
        $CI =& get_instance();
        $web_res = $CI->DBfile->get_data('website',array('w_id'=>$w_id),$type.',w_userid');
        $w_list_data = $web_res[0][$type] != '' ? json_decode($web_res[0][$type],true) : array();
        if(!empty($w_list_data)){
            if( $w_list_data['ar'] != '' && $w_list_data['ar'] != '0' && $w_list_data['listid'] != '' && $w_list_data['listid'] != '0' )
            {
                $res = $CI->DBfile->get_data('autoresponder',array('m_id' => $web_res[0]['w_userid'], 'mkey' => 'autoresponder'));
                $data = (array) json_decode( $res[0]['value'] );
                if(!empty($data[$w_list_data['ar']])){
                    $_POST['email'] = $user_email;
                    $_POST['listid'] = $w_list_data['listid'];
                    $_POST['name'] = $user_name;
                    $api = (array) $data[$w_list_data['ar']];
                    $pathArr = explode('application',__DIR__);
                    require_once $pathArr[0].'application/controllers/subscriber/subscriber.php';
                    $objlist = new subscriber();
                    $response = $objlist->switch_responder($api, 'subsCribe', $w_list_data['ar'] );
                }
            }
        }
      }
      function ftp_rdel($handle, $path) {
        if (@ftp_delete ($handle, $path) === false) {
          if ($children = @ftp_nlist ($handle, $path)) {
            foreach ($children as $p){
                $ar = explode('/', $p);
                  if( end($ar) != '.' && end($ar) != '..' ){
                      ftp_rdel ($handle,  $p);
                  }
            }
          }
          @ftp_rmdir ($handle, $path);
        }
      }
    if (!function_exists('encryptor')) {
    function encryptor($action, $string){
        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'selfcloud/';
        $secret_iv = 'selfcloudr@#0123';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        //do the encyption given text/string/number
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            //decrypt the given text/string/number
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}

function get_mime_type($filename) {
    $idx = explode( '.', $filename );
    $count_explode = count($idx);
    $idx = strtolower($idx[$count_explode-1]);

    $mimet = array( 
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'csv' => 'text/csv',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    if (isset( $mimet[$idx] )) {
     return $mimet[$idx];
    } else {
     return 'application/octet-stream';
    }
 }

 function isFileFolder($name){
    $a = pathinfo($name);
    
    if( isset($a['extension']) && !empty($a['extension']) ){
        return 'file';
    }else{
        return 'folder';
    }
}

?>