<?php
require 'aws/vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException;

/* sns notitfication 
use Aws\Sns\SnsClient;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Aws\Sns\Exception\InvalidSnsMessageException;
sns notitfication */
 
/* Check Connection S3*/
function checkconnection( $bucket ,$amazonKey , $amazonSecretKey , $amazonRegion){
        $args = array(
    		'version'     => 'latest',
    		'region'      => $amazonRegion,
    		'credentials' => array(
    			'key'         => $amazonKey,
    			'secret'      => $amazonSecretKey
    		)                                
		);
	 
	try{
		//Create a S3Client
		$s3Client = new S3Client( $args );
      
		$result = $s3Client->getBucketAcl([
			'Bucket' => $bucket
		]);	
    
		return array( 'status' => 1, 'msg' => $result );
	}catch (S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}catch (Aws\S3\Exception\S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}
}
/* Get Bucket Data (Folder & Files) S3*/
function s3_get_objects( $bucket_name, $amazonKey, $amazonSecretKey, $amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		
		if($bucket_name == ''){
			global $bucket_name;
		}
		
		$s3Client = new S3Client( $args);
	
		$object = array(
			'Bucket' => $bucket_name,
		);
		 
		$result = $s3Client->listObjects( $object );
		return  isset($result['Contents']) ? $result['Contents'] : array();
		
	}catch(S3Exception $e){
		return $e->getMessage();
	}
}

function upload_s3( $file,$ext, $folder, $userID ,$amazonKey,$amazonSecretKey,$amazonRegion,$BucketName){
	if($folder !=null){
		$target = $folder;
		
	}else{
		$target = "upload/";
	}

	$pathData = s3_upload_object( $file,$ext, $target ,$amazonKey,$amazonSecretKey,$amazonRegion,$BucketName);

	return $pathData;	
}

function s3_upload_object($source,$ext, $target,$amazonKey,$amazonSecretKey,$amazonRegion,$BucketName){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		global $bucket_name;
		$name = basename( $ext );
		
		//$key = $target . '/' . uniqid() . '-' . $name;
		$key = $target . $name;
		$file_Path = $source;
		
		//Create a S3Client
		$s3Client = new S3Client($args );
		$object = array(
			'ACL' => 'public-read',
			'Bucket' => $BucketName,
			'Key' => $key,
			'SourceFile' => $file_Path,
		);
	  
		
		$result = $s3Client->putObject( $object );
		
		$ObjectURL = $result->get( 'ObjectURL' );
		
		return array( 'url' => $ObjectURL, 'key' => $key );
		
		return $result;
	
	}catch (S3Exception $e) {
		return $e->getMessage();
	}
	
}
function s3_get_bucket_folder($bucket_name,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
    $s3 = new S3Client( $args );
    $objects = $s3->ListObjects(['Bucket' => $bucket_name, 'Delimiter'=>'/']);
    
    return isset($objects['CommonPrefixes']) ? $objects['CommonPrefixes'] : array();
    // $objects = $s3->ListObjects(['Bucket' => $bucket_name, 'Delimiter'=>'/','Prefix' => 'upload/']);
}
function s3_get_object( $keyname, $bucket_name,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		
		 $s3 = new S3Client( $args );
         $objects = $s3->ListObjects(['Bucket' => $bucket_name, 'Delimiter'=>'/','Prefix' => $keyname]);
         
         $file = isset($objects['Contents']) ? $objects['Contents'] : array();
         $folder = isset($objects['CommonPrefixes']) ? $objects['CommonPrefixes'] : array();
         $data = [];
         $folderName = isset($folder) ? $folder : array();
         array_push($data,$folderName);
         array_push($data,$file);
        //  $data = array('file'=>$file,'folder'=>$folder);
         return $data;
	
	}catch(S3Exception $e){
		return $e->getMessage();
	}
}

function s3_delete_object( $keys, $bucket_name,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	if($bucket_name == ''){
		global $bucket_name;
	}
	
	$s3Client = new S3Client( $args );
	
	$object = array(
		'Bucket' => $bucket_name,
		'Delete' => array(
			'Objects' => array_map(function ($keys) {
				return array('Key' => $keys);
			}, $keys)
		)
	);
	
	$result = $s3Client->deleteObjects( $object );
	
	$mes = $result->get( 'Deleted' );
	
	return $mes;
}

function s3_delete_matching_object($keys, $bucket_name ,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	
	try{
	$s3Client = new S3Client($args );
	
		$result = $s3Client->deleteMatchingObjects($bucket_name ,$keys);
		return $result ;
	}catch (S3Exception $e) {
		return $e->getMessage();
	}
}

function s3_get_bucket($amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		//Create a S3Client
		$s3Client = new S3Client( $args );

		//Listing all S3 Bucket
		$bucketsarray = $s3Client->listBuckets();
		
		$buckets = !empty($bucketsarray['Buckets']) ? $bucketsarray['Buckets'] : array();	

		return array( 'status' => 1, 'msg' => '', 'data' => $buckets );
	}catch (S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}catch (Aws\S3\Exception\S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}
}

function checkBucket( $bucket,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		//Create a S3Client
		$s3Client = new S3Client( $args );

		$result = $s3Client->getBucketAcl([
			'Bucket' => $bucket
		]);	

		return array( 'status' => 1, 'msg' => $result );
	}catch (S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}catch (Aws\S3\Exception\S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}
}

function createBucket( $bucket ,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		//Create a S3Client
		$s3Client = new S3Client( $args );

		$result = $s3Client->createBucket([
			'ACL' => 'public-read-write',
			'Bucket' => $bucket
		]);

		return array( 'status' => 1, 'msg' => $result );
	}catch (S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}catch (Aws\S3\Exception\S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}
}

function getUrl( $bucket, $key ,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);

	try{
		$s3Client = new S3Client( $args );
		return $s3Client->getObjectUrl( $bucket, $key );
	}catch (S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}catch (Aws\S3\Exception\S3Exception $e) {
		return array( 'status' => 0, 'msg' => $e->getMessage() );
	}
}


/*
function s3_sns_notify(){
	try {
		// Create a message from the post data and validate its signature
		$message = Message::fromRawPostData();
		$validator = new MessageValidator();
		$validator->validate($message);
	} catch (InvalidSnsMessageException $e) {
		return $e->getMessage();
		// Pretend we're not here if the message is invalid
		http_response_code(404);
		die;
	}

	if ($message['Type'] === 'SubscriptionConfirmation') {
		// Confirm the subscription by sending a GET request to the SubscribeURL
	   file_get_contents($message['SubscribeURL']);
	   
	   return true;
	}

	if ($message['Type'] === 'Notification') {
	   // Do whatever you want with the message body and data.
		$a = array( 
			'messageid' => $message['MessageId'],
			'message' => $message['Message']
		);
	   
		return $a;
	   
	}
}

function getAllObjects( $folder ){
	global $bucket_name;
	$s3Client = new S3Client( api_arguments() );

	$objects = $s3Client->listObjects( array(
		"Bucket" => $bucket_name,
		"Prefix" => $folder
	)); 

	return $objects;
}

function sendMessage( $second, $sendMessage, $messageBody, $query_url = "" ){
	$client = new SqsClient( api_arguments() );

	$params = array(
		'DelaySeconds' => $second,
		'MessageAttributes' => $sendMessage,
		'MessageBody' => $messageBody,
		'QueueUrl' => $query_url
	);

	try {
		$result = $client->sendMessage($params);
		return $result;
	} catch (AwsException $e) {
		// output error message if fails
		error_log($e->getMessage());
	}
}

function deleteMessageWediosProcess( $Messages, $query_url = '' ){
	$client = new SqsClient( api_arguments() );
	try {
		$result = $client->receiveMessage(array(
			'VisibilityTimeout' => 20,
			'MaxNumberOfMessages' => 10,
			'QueueUrl' => $query_url
		));
		if (!empty($result->get('Messages'))) {
			$deleteMessage = array();
			foreach($result->get('Messages') as $msg){
				if( in_array( $msg['MessageId'], $Messages ) ){
					$dresult = $client->deleteMessage([
						'QueueUrl' => $query_url, // REQUIRED
						'ReceiptHandle' => $msg['ReceiptHandle'] // REQUIRED
					]); 
					$index = array_search( $msg['MessageId'], $Messages );
					$deleteMessage[] = $Messages[$index];
				}
			}
			return $deleteMessage;
		} else {
			return false;
		}
	} catch (AwsException $e) {
		// output error message if fails
		error_log($e->getMessage());
	}
}

function receiveMessage( $query_url = "" ){
	$client = new SqsClient( api_arguments() );
	echo 'asd';
	try {
		$result = $client->receiveMessage(array(
			'AttributeNames' => ['SentTimestamp'],
			'MaxNumberOfMessages' => 1,
			'MessageAttributeNames' => ['All'],
			'QueueUrl' => $query_url, // REQUIRED
			'WaitTimeSeconds' => 0,
		));
		if (!empty($result->get('Messages'))) {
			echo '<pre>';
			var_dump($result->get('Messages')[0]['MessageAttributes']);
			echo ($result->get('Messages')[0]['MessageAttributes']['Author']['StringValue']);
			echo ($result->get('Messages')[0]['MessageAttributes']['Title']['StringValue']);
			echo ($result->get('Messages')[0]['MessageAttributes']['WeeksOn']['StringValue']);
			echo '</pre>';
			$dresult = $client->deleteMessage([
				'QueueUrl' => $query_url, // REQUIRED
				'ReceiptHandle' => $result->get('Messages')[0]['ReceiptHandle'] // REQUIRED
			]);
			return $result->get('body');
		} else {
			echo "No messages in queue. \n";
		}
	} catch (AwsException $e) {
		// output error message if fails
		error_log($e->getMessage());
	}
}
function api_arguments(){
    // $CI =& get_instance();
    // $web_res = $CI->DBfile->get_data('website',array('w_id'=>$w_id),$type.',w_userid');
    $amazonKey = AMAZON_KEY;
    $amazonSecretKey = AMAZON_SECRET_KEY;
    $amazonRegion = AMAZON_REGION;
	$args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);

	return $args;
}
function s3_upload_object_ad($source, $key,$amazonKey,$amazonSecretKey,$amazonRegion){
    $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
	try{
		global $bucket_name;
		//Create a S3Client
		$s3Client = new S3Client( $args );
		
		$object = array(
			'ACL' => 'public-read',
			'Bucket' => $bucket_name,
			'Key' => $key,
			'SourceFile' => $source,
		);
		
		$result = $s3Client->putObject( $object );
		
		$ObjectURL = $result->get( 'ObjectURL' );
		
		return array( 'url' => $ObjectURL, 'key' => $key );
		
		return $result;
	
	}catch (S3Exception $e) {
		return $e->getMessage();
	}
	
}
function s3_create_folder($BucketName,$amazonRegion,$amazonKey,$amazonSecretKey){
    $counter = 0;
     $args = array(
		'version'     => 'latest',
		'region'      => $amazonRegion,
		'credentials' => array(
			'key'         => $amazonKey,
			'secret'      => $amazonSecretKey
		)
	);
        try{
    	
    			$s3Client = new S3Client($args );
    	    return	$s3Client->downloadBucket('/test', $BucketName);
    	    
            // 	$iterator = $s3Client->getIterator('ListObjects', array(
            //             'Bucket' => $BucketName,
            //             'Prefix' => 'test'
            //         ));
                    
            //         foreach ($iterator as $object) {
            //           print_r($object);
            //         }
    
    	}catch (S3Exception $e) {
    		return $e->getMessage();
    	}
}
*/