<?php                                                                                   

/**
 * @description : Library to access file.io Public API
 */
Class file_io {

    function __construct() {

    }
    function uploadFile($userkey="",$file=""){
        if(!empty($userkey)){
            $key =$userkey;
        }else{
            $key = 'ravitestfile.io';
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://file.io/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer '.$key;
        $headers[] = 'Content-Type: multipart/form-data';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        
    }
    

}