<?php
require APPPATH . 'libraries/php-photoslibrary-samples/vendor/autoload.php';
use Google\Photos\Types\Album;
use Google\Photos\Library\V1\PhotosLibraryClient;
use Google\Photos\Library\V1\PhotosLibraryResourceFactory;
use Google\Rpc\Code;

class GooglePhotos{
	function __construct(){
	    $this->secretPath =  APPPATH . 'libraries/google-client/secrets/UserRefreshCredentials.json';
		$this->scopes     = array('https://www.googleapis.com/auth/photoslibrary.readonly','https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata','https://www.googleapis.com/auth/photoslibrary.sharing');
		$this->redirecturi = base_url().'home/get_info_google_photo';
		$this->YOUR_API_KEY = 'AIzaSyDdyUhsPebXKSetuAP1c1sSCp2M2ofaBv0';
	}
	
	function CreateAlbum($albumName){
	    if (isset($albumName)) {
            $newAlbum = new Album();
            $newAlbum->setTitle($albumName);
            try {
                $createdAlbum = $photosLibraryClient->createAlbum($newAlbum);
                $albumId = $createdAlbum->getId();
            } catch (\Google\ApiCore\ApiException $e) {
                echo $templates->render('error', ['exception' => $e]);
            }
        }
	}
	
    function  uploadMedia(){
        try {
            $uploadToken = $photosLibraryClient->upload(file_get_contents($localFilePath), null, $mimeType);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            // Handle error
        }
    }
}

?>