<?php
if(!class_exists("Google_Client")) {
	require_once APPPATH . 'libraries/google-client/vendor/autoload.php';
}
/**
*   Google Drive Class
*/
class Googledrive
{
	function __construct(){
		$this->secretPath =  APPPATH . 'libraries/google-client/secrets/client_secret.json';
		$this->scopes     = implode(' ', array(Google_Service_Drive::DRIVE));
		$this->redirecturi = base_url().'home/get_info_google_drive';
	}

	public function getAuthUrl() {
        $client = new Google_Client();
        $client->setAuthConfigFile($this->secretPath);
        $client->setRedirectUri($this->redirecturi);
        $client->addScope($this->scopes);
        $client->setAccessType("offline");
        $client->setApprovalPrompt('force');
        
        $auth_url = $client->createAuthUrl();
        
        return filter_var($auth_url, FILTER_SANITIZE_URL);
	}
    
	public function clientObject( $accessToken ){
		$client = new Google_Client();
		$client->setAuthConfigFile($this->secretPath);
		$client->setRedirectUri($this->redirecturi);
		$client->addScope($this->scopes);
		$client->setAccessType("offline");
		$accessToken = $this->checkExpireToken( $client, $accessToken );
		$client->setAccessToken( $accessToken );
		return $client;
	}

	public function userInformation( $accessToken ){
		$client = $this->clientObject( $accessToken );
		$drive_user = $this->getDriveUser($client);
        $stDrive = $this->getStorageDrive($client);
        
		return array(
			'image'=> $drive_user->photoLink, 
			'name'=> $drive_user->displayName, 
			'email'=> $drive_user->emailAddress,
			'totalStorage'=>$stDrive->limit,
			'useStorage'=>$stDrive->usage
		);
	}

	public function checkExpireToken( $client, $accessToken ){
		// Refresh the token if it's expired.
		if ($client->isAccessTokenExpired()) {
			$client->fetchAccessTokenWithRefreshToken($accessToken['refresh_token']);
			$accessToken = $client->getAccessToken();

			// Insert New token

		}

		return $accessToken;
	}

	public function getDriveUser($client){
		$drive = new Google_Service_Drive($client);
		return $drive->about->get(["fields" => "user"])->user;
		
	}
    function getStorageDrive($client){
        $service = new Google_Service_Drive($client); 
        $res = $service->about->get(['fields' => 'storageQuota']);
     	return $storageQuota = $res -> getStorageQuota();
    }
	public function createFolder( $accessToken, $folderName ) {
		$client = $this->clientObject( $accessToken );

		if(is_object($client)){
			$service = new Google_Service_Drive($client);

			$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => $folderName,
				'mimeType' => 'application/vnd.google-apps.folder'
			));

			$file = $service->files->create($fileMetadata, array(
				'fields' => 'id'));
			$return = array('status' => 1, 'folderid' => $file->id);
			return $return;
		}else{
			return $client;
		}
    
	}
    
    public function UpdateFileMeta($access_token, $file_id, $file_meatadata) { 
        
        $apiURL = self::DRIVE_FILE_META_URI . $file_id; 
         
        $ch = curl_init();         
        curl_setopt($ch, CURLOPT_URL, $apiURL);         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);         
        curl_setopt($ch, CURLOPT_POST, 1);         
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer '. $access_token)); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH'); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($file_meatadata)); 
        $data = json_decode(curl_exec($ch), true); 
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);         
         
        if ($http_code != 200) { 
            $error_msg = 'Failed to update file metadata'; 
            if (curl_errno($ch)) { 
                $error_msg = curl_error($ch); 
            } 
            throw new Exception('Error '.$http_code.': '.$error_msg); 
        } 
 
        return $data; 
    } 
	public function uploadFile($filename, $parentId, $path, $type, $accessToken, $url_present = false) {
		$client = $this->clientObject( $accessToken );
        
		if(is_object($client)){
			$service = new Google_Service_Drive($client);
		  
			$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => $filename,
				'parents' => array($parentId)
			));
			
			$content = '';
            if(file_exists($path)){
			    $content = file_get_contents($path);
            }
			if ($url_present) {
				$content = file_get_contents($path);
			}
			$file = $service->files->create($fileMetadata, array(
				'data' => $content,
				'mimeType' => $type,
				'uploadType' => 'multipart',
				'fields' => 'id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents')
			);

			$filenew = $service->files->get($file['id']);
			$ex = $file->getMimeType();
			$ex = explode('/', $ex);
			$ex = isset($ex[1]) ? $ex[1] : 'other';

			$return = array('status' => 1, 'filedata' => $file, 'ex' => $ex);
			
			$userPermission = new Google_Service_Drive_Permission(array(
				'type' => 'anyone',
				'role' => 'reader',
				'anyone' => ''
			));
			
			$request = $service->permissions->create($file->id, $userPermission, array('fields' => 'id'));
			return $return;
		}else{
			return $client;
		}
	}

	public function deleteFiles($fileID, $accessToken) {
		$client = $this->clientObject( $accessToken );
		if(is_object($client)){
			$service = new Google_Service_Drive($client);
			try {
				$service->files->delete($fileID);
				$return = array('status' => 1);
			} catch (Exception $e) {
				$return = array('status' => 0, 'msg' => $e->getMessage());
			}
			return json_encode($return);
		}else{
			return json_encode($client);
		}
	}

	public function getAllFolders( $accessToken, $folderId = null ){
// 		print_r($accessToken);
// 		die();
		$client = $this->clientObject( $accessToken );
		if(is_object($client)){
			$service = new Google_Service_Drive($client);
			if($folderId == null){
				$parameters['q'] = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
			}else{
				$parameters = array(
					'pageSize' => 100,
					'orderBy' => 'name',
					'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
					'q' => "'".$folderId."' in parents"
				);
			}
			$files = $service->files->listFiles($parameters)->getFiles();
			return array('status' => 1, 'data' => $files);
		}else{
			return json_encode($client);
		}
	}

	public function recursiveGetAllFolders($accessToken, $folderId, $parentName, &$fileLists){
		$r = $this->getAllFolders($accessToken, $folderId);
		if(isset($r['status']) && $r['status']){
			$fr = $r['data'];
			if(!empty($fr)){
				foreach($fr as $gfile){
					$nm = $parentName . '/' . $gfile['name'];
					
					if($gfile['mimeType'] == 'application/vnd.google-apps.folder'){
						$ar = $this->recursiveGetAllFolders($accessToken, $gfile['id'], $nm, $fileLists);
					}else{
						$fileLists[] = array(
							'id' => $gfile['id'],
							'name' => $nm,
							'file_name' => $gfile['name']
						);
					}
				}
			}
		}
		return $fileLists;
	}

	public function getFileInfo( $accessToken, $fileId ){
	  
		$client = $this->clientObject( $accessToken );
		if(is_object($client)){
			$service = new Google_Service_Drive($client);
			$file = $service->files->get($fileId);
			$name = $file->getName();
			$ex = $file->getMimeType();
			 
			$type = $ex;
			$ex = explode('/', $ex);
			$ex = isset($ex[1]) ? $ex[1] : 'other';
			if($ex == '*'){
				$ex = explode('.', $name);
				$ex = isset($ex[count($ex)-1]) ? $ex[count($ex)-1] : 'other';
			}
			$userPermission = new Google_Service_Drive_Permission(array(
				'type' => 'anyone',
				'role' => 'reader',
				'anyone' => ''
			));
			
			$request = $service->permissions->create($fileId, $userPermission, array('fields' => 'id'));

			$url = "https://drive.google.com/uc?export=view&id={$fileId}&ezex={$ex}";
            
			return array('status' => 1, 'data' => array(
				'name' => $name,
				'ex' => $ex,
				'url' => $url,
				'type'=>$type
			));
		}else{
			return json_encode($client);
		}
	}
	
	function DownloadFile($token,$fileId){
	   // $fileId = "###"; // Please set the file ID.
    	$client = $this->clientObject( $token );
    	$service = new Google_Service_Drive($client);
        // Retrieve filename.
        $file = $service->files->get($fileId);
        $fileName = $file->getName();
        
        // Download a file.
        $content = $service->files->get($fileId, array("alt" => "media"));
        $downloadUrl = $file->getDownloadUrl();
        
        $handle = fopen("./download/".$fileName, "w+"); // Modified
        while (!$content->getBody()->eof()) { // Modified
          fwrite($handle, $content->getBody()->read(1024)); // Modified
        }
        fclose($handle);
        echo "success";
	}
    public function getFile(Google_Service_Drive_DriveFile $file, $fileMimeType='')
    {
        if (!$this->service) {
            $this->buildService();
        }
        $downloadUrl = $file->getExportLinks()[$fileMimeType];

        if ($downloadUrl) {
            $request = new Google_Http_Request($downloadUrl, 'GET', null, null);

            // use authentication parameter to login with previously created credentials
            // and request the file
            $client = new Google_Client();
            $client->setAssertionCredentials($this->auth);
            $client->getAuth()->sign($request);

            $io = $client->getIo();
            $httpRequest = $io->makeRequest($request);

            if ($httpRequest->getResponseHttpCode() == 200) {
                return $httpRequest->getResponseBody();
            } else {
                // An error occurred. Maybe throw some exception?
                return null;
            }
        } else {
            // The file doesn't have any content stored on Drive.

            return null;
        }
    }
/*********************************************************/

	/**
	* Returns an authorized API client.
	* @return Google_Client the authorized client object
	*/
	private function getClient($parentId = "") {

		if( file_exists($this->secretPath) ){
			$client = new Google_Client();
			$client->setAuthConfigFile($this->secretPath);
			$client->setRedirectUri($this->redirecturi);
			$client->addScope($this->scopes);
			$client->setAccessType("offline");
			
			$user_id = current_user_id();
			if($parentId)
			{
				$project = get_project_by_id($parentId);
				if(isset($project['user_id'])){
					$user_id = $project['user_id'];
				}
			}
			$token = find_user_token($user_id);
			
			if( isset($token[0]['tokens']) && !empty($token[0]['tokens']) ){
				$accessToken = (array) json_decode($token[0]['tokens'], true);
			}else{
				return array('status' => 0, 'error' => 'You need to authenticate for google drive.');
			}
			
			$client->setAccessToken($accessToken);
			
			// Refresh the token if it's expired.
			if ($client->isAccessTokenExpired()) {
				$client->fetchAccessTokenWithRefreshToken($accessToken['refresh_token']);
				$accessToken = $client->getAccessToken();
				
				$tokens = setupTokenData($accessToken);
				$user_id = $token[0]['user_id'];
				
				$token_data = array('tokens' => $tokens);
			
				if( insert_new_token($user_id, $token_data) ) {
					
				}
				
				$client->setAccessToken($accessToken);
			}

			return $client;
		}
		else
		{
			return array('status' => 0, 'error' => 'You need to add "client_secret.json" file in the third_party folder');
		}

	}

	public function getClientForAuth() {
		$client = new Google_Client();
		$client->setAuthConfigFile($this->secretPath);
		$client->setRedirectUri($this->redirecturi);
		$client->addScope($this->scopes);
		$client->setAccessType("offline");
		$client->setApprovalPrompt('force');
		
		return $client;
	}
	
	public function getHtmlForUnauth()
	{
		$authUrl = $this->getAuthUrl();
		if(is_array($authUrl)){
			$htmlBody = $authUrl['error'];
		}else{
			$htmlBody = '<h2>Setup your Google Account:</h2>
			<h2>Login to Your Google Account.</h2>
			<p>+ Please login to your Google Account</p>
			<a class="button" href="'.$authUrl.'">Click Here</a>';
		}
		
		return $htmlBody;
	}
	
	public function getHtmlForAuth()
	{
		$client = $this->getClient();
		$htmlBody = '';
		
		// Success Print User Blog Data
		$htmlBody .= "<h2>Google Account</h2>";
		$drive_user = $this->getDriveUser($client);

		return $drive_user;

		$htmlBody .= "<img src=\"{$drive_user->photoLink}\" height=\"70\" />";
		$htmlBody .= "<h3><span>Name:</span> " . $drive_user->displayName . "</h3>";
		$htmlBody .= "<h3><span>Email:</span> " . $drive_user->emailAddress . "</h3>";            
		
		$user_id = current_user_id();
		$blogDataArr = array('thumb_url' => $drive_user->photoLink, 'name' => $drive_user->displayName, 'email' => $drive_user->emailAddress);
		$blogData = array("blog_info" => json_encode($blogDataArr));
		
		insert_new_token($user_id, $blogData);
		
		return $htmlBody;
	}
	public function createSubFolder($folderName, $parentId,$accessToken) {
		$client = $this->clientObject( $accessToken );
		if(is_object($client)){
			$service = new Google_Service_Drive($client);

			$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => $folderName,
				'parents' => array($parentId),
				'mimeType' => 'application/vnd.google-apps.folder'
			));

			$file = $service->files->create($fileMetadata, array(
				'fields' => 'id'
			));

			$return = array('status' => 1, 'folderid' => $file->id);
			return json_encode($return);
		}else{
			return json_encode($client);
		}
    
	}
   
	public function duplicateFile($fileID, $copyTitle, $parentId, $project_id = "") {
		$client = $this->getClient($project_id);
		if(is_object($client)){
			$service = new Google_Service_Drive($client);
			$copiedFile = new Google_Service_Drive_DriveFile(array(
				'name' => $copyTitle,
				'parents' => array($parentId)
			));
			try {
				$file = $service->files->copy($fileID, $copiedFile, array(
					'fields' => 'id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents')
				);

				$return = array('status' => 1, 'filedata' => $file);

				/*$service->getClient()->setUseBatch(true);
				$batch = $service->createBatch();*/
			
				$userPermission = new Google_Service_Drive_Permission(array(
					'type' => 'anyone',
					'role' => 'reader',
					'anyone' => ''
				));
			
				$request = $service->permissions->create($file->id, $userPermission, array('fields' => 'id'));
				/*$batch->add($request, 'anyone');
				$results = $batch->execute();*/
			
				//$service->getClient()->setUseBatch(false);
			} catch (Exception $e) {
				$return = array('status' => 0, 'msg' => $e->getMessage());
			}
			return json_encode($return);
		}
		else
		{
			return json_encode($client);
		}
	}

	public function getallFolderImages($parentId) {
		$client = $this->getClient();
		if(is_object($client)){
			$drive = new Google_Service_Drive($client);
			$optParams = array(
				'pageSize' => 10,
				'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
				'q' => "'".$parentId."' in parents"
			);

			$files = $drive->files->listFiles($optParams);
			echo json_encode($files);
		}else{
			echo json_encode($client);
		}
	}

	public function getallfiles() {
		$client = $this->getClient();
		if(is_object($client)){
			$drive = new Google_Service_Drive($client);
			$parameters['q'] = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
			$files = $drive->files->listFiles($parameters)->getFiles();
			echo json_encode($files);
		}else{
			echo json_encode($client);
		}
	}
	/**
     * Rename a file.
     *
     * @param Google_Service_Drive $service Drive API service instance.
     * @param string $fileId ID of the file to rename.
     * @param string $newTitle New title for the file.
     * @return Google_Service_Drive_DriveFile The updated file. NULL is returned if
     *     an API error occurred.
     */
    function renameFile($accessToken, $fileId, $newTitle,$ext) {
    	$client = $this->clientObject( $accessToken );
	    $service = new Google_Service_Drive($client);
        try {
        $file = new Google_Service_Drive_DriveFile();
        $file->setName($newTitle.'.'.$ext);
       
        $ext = pathinfo($_POST['fileName'], PATHINFO_EXTENSION);
        
        $updatedFile = $service->files->update($fileId, $file);
        
        return $updatedFile;
        } catch (Exception $e) {
            print "An error occurred: " . $e->getMessage();
        }
    }
    function moveFile($service, $fileId, $newParentId) {
    	$client = $this->clientObject( $accessToken );
	    $service = new Google_Service_Drive($client);
      try {
        $file = new Google_Service_Drive_DriveFile();
    
        $parent = new Google_Service_Drive_ParentReference();
        $parent->setId($newParentId);
    
        $file->setParents(array($parent));
    
        $updatedFile = $service->files->patch($fileId, $file);
    
        return $updatedFile;
      } catch (Exception $e) {
        print "An error occurred: " . $e->getMessage();
      }
    }
    
}
