<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Common_lib
{
	public $CI;
	public $siteLogo;
	public $authSiteLogo;
	public $defaultProfilePic;
	public $siteFavicon;
	public $siteDomain = '';
	
	public $siteName = 'Self Drive';
	public $siteTitle = 'Self Drive';
	public $sitePrimaryColor = '#a23fff';
	public $siteSecondaryColor = '#29294c';
	
// 	public $siteAuthorName = 'VineaSX Solutions LLC';
// 	public $siteDescription = 'Create Unlimited Stunning Mobile Apps In Just Few Easy Steps.';
// 	public $siteKeywords = 'Mobile Apps, AppOwls';
// 	public $siteHelpUrl = 'support@vineasx.com';
// 	public $dataPerPage = 2;
// 	public $tableDataPerPage = 10;
// 	public $appImageExt = '.png';
// 	public $appFileExt = '.html';
// 	public $baseUrl = 'https://appowls.app/';
	
// 	public $commercialAppLimit = 100;
// 	public $commercialNotiLimit = 10000;
// 	public $commercialInstallLimit = 10000;
	
// 	public $addSubUserLimit = 0;
// 	public $statusCode = 0;
// 	public $respMessage = '';
	
	function __construct(){
	   // if(!in_array($_SERVER['HTTP_HOST'] , MAIN_DOMAIN_LIST)){
	   //     print_r($_SESSION['wlDetails']);
	   // }
		$this->CI = &get_instance();
		$this->siteLogo = base_url('assets/auth/images/logo.png');
		$this->siteFavicon = base_url('assets/auth/images/favicon.png');
		$this->editorLogo = base_url('assets/auth/images/logo2.png');
// 		$this->authSiteLogo = base_url('assets/auth/images/logo.png');
		$this->authBannerImage = base_url('assets/auth/images/dash-box-logo.jpg');
		
		$this->defaultProfilePic = 'profile.png';
		$this->basePath = explode('application/',dirname(__FILE__))[0];
		$this->siteDomain = $_SERVER['HTTP_HOST'];


     	/**************************** */
		/***** WL Setting start ***** */
		/**************************** */
		if(isset($_SESSION['wlDetails'])){
// 			print_r($_SESSION['wlDetails']); exit;
			$this->siteTitle = $_SESSION['wlDetails']['title'];
			$this->siteName = $_SESSION['wlDetails']['title'];
			$this->siteDescription = $_SESSION['wlDetails']['description'];
			$this->siteAuthorName = $_SESSION['wlDetails']['author_name'];
			$this->siteKeywords = $_SESSION['wlDetails']['keyword'];
			
			$this->sitePrimaryColor = $_SESSION['wlDetails']['wl_primary_color'];
			$this->siteSecondaryColor = $_SESSION['wlDetails']['wl_secondary_color'];
			if(isset($_SESSION['wlDetails']['wl_logo'])&&empty($_SESSION['wlDetails']['wl_logo'])){
			    $logoIMG = 'assets/auth/images/logo.png';
			}else{
			     $logoIMG = "assets/backend/images/white_label/".$_SESSION['wlDetails']['wl_logo'];
			}
			if(isset($_SESSION['wlDetails']['wl_fav_icon'])&&empty($_SESSION['wlDetails']['wl_fav_icon'])){
			    $favIMG = 'assets/auth/images/favicon.png';
			}else{
			     $favIMG = "assets/backend/images/white_label/".$_SESSION['wlDetails']['wl_fav_icon'];
			}
			$this->siteLogo = base_url($logoIMG);
			$this->editorLogo = base_url("assets/backend/images/white_label/".$_SESSION['wlDetails']['wl_logo']);
// 			$this->authSiteLogo = base_url("assets/backend/images/white_label/".$_SESSION['wlDetails']['wl_logo']);
			$this->siteFavicon = base_url($favIMG);
			$this->authBannerImage = base_url('assets/auth/images/dash-box.jpg');
			$this->defaultProfilePic = $_SESSION['wlDetails']['wl_logo'];
			// echo $this->siteTitle; exit;
		}
		

		
		
		
		/**************************** */
		/***** WL Setting end ******* */
		/**************************** */
	}
	
	function uploadFile($params){
		extract($params);
		/*
		$params = array(
			'path' => UPLOAD_IMAGE_PATH,
			'name' => INPUT_TYPE_FILE_NAME,
			'postFix' =>  ANY_STRING_OR_NUMBER
		)
		*/
	// 	$uploadImg = $thixs->common->uploadFile([
	// 		'path' => '/assets/images/users/',
	// 		'name' => 'profile',
	// 		'postFix' => '_'.$_SESSION['id']
	//    ]);
		$sPath = dirname(__FILE__);
		$abs_path = explode('application', $sPath);
		$uploadPath = $abs_path[0] . $path . '/';
		$fileExt = array_reverse(explode('.',$_FILES[$name]['name']))[0];
		if(in_array($fileExt , array('gif','jpg','png','jpeg','GIF','JJPG','PNG','JPEG' ,'JPG'))){
			$config = array(
				'upload_path' => $uploadPath,
				'allowed_types' => '*'
			);
			$this->CI->load->library('upload');
			$this->CI->upload->initialize($config);
			if ($this->CI->upload->do_upload($name)) {
				$uploaddata = $this->CI->upload->data();
				$logo_name = $uploaddata['raw_name'];
				$logo_ext = $uploaddata['file_ext'];
				$randomstr = substr(md5(date('d-m-y h:i:sa')), 0, 12).'_'.rand(0,1000);
				$logo_new_name = $randomstr . $postFix . $logo_ext;
				rename($uploadPath . $logo_name . $logo_ext, $uploadPath . $logo_new_name);
				/*
				$sourceFile = $uploadPath . $logo_name . $logo_ext;
				$newFileName = $uploadPath . $logo_new_name;


				$this->compress($sourceFile, $newFileName, 70);
				unlink($sourceFile); */

				$resp = array(
					'status' => 1,
					'name' => $logo_new_name,
					'path' => $path . '/' . $logo_new_name,
					'full_path' => $uploadPath . $logo_new_name,
					'message' => 'File uploaded.'
				);
			} else {
				$resp = array(
					'status' => 0,
					'path' => '',
					'full_path' => '',
					'message' => $this->CI->upload->display_errors()
				);
			}
		}else{
			$resp = [
				'status' => 0,
				'path' => '',
				'full_path' => '',
				'message' => 'Invalid image format, please upload image only.'
			];
		}
		return $resp;
		die();
	}

}
