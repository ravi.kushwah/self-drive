<?php

/**
 * 
 * This Library is design to add, remove and check check existance of Domain/Sub-domain  
 * Need to provide some basic cPanel detail in Constructor to Authenticate the cPanel, details are given below
 *      1. domain : Provide your Application domain name on which cPanel is installed
 *      2. cPanelUserName : cPanel user name
 *      3. cPanelPassword : cPanel user Password
 * 
 */


// class Manage_cpanel_domain {
    
//     public $cPanelUserName, 
//     $cPanelPassword, 
//     $siteDomain, 
//     $cpanelURL,
//     $cPanelQueryUrl,
//     $cPanelToken,
//     $mainDomainIP;

    

//     function __construct($params) {
//         extract($params);
//         $this->siteDomain = $domain;
//         $this->cPanelUserName = $cPanelUserName;
//         $this->cPanelPassword = $cPanelPassword;
//         $this->mainDomainIP = $mainDomainIP;
//         $this->cpanelURL = 'https://'.$this->siteDomain.':2083/';
//         $this->cPanelToken = self::createSession();
//         $this->cPanelQueryUrl = $this->cpanelURL . $this->cPanelToken ."/json-api/cpanel?cpanel_jsonapi_version=2&";
//     }
       

//     /*
//     This function is help to check the Domain/Sub-domain existance in cPanel
//     The function parameter details 
//         $params = [
//             "domainName" : "example.com", //required - Provide the domain which you want to check in cPanel
// 			"type" : domain / subDomain, //required - Provide the domain type Domain/sub-domain
//         ]
//         $isInternalCall //optional , this params is only for this class also, not require for external call
//     */
//     function checkDomainExist($params , $isInternalCall = false){
//         try{
// 			extract($params);
// 			if($type == 'subDomain'){
// 			    $que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=listsubdomains&cpanel_jsonapi_module=SubDomain";
// 			}else{
// 			    $que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=listaddondomains&cpanel_jsonapi_module=AddonDomain";
// 			}
			
// 			$resp =  $this->request( $que ); //get the all domains list which all are added in cPanel
// // 			echo"<pre>";print_r($resp);die;
// 			if(isset($resp['cpanelresult']) && isset($resp['cpanelresult']['data']) && is_array($resp['cpanelresult']['data'])){ //check key in response
// 			    $isExist = false; //default exist is false
// 			    foreach($resp['cpanelresult']['data'] as $domainData){
//                     // echo $domainData['domain'].' == '.$domainName.' ';
// 			        if($domainData['domain'] === $domainName){ //compare domain with given domain 
// 			            $isExist = true;
// 			            break;
// 			        }
// 			    }
			    
//                 $resp = [
//                     "status" => true,
//                     "isExist" => $isExist
//                 ];

//                 if($isInternalCall){
//                     return $resp;
//                 }else{
//                     echo json_encode($resp);
//                 }
			    
// 			}else{
// 			    throw new Exception("Unable to process this request, please try again later.");
// 			}

// 		}catch(Exception $e) {
//             return $this->showResp(false, $e->getMessage());
//         }

//     }
    

//     /*
//     This function is help to add/parked domain in cPanel
//     The function parameter details 
//         $params = [
//             "domainName" : "example.com", //required - provide domain name which you want to add or park in cPanel
//             "subDomainName" : "example", //required - provide sub-domain name which will relate to above parked domain (without main domin )
//             "dir" : "", // required - provide directory-name which you want to add as a root directory
//             "checkIP" => true/false // optional - to check domain is pointing on our IP or not it will false default
//         ]
//     */

//     function addAddonDomain($params = []){
//         try{
//             // echo"<pre>";print_r($params);die;
// 			extract($params);
//             if(!isset($domainName)){
//                 return $this->showResp(false, "'domainName' is required.");
//             }
            
//             if(!isset($subDomainName)){
//                 return $this->showResp(false, "'subDomainName' is required.");
//             }
            
//             if(!isset($dir)){
//                 return $this->showResp(false, "'dir' is required.");
//             }
            

//             if(isset($checkIP) && $checkIP){
//                 $checkIPPoint = $this->validateDomainWithIP($domainName);
//                 if(!$checkIPPoint){
//                     return $this->showResp(false, "'".$domainName."' is not pointing on our IP which is '".$this->mainDomainIP."', please add this IP in your cName.");
//                 }
//             }
            


//             $checkExist = $this->checkDomainExistInternally([
//                 "domainName" => $domainName,
// 				"type" => 'domain',
//             ], 'add');

//             if(!$checkExist['status']){
//                 return $this->showResp($checkExist['status'], $checkExist['message']);
//             }
            
			
// 			$que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=addaddondomain&cpanel_jsonapi_module=AddonDomain&newdomain=".$domainName."&subdomain=$subDomainName&dir=$dir";

//             return $this->showResp(true, $this->request($que));

// 		}catch(Exception $e) {
//             return $this->showResp(false, $e->getMessage());
//         }

//     }

//     /*
//     This function is help to remove added/parked domain from cPanel
//     The function parameter details 
//         $params = [
//             "domainName" : "example.com", //required - provide domain name which you want to add or park in cPanel
//             "subDomainName" : "example", //required - provide sub-domain name which will relate to above parked domain (without main domin )
//             "dir" : "", // required - provide directory-name which you want to add as a root directory
//         ]
//     */

    
//     function removeAddonDomain($params = []){
// 		try{
// 			extract($params);

//             if(!isset($domainName)){
//                 return $this->showResp(false, "'domainName' is required.");
//             }

            
//             if(!isset($subDomainName)){
//                 return $this->showResp(false, "'subDomainName' is required.");
//             }

//             $checkExist = $this->checkDomainExistInternally([
//                 "domainName" => $domainName,
// 				"type" => 'domain',
//             ], 'remove');

//             if(!$checkExist['status']){
//                 return $this->showResp($checkExist['status'], $checkExist['message']);
//             }
			
            
//             return $this->showResp(true, 
//             $this->request(
// 				$this->cPanelQueryUrl . "cpanel_jsonapi_func=deladdondomain&cpanel_jsonapi_module=AddonDomain&domain=$domainName&subdomain=$subDomainName.".$this->siteDomain
// 			));
// 		}catch(Exception $e) {
// 			return $this->showResp(false, $e->getMessage());
// 		}

        

//     }

//     /*
//     This function is help to add sub-domain in cPanel
//     The function parameter details 
//         $params = [
//             "subDomainName" : "example", //required - provide sub-domain name which you want to add
//             "dir" : "", // required - provide directory-name which you want to add as a root directory
//         ]
//     */
    
//     function addSubDomain($params = []){
        
//         try {
// 			extract($params);

//             if(!isset($subDomainName)){
//                 return $this->showResp(false, "'subDomainName' is required.");
//             }

//             if(!isset($dir)){
//                 return $this->showResp(false, "'dir' is required.");
//             }

//             $checkExist = $this->checkDomainExistInternally([
//                 "domainName" => $subDomainName . "." . $this->siteDomain,
// 				"type" => 'subDomain',
//             ], 'add'); 

//             if(!$checkExist['status']){
//                 return $this->showResp($checkExist['status'], $checkExist['message']);
//             }
            
			
//             $dir = isset($dir) ? $dir : $subDomainName . "." . $this->siteDomain;

            
//             return $this->showResp(true, 
//                 $this->request(
//                     $this->cPanelQueryUrl . "cpanel_jsonapi_func=addsubdomain&cpanel_jsonapi_module=SubDomain&domain=$subDomainName&rootdomain=" . $this->siteDomain . "&dir=$dir" 
//                 )
//             );
//         }catch(Exception $e) {
//             return $this->showResp(false, $e->getMessage());
//         }
//     }


//     /*
//     This function is help to remove added sub-domain from cPanel
//     The function parameter details 
//         $params = [
//             "subDomainName" : "example", //required - provide sub-domain name which you want to remove
//         ]
//     */
//     function removeSubDomain($params = []){
// 		try{
// 			extract($params);

// 			/*
// 			$params = [
// 				"subDomainName" : "example.SITE_DOMAIN", //required
// 				"dir" : "", // optional
// 			]
// 			*/

//             if(!isset($subDomainName)){
//                 return $this->showResp(false, "'subDomainName' is required.");
//             }

//             $checkExist = $this->checkDomainExistInternally([
//                 "domainName" => $subDomainName . "." . $this->siteDomain,
// 				"type" => 'subDomain',
//             ], 'remove');

//             if(!$checkExist['status']){
//                 return $this->showResp($checkExist['status'], $checkExist['message']);
//             }
            
			
// 			$dir = isset($dir) ? $dir : $subDomainName . "." . $this->siteDomain;
	
// 			// $removeSubDomainQuery = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$subDomainName.'.'.$this->siteDomain;
// 			$removeSubDomainQuery = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$subDomainName . "." . $this->siteDomain;
	
// 			// $removeDirQuery = in_array($dir , ['public_html' , '/public_html' , 'public_html/' , '/public_html/']) ?
// 			// 	''
// 			// : 
// 			// 	$this->cPanelQueryUrl . "cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=$dir";
	
// 			// return $this->request($removeSubDomainQuery , $removeDirQuery);

//             return $this->showResp(true, 
//                 $this->request($removeSubDomainQuery)
//             );
// 		}catch(Exception $e) {
// 			return $this->showResp(false, $e->getMessage());
// 		}
        
//     }

//     private function request($query, $dir = false){
// 		try{
		
// 			$curl = curl_init();                                // Create Curl Object
// 			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);       // Allow self-signed certs
// 			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);       // Allow certs that do not match the hostname
// 			curl_setopt($curl, CURLOPT_HEADER,0);               // Do not include header in output
// 			curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);       // Return contents of transfer on curl_exec
// 			$header[0] = "Authorization: Basic " . base64_encode($this->cPanelUserName.":".$this->cPanelPassword) . "\n\r";
// 			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);    // set the username and password
// 			curl_setopt($curl, CURLOPT_URL, $query);            // execute the query
// 			$result = curl_exec($curl);
// 			if ($result == false) {
// 				error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
// 			}
// 			curl_close($curl);
	
// 			if ($dir) {
// 				self::request($dir);
// 			}
			
// 			return $result != '' ? json_decode($result , true) : [];
// 		}catch(Exception $e) {
// 			return $this->showResp(false, $e->getMessage());
// 		}

//     }
    
//     private function createSession() {
//         try {
//             $url = $this->cpanelURL . "login";
//             $cookies = "cookies.txt";

//             // Create new curl handle
//             $ch=curl_init();
//             curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//             curl_setopt($ch, CURLOPT_HEADER, 0);
//             curl_setopt($ch, CURLOPT_URL, $url);
//             curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//             curl_setopt($ch, CURLOPT_COOKIEFILE, $cookies); // Save cookies to
//             // curl_setopt($ch, CURLOPT_POSTFIELDS, "user=" . $this->cPanelUserName . "&pass=". $this->cPanelPassword);
//             curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["user" => $this->cPanelUserName , "pass" => $this->cPanelPassword]));
//             curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
//             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//             // Execute the curl handle and fetch info then close streams.
//             $f = curl_exec($ch);
//             $h = curl_getinfo($ch);
//             curl_close($ch);

//             // If we had no issues then try to fetch the cpsess
//             if ($f == true and strpos($h['url'],"cpsess")){
//                 // Get the cpsess part of the url
//                 $pattern="/.*?(\/cpsess.*?)\/.*?/is";
//                 $preg_res=preg_match($pattern,$h['url'],$cpsess);
//             }

//             // If we have a session then return it otherwise return empty string
//             $token =  (isset($cpsess[1])) ? $cpsess[1] : "";

//             if($token == ''){
//                 throw new Exception("Unable to create session, please check cPanel login details.");
//             }else{
//                 return $token;
//             }
            
//         }catch(Exception $e) {
//             return $this->showResp(false, $e->getMessage());
//         }
//     }

//     private function checkDomainExistInternally($params , $actionType){
//         $check = $this->checkDomainExist($params , true);
//         $resp = [
//             'status' => true
//         ];
//         if(isset($check['status']) && $check['status'] == true){
//             if($actionType == 'add' && $check['isExist'] == true){
//                 $resp = [
//                     'status' => false,
//                     'message' => "'".$params['domainName']."' is already exist."
//                 ];
//             }

//             if($actionType == 'remove' && !$check['isExist']){
//                 $resp = [
//                     'status' => false,
//                     'message' => "'".$params['domainName']."' is not exist."
//                 ];
//             }
//         }

//         return $resp;
//     }

//     private function showResp($status , $data){
//         $resp = [];
//         if(!$status){
//             $resp = [
//                 'status' => false,
//                 'message' => $data
//             ];
//         }else{
//             if(isset($data['cpanelresult']) && isset($data['cpanelresult']['error'])){
//                 $resp = [
//                     'status' => false,
//                     'message' => $data['cpanelresult']['error']
//                 ];
//             }else if(isset($data['cpanelresult']['data'][0]['reason'])){
//                 $resp = [
//                     'status' => true,
//                     'message' => $data['cpanelresult']['data'][0]['reason']
//                 ];
//             }else if(isset($data['cpanelresult']['data']['reason'])){
//                 $resp = [
//                     'status' => true,
//                     'message' => $data['cpanelresult']['data']['reason']
//                 ];
//             }
//         }

//         return $resp;
//         exit;
//     }


//     private function validateDomainWithIP($domainName){
//         $ip = gethostbyname($domainName);
//         $validate = $ip == $this->mainDomainIP ? true : false;
//     }
    
    

// }


class Manage_cpanel_domain {
    
    public $cPanelUserName, 
    $cPanelPassword, 
    $siteDomain, 
    $cpanelURL,
    $cPanelQueryUrl,
    $cPanelToken,
    $mainDomainIP;

    

    function __construct($params) {
        extract($params);
        $this->siteDomain = $domain;
        $this->cPanelUserName = $cPanelUserName;
        $this->cPanelPassword = $cPanelPassword;
        $this->mainDomainIP = $mainDomainIP;
        $this->cpanelURL = 'https://'.$this->siteDomain.':2083/';
        $this->cPanelToken = self::createSession();
       
        $this->cPanelQueryUrl = $this->cpanelURL . $this->cPanelToken ."/json-api/cpanel?cpanel_jsonapi_version=2&";
    }
       

    /*
    This function is help to check the Domain/Sub-domain existance in cPanel
    The function parameter details 
        $params = [
            "domainName" : "example.com", //required - Provide the domain which you want to check in cPanel
			"type" : domain / subDomain, //required - Provide the domain type Domain/sub-domain
        ]
        $isInternalCall //optional , this params is only for this class also, not require for external call
    */
    function checkDomainExist($params , $isInternalCall = false){
        try{
			extract($params);
			if($type == 'subDomain'){
			    $que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=listsubdomains&cpanel_jsonapi_module=SubDomain";
			}else{
			    $que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=listaddondomains&cpanel_jsonapi_module=AddonDomain";
			}
			
			$resp =  $this->request( $que ); //get the all domains list which all are added in cPanel
            // 			echo"<pre>";print_r($resp);die;
			if(isset($resp['cpanelresult']) && isset($resp['cpanelresult']['data']) && is_array($resp['cpanelresult']['data'])){ //check key in response
			    $isExist = false; //default exist is false
			    foreach($resp['cpanelresult']['data'] as $domainData){
                    // echo $domainData['domain'].' == '.$domainName.' ';
			        if($domainData['domain'] === $domainName){ //compare domain with given domain 
			            $isExist = true;
			            break;
			        }
			    }
			    
                $resp = [
                    "status" => true,
                    "isExist" => $isExist
                ];

                if($isInternalCall){
                    return $resp;
                }else{
                    echo json_encode($resp);
                }
			    
			}else{
			    throw new Exception("Unable to process this request, please try again later.");
			}

		}catch(Exception $e) {
            return $this->showResp(false, $e->getMessage());
        }

    }
    

    /*
    This function is help to add/parked domain in cPanel
    The function parameter details 
        $params = [
            "domainName" : "example.com", //required - provide domain name which you want to add or park in cPanel
            "subDomainName" : "example", //required - provide sub-domain name which will relate to above parked domain (without main domin )
            "dir" : "", // required - provide directory-name which you want to add as a root directory
            "checkIP" => true/false // optional - to check domain is pointing on our IP or not it will false default
        ]
    */

    function addAddonDomain($params = []){
        try{
            // echo"<pre>";print_r($params);die;
			extract($params);          
            if(!isset($domainName)){
                return $this->showResp(false, "'domainName' is required.");
            }
            
            if(!isset($subDomainName)){
                return $this->showResp(false, "'subDomainName' is required.");
            }
            
            if(!isset($dir)){
                return $this->showResp(false, "'dir' is required.");
            }
            

            if(isset($checkIP) && $checkIP){
                $checkIPPoint = $this->validateDomainWithIP($domainName);
              
                if(!$checkIPPoint){
                    return $this->showResp(false, "'".$domainName."' is not pointing on our IP which is '".$this->mainDomainIP."', please add this IP in your cName.");
                }
            }
            


            $checkExist = $this->checkDomainExistInternally([
                "domainName" => $domainName,
				"type" => 'domain',
            ], 'add');

            if(!$checkExist['status']){
                return $this->showResp($checkExist['status'], $checkExist['message']);
            }
            
			
			$que = $this->cPanelQueryUrl . "cpanel_jsonapi_func=addaddondomain&cpanel_jsonapi_module=AddonDomain&newdomain=".$domainName."&subdomain=$subDomainName&dir=$dir";

            return $this->showResp(true, $this->request($que));

		}catch(Exception $e) {
            return $this->showResp(false, $e->getMessage());
        }

    }

    /*
    This function is help to remove added/parked domain from cPanel
    The function parameter details 
        $params = [
            "domainName" : "example.com", //required - provide domain name which you want to add or park in cPanel
            "subDomainName" : "example", //required - provide sub-domain name which will relate to above parked domain (without main domin )
            "dir" : "", // required - provide directory-name which you want to add as a root directory
        ]
    */
    function removeAddonDomain($params = []){
		try{
			extract($params);

            if(!isset($domainName)){
                return $this->showResp(false, "'domainName' is required.");
            }

            
            if(!isset($subDomainName)){
                return $this->showResp(false, "'subDomainName' is required.");
            }

            $checkExist = $this->checkDomainExistInternally([
                "domainName" => $domainName,
				"type" => 'domain',
            ], 'remove');

            if(!$checkExist['status']){
                return $this->showResp($checkExist['status'], $checkExist['message']);
            }
			
            
            return $this->showResp(true, 
            $this->request(
				$this->cPanelQueryUrl . "cpanel_jsonapi_func=deladdondomain&cpanel_jsonapi_module=AddonDomain&domain=$domainName&subdomain=$subDomainName.".$this->siteDomain
			));
		}catch(Exception $e) {
			return $this->showResp(false, $e->getMessage());
		}

        

    }
    /*
    This function is help to add sub-domain in cPanel
    The function parameter details 
        $params = [
            "subDomainName" : "example", //required - provide sub-domain name which you want to add
            "dir" : "", // required - provide directory-name which you want to add as a root directory
        ]
    */
    
    function addSubDomain($params = []){
        
        try {
			extract($params);

            if(!isset($subDomainName)){
                return $this->showResp(false, "'subDomainName' is required.");
            }

            if(!isset($dir)){
                return $this->showResp(false, "'dir' is required.");
            }

            $checkExist = $this->checkDomainExistInternally([
                "domainName" => $subDomainName . "." . $this->siteDomain,
				"type" => 'subDomain',
            ], 'add'); 

            if(!$checkExist['status']){
                return $this->showResp($checkExist['status'], $checkExist['message']);
            }
            
			
            $dir = isset($dir) ? $dir : $subDomainName . "." . $this->siteDomain;

            
            return $this->showResp(true, 
                $this->request(
                    $this->cPanelQueryUrl . "cpanel_jsonapi_func=addsubdomain&cpanel_jsonapi_module=SubDomain&domain=$subDomainName&rootdomain=" . $this->siteDomain . "&dir=$dir" 
                )
            );
        }catch(Exception $e) {
            return $this->showResp(false, $e->getMessage());
        }
    }

    /*
    This function is help to remove added sub-domain from cPanel
    The function parameter details 
        $params = [
            "subDomainName" : "example", //required - provide sub-domain name which you want to remove
        ]
    */
   function rMDomain($params){
       
    $query = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$params . ".plrfunnels.in";
    
    $dir = false;
    $curl = curl_init();                                // Create Curl Object
    curl_setopt($curl, CURLOPT_URL, $query);            // execute the query
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);       // Allow self-signed certs
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);       // Allow certs that do not match the hostname
    curl_setopt($curl, CURLOPT_HEADER,0);               // Do not include header in output
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);       // Return contents of transfer on curl_exec
    $header[0] = "Authorization: Basic " . base64_encode(C_USERNAME.":".C_PASSWORD) . "\n\r";
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);    // set the username and password
    $result = curl_exec($curl);
    
     $ree = json_decode($result , true);
    echo "<pre>";print_r($ree);die;
    
    if ($result == false) {
        error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
    }
    curl_close($curl);
   
    
    
    if ($dir) {
        self::request($dir);
    }
    
     $result != '' ? json_decode($result , true) : [];
     
  }
    function removeSubDomain($params = []){    
		try{
			extract($params);
  
			/*
			$params = [
				"subDomainName" : "example.SITE_DOMAIN", //required
				"dir" : "", // optional
			]
			*/

            if(!isset($params)){
                return $this->showResp(false, "'subDomainName' is required.");
            }

            $checkExist = $this->checkDomainExistInternally([
                "domainName" => $subDomainName,
				"type" => 'subDomain',
            ], 'remove');

            if(!$checkExist['status']){
                $res =  $this->showResp($checkExist['status'], $checkExist['message']);
            }
          	
			$dir = isset($dir) ? $dir : $subDomainName;
	
			// $removeSubDomainQuery = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$subDomainName.'.'.$this->siteDomain;
			$removeSubDomainQuery = $this->cPanelQueryUrl . "cpanel_jsonapi_func=delsubdomain&cpanel_jsonapi_module=SubDomain&domain=".$subDomainName;
	      
			// $removeDirQuery = in_array($dir , ['public_html' , '/public_html' , 'public_html/' , '/public_html/']) ?
			// 	''
			// : 
			// 	$this->cPanelQueryUrl . "cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=$dir";
	
			// return $this->request($removeSubDomainQuery , $removeDirQuery);

            return $this->showResp(true, 
                $this->request($removeSubDomainQuery)
            );
		}catch(Exception $e) {
			return $this->showResp(false, $e->getMessage());
		}
        
    }

    private function request($query, $dir = false){
		try{
		
			$curl = curl_init();                                // Create Curl Object
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);       // Allow self-signed certs
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);       // Allow certs that do not match the hostname
			curl_setopt($curl, CURLOPT_HEADER,0);               // Do not include header in output
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);       // Return contents of transfer on curl_exec
			$header[0] = "Authorization: Basic " . base64_encode($this->cPanelUserName.":".$this->cPanelPassword) . "\n\r";
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);    // set the username and password
			curl_setopt($curl, CURLOPT_URL, $query);            // execute the query
			$result = curl_exec($curl);
			if ($result == false) {
				error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
			}
			curl_close($curl);
	
			if ($dir) {
				self::request($dir);
			}
			
			return $result != '' ? json_decode($result , true) : [];
		}catch(Exception $e) {
			return $this->showResp(false, $e->getMessage());
		}

    }
    
    private function createSession() {
        try {
            $url = $this->cpanelURL . "login";
            $cookies = "cookies.txt";

            // Create new curl handle
            $ch=curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookies); // Save cookies to
            // curl_setopt($ch, CURLOPT_POSTFIELDS, "user=" . $this->cPanelUserName . "&pass=". $this->cPanelPassword);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["user" => $this->cPanelUserName , "pass" => $this->cPanelPassword]));
            curl_setopt($ch, CURLOPT_TIMEOUT, 100020);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // Execute the curl handle and fetch info then close streams.
            $f = curl_exec($ch);
            $h = curl_getinfo($ch);
            curl_close($ch);

            // If we had no issues then try to fetch the cpsess
            if ($f == true and strpos($h['url'],"cpsess")){
                // Get the cpsess part of the url
                $pattern="/.*?(\/cpsess.*?)\/.*?/is";
                $preg_res=preg_match($pattern,$h['url'],$cpsess);
            }

            // If we have a session then return it otherwise return empty string
            $token =  (isset($cpsess[1])) ? $cpsess[1] : "";

            if($token == ''){
                throw new Exception("Unable to create session, please check cPanel login details.");
            }else{
                return $token;
            }
            
        }catch(Exception $e) {
            return $this->showResp(false, $e->getMessage());
        }
    }

    private function checkDomainExistInternally($params , $actionType){
        $check = $this->checkDomainExist($params , true);
        $resp = [
            'status' => true
        ];
        if(isset($check['status']) && $check['status'] == true){
            if($actionType == 'add' && $check['isExist'] == true){
                $resp = [
                    'status' => false,
                    'message' => "'".$params['domainName']."' is already exist."
                ];
            }

            if($actionType == 'remove' && !$check['isExist']){
                $resp = [
                    'status' => false,
                    'message' => "'".$params['domainName']."' is not exist."
                ];
            }
        }

        return $resp;
    }

    private function showResp($status , $data){
        $resp = [];
        if(!$status){
            $resp = [
                'status' => false,
                'message' => $data
            ];
        }else{
            if(isset($data['cpanelresult']) && isset($data['cpanelresult']['error'])){
                $resp = [
                    'status' => false,
                    'message' => $data['cpanelresult']['error']
                ];
            }else if(isset($data['cpanelresult']['data'][0]['reason'])){
                $resp = [
                    'status' => true,
                    'message' => $data['cpanelresult']['data'][0]['reason']
                ];
            }else if(isset($data['cpanelresult']['data']['reason'])){
                $resp = [
                    'status' => true,
                    'message' => $data['cpanelresult']['data']['reason']
                ];
            }
        }

        return $resp;
        exit;
    }


    private function validateDomainWithIP($domainName){
        // $ip = gethostbyname($domainName);
        // $validate = $ip == $this->mainDomainIP ? true : false;
        $ip = gethostbyname($domainName);
        $mip = $this->mainDomainIP ;
        if($mip==$ip){
          $validate = true;
        }else{
          $validate = false;
        }
        return $validate;
    }
    function testingIP($domainName){
      $ip = gethostbyname($domainName);
      $mip = $this->mainDomainIP ;
      if($mip==$ip){
          $validate = true;
      }else{
          $validate = false;
      }
    }
}
				