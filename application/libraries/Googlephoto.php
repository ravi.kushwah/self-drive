<?php
if(!class_exists("Google_Client")) {
	require_once APPPATH . 'libraries/google-client/vendor/autoload.php';
}
/**
*   Google Drive Class
*/
class Googlephoto
{
	function __construct(){
	    $this->secretPath =  APPPATH . 'libraries/google-client/secrets/UserRefreshCredentials.json';
// 		$this->scopes     = array('https://www.googleapis.com/auth/photoslibrary.readonly','https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata','https://www.googleapis.com/auth/photoslibrary.sharing');
		$this->scopes     = array('https://www.googleapis.com/auth/photoslibrary.appendonly','https://www.googleapis.com/auth/photoslibrary','https://www.googleapis.com/auth/photoslibrary.sharing','https://www.googleapis.com/auth/photoslibrary.readonly','https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata');
		$this->redirecturi = base_url().'home/get_info_google_photo';
		$this->YOUR_API_KEY = 'AIzaSyD8JloYOEtVNFeijlSpEYFsrxrjVTStbLM';
	}
	
    function googlePhotoAuth(){
        
        $client = new Google_Client();
        $client->setAuthConfigFile($this->secretPath);
        $client->setRedirectUri($this->redirecturi);
        $client->addScope($this->scopes);
        $client->setAccessType("offline");
        $client->setApprovalPrompt('force');
        
        // $client = new Google_Client();
        // $client->setAuthConfig($this->secretPath);
        // $client->addScope($this->scopes);
        // $client->setRedirectUri($this->redirecturi);
        // $client->setAccessType('offline');        // offline access
        // $client->setIncludeGrantedScopes(true);   // incremental auth
        
        $auth_url = $client->createAuthUrl();
        
        return filter_var($auth_url, FILTER_SANITIZE_URL);
    }
	public function getClientForAuth() {
		$client = new Google_Client();
		$client->setAuthConfigFile($this->secretPath);
		$client->setRedirectUri($this->redirecturi);
		$client->addScope($this->scopes);
		$client->setAccessType("offline");
		$client->setApprovalPrompt('force');
		
		return $client;
	}
   
    function CreateAlbum($albumName,$token){
        $client = $this->clientObject( $token );
        
        $ch = curl_init();
    
        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/albums?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"album\":{\"title\":\"$albumName\"}}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        return $result;
        
    }
    function ListAlbum($token){
        
         $client = $this->clientObject( $token );
        
         $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/albums?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        return $result;
        
    }
    function getAlbumItem($token,$albumId){
      
        $client = $this->clientObject( $token );
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/mediaItems:search?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"albumId\":\"$albumId\"}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
       return json_decode($result,true);
    }
    function GetAlbum($token,$albumId){
        
        $client = $this->clientObject( $token );
       
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/albums/'.$albumId.'?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        return $result;
    
    }
    function Getbatch($token,$albumId){
       
        $client = $this->clientObject( $token );
       
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/mediaItems:batchGet?mediaItemIds='.$albumId.'&key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
       return $result;
    }
    function listShare($token){
       
       $client = $this->clientObject( $token );
      
       $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/sharedAlbums?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
       return $result;
    }
    function shareURLAlbum($token,$albumId){
        
        $client = $this->clientObject( $token );
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/albums/'.$albumId.'?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"sharedAlbumOptions\":{\"isCollaborative\":false,\"isCommentable\":false}}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        
        return $result;
    }
    function unShare($token,$albumId){
        
         $client = $this->clientObject( $token );
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/albums/'.$albumId.'?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        
        return $result;
    }
    function MediaItem($token){
       
        $client = $this->clientObject( $token );
       
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/mediaItems?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        
        return $result;
    }
    function BatchAddmedia($token,$mediaId,$albumId){
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/albums/'.$albumId.':batchAddMediaItems?key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"mediaItemIds\":[\"$mediaId\"]}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$token;
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
       return $result;
    }
    
    function GetUploadToken($token,$file){
       
        $client = $this->clientObject( $token );
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://photoslibrary.googleapis.com/v1/uploads',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => file_get_contents($file),
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$client['access_token'],
          ),
        ));
        
        $response = curl_exec($curl);
       
       return $response;
    }

    function getShareUrl($token,$fileid){
        $client = $this->clientObject( $token );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/mediaItems/'.$fileid);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = array();
        $headers[] = 'Authorization: Bearer '.$client['access_token'];
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $data  = json_decode($result,true);
        return $data;
    }

    function uploadMedia($token,$albumId,$file,$fileName,$mimeType){
       
        $UploadToken = $this->GetUploadToken($token,$file);
        
        $client = $this->clientObject( $token );
        
        if($albumId==1 || $albumId==2 || $albumId==3){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate?key='.$this->YOUR_API_KEY);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"albumId\":\"\",\"newMediaItems\":[{\"description\":\"uploadMedia\",\"simpleMediaItem\":{\"uploadToken\":\"$UploadToken\",\"fileName\":\"$fileName\"}}]}");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
            
            $headers = array();
            $headers[] = 'Authorization: Bearer '.$client['access_token'];
            $headers[] = 'Accept: application/json';
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            
            $result = curl_exec($ch);
            $data  = json_decode($result,true);
            // print_r($data);die;
            if(isset($data['newMediaItemResults'][0]['status']['message'])=="Success"){
                $res = array('status'=>1,'smg'=>'Successfully Upload Media Item ','data'=>$data);
            }else{
                    $res = array('status'=>0,'smg'=>$data['error'],'data'=>$data);
            }
        }else{
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate?key='.$this->YOUR_API_KEY);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"albumId\":\"\",\"newMediaItems\":[{\"description\":\"uploadMedia\",\"simpleMediaItem\":{\"uploadToken\":\"$UploadToken\",\"fileName\":\"$fileName\"}}]}");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
            
            $headers = array();
            $headers[] = 'Authorization: Bearer '.$client['access_token'];
            $headers[] = 'Accept: application/json';
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            
            $result = curl_exec($ch);
            $data  = json_decode($result,true);
            // print_r($data);die;
            if(isset($data['newMediaItemResults'][0]['status']['message'])=="Success"){
                $mediaId = $data['newMediaItemResults'][0]['mediaItem']['id'];
                $UploadToken = $this->BatchAddmedia($client['access_token'],$mediaId,$albumId);
                if(empty($UploadToken)){
                    $res = array('status'=>1,'smg'=>'Successfully Upload Media Item ','data'=>$UploadToken);
                }else{
                     $res = array('status'=>0,'smg'=>"Won't be able to upload any new media file.",'data'=>$UploadToken);
                }
            }else{
                    $res = array('status'=>0,'smg'=>$data['error'],'data'=>$UploadToken);
            }
        }
        
        return json_encode($res,true);

    }
    
	public function clientObject( $accessToken ){
		$client = new Google_Client();
		$client->setAuthConfigFile($this->secretPath);
		$client->setRedirectUri($this->redirecturi);
		$client->addScope($this->scopes);
		$client->setAccessType("offline");
		$accessToken = $this->checkExpireToken( $client, $accessToken );
	
		return $accessToken;
	}

	public function checkExpireToken( $client, $accessToken ){
	    
		// Refresh the token if it's expired.
		if ($client->isAccessTokenExpired()) {
			$client->fetchAccessTokenWithRefreshToken($accessToken['refresh_token']);
			$accessToken = $client->getAccessToken();

			// Insert New token

		}

		return $accessToken;
	}
    function test($token){
       
        $client = $this->clientObject( $token );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/drive/v3/files?q=parents%20in%20%27root%27%20and%20mimeType%20%3D%20%27application%2Fvnd.google-apps.folder%27%20and%20name%20%3D%27YouTube%27&key='.$this->YOUR_API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer  '.$client['access_token'];
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
    }
}
