<?php
if(!class_exists("Onedrive")) {
	require_once APPPATH . 'libraries/one-drive/vendor/autoload.php';
}
use myPHPnotes\Microsoft\Auth;
use myPHPnotes\Microsoft\Handlers\Session;
use myPHPnotes\Microsoft\Models\User;
use Microsoft\src\Graph\Graph;
use Microsoft\src\Graph\Model;
class onedrive{
    function getAuthUrl(){
        require_once APPPATH . 'libraries/one-drive/vendor/autoload.php';
        $tenant = "common";
        $client_id = "f54c818f-6d0b-4b51-bc0d-6dd0208edbf2";
        $client_secret = "bWX8Q~OuHOmod-_7QWsCPJ~c5bWyxZwlHWwyec3H";//PH18Q~8RIF3pfomcL41Y.b~wj~0cZ2L~a3aEmdv_
        $callback = "https://selfdrive.work/home/OneDriveAuth";
        
        $scopes = [
            'User.Read',
            'files.readwrite',
            'offline_access'
        ];
        
        $microsoft = new Auth($tenant, $client_id,  $client_secret, $callback, $scopes);
        return $microsoft->getAuthUrl();
    }
    
    function authResponse($code){
        require_once APPPATH . 'libraries/one-drive/vendor/autoload.php';
        $auth = new Auth(Session::get("tenant_id"),Session::get("client_id"),  Session::get("client_secret"), Session::get("redirect_uri"), Session::get("scopes"));
        $data = $auth->getToken($code, Session::get("state"));
   
        $auth->setAccessToken($data->access_token);
        $user = new User;
        $email = $user->data->getUserPrincipalName();
        $name = $user->data->getGivenName();
        $userInfo = array(
                'name'=>$name,
                'email'=>$email,
                'totalStorage'=>'',
                'useStorage'=>''
            );
        $data1 = array(
            'user_id' => $_SESSION['id'],
            'access_token' => json_encode( $data ),
            'user_info' => json_encode( $userInfo ),
            'connection_type' => 'OneDrive',
            'email' => $email,
            'status' => 1
        );
        return array(
                'data'=>$data1,
                'userInfo'=>$userInfo,
                'token'=>$data
            );
      
    }
    public function getAccesTokenFromRefreshToken($refresh_token){
        $client_id = "f54c818f-6d0b-4b51-bc0d-6dd0208edbf2";
        $client_secret = "bWX8Q~OuHOmod-_7QWsCPJ~c5bWyxZwlHWwyec3H";
        $callback = "https://selfdrive.work/home/OneDriveAuth";
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://login.live.com/oauth20_token.srf',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS => 'client_id='.$client_id.'&redirect_uri='.$callback.'&client_secret='.$client_secret.'&refresh_token='.$refresh_token.'&grant_type=refresh_token',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded',
            'Cookie: MSPRequ=id=N&lt=1669035016&co=1; OParams=11O.DRxM6fCKx1geHjnjPSBSh7p5lKulrMlvdUdC3r0vDl3fIeuDGP8Tm4hpA3ssRjksnmqEXTtWteOBGFyt6dC!h76iImMSD!nuat9RbnRU*Ury22nf8H7fyJ3Zcs0*jgJi*M06b9l9FmV0kVxij3o14vyarOu61LyXa62moCI2Mp!P*0dfDduAE24L*t*81WTimXpi1PK!WLW47jWLRtmk6XO69FUCI0WTpm9pirvUKRBlBbqC!hDetJ7AQneW02gkyx*pb6ZCPbNjkeu1RbbTNNAGDWOPAGIxep6F6*90iT7wRl6ihrm3Ai35*A7e8VwHIw$$; ci_session=006dea1aaba8dd7962eff7f0c3074703becf0f13; uaid=7b8b7b7bc33a42d88a843de32d946ab8'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        // echo"<pre>";print_r($response);
        return $response;
    }
    function getFiles($accessToken,$folderId=null,$driveId=null){
      
        if(!empty($folderId) && !empty($driveId)){
            $path = '/drives/'.$driveId.'/items/'.$folderId.'/children';
        }else{
             $path ='me/drive/root/children';
        }
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/'.$path,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'SdkVersion: postman-graph/v1.0',
            'Authorization: Bearer '.$accessToken.''
          ),
        ));
        $response = curl_exec($curl);
         
        curl_close($curl);
        $getAllFolder =  json_decode($response,true);
            
        if(isset($getAllFolder['error'])){
            $return = array(
                'status' => 1,
                'data' => ''
            );
        }else{
            $data = [];
            foreach($getAllFolder['value'] as $value){
                array_push($data,$value);
            }
            $return = array(
                'status' => 1,
                'data' => $data
            );
        }
        return $return;

    }
    function CreateFolder($accessToken,$folderName,$driveId=null,$folderId=null){
       
        if(!empty($folderId) && !empty($driveId)){
            $path = 'me/drives/'.$driveId.'/items/'.$folderId.'/children';
        }else{
             $path ='me/drive/root/children';
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/'.$path,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS=>'{
              "name": "'.$folderName.'",
              "folder": { },
              "@microsoft.graph.conflictBehavior": "rename"
            }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'SdkVersion: postman-graph/v1.0',
            'Authorization: Bearer '.$accessToken.''
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
    function subFolderCreate($token,$FolderName,$driveId,$parentId){
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/drives/'.$driveId.'/items/'.$parentId.'/children',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => '{
            "name": "'.$FolderName.'",
            "folder": { },
            "@microsoft.graph.conflictBehavior": "rename"
          }',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token.'',
            'Content-Type: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;
    }
    function ShareURL($token,$parentId){
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/me/drive/items/'.$parentId.'',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS =>'{
              "type": "view",
              "scope": "anonymous"
            }',
           CURLOPT_HTTPHEADER => array(
            'Authorization: '.$token.'',
            'Content-Type: text/plain'
          ),
        ));
        $response = curl_exec($curl);
         
        curl_close($curl);
        return json_decode($response,true);
    }
    function DeleteFiles($token,$driveId,$parentId){
     
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/drives/'.$driveId.'/items/'.$parentId.'',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'DELETE',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token.''
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;
    }
    function UPloadFile($token,$driveId,$parentId,$file,$name,$mime_type){

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/drives/'.$driveId.'/items/'.$parentId.':/'.$name.':/content',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PUT',
          CURLOPT_POSTFIELDS => file_get_contents($file),
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token.'',
            'Content-Type: '.$mime_type.''
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response,true);


    }
    function downloadFile($token,$parentId){
           
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://graph.microsoft.com/v1.0/me/drive/items/'.$parentId.'/content',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS =>'{
          "type": "view",
          "scope": "anonymous"
        }',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token.'',
            'Content-Type: text/plain'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;
    }
   function MoveFile($token,$driveId,$parentId,$file,$name,$mime_type){
       
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://graph.microsoft.com/v1.0/drives/'.$driveId.'/items/'.$parentId.'',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'PATCH',
      CURLOPT_POSTFIELDS => file_get_contents($file),
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer '.$token.'',
        'Content-Type: '.$mime_type.''
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    echo $response;

   }
}