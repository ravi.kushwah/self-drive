<?php

    class Pcloud_lib{
        
        public $CI = '';
        
        public function __construct()
       {    
            $this->CI =& get_instance();
            // $this->CI->load->library('ftp');
       }

       public function getToken( $appKey, $appSecret, $code ) {

		$params = [
			"client_id"     => $appKey,
			"client_secret" => $appSecret,
			"code"          => $code
		];

		$url = "https://api.pcloud.com/oauth2_token?".http_build_query($params);

		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($curl);

		if (strpos(curl_getinfo($curl, CURLINFO_CONTENT_TYPE), "application/json") !== false) {
			$response = json_decode($response);
		}

		if ($response->result == 0) {
			$response = ["access_token" => $response->access_token, 'status' => 1];
			
		} else {
            $response = ["access_token" => '', 'status' => 0];
			// throw new Exception($response->error);
		}
        return $response;
	}

    function pcloud_userdetails($token){
        require_once(APPPATH.'libraries/pcloud/autoload.php');

		try {
			$pCloudUser = new pCloud\User();
			
			$info = $pCloudUser->getUser($token);
			$info = json_decode(json_encode($info), true);
            $res  = array('status'=> 1, 'data'=> $info, 'message'=>'Success');
		} catch (Exception $e) {
			$msg  = $e->getMessage();
            $res  = array('status'=> 0, 'data'=> [], 'message'=>$msg);
		}
        return $res;
    }

    public function getFiles() {
        require_once("../lib/pcloud/autoload.php");

        try {
            $pCloudFolder = new pCloud\Folder();
            
            function appendFolder($folderid, $pCloudFolder) {
                $pCloudFile = new pCloud\File();
                echo "<ul style=\"list-style-type: none;\">";
                $content = $pCloudFolder->getContent($folderid);
                // print_r('<pre>');print_r($content);die();
                foreach ($content as $item) {
                    echo "<li>{$item->name}</li>";
                    if ($item->isfolder) {
                        appendFolder($item->folderid, $pCloudFolder);
                    }else{
                        // $pCloudFile->download($item->fileid, '../download');
                    }
                }
                echo "</ul>";
            }

            appendFolder(0, $pCloudFolder);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
	}

    public function getFolderById($folderid, $token){
        require_once(APPPATH.'libraries/pcloud/autoload.php');

		try {
			$pCloudFolder = new pCloud\Folder();
			
			$folder_data = $pCloudFolder->getContent($folderid, $token);
            $folder_data = json_decode(json_encode($folder_data), true);
            $res = array('status' => 1, 'folder_data' => $folder_data, 'message' => 'Success');

		} catch (Exception $e) {
            $res = array('status' => 0, 'folder_data' => [], 'message' => $e->getMessage());
		}
        return $res;
    }
    
    public function getFileDownloadLink($file_id, $token){
        require_once(APPPATH.'libraries/pcloud/autoload.php');

		try {
			$pCloudFile = new pCloud\File();
            
            $link = $pCloudFile->getLink($file_id, $token);
            $res = array('status' => 1, 'file_id' => $link, 'message' => 'Success');

		} catch (Exception $e) {
            $res = array('status' => 0, 'file_id' => [], 'message' => $e->getMessage());
		}
        return $res;
    }
    
    public function uploadFile($location, $name, $token, $folderid){
        require_once( APPPATH.'libraries/pcloud/autoload.php' );
        try {
            $pCloudFile = new pCloud\File();
            $ar['msg']      = $pCloudFile->upload($location, $token, $name, $folderid);
            $ar['status']   = 1;
        } catch (Exception $e) {
            $ar['status']   = 0;
            $ar['msg']      = $e;
        
        }
        return $ar;

        // $file = $location;
        // $url = 'https://api.pcloud.com/uploadfile';
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_HEADER, false);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, true);
        // $post = array(
        //     "file"=>"@".$file,
        //     "auth"=>$token,
        //     "folderid"=>$folderid,
        //     "nopartial"=>"1",
        //     "filename"=>$name
        // );
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
        // $response = curl_exec($ch);
        // curl_close($ch);
        // return $response;

    }
    
    public function createFolder($foldername, $token, $parentId){
        require_once( APPPATH.'libraries/pcloud/autoload.php' );
        try {
            $pCloudFolder = new pCloud\Folder();
            $data['folderid'] = $pCloudFolder->create( $foldername, $token, $parentId); 
            $data['status'] = 1;
        } 
        catch (Exception $e) {
            $err_msg = $e->getMessage();
            $data['status'] = 0;
            $data['message'] = $err_msg;
        }
        return $data;
    }
        
    
    }
?>