<?php

    class Gofile_lib{
        function send_response_gofile($status, $data, $msg)
        {
            return ['status' => $status, 'data' => $data, 'msg' => $msg];
        }

        function gofile_user_info( $token )
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.gofile.io/getAccountDetails?token='.$token.'&allDetails=true');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);

            $json_response = json_decode( $result, TRUE );
            // echo"<pre>";print_r($json_response);
            // die;
            return $json_response;
        }
        
        function gofile_get_folder( $token, $folder_id )
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.gofile.io/getContent?contentId='.$folder_id.'&token='.$token.'');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);

            $json_response = json_decode( $result, TRUE );
            
            return $json_response;
        }        

        function gofile_create_folder( $token, $parentId, $folderName )
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.gofile.io/createFolder');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

            curl_setopt($ch, CURLOPT_POSTFIELDS, "parentFolderId=".$parentId."&token=".$token."&folderName=".$folderName);

            $headers = array();
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            
            $json_response = json_decode( $result, TRUE );

            return $json_response;
        }    
        
        function gofile_upload_file( $token, $parentId, $file_path ){
            
            $server = $this->getfile_getServer();

            $ch = curl_init();
    
            curl_setopt($ch, CURLOPT_URL, 'https://'.$server['data']['server'].'.gofile.io/uploadFile');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 3);
            $post = array(
                'file'      => new CURLFile($file_path),
                'token'     => $token,
                'folderId'  => $parentId,
            );
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            
            $json_response = json_decode( $result, TRUE );

            return $json_response;
        }

        function getfile_delete_id($token, $content_id){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.gofile.io/deleteContent');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            curl_setopt($ch, CURLOPT_POSTFIELDS, "contentsId=".$content_id."&token=".$token);

            $headers = array();
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);

            $json_response = json_decode( $result, TRUE );

            return $json_response;
        }

        function getfile_getServer(){
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.gofile.io/getServer');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);

            $json_response = json_decode( $result, TRUE );

            return $json_response;
        }

        function gofile_getFileLink($id, $name){
            $server = $this->getfile_getServer();
            $link = 'https://'.$server['data']['server'].'.gofile.io/download/direct/'.$id.'/'.$name;
            return $link;
        }

    }
    
?>