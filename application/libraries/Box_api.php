<?php
class Box_api{
    
    public function getAuthUrl(){
        $redirect_uri = urlencode(base_url('api/box'));
        return "https://account.box.com/api/oauth2/authorize?response_type=code&client_id=".BOXCLIENTAPIKEY."&redirect_uri={$redirect_uri}";
    }
    
    public function getAccesToken($code){
        // $url = urlencode(base_url('api/box'));
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=".BOXCLIENTAPIKEY."&client_secret=".BOXCLIENTSECRET."&code=".$code."&grant_type=authorization_code");
        curl_setopt($ch, CURLOPT_POST, 1);
        
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);

        return $result;
    }
    
    public function getAccesTokenFromRefreshToken( $refresh_token ){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=".BOXCLIENTAPIKEY."&client_secret=".BOXCLIENTSECRET."&refresh_token=".$refresh_token."&grant_type=refresh_token");
        curl_setopt($ch, CURLOPT_POST, 1);
        
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        $resp = json_decode($result, true);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function getUserDetails($accountID, $accessToken ){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/users/'.$accountID);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        
        $headers = array();
        $headers[] = "Authorization: Bearer {$accessToken}";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function getFileDetails($accessToken, $fileId){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function getAllFolders($accessToken, $folderId){
        // echo $accessToken;die;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/folders/'.$folderId.'/items');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
	    $json = json_decode( $result, true );
	   // if($folderId != 0){
	   //     echo"<pre>";print_r($json);die;
	   // }
        if(isset($json['entries'])){
            $return = array(
                'status' => 1,
                'data' => $json['entries']
            );
        }else{
            $return = array(
                'status' => 0,
                'data' => array(),
                'error'=> $result
            );
        }
        return $return;
    }
    
    public function createFolder($accessToken, $folderName, $folderId ){
        $ch = curl_init();
        $args = array(
            "name" => ($folderName),
            "parent" => ['id' => $folderId]
        );
        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/folders');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
        curl_setopt($ch, CURLOPT_POST, 1);
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        // echo"<pre>";print_r($json);die;
        return $result;        
    }
    
    public function fileRename($accessToken, $fileName, $fileId){
        
        $ch = curl_init();
        $args = array(
            "name" => $fileName
        );
        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function folderRename($accessToken, $folderName, $folderId){
        $ch = curl_init();
        $args = array(
            "name" => $folderName
        );
        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/folders/'.$folderId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function downloadFile($accessToken, $fileId, $version){
        // print_r($path);die;
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId.'/content?version='.$version);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        echo"<pre>";print_r($result);die;
        return $result;
    }
    
    public function uploadFile($accessToken, $folderId, $file, $name){
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://upload.box.com/api/2.0/files/content');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $post = array(
            'attributes' => "{\"name\":\"$name\", \"parent\":{\"id\":\"$folderId\"}}",
            'file' => new CURLFile($file)
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        $headers[] = 'Content-Type: multipart/form-data';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        // echo"<pre>";print_r($result);
        // echo"<pre>";print_r(json_decode($result, true));
        // die;
        
        return $result;
    }

    public function getFileAbsoluteUrl($accessToken, $fileId){
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId.'?fields=shared_link');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;

    }

    // if shared link is not available 
    public function getSharedLinkOfFile($accessToken, $fileId){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"shared_link\": {\"access\": \"open\"}}");

        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
    
    public function file_get_contents_curl($url) {
        $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_ENCODING, 0);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST , "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);  
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      
        $data = curl_exec($ch);
      
        $info = curl_getinfo($ch);
      
      
        if(curl_errno($ch)) {
            throw new Exception('Curl error: ' . curl_error($ch));
        }
      
        curl_close($ch);
      
        if ($data === FALSE) {
            throw new Exception("curl_exec returned FALSE. Info follows:\n" . print_r($info, TRUE));
        }
      
        return $data;
    }
      
    
    public function deleteFile($accessToken, $fileId){
        // echo $accessToken;die;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function fileMoveBoxToBox($accessToken, $folderId, $fileId, $name){
        // echo"<pre>";print_r($folderId);
        // echo"<pre>";print_r($fileId);
        // echo"<pre>";print_r($name);
        // die;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.box.com/2.0/files/'.$fileId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n       \"name\": \"$name\",\n       \"parent\": {\n         \"id\": \"$folderId\"\n       }\n     }");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$accessToken;
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    
}