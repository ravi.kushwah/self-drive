<?php
class Ddownload_api{
    
    public function getUserDetails( $accessKey ){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/account/info?key='.$accessKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function createFolder($accessKey, $folderName, $parent_id ){
        // https://api-v2.ddownload.com/api/folder/create?key=key&parent_id=parent_id&name=name
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/folder/create?key='.$accessKey.'&parent_id='.$parent_id.'&name='.urlencode($folderName));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        echo"<pre>";print_r($result);die;
        return $result;
    }
    
    public function uploadFile($accessKey, $file){
        $jsonstr = file_get_contents('https://api-v2.ddownload.com/api/upload/server?key='.$accessKey);
		$json = json_decode( $jsonstr, true );
		//print_r($json);
		if(isset($json) && $json['status'] === 200){
			$url = $json['result'];
			$sess_id = $json['sess_id'];
			$curl = curl_init();
			
// 			curl_setopt($curl_handle, CURLOPT_POST, 1);
//             $args['file'] = new CurlFile('filename.png', 'image/png', 'filename.png');
//             curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $args);

            $path=dirname(__FILE__);
            $abs_path=explode('application',$path);
            
            $certificate_location = $abs_path[0] . "assets/test.pem";
            
			curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_FOLLOWLOCATION  => true, 
				CURLOPT_SSL_VERIFYHOST => $certificate_location, 
            	CURLOPT_SSL_VERIFYPEER => $certificate_location, 
                CURLOPT_POSTFIELDS => array('sess_id' => $sess_id, 'utype' => 'prem', 'file'=> new CURLFILE($file)),
            ));
		}
		$result = curl_exec($curl);

        /*if (curl_errno($curl)) {
 			echo 'Error:' . curl_error($curl);
 		}*/

		curl_close($curl);
// 		echo"<pre>";print_r($result);die;

        return $result;
    }
    
    public function getFileInfo($accessKey, $file_code){
        
        $ch = curl_init();
        
        // https://api-v2.ddownload.com/api/file/info?key=key&file_code=file_code
        // file_code, String, file code, or list separated by comma Example: gi4o0tlro01u,gi4o0tlro012.
        
        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/file/info?key='.$accessKey.'&file_code='.$file_code);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function getDeleteFile($accessKey, $last){
        
        $ch = curl_init();
        
        // https://api-v2.ddownload.com/api/files/deleted?key=key&last=last
        // last (number of files limit Example: 20.)
        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/files/deleted?key='.$accessKey.'&last='.$last);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function getAllFolders($accessKey, $fld_id){
        $ch = curl_init();
        
        // https://api-v2.ddownload.com/api/folder/list?key=key&fld_id=fld_id
        //fld_id,Number,folder id Example: 15.

        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/folder/list?key='.$accessKey.'&fld_id='.$fld_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        $r = json_decode( $result, true );
        // echo"<pre>";print_r($r);die;
        $data = [];
        if(isset($r['result']['folders']) || isset($r['result']['files'])){
            foreach($r['result']['folders'] as $key => $folderData){
                $a = $folderData;
                $a['tag'] = "folder";
                array_push($data, $a);
            }
            
            foreach($r['result']['files'] as $key => $folderData){
                $a = $folderData;
                $a['tag'] = "file";
                array_push($data, $a);
            }
            $return = array(
                'status' => 1,
                'data' => $data
            );
        }else{
            $return = array(
                'status' => 0,
                'data' => array()
            );
        }
        return $return;
    }
    
    public function fileRename($accessKey, $file_code, $fileName){
        $ch = curl_init();
        
        // https://api-v2.ddownload.com/api/file/rename?key=key&file_code=file_code&name=name

        /*file_code, String, file code, or list separated by comma Example: gi4o0tlro01u,gi4o0tlro012.
          name, String, filter file names Example: cool_video.mp4.*/
        curl_setopt($ch, CURLOPT_URL, "https://api-v2.ddownload.com/api/file/rename?key=".$accessKey."&file_code=".$file_code."&name=".urlencode($fileName));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch); 
        return $result;
    }
    
    public function folderRename($accessKey, $fld_id, $folderName){
        $ch = curl_init();
        
        // https://api-v2.ddownload.com/api/folder/rename?key=key&fld_id=fld_id&name=name
        
        // fld_id, Number, folder id Example: 15.
        // name, String, folder name Example: New Folder.

        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/folder/rename?key='.$accessKey.'&fld_id='.$fld_id.'&name='.urlencode($folderName));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    public function setFileInFolder($accessKey, $file_code, $fld_id){
        $ch = curl_init();
        
        // https://api-v2.ddownload.com/api/file/set_folder?key=key&file_code=file_code&fld_id=fld_id
        
        // file_code String file code, or list separated by comma Example: gi4o0tlro01u,gi4o0tlro012.
        // fld_id Number folder id Example: 15.

        curl_setopt($ch, CURLOPT_URL, 'https://api-v2.ddownload.com/api/file/set_folder?key='.$accessKey.'&file_code='.$file_code.'&fld_id='.$fld_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
    
    
}