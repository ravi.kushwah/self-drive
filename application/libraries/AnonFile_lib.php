<?php
class AnonFile_lib{
    
    public function uploadFile($token, $file){
        // echo $file;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.anonfiles.com/upload?token='.$token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $post = array(
            'file' => new CURLFile($file)
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }
    
    public function getFileInfo($fileId){
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.anonfiles.com/v2/file/'.$fileId.'/info');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        return $result;
    }
    
}